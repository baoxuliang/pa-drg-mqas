export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/departmentMap/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/departmentMap/export'
  }
}
