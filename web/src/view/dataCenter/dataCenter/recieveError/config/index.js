export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/fail/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/fail/export'
  }
}
