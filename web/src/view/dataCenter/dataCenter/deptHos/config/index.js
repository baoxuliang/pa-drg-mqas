export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/normalDepartment/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/normalDepartment/export'
  }
}
