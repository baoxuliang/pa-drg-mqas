export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/standardDepartment/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/standardDepartment/export'
  }
}
