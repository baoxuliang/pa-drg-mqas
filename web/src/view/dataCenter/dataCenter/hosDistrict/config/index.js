export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/hospitalArea/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/hospitalArea/export'
  }
}
