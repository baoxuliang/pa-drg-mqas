export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/departmentGroup/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/departmentGroup/export'
  }
}
