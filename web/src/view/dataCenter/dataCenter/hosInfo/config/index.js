export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/hospital/query',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/hospital/export'
  }
}
