export default {
  api: {
    get: kindo.config.api.cDrgDAC + 'dataCenter/minBainfo/query',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail',
    exportTable: kindo.config.api.cDrgDAC + 'dataCenter/minBainfo/export'
  }
}
