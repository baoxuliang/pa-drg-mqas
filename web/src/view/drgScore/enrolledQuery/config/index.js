export default {
  api: {
    get: kindo.config.api.cDrgKPI + 'drgkpi/inbas/queryListpage',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail',
    exportTable: kindo.config.api.cDrgKPI + 'drgkpi/inbas/export'
  }
}
