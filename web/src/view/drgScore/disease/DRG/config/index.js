export default {
  api: {
    get: kindo.config.api.cDrgKPI + 'bzjxpj/drg/listpage',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail',
    getRZBA: kindo.config.api.cDrgKPI + 'bzjxpj/drg/rzbas',
    show: kindo.config.api.cDrgKPI + 'bzjxpj/drg/view',
    exportTable: kindo.config.api.cDrgKPI + 'bzjxpj/drg/exportlistpage',
    exportInTable: kindo.config.api.cDrgKPI + 'bzjxpj/drg/exportrzbas',
    exportDrgTable: kindo.config.api.cDrgKPI + 'bzjxpj/drg/exportview'
  }
}
