let reqPrefix = kindo.config.api.cDrgKPI
export default {
  api: {
    // 查询数据字典分类列表
    listpage: reqPrefix + 'bzjxpj/mdc/listpage',
    exportlistpage: reqPrefix + 'bzjxpj/mdc/exportlistpage',
    yy: reqPrefix + 'bzjxpj/mdc/yy',
    exportyy: reqPrefix + 'bzjxpj/mdc/exportyy',
    rzbas: reqPrefix + 'bzjxpj/mdc/rzbas',
    exportrzbas: reqPrefix + 'bzjxpj/mdc/exportrzbas',
    drg: reqPrefix + 'bzjxpj/mdc/drg',
    exportdrg: reqPrefix + 'bzjxpj/mdc/exportdrg',
    view: reqPrefix + 'bzjxpj/mdc/view',
    exportview: reqPrefix + 'bzjxpj/mdc/exportview',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail'
  },
  mock: {}
}
