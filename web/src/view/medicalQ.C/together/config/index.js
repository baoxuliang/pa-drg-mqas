export default {
  api: {
    get: kindo.config.api.cDrgQC + 'baqc/colligation/queryListpage',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail',
    exportTable: kindo.config.api.cDrgQC + 'baqc/colligation/export'
  }
}
