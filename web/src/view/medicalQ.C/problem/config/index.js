export default {
  api: {
    get: kindo.config.api.cDrgQC + 'baqc/errorbas/queryListpage',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail',
    exportTable: kindo.config.api.cDrgQC + 'baqc/errorbas/export',
    getErrCodes: kindo.config.api.cDrgQC + 'baqc/errorbas/queryErrsCode'
  }
}
