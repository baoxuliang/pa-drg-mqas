export default {
  api: {
    getTable: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/queryByPage',
    exportTable: kindo.config.api.cDrgKPI + '/drgjx/hospitalsurgical/hospitalskills/exportHosSkillsQuery',
    getChart: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/query',
    show: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/queryInfo',
    show2: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/queryByNumClick',
    exportTable1: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/exportQueryInfoByHos/export',
    exportTable2: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/exportByNumClick/export',
    sum: kindo.config.api.cDrgKPI + 'drgjx/hospitalsurgical/hospitalskills/queryHosDataSum'
  }
}
