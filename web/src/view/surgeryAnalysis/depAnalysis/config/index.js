export default {
  api: {
    getTable: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/queryByPage',
    exportTable: kindo.config.api.cDrgKPI + '/drgjx/departmentsurgical/departmentskills/exportDepSkillsQuery',
    getChart: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/query',
    show: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/queryInfo',
    show2: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/queryByNumClick',
    exportTable1: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/exportQueryInfoData/export',
    exportTable2: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/exportByNumClick/export',
    sum: kindo.config.api.cDrgKPI + 'drgjx/departmentsurgical/departmentskills/queryHosDataSum'
  }
}
