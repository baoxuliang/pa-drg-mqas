export default {
  api: {
    getTable: kindo.config.api.cDrgKPI + 'drgjx/surgicalquery/surgical/query',
    exportTable: kindo.config.api.cDrgKPI + 'drgjx/surgicalquery/surgical/exportData/export',
    show: kindo.config.api.cDrgKPI + 'drgjx/surgicalquery/surgical/queryByNumData',
    getById: kindo.config.api.cDrgQC + 'baqc/colligation/queryDetail',
    clickExportTable: kindo.config.api.cDrgKPI + 'drgjx/surgicalquery/surgical/exportByNumData/export',
    sum: kindo.config.api.cDrgKPI + 'drgjx/surgicalquery/surgical/queryByDataSum'
  }
}
