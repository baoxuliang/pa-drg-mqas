package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 非手术费用校验
 * 非手术治疗项目费>=临床物理治疗费
 * @author jindoulixing
 */
public class CheckRule19 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule19.class);

    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_FSSZLXMF = drgBAData.getFSSZLXMF();
			Double in_WLZLF = drgBAData.getWLZLF();
			
			if((UtilObject.isNullOrEmpty(in_FSSZLXMF)?0:in_FSSZLXMF) < (UtilObject.isNullOrEmpty(in_WLZLF)?0:in_WLZLF)){
				flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("119"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule19数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
