package com.kindo.DRGDataCheck.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;

@Component
public class DrgRule1 {
    private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule1.class);

    private static Map<String, String> drgRule1Cache = new ConcurrentHashMap<>();
    
    private DRGDataCheck dRGDataCheck;
    @Autowired
    public void setDRGDataCheck(DRGDataCheck DRGDataCheck) {
		this.dRGDataCheck = DRGDataCheck;
    	if(drgRule1Cache==null || drgRule1Cache.size()==0){//
    		refreshCache();
    	}
	}

	/*static {
    	if(drgRule1Cache==null || drgRule1Cache.size()==0){//
    		refreshCache();
    	}
    }*/

    private static void refreshCache() {
    	List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule1();
    	
		for (Rule item : list) {
			drgRule1Cache.put(item.getZDBM(), item.getZDBM());
		}
    	    
    }
    
    private static void cleanupCache() {
    	drgRule1Cache.clear();
    }
    
    public static void refreshCacheRule1() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static String getZDBM(String key){
    	if(drgRule1Cache ==null || drgRule1Cache.size() == 0){
    		refreshCache();
    	}
    	return drgRule1Cache.get(key);
    }
}
