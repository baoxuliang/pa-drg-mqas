package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 是否治疗校验
 * 填写过诊断必须填写是否治疗
 * @author jindoulixing
 */
public class CheckRule23 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule23.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	
    	try {/*
			if(flag){
				String in_JBDM1 = drgBAData.getJBDM1();
				Integer in_sfzy1 = drgBAData.getSFZY1();
				if(!UtilObject.isNullOrEmpty(in_JBDM1) && in_JBDM1.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy1) || in_sfzy1< 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM2 = drgBAData.getJBDM2();
				Integer in_sfzy2 = drgBAData.getSFZY2();
				if(!UtilObject.isNullOrEmpty(in_JBDM2) && in_JBDM2.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy2) || in_sfzy2< 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM3 = drgBAData.getJBDM3();
				Integer in_sfzy3 = drgBAData.getSFZY3();
				if(!UtilObject.isNullOrEmpty(in_JBDM3) && in_JBDM3.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy3) || in_sfzy3< 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM4 = drgBAData.getJBDM4();
				Integer in_sfzy4 = drgBAData.getSFZY4();
				if(!UtilObject.isNullOrEmpty(in_JBDM4) && in_JBDM4.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy4) || in_sfzy4< 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM5 = drgBAData.getJBDM5();
				Integer in_sfzy5 = drgBAData.getSFZY5();
				if(!UtilObject.isNullOrEmpty(in_JBDM5) && in_JBDM5.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy5) || in_sfzy5< 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM6 = drgBAData.getJBDM6();
				Integer in_sfzy6 = drgBAData.getSFZY6();
				if(!UtilObject.isNullOrEmpty(in_JBDM6) && in_JBDM6.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy6) || in_sfzy6 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM7 = drgBAData.getJBDM7();
				Integer in_sfzy7 = drgBAData.getSFZY7();
				if(!UtilObject.isNullOrEmpty(in_JBDM7) && in_JBDM7.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy7) || in_sfzy7 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM8 = drgBAData.getJBDM8();
				Integer in_sfzy8 = drgBAData.getSFZY8();
				if(!UtilObject.isNullOrEmpty(in_JBDM8) && in_JBDM8.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy8) || in_sfzy8 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM9 = drgBAData.getJBDM9();
				Integer in_sfzy9 = drgBAData.getSFZY9();
				if(!UtilObject.isNullOrEmpty(in_JBDM9) && in_JBDM9.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy9) || in_sfzy9 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM10 = drgBAData.getJBDM10();
				Integer in_sfzy10 = drgBAData.getSFZY10();
				if(!UtilObject.isNullOrEmpty(in_JBDM10) && in_JBDM10.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy10) || in_sfzy10 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM11 = drgBAData.getJBDM11();
				Integer in_sfzy11 = drgBAData.getSFZY11();
				if(!UtilObject.isNullOrEmpty(in_JBDM11) && in_JBDM11.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy11) || in_sfzy11 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM12 = drgBAData.getJBDM12();
				Integer in_sfzy12 = drgBAData.getSFZY12();
				if(!UtilObject.isNullOrEmpty(in_JBDM12) && in_JBDM12.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy12) || in_sfzy12 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM13 = drgBAData.getJBDM13();
				Integer in_sfzy13 = drgBAData.getSFZY13();
				if(!UtilObject.isNullOrEmpty(in_JBDM13) && in_JBDM13.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy13) || in_sfzy13 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM14 = drgBAData.getJBDM14();
				Integer in_sfzy14 = drgBAData.getSFZY14();
				if(!UtilObject.isNullOrEmpty(in_JBDM14) && in_JBDM14.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy14) || in_sfzy14 < 0){
						flag = false;
			    	}
				}
			}
			
			if(flag){
				String in_JBDM15 = drgBAData.getJBDM15();
				Integer in_sfzy15 = drgBAData.getSFZY15();
				if(!UtilObject.isNullOrEmpty(in_JBDM15) && in_JBDM15.length()>0){
					if(UtilObject.isNullOrEmpty(in_sfzy15) || in_sfzy15 < 0){
						flag = false;
			    	}
				}
			}
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("123"));
			}
		*/} catch (Exception e) {
			LOGGER.error("CheckRule23数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	
    	latch.countDown();
    }  
      
}  
