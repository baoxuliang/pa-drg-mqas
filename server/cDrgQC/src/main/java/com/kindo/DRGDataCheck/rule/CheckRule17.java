package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 自付金额校验
 * 住院总费用>=自付金额
 * @author jindoulixing
 */
public class CheckRule17 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule17.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_ZFY = drgBAData.getZFY();
			Double in_ZFJE = drgBAData.getZFJE();
			if((UtilObject.isNullOrEmpty(in_ZFY)?0:in_ZFY) < (UtilObject.isNullOrEmpty(in_ZFJE)?0:in_ZFJE)){
					flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("117"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule17数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	latch.countDown();
    }  
      
}  
