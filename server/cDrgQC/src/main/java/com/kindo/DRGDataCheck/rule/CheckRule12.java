package com.kindo.DRGDataCheck.rule;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
import com.kindo.utils.DateUtil;
/**
 * 年龄校验
 * 年龄=入院日期-出生日期，按实足年龄计算（±1 岁误差范围内都允许通过）
 * @author jindoulixing
 */
public class CheckRule12 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule12.class);
  
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
	    	Integer in_nl = drgBAData.getNL();
	    	Date in_ryrq = drgBAData.getRYSJ();
	    	Date in_csrq = drgBAData.getCSRQ();
	    	if(UtilObject.isNullOrEmpty(in_nl)){
	    		in_nl = 0;
			}
			/*long from =0l;
			long to = 0l;
			int days = 0;*/
		    if(null != in_csrq){
		    	/*from = in_csrq.getTime();  
				to = in_ryrq.getTime();
				days = (int) ((to - from)/(1000 * 60 * 60 * 24)); 
				float fl = (float)days/365;
				int l_nl = (int) Math.ceil(fl);*/
				int l_nl = DateUtil.getAge(in_csrq, in_ryrq);
				if((in_nl < l_nl - 1) || (in_nl > l_nl + 1)){
					flag = false;
				}		 
		    }
			
	    	if(!flag){//记录校验失败信息
	    		countfail.addAndGet(1);
	    		DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("112"));
	    	}    	
    	} catch (Exception e) {
			LOGGER.error("CheckRule12数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	latch.countDown();
    }  
      
}  
