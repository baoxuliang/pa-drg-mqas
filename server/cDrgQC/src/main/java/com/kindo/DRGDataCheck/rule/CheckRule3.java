package com.kindo.DRGDataCheck.rule;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRule3;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 互不共存诊断校验
 * 部分特定诊断不能与其他相关诊断同时出现
 * @author jindoulixing
 */
public class CheckRule3 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule3.class);
	
    @Override  
    public void run() { 
    	try {
			List<String> list = new ArrayList<String>();
			
			String in_JBDM = drgBAData.getJBDM();
			if(null!=in_JBDM && !in_JBDM.equals("")){
				list.add(in_JBDM.trim().substring(0, 9));
			}
			String in_JBDM1 = drgBAData.getJBDM1();
			if(null!=in_JBDM1 && !in_JBDM1.equals("")){
				list.add(in_JBDM1.trim().substring(0, 9));
			}
			String in_JBDM2 = drgBAData.getJBDM2();
			if(null!=in_JBDM2 && !in_JBDM2.equals("")){
				list.add(in_JBDM2.trim().substring(0, 9));
			}
			String in_JBDM3 = drgBAData.getJBDM3();
			if(null!=in_JBDM3 && !in_JBDM3.equals("")){
				list.add(in_JBDM3.trim().substring(0, 9));
			}
			String in_JBDM4 = drgBAData.getJBDM4();
			if(null!=in_JBDM4 && !in_JBDM4.equals("")){
				list.add(in_JBDM4.trim().substring(0, 9));
			}
			String in_JBDM5 = drgBAData.getJBDM5();
			if(!UtilObject.isNullOrEmpty(in_JBDM5)){
				list.add(in_JBDM5.trim().substring(0, 9));
			}
			String in_JBDM6 = drgBAData.getJBDM6();
			if(!UtilObject.isNullOrEmpty(in_JBDM6)){
				list.add(in_JBDM6.trim().substring(0, 9));
			}
			String in_JBDM7 = drgBAData.getJBDM7();
			if(!UtilObject.isNullOrEmpty(in_JBDM7)){
				list.add(in_JBDM7.trim().substring(0, 9));
			}
			String in_JBDM8 = drgBAData.getJBDM8();
			if(!UtilObject.isNullOrEmpty(in_JBDM8)){
				list.add(in_JBDM8.trim().substring(0, 9));
			}
			String in_JBDM9 = drgBAData.getJBDM9();
			if(!UtilObject.isNullOrEmpty(in_JBDM9)){
				list.add(in_JBDM9.trim().substring(0, 9));
			}
			String in_JBDM10 = drgBAData.getJBDM10();
			if(!UtilObject.isNullOrEmpty(in_JBDM10)){
				list.add(in_JBDM10.trim().substring(0, 9));
			}
			String in_JBDM11 = drgBAData.getJBDM11();
			if(!UtilObject.isNullOrEmpty(in_JBDM11)){
				list.add(in_JBDM11.trim().substring(0, 9));
			}
			String in_JBDM12 = drgBAData.getJBDM12();
			if(!UtilObject.isNullOrEmpty(in_JBDM12)){
				list.add(in_JBDM12.trim().substring(0, 9));
			}
			String in_JBDM13 = drgBAData.getJBDM13();
			if(!UtilObject.isNullOrEmpty(in_JBDM13)){
				list.add(in_JBDM13.trim().substring(0, 9));
			}
			String in_JBDM14 = drgBAData.getJBDM14();
			if(!UtilObject.isNullOrEmpty(in_JBDM14)){
				list.add(in_JBDM14.trim().substring(0, 9));
			}
			String in_JBDM15 = drgBAData.getJBDM15();
			if(!UtilObject.isNullOrEmpty(in_JBDM15)){
				list.add(in_JBDM15.trim().substring(0, 9));
			}
			
			if(DrgRule3.checkRule3_1A(list)){//存在互补共存诊断,记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("103"));
			}else{
				if(DrgRule3.checkRule3_2A(list) && DrgRule3.checkRule3_2B(list)){//存在互补共存诊断,记录校验失败信息
					countfail.addAndGet(1);
					DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("103"));
				}
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule3数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	latch.countDown();
    }  
      
}  
