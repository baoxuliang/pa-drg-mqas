package com.kindo.DRGDataCheck;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.kindo.DRGDataCheck.cache.DrgRule1;
import com.kindo.DRGDataCheck.cache.DrgRule2;
import com.kindo.DRGDataCheck.cache.DrgRule3;
import com.kindo.DRGDataCheck.cache.DrgRule4;
import com.kindo.DRGDataCheck.cache.DrgRule5;
import com.kindo.DRGDataCheck.cache.DrgRule6;
import com.kindo.DRGDataCheck.cache.DrgRule7;
import com.kindo.DRGDataCheck.cache.DrgRule8;
import com.kindo.DRGDataCheck.cache.DrgRule9;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.dao.DRGDataCheckMapper;
import com.kindo.DRGDataCheck.model.DRGRuleConfig;
import com.kindo.DRGDataCheck.model.DrgCheckBaData;
import com.kindo.DRGDataCheck.model.DrgFailLog;
import com.kindo.DRGDataCheck.model.QCErrResult;
import com.kindo.DRGDataCheck.rule.AbstractCheckRule;

@Service
@Component
public class DRGDataCheck {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DRGDataCheck.class);
	
	private ExecutorService executor = Executors.newFixedThreadPool(28);
	
	/*private static class SingleTonBuilder {
	   private static DRGDataCheck singleTon = new DRGDataCheck();
	}
	public static DRGDataCheck getInstance() {
	   return SingleTonBuilder.singleTon;
    }*/
	
	private static DRGDataCheckMapper dRGDataCheckMapper;
	@Autowired
    public DRGDataCheck(DRGDataCheckMapper DRGDataCheckMapper) {
    	DRGDataCheck.dRGDataCheckMapper = DRGDataCheckMapper;
    }
    
	private ConcurrentHashMap<String, StringBuffer> resultMap = null;
    public void setResultMap(ConcurrentHashMap<String, StringBuffer> resultMap){
   	 	this.resultMap = resultMap;
    }
    
    private volatile static ConcurrentSkipListSet<QCErrResult>  failList = null;
    public void setFailList(ConcurrentSkipListSet<QCErrResult>  failList){
   	 	this.failList = failList;
    }
    
    private Set<AbstractCheckRule> drgRuleConfiguration = null;
	
	public boolean execDRGBACheck(final DrgCheckBaData drgBAData){
		boolean result = true;
		
		final AtomicInteger countfail = new AtomicInteger(0);
		
		drgRuleConfiguration = DrgRuleConfiguration.getInstance().getDrgRuleCache();
		
		final CountDownLatch latch = new CountDownLatch(drgRuleConfiguration.size());
		
		try {
			Iterator<AbstractCheckRule> it = drgRuleConfiguration.iterator();  
			while (it.hasNext()) {  
				AbstractCheckRule rule = it.next();
				rule.CheckRule(latch, drgBAData, countfail);
				executor.submit(rule);
			}
			
			latch.await();//主线程等待
			
			String DRG_CHECK = "Y";
			if(countfail.get()>0){//是否需要放回校验结果
				result = false;
				DRG_CHECK = "N";
			}
			resultMap.get(DRG_CHECK).append("'").append(drgBAData.getID()).append("'").append(",");
			//updateDrgCheckBaData(drgBAData.getID(),DRG_CHECK);
			
		} catch (InterruptedException e) {
			result = false;
			e.printStackTrace();
		}
		
	    //executor.shutdown();
		return result;
	}
	
	public static DRGDataCheckMapper getDRGDataCheckMapper(){
		return dRGDataCheckMapper;
	} 
	
	private void updateDrgCheckBaData(String id, String DRG_CHECK){
		dRGDataCheckMapper.updateDrgCheckBaData(id,DRG_CHECK);
	}
	
	public void updateBatchDrgCheckBaData(String IDS, String DRG_CHECK){
		dRGDataCheckMapper.updateBatchDrgCheckBaData(IDS,DRG_CHECK);
	}

	public void refreshCache() {
		DrgRuleConfiguration.getInstance().refreshDrgRuleConfiguration();
		DrgRule1.refreshCacheRule1();
		DrgRule2.refreshCacheRule2();
		DrgRule3.refreshCacheRule3();
		DrgRule4.refreshCacheRule4();
		DrgRule5.refreshCacheRule5();
		DrgRule6.refreshCacheRule6();
		DrgRule7.refreshCacheRule7();
		DrgRule8.refreshCacheRule8();
		DrgRule9.refreshCacheRule9();
	}
	
	/*public List<DrgBAData> selectDrgBAData(String id){
		return dRGDataCheckMapper.selectDrgBAData(id);
	}*/
	
	public static void insertDrgFailLog(DrgFailLog log) {
		LOGGER.info("DRGDataCheck数据校验记录校验未通过日志:"+log.toString());
		String id=log.getId();
		String rule=log.getRule();
		DrgFailLog record = dRGDataCheckMapper.selectByPrimaryKey(id, rule);
		if(record!=null) {
			record.setGenerateDate(new Date());
			dRGDataCheckMapper.updateByPrimaryKey(record);
		}else {
			dRGDataCheckMapper.insert(log);
		}
	}
	
	public static void insertQCErrResult(DrgCheckBaData drgBAData,DRGRuleConfig dRGConfig) {
		QCErrResult qCErrResult  = new QCErrResult();
		//qCErrResult.setUID(drgBAData.getUID());
		qCErrResult.setMEMBER_CODE(drgBAData.getMEMBER_CODE());
		qCErrResult.setBAH(drgBAData.getBAH());
		qCErrResult.setZYCS(1);
		qCErrResult.setERR_CODE(dRGConfig.getErrCode());
		qCErrResult.setERR_NAME(dRGConfig.getErrName());
		qCErrResult.setERR_MEMO(dRGConfig.getErrDesc());
		qCErrResult.setERR_TPYE("3");
		qCErrResult.setCHECK_TIME(new Date());
		qCErrResult.setID(drgBAData.getID());
		failList.add(qCErrResult);
		/*QCErrResult record = dRGDataCheckMapper.selectDrgQCErrResult(drgBAData.getID(), dRGConfig.getErrCode());
		if(record!=null) {
			dRGDataCheckMapper.updateDrgQCErrResult(qCErrResult);
		}else {
			dRGDataCheckMapper.insertDrgQCErrResult(qCErrResult);
		}*/
		
	}
	public void deleteBatchQcErrResult(ConcurrentSkipListSet<QCErrResult>  failList){
		dRGDataCheckMapper.deleteBatchQcErrResult(failList);
	}
	public void insertBatchQCErrResultDatas(List<QCErrResult>  failList){
		dRGDataCheckMapper.insertBatchQCErrResultDatas(failList);
	}
	
	public static void main(String[] args) {
		String jsonStr = "{\"aPPID\":\"aaa\",\"bAH\":\"132019\",\"bA_TYPE\":\"01\",\"bDBLZPF\":0.0,\"bLZDF\":0.0,\"batchNum\":\"aaa201803051721111533\",\"cBCWF\":0.0,\"cBLX_CODE\":\"310\",\"cCHIBM1\":\"XXX00099\",\"cCHIBM2\":\"KM832701\",\"cCHIBM3\":\"HME62201\",\"cCHIBM4\":\"HJM45301\",\"cCHIBM5\":\"HJE48601\",\"cCHIBM6\":\"FJD01601\",\"cCHIBM7\":\"\",\"cCHIBM8\":\"\",\"cCHIMC1\":\"其他非手术治疗\",\"cCHIMC2\":\"压力抗栓治疗\",\"cCHIMC3\":\"经皮穿刺锁骨下静脉置管术\",\"cCHIMC4\":\"经肋间胸腔闭式引流术\",\"cCHIMC5\":\"经支气管镜支气管肺泡灌洗术\",\"cCHIMC6\":\"纤维支气管镜检查\",\"cCHIMC7\":\"\",\"cCHIMC8\":\"\",\"cSRQ\":\"1932-08-02\",\"cWHCF\":0.0,\"cYKB\":\"重症医学科\",\"cYSJ\":\"2018-03-05\",\"cYSJS\":12.0,\"eJHL\":0,\"fSSZLXMF\":17061.0,\"hCYYCLF\":92.95,\"hLF\":3522.0,\"hOS_ID\":\"0101\",\"hXJSYSJ\":442.5,\"iD\":\"cb48a248-d3d5-4f0d-8779-847f4a46010f\",\"kFF\":4392.0,\"kJYWF\":19680.26,\"lCZDXMF\":3756.0,\"lYFS\":5,\"mEDICALFEES\":[],\"mZF\":342.0,\"nL\":86,\"nXYZLZPF\":0.0,\"pTCWF\":0.0,\"qDBLZPF\":0.0,\"qJCGCS\":2,\"qJCS\":2,\"qTF\":0.0,\"qTFY\":80.0,\"rYKB\":\"重症医学科\",\"rYSJ\":\"2018-02-15\",\"rYSJS\":0,\"rYTJ\":1,\"rZCCU\":0.0,\"rZEICU\":0.0,\"rZNICU\":0.0,\"rZPICU\":0.0,\"rZQTJHS\":444.0,\"rZSICU\":0.0,\"sFZY1\":1,\"sFZY10\":1,\"sFZY11\":0,\"sFZY2\":1,\"sFZY3\":1,\"sFZY4\":1,\"sFZY5\":1,\"sFZY6\":1,\"sFZY7\":1,\"sFZY8\":1,\"sFZY9\":1,\"sJHL\":0,\"sJZYTS\":18,\"sSF\":430.0,\"sSZLF\":772.0,\"sYBM\":\"J15.9030101\",\"sYBM1\":\"J80.x000101\",\"sYBM10\":\"K40.9000101\",\"sYBM11\":\"\",\"sYBM12\":\"\",\"sYBM13\":\"\",\"sYBM14\":\"\",\"sYBM15\":\"\",\"sYBM2\":\"R65.2010101\",\"sYBM3\":\"R65.3010101\",\"sYBM4\":\"J90.x000101\",\"sYBM5\":\"D64.9000101\",\"sYBM6\":\"I10.x050301\",\"sYBM7\":\"D69.5010201\",\"sYBM8\":\"L10.9000101\",\"sYBM9\":\"E87.8010101\",\"sYMC\":\"重症肺炎\",\"sYMC1\":\"急性呼吸窘迫综合征\",\"sYMC10\":\"腹股沟疝\",\"sYMC11\":\"\",\"sYMC12\":\"\",\"sYMC13\":\"\",\"sYMC14\":\"\",\"sYMC15\":\"\",\"sYMC2\":\"脓毒性休克（感染性休克）\",\"sYMC3\":\"多器官功能障碍综合症（MODS）\",\"sYMC4\":\"胸腔积液\",\"sYMC5\":\"贫血\",\"sYMC6\":\"高血压病3级（高危）\",\"sYMC7\":\"继发性血小板减少症\",\"sYMC8\":\"天疱疮\",\"sYMC9\":\"电解质紊乱\",\"sYSZDF\":16212.2,\"tJHL\":19,\"uSERNAME\":\"三明市第一医院\",\"wLZLF\":0.0,\"xB\":1,\"xBYZLZPF\":0.0,\"xF\":936.0,\"xSECSTZ\":0,\"xSERYTZ\":0,\"xYF\":35992.72,\"yBLSH\":\"105375722\",\"yCXYYCLF\":74.1,\"yJHL\":0,\"yLFKFS\":1,\"yLFUF\":3045.0,\"yXXZDF\":6430.0,\"yYCLF\":2522.47,\"zCF\":315.0,\"zCYF\":0.0,\"zCYF1\":0.0,\"zFJE\":0.0,\"zFY\":111656.94,\"zKKB\":\"\",\"zKKB2\":\"\",\"zKKB3\":\"\",\"zLCZF\":16768.5,\"zYZLF\":0.0,\"zZJHCWF\":2700.0}";
		JSONObject jsonobject = JSONObject.parseObject(jsonStr);
		/*DrgBAData data = null;
		data = ((JSONObject) jsonobject).toJavaObject(DrgBAData.class);
		DRGDataCheck.execDRGBACheck(data);*/
		
	}
}
