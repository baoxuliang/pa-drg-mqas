package com.kindo.DRGDataCheck.rule;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import com.kindo.DRGDataCheck.model.DrgCheckBaData;


public abstract class AbstractCheckRule implements Runnable{
	
	protected DrgCheckBaData drgBAData;  
	protected CountDownLatch latch; 
	protected AtomicInteger countfail; 
    
    public void CheckRule(final CountDownLatch latch,final DrgCheckBaData drgBAData,final AtomicInteger countfail) {  
        this.drgBAData = drgBAData;  
        this.latch = latch;  
        this.countfail = countfail;
    }
    
}
