package com.kindo.DRGDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.DRGRuleConfig;
import com.kindo.DRGDataCheck.rule.AbstractCheckRule;

public class DrgRuleConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgRuleConfiguration.class);
	
	//private static Map<String, String> drgRuleCache = new ConcurrentHashMap<>();
	private final static Set<AbstractCheckRule> drgRuleCache = new HashSet<AbstractCheckRule>();
	private final static Map<String, DRGRuleConfig> drgRuleConfigCache = new ConcurrentHashMap<>();
	
    private static DrgRuleConfiguration instance = null;
    
    public static DrgRuleConfiguration getInstance() {
        if (instance == null) {
            synchronized (DrgRuleConfiguration.class) {
                if (instance == null) {
                    instance = new DrgRuleConfiguration();
                }
            }
        }
        return instance;
    }
    
     
    private DrgRuleConfiguration() {
    	refreshCache();
    }
    
    
    /*static {
    	if(drgRuleCache==null || drgRuleCache.size()==0){//
    		refreshCache();
    	}
    }*/

    private void refreshCache() {
    	List<DRGRuleConfig> list = DRGDataCheck.getDRGDataCheckMapper().queryDrgRuleConfiguration();
    	
		for (DRGRuleConfig item : list) {
			AbstractCheckRule rule = getRuleClass(item.getCheckClass());
			if (rule != null) {
				drgRuleCache.add(rule);
			}
			drgRuleConfigCache.put(item.getErrCode(), item);
			
		}
    	    
    }
    
    private void cleanupCache() {
    	drgRuleCache.clear();
    	drgRuleConfigCache.clear();
    }
    
    public void refreshDrgRuleConfiguration() {
    	cleanupCache();
    	refreshCache();
    }
    
    public Set<AbstractCheckRule> getDrgRuleCache(){
    	if(drgRuleCache ==null || drgRuleCache.size() == 0){
    		refreshCache();
    	}
    	return drgRuleCache;
    }
    
    private AbstractCheckRule getRuleClass(String clsName) {
		try {
			Class<?> cls = Class.forName(clsName);
			return (AbstractCheckRule) cls.newInstance();
		} catch (ClassNotFoundException e) {
			LOGGER.error("Failed to find the class:" + clsName);
			return null;
		} catch (Exception e) {
			LOGGER.error("Failed to initialize the class:" + clsName, e);
			return null;
		}
	}
   
    public DRGRuleConfig getDRGConfigCache(String key){
    	if(drgRuleConfigCache ==null || drgRuleConfigCache.size() == 0){
    		refreshCache();
    	}
    	return drgRuleConfigCache.get(key);
    }
}
