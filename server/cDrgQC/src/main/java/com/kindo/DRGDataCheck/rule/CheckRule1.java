package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRule1;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 诊断范围校验
 * @author jindoulixing
 */
public class CheckRule1 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule1.class);

	@Override  
    public void run() {
    	boolean flag = true;
    	try {
			if(flag){
				String in_JBDM = drgBAData.getJBDM();
				if(!UtilObject.isNullOrEmpty(in_JBDM)){
					String key = in_JBDM.trim();
					String zdbm = DrgRule1.getZDBM(key);
					
					if(zdbm==null || !zdbm.equals(in_JBDM)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM1 = drgBAData.getJBDM1();
				if(!UtilObject.isNullOrEmpty(in_JBDM1)){
					String key1 = in_JBDM1.trim();
					String zdbm1 = DrgRule1.getZDBM(key1);
					
					if(zdbm1==null || !zdbm1.equals(in_JBDM1)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM2 = drgBAData.getJBDM2();
				if(!UtilObject.isNullOrEmpty(in_JBDM2)){
					String key2 = in_JBDM2.trim();
					String zdbm2 = DrgRule1.getZDBM(key2);
					
					if(zdbm2==null || !zdbm2.equals(in_JBDM2)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM3 = drgBAData.getJBDM3();
				if(!UtilObject.isNullOrEmpty(in_JBDM3)){
					String key3 = in_JBDM3.trim();
					String zdbm3 = DrgRule1.getZDBM(key3);
					
					if(zdbm3==null || !zdbm3.equals(in_JBDM3)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM4 = drgBAData.getJBDM4();
				if(!UtilObject.isNullOrEmpty(in_JBDM4)){
					String key4 = in_JBDM4.trim();
					String zdbm4 = DrgRule1.getZDBM(key4);
					
					if(zdbm4==null || !zdbm4.equals(in_JBDM4)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM5 = drgBAData.getJBDM5();
				if(!UtilObject.isNullOrEmpty(in_JBDM5)){
					String key5 = in_JBDM5.trim();
					String zdbm5 = DrgRule1.getZDBM(key5);
					
					if(zdbm5==null || !zdbm5.equals(in_JBDM5)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM6 = drgBAData.getJBDM6();
				if(!UtilObject.isNullOrEmpty(in_JBDM6)){
					String key6 = in_JBDM6.trim();
					String zdbm6 = DrgRule1.getZDBM(key6);
					
					if(zdbm6==null || !zdbm6.equals(in_JBDM6)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM7 = drgBAData.getJBDM7();
				if(!UtilObject.isNullOrEmpty(in_JBDM7)){
					String key7 = in_JBDM7.trim();
					String zdbm7 = DrgRule1.getZDBM(key7);
					
					if(zdbm7==null || !zdbm7.equals(in_JBDM7)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM8 = drgBAData.getJBDM8();
				if(!UtilObject.isNullOrEmpty(in_JBDM8)){
					String key8 = in_JBDM8.trim();
					String zdbm8 = DrgRule1.getZDBM(key8);
					
					if(zdbm8==null || !zdbm8.equals(in_JBDM8)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM9 = drgBAData.getJBDM9();
				if(!UtilObject.isNullOrEmpty(in_JBDM9)){
					String key9 = in_JBDM9.trim();
					String zdbm9 = DrgRule1.getZDBM(key9);
					
					if(zdbm9==null || !zdbm9.equals(in_JBDM9)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM10 = drgBAData.getJBDM10();
				if(!UtilObject.isNullOrEmpty(in_JBDM10)){
					String key10 = in_JBDM10.trim();
					String zdbm10 = DrgRule1.getZDBM(key10);
					
					if(zdbm10==null || !zdbm10.equals(in_JBDM10)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM11 = drgBAData.getJBDM11();
				if(!UtilObject.isNullOrEmpty(in_JBDM11)){
					String key11 = in_JBDM11.trim();
					String zdbm11 = DrgRule1.getZDBM(key11);
					
					if(zdbm11==null || !zdbm11.equals(in_JBDM11)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM12 = drgBAData.getJBDM12();
				if(!UtilObject.isNullOrEmpty(in_JBDM12)){
					String key12 = in_JBDM12.trim();
					String zdbm12 = DrgRule1.getZDBM(key12);
					
					if(zdbm12==null || !zdbm12.equals(in_JBDM12)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM13 = drgBAData.getJBDM13();
				if(!UtilObject.isNullOrEmpty(in_JBDM13)){
					String key13 = in_JBDM13.trim();
					String zdbm13 = DrgRule1.getZDBM(key13);
					
					if(zdbm13==null || !zdbm13.equals(in_JBDM13)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM14 = drgBAData.getJBDM14();
				if(!UtilObject.isNullOrEmpty(in_JBDM14)){
					String key14 = in_JBDM14.trim();
					String zdbm14 = DrgRule1.getZDBM(key14);
					
					if(zdbm14==null || !zdbm14.equals(in_JBDM14)){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_JBDM15 = drgBAData.getJBDM15();
				if(!UtilObject.isNullOrEmpty(in_JBDM15)){
					String key15 = in_JBDM15.trim();
					String zdbm15 = DrgRule1.getZDBM(key15);
					
					if(zdbm15==null || !zdbm15.equals(in_JBDM15)){
						flag = false;
					}
				}
			}
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("101"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule1数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
