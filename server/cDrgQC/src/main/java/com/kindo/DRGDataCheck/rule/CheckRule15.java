package com.kindo.DRGDataCheck.rule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
/**
 * 总费用校验
 * 总费用应大于等于各分项费用（除自付金额,临床物理治疗费,麻醉费,手术费,抗菌药物费用外）之和
 * @author jindoulixing
 *
 */
public class CheckRule15 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule15.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_zfy = drgBAData.getZFY();
			Double l_YLFUF = drgBAData.getYLFUF();
			Double l_ZLCZF = drgBAData.getZLCZF();
			Double l_HLF = drgBAData.getHLF();
			Double l_QTFY = drgBAData.getQTFY();
			Double l_BLZDF = drgBAData.getBLZDF();
			Double l_SYSZDF = drgBAData.getSYSZDF();
			Double l_YXXZDF = drgBAData.getYXXZDF();
			Double l_LCZDXMF = drgBAData.getLCZDXMF();
			Double l_FSSZLXMF = drgBAData.getFSSZLXMF();
			Double l_SSZLF = drgBAData.getSSZLF();
			Double l_KFF = drgBAData.getKFF();
			Double l_ZYZLF = drgBAData.getZYZLF();
			Double l_XYF = drgBAData.getXYF();
			Double l_ZCYF = drgBAData.getZCYF();
			Double l_ZCYF1 = drgBAData.getZCYF1();
			Double l_XF = drgBAData.getXF();
			Double l_BDBLZPF = drgBAData.getBDBLZPF();
			Double l_QDBLZPF = drgBAData.getQDBLZPF();
			Double l_NXYZLZPF = drgBAData.getNXYZLZPF();
			Double l_XBYZLZPF = drgBAData.getXBYZLZPF();
			Double l_HCYYCLF = drgBAData.getHCYYCLF();
			Double l_YYCLF = drgBAData.getYYCLF();
			Double l_YCXYYCLF = drgBAData.getYCXYYCLF();
			Double l_QTF = drgBAData.getQTF();
			 
			Double in_qtzfy = l_YLFUF + l_ZLCZF + l_HLF  + l_QTFY + l_BLZDF + l_SYSZDF + l_YXXZDF + l_LCZDXMF + l_FSSZLXMF + l_SSZLF + l_KFF + l_ZYZLF + l_XYF + l_ZCYF + l_ZCYF1 + l_XF +l_BDBLZPF + l_QDBLZPF + l_NXYZLZPF + l_XBYZLZPF + l_HCYYCLF + l_YYCLF + l_YCXYYCLF + l_QTF;
			BigDecimal bg = new BigDecimal(in_qtzfy);    
		    in_qtzfy = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	         
			if(in_zfy < in_qtzfy){
				flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("115"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule15数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
