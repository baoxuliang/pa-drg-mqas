package com.kindo.DRGDataCheck.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;
import com.kindo.DRGDataCheck.utils.UtilObject;

@Component
public class DrgRule3 {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule3.class);

	private static Set<String> drgRule3_1A = new HashSet<String>();
	private static Set<String> drgRule3_2A = new HashSet<String>();
	private static Set<String> drgRule3_2B = new HashSet<String>();

	
	static {
		if (UtilObject.isNullOrEmpty(drgRule3_1A)){ 
			refreshCache(); 
		} 
	}
	 

	private static void refreshCache() {
		List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule3();

		String ZDBM = "";
		String dataType = "";
		String dataFlag = "";
		for (Rule item : list) {
			dataType = item.getDATA_TYPE();
			dataFlag = item.getDATA_FLAG();
			ZDBM = item.getZDBM();
			if (dataType.equals("1") && dataFlag.equals("A")) {
				drgRule3_1A.add(ZDBM);
			}
			if (dataType.equals("2") && dataFlag.equals("A")) {
				drgRule3_2A.add(ZDBM);
			}
			if (dataType.equals("2") && dataFlag.equals("B")) {
				drgRule3_2B.add(ZDBM);
			}
		}
	}

	private static void cleanupCache() {
		drgRule3_1A.clear();
		drgRule3_2A.clear();
		drgRule3_2B.clear();
	}
	public static void refreshCacheRule3() {
    	cleanupCache();
    	refreshCache();
    }
	public static boolean checkRule3_1A(List<String> list) {
		if (drgRule3_1A == null || drgRule3_1A.size() == 0) {
			refreshCache();
		}
		// 创建集合
		List<String> realA = new ArrayList<String>(drgRule3_1A);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		
		return realA.size() > 1;
	}

	public static boolean checkRule3_2A(List<String> list) {
		if (drgRule3_2A == null || drgRule3_2A.size() == 0) {
			refreshCache();
		}
		// 创建集合
		List<String> realA = new ArrayList<String>(drgRule3_2A);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		
		return realA.size() > 0;
	}

	public static boolean checkRule3_2B(List<String> list) {
		if (drgRule3_2B == null || drgRule3_2B.size() == 0) {
			refreshCache();
		}
		// 创建集合
		List<String> realA = new ArrayList<String>(drgRule3_2B);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		
		return realA.size() > 0;
	}

}
