package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 手术费用校验
 * 手术治疗费≥麻醉费+手术费
 * @author jindoulixing
 */
public class CheckRule20 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule20.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_SSZLF = drgBAData.getSSZLF();
			Double in_MAF = drgBAData.getMAF();
			Double in_SSF = drgBAData.getSSF();
			in_SSZLF = UtilObject.isNullOrEmpty(in_SSZLF)?0:in_SSZLF;
			in_MAF = UtilObject.isNullOrEmpty(in_MAF)?0:in_MAF;
			in_SSF = UtilObject.isNullOrEmpty(in_SSF)?0:in_SSF;
			
			if(in_SSZLF < (in_MAF + in_SSF)){
				flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("120"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule20数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
