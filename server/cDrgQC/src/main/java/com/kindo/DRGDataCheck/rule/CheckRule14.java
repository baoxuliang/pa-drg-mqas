package com.kindo.DRGDataCheck.rule;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;

/**
 * 住院天数校验
 * 住院天数=出院日期-入院日期，按实足天数计算（±1天误差范围内动允许通过）；当出院日期=入院日期时，入院天数为1天
 * @author jindoulixing
 */
public class CheckRule14 extends AbstractCheckRule {
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule14.class);

	@Override
	public void run() {
		boolean flag = true;
		try {
			Date in_ryrq = drgBAData.getRYSJ();
			Date in_cyrq = drgBAData.getCYSJ();
			Double in_sjzyts = drgBAData.getSJZYTS();

			if (in_ryrq == in_cyrq) {
				if (in_sjzyts != 1) {
					flag = false;
				}
			} else {
				long from = 0l;
				long to = 0l;
				int days = 0;

				from = in_ryrq.getTime();
				to = in_cyrq.getTime();
				days = (int) ((to - from) / (1000 * 60 * 60 * 24));

				if ((in_sjzyts < days - 1) || (in_sjzyts > days + 1)) {
					flag = false;
				}
			}

			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("114"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule14数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}

		latch.countDown();
	}

}
