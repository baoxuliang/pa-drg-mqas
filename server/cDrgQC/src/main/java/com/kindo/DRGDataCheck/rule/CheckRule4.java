package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRule4;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 手术操作范围校验
 * 医院上传手术操作在CCHI中
 * @author jindoulixing
 */
public class CheckRule4 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule4.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
			String in_cchi1 = drgBAData.getSSJCZBM1();
			
			if(!UtilObject.isNullOrEmpty(in_cchi1)){
				flag = DrgRule4.checkRule4(in_cchi1);
			}
			
			if(flag){
				String in_cchi2 = drgBAData.getSSJCZBM2();
				
				if(!UtilObject.isNullOrEmpty(in_cchi2)){
					flag = DrgRule4.checkRule4(in_cchi2);
				}
			}
			
			if(flag){
				String in_cchi3 = drgBAData.getSSJCZBM3();
				
				if(!UtilObject.isNullOrEmpty(in_cchi3)){
					flag = DrgRule4.checkRule4(in_cchi3);
				}
			}
			
			if(flag){
				String in_cchi4 = drgBAData.getSSJCZBM4();
				
				if(!UtilObject.isNullOrEmpty(in_cchi4)){
					flag = DrgRule4.checkRule4(in_cchi4);
				}
			}
			
			if(flag){
				String in_cchi5 = drgBAData.getSSJCZBM5();
				
				if(!UtilObject.isNullOrEmpty(in_cchi5)){
					flag = DrgRule4.checkRule4(in_cchi5);
				}
			}
			
			if(flag){
				String in_cchi6 = drgBAData.getSSJCZBM6();
				
				if(!UtilObject.isNullOrEmpty(in_cchi6)){
					flag = DrgRule4.checkRule4(in_cchi6);
				}
			}
			
			if(flag){
				String in_cchi7 = drgBAData.getSSJCZBM7();
				
				if(!UtilObject.isNullOrEmpty(in_cchi7)){
					flag = DrgRule4.checkRule4(in_cchi7);
				}
			}
			
			if(flag){
				String in_cchi8 = drgBAData.getSSJCZBM8();
				
				if(!UtilObject.isNullOrEmpty(in_cchi8)){
					flag = DrgRule4.checkRule4(in_cchi8);
				}
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("104"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule4数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	
    	latch.countDown();
    }  
      
}  
