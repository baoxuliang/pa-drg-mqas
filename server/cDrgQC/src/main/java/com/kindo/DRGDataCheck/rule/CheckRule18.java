package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 一般医疗服务费校验
 * 一般医疗服务费>=诊察费+普通床位费+重症监护床位费
 * 华北理工没有这几项费用
 * @author jindoulixing
 */
public class CheckRule18 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule18.class);

    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_YLFUF = drgBAData.getYLFUF();
			//Double in_ZCF = drgBAData.getZCF();
			Double in_PTCWF = drgBAData.getPTCWF();
			Double in_ZZJHCWF = drgBAData.getZZJHCWF();
			in_YLFUF = UtilObject.isNullOrEmpty(in_YLFUF)?0:in_YLFUF;
			/*in_ZCF = UtilObject.isNullOrEmpty(in_ZCF)?0:in_ZCF;*/
			in_PTCWF = UtilObject.isNullOrEmpty(in_PTCWF)?0:in_PTCWF;
			in_ZZJHCWF = UtilObject.isNullOrEmpty(in_ZZJHCWF)?0:in_ZZJHCWF;
			
			if(in_YLFUF < (in_PTCWF + in_ZZJHCWF)){
				flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("118"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule18数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
