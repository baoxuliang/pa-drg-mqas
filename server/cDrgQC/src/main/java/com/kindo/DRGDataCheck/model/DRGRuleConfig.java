package com.kindo.DRGDataCheck.model;

public class DRGRuleConfig {
	
	private static final long serialVersionUID = 8609584659719572867L;
	
    private String id;

    private String errCode;

    private String errName;

    private String errType;

    private String errDesc;

    private String errMemo;

    private String enable;
    
    private String errCode2;
    
    private String checkClass;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrName() {
		return errName;
	}

	public void setErrName(String errName) {
		this.errName = errName;
	}

	public String getErrType() {
		return errType;
	}

	public void setErrType(String errType) {
		this.errType = errType;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getErrMemo() {
		return errMemo;
	}

	public void setErrMemo(String errMemo) {
		this.errMemo = errMemo;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getErrCode2() {
		return errCode2;
	}

	public void setErrCode2(String errCode2) {
		this.errCode2 = errCode2;
	}

	public String getCheckClass() {
		return checkClass;
	}

	public void setCheckClass(String checkClass) {
		this.checkClass = checkClass;
	}
  
}