package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 年龄和不足一周岁年龄校验
 * 年龄和不足一周岁年龄只能存在一个大于0
 * @author jindoulixing
 */
public class CheckRule25 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule25.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Integer in_nl = drgBAData.getNL();
			Double in_BZYZSNL = drgBAData.getBZYZSNL();
			
			if ((UtilObject.isNullOrEmpty(in_nl) && UtilObject.isNullOrEmpty(in_BZYZSNL)) || ((UtilObject.isNullOrEmpty(in_nl)?0:in_nl) > 0 && (UtilObject.isNullOrEmpty(in_BZYZSNL)?0:in_BZYZSNL) > 0)){
				flag = false;
			}
	    	if(!flag){//记录校验失败信息
	    		saveFailLog();
	    	}    	
    	} catch (Exception e) {
    		try {
				saveFailLog();
			} catch (Exception e1) {
				LOGGER.error("CheckRule25数据校验入库失败:"+drgBAData.toString()+"异常信息"+e.getMessage());
			}
			LOGGER.error("CheckRule25数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		} 
		latch.countDown();
    }
    
    private void saveFailLog(){
    	countfail.addAndGet(1);
    	DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("125"));
    }  
}  
