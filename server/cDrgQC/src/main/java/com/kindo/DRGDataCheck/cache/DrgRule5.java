package com.kindo.DRGDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;

@Component
public class DrgRule5 {
    private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule5.class);

    private static Set<String> drgRule5 = new HashSet<String>();
    
    static {
    	if(drgRule5==null || drgRule5.size()==0){//
    		refreshCache();
    	}
    }

    private static void refreshCache() {
	    List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule5();
    	
		for (Rule item : list) {
			drgRule5.add(item.getZDBM());
		}
    	    
    }
    
    private static void cleanupCache() {
    	drgRule5.clear();
    }
    
    public static void refreshCacheRule5() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static boolean checkRule5(String key) {
    	if(drgRule5 ==null || drgRule5.size() == 0){
    		refreshCache();
    	}
    	
		return drgRule5.contains(key);
	}

}

