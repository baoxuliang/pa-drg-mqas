package com.kindo.DRGDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;

@Component
public class DrgRule6 {
    private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule6.class);

    private static Set<String> drgRule6 = new HashSet<String>();
    
    static {
    	if(drgRule6==null || drgRule6.size()==0){//
    		refreshCache();
    	}
    }

    private static void refreshCache() {
	    List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule6();
    	
		for (Rule item : list) {
			drgRule6.add(item.getZDBM());
		}
    	    
    }
    
    private static void cleanupCache() {
    	drgRule6.clear();
    }
    
    public static void refreshCacheRule6() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static boolean checkRule6(String key) {
    	if(drgRule6 ==null || drgRule6.size() == 0){
    		refreshCache();
    	}
    	
		return drgRule6.contains(key);
	}

}

