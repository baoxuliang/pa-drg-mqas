package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRule6;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 手术操作顺序校验
 * 除特殊情况外，在CCHI中选择手术操作时，优先选择临床诊断类、临床手术治疗类、中医医疗服务类的操作，其次为临床非手术治疗类、康复医疗类等操作
 * @author jindoulixing
 */
public class CheckRule6 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule6.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
			StringBuffer tmp_str = new StringBuffer();
			String in_cchi1 = drgBAData.getSSJCZBM1();
			
			if(!UtilObject.isNullOrEmpty(in_cchi1)){
				if(DrgRule6.checkRule6(in_cchi1)){
					tmp_str.append("1");
				}else{
					tmp_str.append("0");
				}
			}
			
			if(flag){
				String in_cchi2 = drgBAData.getSSJCZBM2();
				
				if(!UtilObject.isNullOrEmpty(in_cchi2)){
					if(DrgRule6.checkRule6(in_cchi2)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
					
				}
			}
			
			if(flag){
				String in_cchi3 = drgBAData.getSSJCZBM3();
				
				if(!UtilObject.isNullOrEmpty(in_cchi3)){
					if(DrgRule6.checkRule6(in_cchi3)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_cchi4 = drgBAData.getSSJCZBM4();
				
				if(!UtilObject.isNullOrEmpty(in_cchi4)){
					if(DrgRule6.checkRule6(in_cchi4)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_cchi5 = drgBAData.getSSJCZBM5();
				
				if(!UtilObject.isNullOrEmpty(in_cchi5)){
					if(DrgRule6.checkRule6(in_cchi5)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_cchi6 = drgBAData.getSSJCZBM6();
				
				if(!UtilObject.isNullOrEmpty(in_cchi6)){
					if(DrgRule6.checkRule6(in_cchi6)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_cchi7 = drgBAData.getSSJCZBM7();
				
				if(!UtilObject.isNullOrEmpty(in_cchi7)){
					if(DrgRule6.checkRule6(in_cchi7)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
				}
			}
			
			if(flag){
				String in_cchi8 = drgBAData.getSSJCZBM8();
				
				if(!UtilObject.isNullOrEmpty(in_cchi8)){
					if(DrgRule6.checkRule6(in_cchi8)){
						tmp_str.append("1");
					}else{
						tmp_str.append("0");
					}
					if(tmp_str.indexOf("01")!=-1){
						flag = false;
					}
				}
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("106"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule6数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	
    	latch.countDown();
    }  
      
}  
