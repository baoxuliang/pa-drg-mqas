package com.kindo.DRGDataCheck.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;
import com.kindo.DRGDataCheck.utils.UtilObject;

@Component
public class DrgRule7 {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule7.class);

	private static Set<String> drgRule7 = new HashSet<String>();

	
	static {
		if (UtilObject.isNullOrEmpty(drgRule7)){ 
			refreshCache(); 
		} 
	}
	 

	private static void refreshCache() {
		List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule7();

		for (Rule item : list) {
			drgRule7.add(item.getZDBM());
		}
	}

	private static void cleanupCache() {
		drgRule7.clear();
	}

	public static void refreshCacheRule7() {
    	cleanupCache();
    	refreshCache();
    }
	
	public static boolean checkRule7(List<String> list) {
		if (UtilObject.isNullOrEmpty(drgRule7)){ 
			refreshCache(); 
		} 
		// 创建集合
		List<String> realA = new ArrayList<String>(drgRule7);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		
		return realA.size() > 0;
	}
}
