package com.kindo.DRGDataCheck.rule;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 诊断重复校验
 * @author jindoulixing
 * 病案不可出现相同的诊断
 */
public class CheckRule24 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule24.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			List<String> list = new ArrayList<String>();
			
			String in_JBDM = drgBAData.getJBDM();
			list.add(in_JBDM);
			
			String in_JBDM1 = drgBAData.getJBDM1();
			if(!UtilObject.isNullOrEmpty(in_JBDM1)){
				if(list.contains(in_JBDM1)){
					flag = false;
				}else{
					list.add(in_JBDM1);
				}
			}
			
			if(flag){
				String in_JBDM2 = drgBAData.getJBDM2();
				if(!UtilObject.isNullOrEmpty(in_JBDM2)){
					if(list.contains(in_JBDM2)){
						flag = false;
					}else{
						list.add(in_JBDM2);
					}
				}
			}
			
			if(flag){
				String in_JBDM3 = drgBAData.getJBDM3();
				if(!UtilObject.isNullOrEmpty(in_JBDM3)){
					if(list.contains(in_JBDM3)){
						flag = false;
					}else{
						list.add(in_JBDM3);
					}
				}
			}
			
			if(flag){
				String in_JBDM4 = drgBAData.getJBDM4();
				if(!UtilObject.isNullOrEmpty(in_JBDM4)){
					if(list.contains(in_JBDM4)){
						flag = false;
					}else{
						list.add(in_JBDM4);
					}
				}
			}
			
			if(flag){
				String in_JBDM5 = drgBAData.getJBDM5();
				if(!UtilObject.isNullOrEmpty(in_JBDM5)){
					if(list.contains(in_JBDM5)){
						flag = false;
					}else{
						list.add(in_JBDM5);
					}
				}
			}
			
			if(flag){
				String in_JBDM6 = drgBAData.getJBDM6();
				if(!UtilObject.isNullOrEmpty(in_JBDM6)){
					if(list.contains(in_JBDM6)){
						flag = false;
					}else{
						list.add(in_JBDM6);
					}
				}
			}
			
			if(flag){
				String in_JBDM7 = drgBAData.getJBDM7();
				if(!UtilObject.isNullOrEmpty(in_JBDM7)){
					if(list.contains(in_JBDM7)){
						flag = false;
					}else{
						list.add(in_JBDM7);
					}
				}
			}
			
			if(flag){
				String in_JBDM8 = drgBAData.getJBDM8();
				if(!UtilObject.isNullOrEmpty(in_JBDM8)){
					if(list.contains(in_JBDM8)){
						flag = false;
					}else{
						list.add(in_JBDM8);
					}
				}
			}
			
			if(flag){
				String in_JBDM9 = drgBAData.getJBDM9();
				if(!UtilObject.isNullOrEmpty(in_JBDM9)){
					if(list.contains(in_JBDM9)){
						flag = false;
					}else{
						list.add(in_JBDM9);
					}
				}
			}
			
			if(flag){
				String in_JBDM10 = drgBAData.getJBDM10();
				if(!UtilObject.isNullOrEmpty(in_JBDM10)){
					if(list.contains(in_JBDM10)){
						flag = false;
					}else{
						list.add(in_JBDM10);
					}
				}
			}
			if(flag){
				String in_JBDM11 = drgBAData.getJBDM11();
				if(!UtilObject.isNullOrEmpty(in_JBDM11)){
					if(list.contains(in_JBDM11)){
						flag = false;
					}else{
						list.add(in_JBDM11);
					}
				}
			}
			if(flag){
				String in_JBDM12 = drgBAData.getJBDM12();
				if(!UtilObject.isNullOrEmpty(in_JBDM12)){
					if(list.contains(in_JBDM12)){
						flag = false;
					}else{
						list.add(in_JBDM12);
					}
				}
			}
			if(flag){
				String in_JBDM13 = drgBAData.getJBDM13();
				if(!UtilObject.isNullOrEmpty(in_JBDM13)){
					if(list.contains(in_JBDM13)){
						flag = false;
					}else{
						list.add(in_JBDM13);
					}
				}
			}
			if(flag){
				String in_JBDM14 = drgBAData.getJBDM14();
				if(!UtilObject.isNullOrEmpty(in_JBDM14)){
					if(list.contains(in_JBDM14)){
						flag = false;
					}else{
						list.add(in_JBDM14);
					}
				}
			}
			if(flag){
				String in_JBDM15 = drgBAData.getJBDM15();
				if(!UtilObject.isNullOrEmpty(in_JBDM15)){
					if(list.contains(in_JBDM15)){
						flag = false;
					}else{
						list.add(in_JBDM15);
					}
				}
			}
			if(!flag){
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("124"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule24数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
    	latch.countDown();
    }  
      
}  
