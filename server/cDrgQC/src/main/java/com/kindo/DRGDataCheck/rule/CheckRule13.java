package com.kindo.DRGDataCheck.rule;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;

/**
 * 出入院时间校验
 * 入院日期<=出院日期
 * @author jindoulixing
 */
public class CheckRule13 extends AbstractCheckRule {
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule13.class);
	
	@Override
	public void run() {
		boolean flag = true;
		try {
			Date in_ryrq = drgBAData.getRYSJ();
			Date in_cyrq = drgBAData.getCYSJ();

			if (in_ryrq.getTime() > in_cyrq.getTime()) {
				flag = false;
			}

			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("113"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule13数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}
		latch.countDown();
	}

}
