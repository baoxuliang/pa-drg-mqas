package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRule5;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 非主要手术操作编码校验
 * 除特殊情况外，CCHI中综合医疗服务类、实验室诊断、临床物理治疗的项目（首字母为A/C/L的操作）不作为手术操作
 * @author jindoulixing
 */
public class CheckRule5 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule5.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			String in_cchi1 = drgBAData.getSSJCZBM1();
			if(UtilObject.isNullOrEmpty(in_cchi1)){
				flag = false;
			}
			
			if(flag && DrgRule5.checkRule5(in_cchi1)){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("105"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule5数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
