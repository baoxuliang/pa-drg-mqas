package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 抢救次数校验
 * 抢救次数=抢救成功次数。当离院状态为“死亡”时：抢救次数=抢救成功次数或抢救成功次数+1
 * @author jindoulixing
 *
 */
public class CheckRule9 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule9.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			String in_lyfs = drgBAData.getLYFS();
			
			if(!UtilObject.isNullOrEmpty(in_lyfs)){
				Integer in_qjcs = drgBAData.getQJCS();
				Integer in_qjcgcs = drgBAData.getCGCS();
				
				if(UtilObject.isNullOrEmpty(in_qjcs)){
					in_qjcs = 0;
				}
				if(UtilObject.isNullOrEmpty(in_qjcgcs)){
					in_qjcgcs = 0;
				}
				if(in_lyfs.equals("5")){
					if(in_qjcs!=in_qjcgcs && in_qjcs!=(in_qjcgcs+1)){
						flag = false;
					}
				}else{
					if(in_qjcs!=in_qjcgcs){
						flag = false;
					}
				}
				
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("109"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule9数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
