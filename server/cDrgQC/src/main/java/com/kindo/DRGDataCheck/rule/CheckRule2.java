package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRule2;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
/**
 * 主要诊断校验
 * 部分损伤、外因的临床诊断不能做主要诊断
 * @author jindoulixing
 */
public class CheckRule2 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule2.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			String JBDM = drgBAData.getJBDM();
			String key = "";
			if(JBDM==null || JBDM.equals("")){
				flag = false;
			}else{
				key = JBDM.trim().substring(0, 9);
			}
			
			if(!flag || DrgRule2.checkRule2(key)){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("102"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule10数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
