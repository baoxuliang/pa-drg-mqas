package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 总费用校验
 * 总费用必须大于0
 * @author jindoulixing
 */
public class CheckRule22 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule22.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double IN_ZFY = drgBAData.getZFY();
			
			if(UtilObject.isNullOrEmpty(IN_ZFY) || IN_ZFY <= 0){
				flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("122"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule22数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
