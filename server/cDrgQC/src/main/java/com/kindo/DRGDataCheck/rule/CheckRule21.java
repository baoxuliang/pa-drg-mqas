package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 西药费用校验
 * 西药费≥抗菌药物费用
 * @author jindoulixing
 */
public class CheckRule21 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule21.class);

    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_XYF = drgBAData.getXYF();
			Double in_KJYWF = drgBAData.getKJYWF();
			
			if((UtilObject.isNullOrEmpty(in_XYF)?0:in_XYF) < (UtilObject.isNullOrEmpty(in_KJYWF)?0:in_KJYWF)){
				flag = false;
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("121"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule21数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
