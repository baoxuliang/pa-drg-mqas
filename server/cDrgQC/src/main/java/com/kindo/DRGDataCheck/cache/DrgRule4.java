package com.kindo.DRGDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;

@Component
public class DrgRule4 {
    private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule4.class);

    private static Set<String> drgRule4 = new HashSet<String>();
    
    static {
    	if(drgRule4==null || drgRule4.size()==0){//
    		refreshCache();
    	}
    }

    private static void refreshCache() {
	    List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule4();
    	
		for (Rule item : list) {
			drgRule4.add(item.getZDBM());
		}
    	    
    }
    
    private static void cleanupCache() {
    	drgRule4.clear();
    }
    
    public static void refreshCacheRule4() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static boolean checkRule4(String key) {
    	if(drgRule4 ==null || drgRule4.size() == 0){
    		refreshCache();
    	}
    	
		return drgRule4.contains(key);
	}

}

