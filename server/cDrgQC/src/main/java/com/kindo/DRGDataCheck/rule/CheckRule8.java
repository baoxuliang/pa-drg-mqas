package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.utils.UtilObject;
/**
 * 新生儿体重校验
 * 年龄不足1周岁(月)<=28天的新生儿必填
 * @author jindoulixing
 */
public class CheckRule8 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule8.class);
	
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_bzyzsnl = drgBAData.getBZYZSNL();
			
			if(!UtilObject.isNullOrEmpty(in_bzyzsnl) && in_bzyzsnl > 0){
				if((in_bzyzsnl*30) <= 28){
					String XSECSTZ_1 = drgBAData.getXSECSTZ_1();
					String XSECSTZ_2 = drgBAData.getXSECSTZ_2();
					String XSECSTZ_3 = drgBAData.getXSECSTZ_3();
					String XSECSTZ_4 = drgBAData.getXSECSTZ_4();
					String XSECSTZ_5 = drgBAData.getXSECSTZ_5();
					String in_xserytz = drgBAData.getXSERYTZ(); 
					if((null == XSECSTZ_1 || XSECSTZ_1.equals("") || XSECSTZ_1.equals("0")) && ((UtilObject.isNullOrEmpty(in_xserytz)) || in_xserytz.equals("0"))){
						flag = false;
					}else{
						String[] stra = new String[] {XSECSTZ_2,XSECSTZ_3,XSECSTZ_4,XSECSTZ_5};
						for(String str : stra){
							if(null != str && ((UtilObject.isNullOrEmpty(in_xserytz)) || in_xserytz.equals("0"))){
								if(str.equals("") || str.equals("0")){
									flag = false;
								}
							}
						}
					}
				}
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("108"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule8数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
