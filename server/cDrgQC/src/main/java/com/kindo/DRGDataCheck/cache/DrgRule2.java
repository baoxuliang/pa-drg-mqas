package com.kindo.DRGDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;

@Component
public class DrgRule2 {
    private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule2.class);

    private static Set<String> drgRule2 = new HashSet<String>();
    
    static {
    	if(drgRule2==null || drgRule2.size()==0){//
    		refreshCache();
    	}
    }

    private static void refreshCache() {
	    List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule2();
    	
		for (Rule item : list) {
			drgRule2.add(item.getZDBM());
		}
    	    
    }
    
    private static void cleanupCache() {
    	drgRule2.clear();
    }
    
    public static void refreshCacheRule2() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static boolean checkRule2(String key) {
    	if(drgRule2 ==null || drgRule2.size() == 0){
    		refreshCache();
    	}
    	
		return drgRule2.contains(key);
	}

}

