package com.kindo.DRGDataCheck.dao;

import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import org.apache.ibatis.annotations.Param;

import com.kindo.DRGDataCheck.model.DRGRuleConfig;
import com.kindo.DRGDataCheck.model.DrgFailLog;
import com.kindo.DRGDataCheck.model.QCErrResult;
import com.kindo.DRGDataCheck.model.Rule;

public interface DRGDataCheckMapper {
	
	List<DRGRuleConfig> queryDrgRuleConfiguration();
	
	List<Rule> queryRule1();

	List<Rule> queryRule2();
	
	List<Rule> queryRule3();
	
	List<Rule> queryRule4();
	
	List<Rule> queryRule5();
	
	List<Rule> queryRule6();
	
	List<Rule> queryRule7();
	
	List<Rule> queryRule8();
	
	List<Rule> queryRule9();
	
	int updateDrgCheckBaData(@Param("id")String id,@Param("DRG_CHECK")String DRG_CHECK);
	
	//List<DrgBAData> selectDrgBAData(@Param("id")String id);
	
	int insert(DrgFailLog record);

    DrgFailLog selectByPrimaryKey(@Param("id") String id, @Param("rule") String rule);

    int updateByPrimaryKey(DrgFailLog record);

    int insertDrgQCErrResult(QCErrResult qCErrResult);

    QCErrResult selectDrgQCErrResult(@Param("ID") String ID, @Param("ERR_CODE") String ERR_CODE);

    int updateDrgQCErrResult(QCErrResult qCErrResult);
    
    int updateBatchDrgCheckBaData(@Param("IDS")String IDS,@Param("DRG_CHECK")String DRG_CHECK);
    
    int deleteBatchQcErrResult(@Param("list")ConcurrentSkipListSet<QCErrResult>  failList);
    
    int insertBatchQCErrResultDatas(@Param("list")List<QCErrResult>  failList);

}