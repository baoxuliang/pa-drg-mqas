package com.kindo.DRGDataCheck.model;

import java.io.Serializable;

public class Rule implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8609584659719572867L;

	private String APP_ID;
	
	private String ZDBM;
	
	private String DATA_TYPE;
	
	private String DATA_FLAG;

	public String getAPP_ID() {
		return APP_ID;
	}

	public void setAPP_ID(String aPP_ID) {
		APP_ID = aPP_ID;
	}

	public String getZDBM() {
		return ZDBM;
	}

	public void setZDBM(String zDBM) {
		ZDBM = zDBM;
	}

	public String getDATA_TYPE() {
		return DATA_TYPE;
	}

	public void setDATA_TYPE(String dATA_TYPE) {
		DATA_TYPE = dATA_TYPE;
	}

	public String getDATA_FLAG() {
		return DATA_FLAG;
	}

	public void setDATA_FLAG(String dATA_FLAG) {
		DATA_FLAG = dATA_FLAG;
	}
	
}
