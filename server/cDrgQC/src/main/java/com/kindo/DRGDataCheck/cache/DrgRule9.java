package com.kindo.DRGDataCheck.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.Rule;
import com.kindo.DRGDataCheck.utils.UtilObject;

@Component
public class DrgRule9 {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgRule9.class);

	private static Set<String> drgRule9 = new HashSet<String>();

	
	static {
		if (UtilObject.isNullOrEmpty(drgRule9)){ 
			refreshCache(); 
		} 
	}
	 

	private static void refreshCache() {
		List<Rule> list = DRGDataCheck.getDRGDataCheckMapper().queryRule9();

		for (Rule item : list) {
			drgRule9.add(item.getZDBM());
		}
	}

	private static void cleanupCache() {
		drgRule9.clear();
	}

	public static void refreshCacheRule9() {
    	cleanupCache();
    	refreshCache();
    }
	
	public static boolean checkRule11(List<String> list) {
		if (UtilObject.isNullOrEmpty(drgRule9)){ 
			refreshCache(); 
		} 
		// 创建集合
		List<String> realA = new ArrayList<String>(drgRule9);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		
		return realA.size() > 0;
	}
}
