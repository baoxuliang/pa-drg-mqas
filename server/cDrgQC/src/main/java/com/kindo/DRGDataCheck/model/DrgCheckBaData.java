package com.kindo.DRGDataCheck.model;

import java.util.Date;

import lombok.Data;

@Data
public class DrgCheckBaData {

	private static final long serialVersionUID = -1429544213057969761L;
	
	private String UID;
	private String ID;
	private String MEMBER_CODE;
	//private String MEMBER_NAME;
	private String BAH;
	private Integer ZYCS;
	private Integer NL;
	private Date RYSJ;
	private Date CSRQ;
	private Date CYSJ;
	private Double SJZYTS;
	private String XB;
	private Double BZYZSNL;
	private String LYFS;
	private Integer QJCS;
	private Integer CGCS;
	//private String XSECSTZ;
	private String XSECSTZ_1;
	private String XSECSTZ_2;
	private String XSECSTZ_3;
	private String XSECSTZ_4;
	private String XSECSTZ_5;
	
	private String XSERYTZ;
	private String JBDM;
	private String JBDM1;
	private String JBDM2;
	private String JBDM3;
	private String JBDM4;
	private String JBDM5;
	private String JBDM6;
	private String JBDM7;
	private String JBDM8;
	private String JBDM9;
	private String JBDM10;
	private String JBDM11;
	private String JBDM12;
	private String JBDM13;
	private String JBDM14;
	private String JBDM15;
	private String SSJCZBM1;
	private String SSJCZBM2;
	private String SSJCZBM3;
	private String SSJCZBM4;
	private String SSJCZBM5;
	private String SSJCZBM6;
	private String SSJCZBM7;
	private String SSJCZBM8;
	private Double ZFY;
	private Double ZFJE;
	private Double YLFUF;
	private Double ZLCZF;
	private Double HLF;
	private Double QTFY;
	private Double BLZDF;
	private Double SYSZDF;
	private Double YXXZDF;
	private Double LCZDXMF;
	private Double FSSZLXMF;
	private Double WLZLF;
	private Double SSZLF;
	private Double MAF;
	private Double SSF;
	private Double KFF;
	private Double ZYZLF;
	private Double XYF;
	private Double KJYWF;
	private Double ZCYF;
	private Double ZCYF1;
	private Double XF;
	private Double BDBLZPF;
	private Double QDBLZPF;
	private Double NXYZLZPF;
	private Double XBYZLZPF;
	private Double HCYYCLF;
	private Double YYCLF;
	private Double YCXYYCLF;
	private Double QTF;
	//private Double NXYZLZPF2;
	private Double PTCWF;
	private Double ZZJHCWF;
	
}
