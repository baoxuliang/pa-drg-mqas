package com.kindo.DRGDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
/**
 * 分项费用校验
 * 分项费用非空时，分项费用≥0
 * PTCWF 去掉
 * @author jindoulixing
 */
public class CheckRule16 extends AbstractCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckRule16.class);
    @Override  
    public void run() { 
    	boolean flag = true;
    	try {
			Double in_YLFUF = drgBAData.getYLFUF();
			Double in_ZLCZF = drgBAData.getZLCZF();
			Double in_HLF = drgBAData.getHLF();
			Double in_QTFY = drgBAData.getQTFY();
			Double in_BLZDF = drgBAData.getBLZDF();
			Double in_SYSZDF = drgBAData.getSYSZDF();
			Double in_YXXZDF = drgBAData.getYXXZDF();
			Double in_FSSZLXMF = drgBAData.getFSSZLXMF();
			Double in_SSZLF = drgBAData.getSSZLF();
			Double in_KFF = drgBAData.getKFF();
			Double in_ZYZLF = drgBAData.getZYZLF();
			Double in_XYF = drgBAData.getXYF();
			Double in_ZCYF = drgBAData.getZCYF();
			Double in_ZCYF1 = drgBAData.getZCYF1();
			Double in_XF = drgBAData.getXF();
			Double in_BDBLZPF = drgBAData.getBDBLZPF();
			Double in_QDBLZPF = drgBAData.getQDBLZPF();
			Double in_NXYZLZPF = drgBAData.getNXYZLZPF();
			Double in_XBYZLZPF = drgBAData.getXBYZLZPF();
			Double in_HCYYCLF = drgBAData.getHCYYCLF();
			Double in_YYCLF = drgBAData.getYYCLF();
			Double in_YCXYYCLF = drgBAData.getYCXYYCLF();
			Double in_QTF = drgBAData.getQTF();
			Double in_ZFJE = drgBAData.getZFJE();
			//Double in_PTCWF = drgBAData.getPTCWF();
			Double in_ZZJHCWF = drgBAData.getZZJHCWF();
			Double in_SSF = drgBAData.getSSF();
			Double in_MAF = drgBAData.getMAF();
			Double in_KJYWF = drgBAData.getKJYWF();
			Double in_WLZLF = drgBAData.getWLZLF();
			
			if (in_ZFJE < 0 || in_YLFUF < 0 || in_ZLCZF < 0 || in_HLF < 0 || /*in_PTCWF < 0 ||*/ in_ZZJHCWF < 0 || in_QTFY < 0 || in_BLZDF < 0 || in_SYSZDF < 0  ){
				flag = false;
			} else {
				if((in_YXXZDF < 0 || in_FSSZLXMF < 0 || in_SSZLF < 0 || in_KFF < 0 || in_ZYZLF < 0 || in_XYF < 0 || in_ZCYF < 0 || in_ZCYF1 < 0 || in_XF < 0)){
					flag = false;
				}
				if(in_BDBLZPF < 0 || in_QDBLZPF < 0 || in_NXYZLZPF < 0 || in_XBYZLZPF < 0 || in_HCYYCLF < 0 || in_YYCLF < 0 || in_YCXYYCLF < 0 || in_QTF < 0){
					flag = false;
				}
				if(in_MAF < 0 || in_SSF < 0 || in_KJYWF < 0 || in_WLZLF < 0){
					flag = false;
				}
			}
			
			if(!flag){//记录校验失败信息
				countfail.addAndGet(1);
				DRGDataCheck.insertQCErrResult(drgBAData, DrgRuleConfiguration.getInstance().getDRGConfigCache("116"));
			}
		} catch (Exception e) {
			LOGGER.error("CheckRule16数据校验异常,数据:"+drgBAData.toString()+"异常信息"+e.getMessage());
		}    	
    	
    	latch.countDown();
    }  
      
}  
