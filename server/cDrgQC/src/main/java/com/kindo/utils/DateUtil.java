package com.kindo.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public static int getAge(Date birthDay,Date localDate) throws Exception {
        int age = 0;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(localDate);
			if (cal.before(birthDay)) {
			    throw new IllegalArgumentException(
			            "The birthDay is before Now.It's unbelievable!");
			}
			int yearNow = cal.get(Calendar.YEAR);
			int monthNow = cal.get(Calendar.MONTH);
			int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
			cal.setTime(birthDay);
 
			int yearBirth = cal.get(Calendar.YEAR);
			int monthBirth = cal.get(Calendar.MONTH);
			int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
 
			age = yearNow - yearBirth;
 
			if (monthNow <= monthBirth) {
			    if (monthNow == monthBirth) {
			        if (dayOfMonthNow < dayOfMonthBirth) age--;
			    }else{
			        age--;
			    }
			}
		} catch (Exception e) {
			return age;
		}
        return age;
    }
}
