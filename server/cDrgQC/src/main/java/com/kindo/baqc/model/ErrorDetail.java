package com.kindo.baqc.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ErrorDetail {

    private String id;
    private String memberCode;
    private String bah;
    private Integer zycs;
    private String errCode;
    private String errName;
    private String errMemo;
    private String errType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date checkTime;
}
