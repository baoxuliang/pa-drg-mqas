package com.kindo.baqc.model;

import java.util.Date;

import lombok.Data;

@Data
public class BaDropDownQo {
    private String year;
    private String quarter;
    private String month;
    private String memberCode;
    private String deptCode;
    private String city;
    private String cnty;
    private String memberTypeCode;
    private String memberLevelCode1;
    private String memberLevelCode2;
}
