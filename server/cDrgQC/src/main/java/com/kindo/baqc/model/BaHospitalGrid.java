package com.kindo.baqc.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class BaHospitalGrid {

	@PoiExcelField(index = 0, title = "医院名称")
    private String memberName;
	@PoiExcelField(index = 1, title = "总病案数")
    private Integer sumNum;
	@PoiExcelField(index = 2, title = "问题病案总数")
    private Integer errorNum;
	@PoiExcelField(index = 3, title = "HQMS校验未通过")
    private Integer hqmsNum;
	@PoiExcelField(index = 4, title = "DRG校验未通过")
	private Integer drgNum;
	@PoiExcelField(index = 5, title = "入组病案数")
    private Integer inBa;
	@PoiExcelField(index = 6, title = "未入组")
    private Integer notInBa;
    private String memberCode;
    
}
