package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaqcErrResult {

    private String id;
    private String uid;
    private String memberCode;
    private String bah;
    private Integer zycs;
    private String errCode;
    private String errName;
    private String errMemo;
    private String errType;
}
