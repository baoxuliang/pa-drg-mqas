package com.kindo.baqc.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class BaAreaErrorQo {

	private String city;
    private String cnty;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bgTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    private String memberTypeCode;
    private String memberLevelCode1;
    private String memberLevelCode2;
    private String memberCode;
    private String lyfs;
    private String errType;
    private String errCode;
    private String deptCode;
    private String year;
    private String quarter;
    private String month;
    private UserLoginInfo user;
}
