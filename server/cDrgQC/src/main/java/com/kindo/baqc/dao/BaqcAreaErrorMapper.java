package com.kindo.baqc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaAreaErrorGrid;
import com.kindo.baqc.model.BaAreaErrorQo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.DrgGroupResult;

public interface BaqcAreaErrorMapper {

    List<BaAreaErrorGrid> queryListpage(@Param("param") BaAreaErrorQo param, @Param("page") Pageable page);

    BaInfoSmall queryDetail(String id);
    
    DrgGroupResult getDrgGroupResult(String id);
    
    List<BaqcErrResult> getErrorResult(String id);
    
    List<Map<String,Object>> getHqmsCode();
    
    List<Map<String,Object>> getDrgCode();
}
