package com.kindo.baqc.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.baqc.model.BaAreaErrorQo;
import com.kindo.baqc.model.BaColligationGrid;
import com.kindo.baqc.model.BaColligationQo;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.service.BaqcColligationService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/baqc/colligation")
public class BaqcColligationApi extends BaseApi {
	@Autowired
	private BaqcColligationService service;
    
	 @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(BaColligationQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    	vo.setUser(info);
    };

    /** 
	 * queryListpage:综合病案表格查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/queryListpage", method = RequestMethod.GET)
    public ApiResult queryListpage(BaColligationQo qo, Pageable page, HttpServletRequest request) throws Exception {
    	if(page.getSorts().size()==0||(page.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" memberLevelCode1 ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" , memberLevelCode2 ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		SortPair sp3= new SortPair();
    		sp3.setField(" , memberName ");
    		sp3.setAsc(true);
    		sorts.add(sp3);
    		SortPair sp4= new SortPair();
    		sp4.setField(" , CYSJ ");
    		sp4.setAsc(false);
    		sorts.add(sp4);
    		page.setSorts(sorts);
    	}
    	service.getTheRegion(qo);
    	List<BaColligationGrid> list = service.queryListpage(qo,page);
    	RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }
    
    /** 
	 * queryDetail:查看详情. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param id
	 * @return 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/queryDetail", method = RequestMethod.GET)
    public ApiResult queryDetail(String id) {
    	BaInfoSmall baInfo = service.queryDetail(id);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, baInfo);
    }
    
    /** 
	 * tableExport:问题病案表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void tableExport(HttpServletResponse response,BaColligationQo qo) throws Exception {
    	service.getTheRegion(qo);
    	service.exportDrgBas(response,qo);
    }
    
}
