package com.kindo.baqc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaErrorInfo;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoQo;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.ErrorDetail;
import com.kindo.baqc.model.ErrorDetailQo;

public interface BaqcErrorMapper {

    List<BaErrorInfo> listPageBaError(@Param("param") BaInfoQo param, @Param("page") Pageable page);

    BaInfo getBaError(String id);

    List<BaqcErrResult> getErrorResult(String id);

    List<ErrorDetail> listPageErrorDetail(@Param("param") ErrorDetailQo param, @Param("page") Pageable page);
}
