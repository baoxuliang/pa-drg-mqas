package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaErrorSummary {

    private Integer baCount;
    private Integer errCount;
    private String errPercent;
    private Integer typeCount;
}
