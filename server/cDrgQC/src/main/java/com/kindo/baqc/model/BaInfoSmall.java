package com.kindo.baqc.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class BaInfoSmall implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3421990660760935140L;

    private String xm;
    private String id;
    private String hosId;
    private String username;
    private Integer zycs;
    private String ylfkfs;
    private String  bah;
    private String  xb;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date csrq;
    private Integer  nl;
    private Double bzyzsnl;
    private Integer xsecstz;
	private Integer xserytz;
	private String rytj;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date rysj;
	private Integer rysjs;
	private String rykb;
	private String  rykbbm;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date zksj1;
	private Integer  zksjs1;
	private String  zkkb;
	private String  zkkbbm;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  zksj2;
	private Integer  zksjs2;
	private String  zkkb2;
	private String  zkkbbm2;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  zksj3;
	private Integer  zksjs3;
	private String  zkkb3;
	private String  zkkbbm3;
	private Double  hxjsysj;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  cysj;
	private String  cykb;
	private Double  cysjs;
	private String  cykbbm;
	private String  lyfs;
	private Integer  sjzyts;
	private Double  sjhl;
	private Double  ejhl;
	private Double  yjhl;
	private Double  tjhl;
	private Integer  tjhls;
	private Double  zfy;
	private Double  zfje;
	private Double  ylfuf;
	private Double  zlczf;
	private Double  hlf;
	private Double  zcf;
	private Double  ptcwf;
	
	private Double  zzjhcwf;
	private Double  qtfy;
	private Double  blzdf;
	private Double  syszdf;
	private Double  yxxzdf;
	private Double  lczdxmf;
	private Double  fsszlxmf;
	private Double  sszlf;
	private Double  kff;
	private Double  zyzlf;
	private Double  xyf;
	private Double  zcyf;
	private Double  zcyf1;
	private Double  xf;
	private Double  bdblzpf;
	private Double  qdblzpf;
	
	private Double  nxyzlzpf;
	private Double  xbyzlzpf;
	private Double  hcyyclf;
	private Double  yyclf;
	private Double  ycxyyclf;
	private Double  qtf;
	private Double  mzf;
	private Double  ssf;
	private Double  kjywf;
	private Double  wlzlf;
	private Integer  qjcs;
	private Integer  qjcgcs;
	private String  mjzsymc;
	private String  mjzsybm;
	private String  mjzsybw;
	private String  mjzsysx;
	
	private String  mjzzdms;
	private String  symc;
	private String  sybm;
	private String  sybw;
	private String  sysx;
	private String  zdms;
	private String  symc1;
	private String  sybm1;
	private String  sybw1;
	private String  sysx1;
	private String  zdms1;
	private Integer  sfzy1;
	private String  symc2;
	private String  sybm2;
	private String  sybw2;
	private String  sysx2;
	
	private String  zdms2;
	private Integer  sfzy2;
	private String  symc3;
	private String  sybm3;
	private String  sybw3;
	private String  sysx3;
	private String  zdms3;
	private Integer  sfzy3;
	private String  symc4;
	private String  sybm4;
	private String  sybw4;
	private String  sysx4;
	private String  zdms4;
	private Integer  sfzy4;
	private String  symc5;
	private String  sybm5;
	
	private String  sybw5;
	private String  sysx5;
	private String  zdms5;
	private Integer  sfzy5;
	private String  symc6;
	private String  sybm6;
	private String  sybw6;
	private String  sysx6;
	private String  zdms6;
	private Integer  sfzy6;
	private String  symc7;
	private String  sybm7;
	private String  sybw7;
	private String  sysx7;
	private String  zdms7;
	private String  sfzy7;
	
	private String  symc8;
	private String  sybm8;
	private String  sybw8;
	private String  sysx8;
	private String  zdms8;
	private Integer  sfzy8;
	private String  symc9;
	private String  sybm9;
	private String  sybw9;
	private String  sysx9;
	private String  zdms9;
	private Integer  sfzy9;
	private String  symc10;
	private String  syb10;
	private String  sybw10;
	private String  sysx10;
	
	private String  zdms10;
	private Integer  sfzy10;
	private String  symc11;
	private String  sybm11;
	private String  sybw11;
	private String  sysx11;
	private String  zdms11;
	private Integer  sfzy11;
	private String  symc12;
	private String  sybm12;
	private String  sybw12;
	private String  sysx12;
	private String  zdms12;
	private Integer  sfzy12;
	
	private String  symc13;
	private String  sybm13;
	private String  sybw13;
	private String  sysx13;
	private String  zdms13;
	private Integer  sfzy13;
	private String  symc14;
	private String  sybm14;
	private String  sybw14;
	private String  sysx14;
	private String  zdms14;
	private Integer  sfzy14;
	private String  symc15;
	private String  sybm15;
	
	private String  sybw15;
	private String  sysx15;
	private String  zdms15;
	private Integer  sfzy15;
	private String  cchibm1;
	private String  cchimc1;
	private String  cchixsf1;
	private Integer  sfzyss1;
	private String  cchibm2;
	private String  cchimc2;
	private String  cchixsf2;
	private Integer  sfzyss2;
	private String  cchibm3;
	private String  cchimc3;
	
	private String  cchixsf3;
	private Integer  sfzyss3;
	private String  cchibm4;
	private String  cchimc4;
	private String  cchixsf4;
	private Integer  sfzyss4;
	private String  cchibm5;
	private String  cchimc5;
	private String  cchixsf5;
	private Integer  sfzyss5;
	private String  cchibm6;
	private String  cchimc6;
	private String  cchisf6;
	private Integer  sfzyss6;
	
	private String  cchibm7;
	private String  cchimc7;
	private String  cchixsf7;
	private Integer  sfzyss7;
	private String  cchibm8;
	private String  cchimc8;
	private String  cchixsf8;
	private Integer  sfzyss8;
	private Integer  rzzjhs1;
	private Integer  rzzjhslx1;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  rzzjhsj1;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  czzjhsj1;
	private Integer  rzzjhs2;
	private Integer  rzzjhslx2;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  rzzjhsj2;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  czzjhsj2;
	private Integer  rzzjhs3;
	private Integer  rzzjhslx3;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  rzzjhsj3;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  czzjhsj3;
	
	private String  cblxCode;
	private String  yblsh;
	private Double  cbcwf;
	private Double  cwhcf;
	private String  hqmsCheck;
	private String  drgCheck;
	private String  drgState;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date  syndate;
	
	private String drgName ;
	private String drgCode;
	private List<String> errs;
	
	private Integer rycs;
	private String csd ;
	private String jg;
	private String mz ;
	private String gj;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ryqzrq ;
	private String ryzdbm;
	private String ryzdmc ;
	private Integer hxjsysjf;
	
	/** 
	 * if_department_info_V 
	 **/
	private String qtzd;
	private String zyzd;
	private String jbdm;
	private String rybq;
	private String qtzd1;
	private String jbdm1;
	private String rybq1;
	private String qtzd2;
	private String jbdm2;
	private String rybq2;
	private String qtzd3;
	private String jbdm3;
	private String rybq3;
	private String qtzd4;
	private String jbdm4;
	private String rybq4;
	private String qtzd5;
	private String jbdm5;
	private String rybq5;
	private String qtzd6;
	private String jbdm6;
	private String rybq6;
	private String qtzd7;
	private String jbdm7;
	private String rybq7;
	private String qtzd8;
	private String jbdm8;
	private String rybq8;
	private String qtzd9;
	private String jbdm9;
	private String rybq9;
	private String qtzd10;
	private String jbdm10;
	private String rybq10;
	private String qtzd11;
	private String jbdm11;
	private String rybq11;
	private String qtzd12;
	private String jbdm12;
	private String rybq12;
	private String qtzd13;
	private String jbdm13;
	private String rybq13;
	private String qtzd14;
	private String jbdm14;
	private String rybq14;
	private String qtzd15;
	private String jbdm15;
	private String rybq15;
	private String tid;
	
	private String ssjczbm1;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq1;
	private String ssjczmc1;
	private String ssjb1;
	private String sz1;
	private String yz1;
	private String ez1;
	private String qkyhdj1;
	private String mzfs1;
	private String mzys1;
	
	private String ssjczbm2;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq2;
	private String ssjczmc2;
	private String ssjb2;
	private String sz2;
	private String yz2;
	private String ez2;
	private String qkyhdj2;
	private String mzfs2;
	private String mzys2;
	
	private String ssjczbm3;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq3;
	private String ssjczmc3;
	private String ssjb3;
	private String sz3;
	private String yz3;
	private String ez3;
	private String qkyhdj3;
	private String mzfs3;
	private String mzys3;
	
	private String ssjczbm4;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq4;
	private String ssjczmc4;
	private String ssjb4;
	private String sz4;
	private String yz4;
	private String ez4;
	private String qkyhdj4;
	private String mzfs4;
	private String mzys4;
	
	private String ssjczbm5;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq5;
	private String ssjczmc5;
	private String ssjb5;
	private String sz5;
	private String yz5;
	private String ez5;
	private String qkyhdj5;
	private String mzfs5;
	private String mzys5;
	
	private String ssjczbm6;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq6;
	private String ssjczmc6;
	private String ssjb6;
	private String sz6;
	private String yz6;
	private String ez6;
	private String qkyhdj6;
	private String mzfs6;
	private String mzys6;
	
	private String ssjczbm7;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq7;
	private String ssjczmc7;
	private String ssjb7;
	private String sz7;
	private String yz7;
	private String ez7;
	private String qkyhdj7;
	private String mzfs7;
	private String mzys7;
	
	private String ssjczbm8;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date ssjczrq8;
	private String ssjczmc8;
	private String ssjb8;
	private String sz8;
	private String yz8;
	private String ez8;
	private String qkyhdj8;
	private String mzfs8;
	private String mzys8;
	private String bastate;
}
