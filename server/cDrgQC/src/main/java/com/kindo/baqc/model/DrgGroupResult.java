package com.kindo.baqc.model;

import lombok.Data;

@Data
public class DrgGroupResult {

    private String id;
    private String memberCode;
    private String bah;
    private Integer zycs;
    private String drgCode;
    private String drgName;
    private String aDrgCode;
    private String aDrgName;
    private String mdcCode;
    private String mdcName;
    private String rwt;
}

