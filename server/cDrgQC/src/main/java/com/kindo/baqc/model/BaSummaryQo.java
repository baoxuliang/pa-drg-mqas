package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaSummaryQo {

    private Integer year;
    private String errCode;
    private String memberCode;
    private String order;
}
