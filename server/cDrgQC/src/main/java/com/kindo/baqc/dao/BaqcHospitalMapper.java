package com.kindo.baqc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaDropDownQo;
import com.kindo.baqc.model.BaHospitalGrid;
import com.kindo.baqc.model.BaHospitalQo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.DrgGroupResult;

public interface BaqcHospitalMapper {

    List<BaHospitalGrid> queryListpage(@Param("param") BaHospitalQo param, @Param("page") Pageable page);
    
    List<BaHospitalGrid> queryList(@Param("param") BaHospitalQo param);

    BaInfoSmall queryDetail(String id);
    
    Map<String,Object> querySum(@Param("param") BaHospitalQo qo);
    
    List<Map<String,Object>> dropdownStatistics(@Param("param") BaHospitalQo qo);
}
