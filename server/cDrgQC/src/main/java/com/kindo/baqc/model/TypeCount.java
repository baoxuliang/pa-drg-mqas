package com.kindo.baqc.model;

import lombok.Data;

@Data
public class TypeCount {

    private String type;
    private Integer count;
    private String code;
}
