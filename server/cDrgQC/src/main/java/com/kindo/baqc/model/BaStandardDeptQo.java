package com.kindo.baqc.model;

import java.util.Date;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class BaStandardDeptQo {

	private String city;
    private String cnty;
    private Integer year;
    private Integer quarter;
    private Integer month;
    private String memberTypeCode;
    private String memberLevelCode1;
    private String memberLevelCode2;
    private String deptCode;
    private String memberCode;
    private UserLoginInfo user;
}
