package com.kindo.baqc.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.baqc.dao.BaqcNormalMapper;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoQo;
import com.kindo.baqc.model.BaNormalInfo;
import com.kindo.baqc.model.BaqcDrgGroupResult;

@Service
public class BaqcNormalService {

    @Autowired
    private BaqcNormalMapper mapper;

    public List<BaNormalInfo> getBaNormalList(BaInfoQo qo, Pageable page) {
        return mapper.listPageBaNormal(qo, page);
    }

    public void exportNormalBas(HttpServletResponse response, BaInfoQo qo) {
        String fileName = "合格病案" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("application/ms-excel");
            response.setHeader("Content-disposition",
                    "attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");

            PageableSupplier<BaNormalInfo, BaInfoQo> supplier = new PageableSupplier<BaNormalInfo, BaInfoQo>();
            supplier.setFunc(this::getBaNormalList);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(BaNormalInfo.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }

    public BaInfo getBaNorma(String id) {
        BaInfo ba = mapper.getBaNormal(id);
        if (ba != null) {
            BaqcDrgGroupResult result = mapper.getDrgGroupResult(id);
            if (result != null) {
                ba.setDrgCode(result.getDrgCode());
                ba.setDrgName(result.getDrgName());
            }
            return ba;
        }
        return null;
    }
}
