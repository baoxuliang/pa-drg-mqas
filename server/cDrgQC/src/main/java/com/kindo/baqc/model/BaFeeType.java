package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaFeeType {

    private Double ypfy;
    private Double zhfy;
    private Double zdfy;
    private Double zlfy;
    private Double xyfy;
    private Double clfy;
    private Double qtfy;
    private Double zfy;
}
