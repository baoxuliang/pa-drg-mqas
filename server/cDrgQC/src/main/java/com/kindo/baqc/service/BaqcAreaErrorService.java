package com.kindo.baqc.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.baqc.dao.BaqcAreaErrorMapper;
import com.kindo.baqc.model.BaAreaErrorGrid;
import com.kindo.baqc.model.BaAreaErrorQo;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class BaqcAreaErrorService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BaqcAreaErrorService.class);
	 /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(BaAreaErrorQo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		LOGGER.info("BaqcAreaErrorService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("BaqcAreaErrorService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("BaqcAreaErrorService.getTheRegion,用户为管理员级别!不限区域!");
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("BaqcAreaErrorService.getTheRegion,用户为省级别!");
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("BaqcAreaErrorService.getTheRegion,用户为市级别!");
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("BaqcAreaErrorService.getTheRegion,用户为县级别!");
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("BaqcAreaErrorService.getTheRegion,用户为医院用户级别!");
			region = user.getOrgaId();
			vo.setMemberCode(region);
		}
		LOGGER.info("BaqcAreaErrorService.getTheRegion,用户region:{}",region);
	};

    @Autowired
    private BaqcAreaErrorMapper mapper;
    
    /** 
	 * queryListpage:问题病案表格查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @return 
	 * @since JDK 1.8 
	 **/
    public List<BaAreaErrorGrid> queryListpage(BaAreaErrorQo qo, Pageable page) {
        List<BaAreaErrorGrid> list = mapper.queryListpage(qo,page);
        if(list!=null){
	        list.forEach(item -> {
	            if (item.getErrType() != null && item.getErrMemo() != null) {
	
	                List<String> errTypes = Arrays.asList(item.getErrType().split("#"));
	                List<String> errNames = Arrays.asList(item.getErrMemo().split("#"));
	
	                int size = errTypes.size();
	                if (size != errNames.size()) {
	                    return;
	                }
	
	                for (int i = 0; i < size; i++) {
	                    errNames.set(i, new StringBuffer().append('【').append(getErrTypeName(errTypes.get(i))).append('】')
	                            .append(errNames.get(i)).toString());
	                }
	                item.setErrMemo(String.join(";", errNames));
	                item.setErrType(errTypes.stream().distinct().map(BaqcAreaErrorService::getErrTypeName)
	                        .collect(Collectors.joining(";")));
	            }
	        });
	        
	        list.forEach(item->{
	        	item.setLyfs(getDicValue(item.getLyfs()));
	        });
        }
        return list;
    }
    
    private String getDicValue(String key){
   	 DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("CC06_00_223_00");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) {
       	 if(entry.getKey().equals(key)){
       		 return entry.getValue();
       	 }
        }
        return "";
   }
    
    public static String getErrTypeName(String errType) {
        return "2".equals(errType) ? "HQMS" : "3".equals(errType) ? "DRG" : "";
    }
    
    /** 
   	 * exportErrsBas:问题病案表格导出. <br/>
   	 * @author whk00216 
   	 * @date 2018年4月11日 下午9:21:13 *
   	 * @param qo
   	 * @param response
   	 * @return 
   	 * @since JDK 1.8 
   	 **/
    public void exportErrsBas(HttpServletResponse response,BaAreaErrorQo qo) {
    	String fileName = "问题病案" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
        	response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<BaAreaErrorGrid, BaAreaErrorQo> supplier = new PageableSupplier<BaAreaErrorGrid, BaAreaErrorQo>();
            supplier.setFunc(this::queryListpage);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(BaAreaErrorGrid.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
    
    /** 
	 * queryErrsCode:错误名称和编码查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @return 
	 * @since JDK 1.8 
	 **/
    public Map<String,Object> queryErrsCode(){
    	Map<String,Object> result = new HashMap<>();
    	List<Map<String,Object>> drgCode = mapper.getDrgCode();
    	List<Map<String,Object>> hqmsCode = mapper.getHqmsCode();
    	result.put("hqms", hqmsCode);
    	result.put("drg", drgCode);
    	return result;
    }
}
