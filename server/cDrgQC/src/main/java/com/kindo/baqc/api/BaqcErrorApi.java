package com.kindo.baqc.api;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaErrorInfo;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoQo;
import com.kindo.baqc.model.ErrorDetail;
import com.kindo.baqc.model.ErrorDetailQo;
import com.kindo.baqc.service.BaqcErrorService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;

@RestController
@RequestMapping("/baqc/error")
public class BaqcErrorApi extends BaseApi {

    @Autowired
    private BaqcErrorService service;

    @Value("${hos.memberCode}")
    private String memberCode;

    @RequestMapping(value = "/bas", method = RequestMethod.GET)
    public ApiResult queryBrrorBas(BaInfoQo qo, Pageable page) {
        qo.setMemberCode(memberCode);
        List<BaErrorInfo> list = service.getBaErrorList(qo, page);

        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());

        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }

    @RequestMapping(value = "/bas/export", method = RequestMethod.GET)
    public void exportErrorBas(HttpServletResponse response, BaInfoQo qo) {
        qo.setMemberCode(memberCode);
        service.exportErrorBas(response, qo);
    }

    @RequestMapping(value = "/bas/{id}", method = RequestMethod.GET)
    public ApiResult getErrorBaById(@PathVariable String id) {
        BaInfo baInfo = service.getBaError(id);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, baInfo);
    }

    /**
     * 校验失败结果查询
     * 
     * @param bah
     * @param errName
     * @param errMemo
     * @param errType
     * @param page
     * @return
     */
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public ApiResult queryErrorDetails(ErrorDetailQo qo, String errType, Pageable page) {
        qo.setMemberCode(memberCode);
        List<ErrorDetail> list = service.getErrorDetailList(qo, page);

        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());

        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }
}
