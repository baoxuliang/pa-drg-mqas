package com.kindo.baqc.dao;

import java.util.HashMap;
import java.util.List;

import com.kindo.baqc.model.BaFeeType;
import com.kindo.baqc.model.KeyValueInt;
import com.kindo.baqc.model.BaNormalAnalyQo;
import com.kindo.baqc.model.BaNormalAnalyStaticSum;
import com.kindo.baqc.model.BaNormalTotalKpi;

public interface BaqcNormalAnalyMapper {

    BaNormalTotalKpi getBaAnalyStatic(BaNormalAnalyQo qo);
    
    Integer getZzyrs(BaNormalAnalyQo qo);

    BaFeeType getBaFeeType(BaNormalAnalyQo qo);

    List<KeyValueInt> getCountByFee(BaNormalAnalyQo qo);

    List<KeyValueInt> getCountBySjzyts(BaNormalAnalyQo qo);
    
    BaNormalAnalyStaticSum getBaAnalySum(BaNormalAnalyQo qo);
}
