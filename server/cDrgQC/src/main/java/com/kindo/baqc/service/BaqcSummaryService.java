package com.kindo.baqc.service;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.dict.util.DictUtils;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.baqc.dao.BaqcSummaryMapper;
import com.kindo.baqc.model.BaErrorSummary;
import com.kindo.baqc.model.BaSummaryCykbMonth;
import com.kindo.baqc.model.BaSummaryQo;
import com.kindo.baqc.model.IndexFieldTitle;
import com.kindo.baqc.model.TypeCount;
import com.kindo.baqc.model.TypeDeptCount;
import com.kindo.baqc.model.TypeOneDeptCount;

@Service
public class BaqcSummaryService {

	// 标题字体
    private static final String FONT_NAME_HEADER = "宋体 Light";
    // 数据字体
    private static final String FONT_NAME_DATA = "宋体";
	
    @Autowired
    private BaqcSummaryMapper mapper;

    public List<BaSummaryCykbMonth> queryDeptSummary(BaSummaryQo qo, Pageable page) {
        return mapper.listPageDeptSummary(qo, page);
    }

    public Date getMaxSynDate(String memberCode) {
        return mapper.getMaxSynDate(memberCode);
    }

    public void exportDeptSummary(HttpServletResponse response, BaSummaryQo qo) {
        String fileName = "问题病例汇总" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("application/ms-excel");
            response.setHeader("Content-disposition",
                    "attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");

            PageableSupplier<BaSummaryCykbMonth, BaSummaryQo> supplier = new PageableSupplier<BaSummaryCykbMonth, BaSummaryQo>();
            supplier.setFunc(this::queryDeptSummary);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(BaSummaryCykbMonth.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }

    public BaErrorSummary baqcStatic(BaSummaryQo qo) {
        BaErrorSummary summary = mapper.getBaqcStatic(qo);
        summary.setErrPercent(summary.getBaCount() == 0 ? ""
                : String.format("%2.2f%%", 100.0 * summary.getErrCount() / summary.getBaCount()));
        summary.setTypeCount(mapper.countBaErrType(qo));
        return summary;
    }

    public List<TypeCount> getBaErrTypeCount(BaSummaryQo qo) {
        return mapper.getBaErrTypeCount(qo);
    }
    
    public List<TypeOneDeptCount> getBaErrTypeOnedeptCount(BaSummaryQo qo) {
        return mapper.getBaErrTypeOnedeptCount(qo);
    }
    
    public Map<String, Object> getBaErrTypeDeptCount(BaSummaryQo qo) {
        List<TypeDeptCount> list = mapper.getBaErrTypeDeptCount(qo);
        List<Map<String, Object>> datas = list.stream().collect(Collectors.groupingBy(TypeDeptCount::getType))
                .entrySet().stream().map(item -> item.getValue().stream().reduce(new HashMap<String, Object>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("count", 0);
                        put("type", item.getKey());
                    }
                }, (data1, val) -> {
                    data1.put(val.getCykbbm(), val.getCount());
                    data1.put("count", (Integer) data1.get("count") + val.getCount());
                    return data1;
                }, (data1, data2) -> {
                    data2.put("count", (Integer) data1.get("count") + (Integer) data2.get("count"));
                    data1.putAll(data2);
                    return data1;
                })).sorted((item1, item2) -> (Integer) item2.get("count") - (Integer) item1.get("count"))
                .collect(Collectors.toList());

        int[] index = { 2 };
        Map<String, String> cyksDict = DictUtils.getDict("DICT_DYN_KESHI");
        List<IndexFieldTitle> header = cyksDict.entrySet().stream()
                .filter(item -> datas.stream().anyMatch(data -> data.get(item.getKey()) != null))
                .map(item -> new IndexFieldTitle(index[0]++, item.getKey(), item.getValue()))
                .reduce(new ArrayList<IndexFieldTitle>() {
                    private static final long serialVersionUID = 1L;
                    {
                        add(new IndexFieldTitle(0, "type", "问题描述"));
                        add(new IndexFieldTitle(1, "count", "问题病案统计"));
                    }
                }, (columns, item) -> {
                    columns.add(item);
                    return columns;
                }, (columns1, columns2) -> {
                    columns1.addAll(columns2);
                    return columns1;
                });
        header.sort(Comparator.comparing(IndexFieldTitle::getIndex));

        datas.forEach(item -> header.forEach(head -> item.putIfAbsent(head.getField(), 0)));

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("header", header);
        map.put("data", datas);
        return map;
    }

    public void exportErrByTypeDept(HttpServletResponse response, BaSummaryQo qo) {

        Map<String, Object> ret = getBaErrTypeDeptCount(qo);

        @SuppressWarnings("unchecked")
        List<IndexFieldTitle> configs = (List<IndexFieldTitle>) ret.get("header");
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> datas = (List<Map<String, Object>>) ret.get("data");

        String fileName = "问题病例质控分析" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("application/ms-excel");
            response.setHeader("Content-disposition",
                    "attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
            OutputStream os = response.getOutputStream();
            // 创建写入工作簿
            Workbook wb = new SXSSFWorkbook(100);

            Font font = wb.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setFontName(FONT_NAME_HEADER);
            font.setBold(true);
            CellStyle style = wb.createCellStyle();
            style.setFont(font);
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            // 创建第一个sheet页
            Sheet sheet1 = wb.createSheet();

            int[] rowIndex = { 0 };
            // 设置标题行
            Row header = sheet1.createRow(rowIndex[0]++);
            Map<String, Object> rooter = new HashMap<String, Object>();
            configs.forEach(config -> {
                Cell cell = header.createCell(config.getIndex());
                cell.setCellValue(config.getTitle());
                cell.setCellStyle(style);
                rooter.put(config.getField(), 0);
            });
            rooter.put("type", "合计");

            Font fontcell = wb.createFont();
            fontcell.setFontHeightInPoints((short) 10);
            fontcell.setFontName(FONT_NAME_DATA);
            CellStyle stylecell = wb.createCellStyle();
            stylecell.setFont(fontcell);
            // 设置数据行
            datas.stream().forEach((data) -> {
                Row row = sheet1.createRow(rowIndex[0]++);
                if (data != null) {
                    configs.forEach(config -> {
                        Object val = data.get(config.getField());
                        Cell cell = row.createCell(config.getIndex());
                        cell.setCellStyle(stylecell);
                        if (val instanceof String) {
                            cell.setCellValue((String) val);
                        } else if (val instanceof Integer) {
                            row.createCell(config.getIndex()).setCellValue((Integer) val);
                            rooter.put(config.getField(), (Integer) rooter.get(config.getField()) + (Integer) val);
                        } else if (val != null) {
                            row.createCell(config.getIndex()).setCellValue(val.toString());
                        }
                    });
                }
            });

            // 合计行
            Row row = sheet1.createRow(rowIndex[0]++);
            configs.forEach(config -> {
                Object val = rooter.get(config.getField());
                Cell cell = row.createCell(config.getIndex());
                if (val instanceof String) {
                    cell.setCellValue((String) val);
                } else if (val instanceof Integer) {
                    row.createCell(config.getIndex()).setCellValue((Integer) val);
                } else if (val != null) {
                    row.createCell(config.getIndex()).setCellValue(val.toString());
                }
            });

            saveToOutput(wb, os);
        } catch (IOException e) {}
    }

    private static boolean saveToOutput(Workbook wb, OutputStream os) {
        try {
            wb.write(os);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeSafe(wb);
            closeSafe(os);
        }
        return true;
    }

    public static void closeSafe(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {}
        }
    }
}
