package com.kindo.baqc.model;

import lombok.Data;

@Data
public class IfOrganizationPart {

    private String originalCode;
    private String originalName;
    private String parentCode;
}
