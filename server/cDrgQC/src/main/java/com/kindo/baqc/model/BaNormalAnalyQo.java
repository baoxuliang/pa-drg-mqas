package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaNormalAnalyQo {

    private Integer year;
    private Integer quarter;
    private Integer month;
    private String cykbbm;
    private String zlzbm;
}
