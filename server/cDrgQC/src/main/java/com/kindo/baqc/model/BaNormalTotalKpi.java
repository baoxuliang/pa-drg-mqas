package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaNormalTotalKpi {

    /**
     * 出院人数
     */
    private Integer cyrs;
    private Double cyrsRate;

    /**
     * 总费用
     */
    private Double zfy;
    private Double zfy10000;
    private Double zfyRate;

    /**
     * 平均住院日
     */
    private Double pjzyr;
    private Double pjzyrRate;

    /**
     * 次均费用
     */
    private Double cjfy;
    private Double cjfyRate;

    /**
     * 死亡率
     */
    private Double swrs;
    private Double swl;
    /**
     * 2周內再入院率
     */
    private Double zzyl;
    /**
     * 医保人次
     */
    private Integer ybrc;
    /**
     * 医保占比
     */
    private Double ybfy;
    private Double ybzb;
}
