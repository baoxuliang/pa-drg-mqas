package com.kindo.baqc.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class BaNormalInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8162280193436708165L;
    private String id;
    @PoiExcelField(index = 0, title = "病案号")
    private String bah;
    @PoiExcelField(index = 1, title = "姓名")
    private String xm;
    @PoiExcelField(index = 2, title = "性别", dict = "GB_T2261_1")
    private String xb;
    @PoiExcelField(index = 3, title = "年龄")
    private Integer nl;
    @PoiExcelField(index = 5, title = "出院日期", format="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date cysj;
    @PoiExcelField(index = 4, title = "住院天数")
    private Integer sjzyts;
    @PoiExcelField(index = 6, title = "主要诊断")
    private String zyzd;
    @PoiExcelField(index = 7, title = "主要操作ICD9")
    private String ssjczmc1;
    @PoiExcelField(index = 8, title = "出院科室")
    private String cykb;
    @PoiExcelField(index = 9, title = "主任医生")
    private String zrys;
    @PoiExcelField(index = 10, title = "主治医生")
    private String zzys;
    @PoiExcelField(index = 11, title = "住院医生")
    private String zyys;
    @PoiExcelField(index = 12, title = "医疗付款方式", dict = "OT_01")
    private String ylfkfs;
    @PoiExcelField(index = 13, title = "总费用")
    private Double zfy;
    @PoiExcelField(index = 14, title = "入组状态", dict = "INDRG_STATE")
    private String drgState;
}
