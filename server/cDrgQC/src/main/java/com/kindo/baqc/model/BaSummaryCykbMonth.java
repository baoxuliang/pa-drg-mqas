package com.kindo.baqc.model;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class BaSummaryCykbMonth {

    @PoiExcelField(index = 0, title = "科室名称")
    private String cykb;
    private String cykbbm;
    @PoiExcelField(index = 1, title = "总计总数")
    private Integer allTotal;
    @PoiExcelField(index = 2, title = "总计通过")
    private Integer passTotal;
    @PoiExcelField(index = 3, title = "总计未通过")
    private Integer failTotal;
    @PoiExcelField(index = 4, title = "1月总数")
    private Integer all1;
    @PoiExcelField(index = 5, title = "1月通过")
    private Integer pass1;
    @PoiExcelField(index = 6, title = "1月未通过")
    private Integer fail1;
    @PoiExcelField(index = 7, title = "2月总数")
    private Integer all2;
    @PoiExcelField(index = 8, title = "2月通过")
    private Integer pass2;
    @PoiExcelField(index = 9, title = "2月未通过")
    private Integer fail2;
    @PoiExcelField(index = 10, title = "3月总数")
    private Integer all3;
    @PoiExcelField(index = 11, title = "3月通过")
    private Integer pass3;
    @PoiExcelField(index = 12, title = "3月未通过")
    private Integer fail3;
    @PoiExcelField(index = 13, title = "4月总数")
    private Integer all4;
    @PoiExcelField(index = 14, title = "4月通过")
    private Integer pass4;
    @PoiExcelField(index = 15, title = "4月未通过")
    private Integer fail4;
    @PoiExcelField(index = 16, title = "5月总数")
    private Integer all5;
    @PoiExcelField(index = 17, title = "5月通过")
    private Integer pass5;
    @PoiExcelField(index = 18, title = "5月未通过")
    private Integer fail5;
    @PoiExcelField(index = 19, title = "6月总数")
    private Integer all6;
    @PoiExcelField(index = 20, title = "6月通过")
    private Integer pass6;
    @PoiExcelField(index = 21, title = "6月未通过")
    private Integer fail6;
    @PoiExcelField(index = 22, title = "7月总数")
    private Integer all7;
    @PoiExcelField(index = 23, title = "7月通过")
    private Integer pass7;
    @PoiExcelField(index = 24, title = "7月未通过")
    private Integer fail7;
    @PoiExcelField(index = 25, title = "8月总数")
    private Integer all8;
    @PoiExcelField(index = 26, title = "8月通过")
    private Integer pass8;
    @PoiExcelField(index = 27, title = "8月未通过")
    private Integer fail8;
    @PoiExcelField(index = 28, title = "9月总数")
    private Integer all9;
    @PoiExcelField(index = 29, title = "9月通过")
    private Integer pass9;
    @PoiExcelField(index = 30, title = "9月未通过")
    private Integer fail9;
    @PoiExcelField(index = 31, title = "10月总数")
    private Integer all10;
    @PoiExcelField(index = 32, title = "10月通过")
    private Integer pass10;
    @PoiExcelField(index = 33, title = "10月未通过")
    private Integer fail10;
    @PoiExcelField(index = 34, title = "11月总数")
    private Integer all11;
    @PoiExcelField(index = 35, title = "11月通过")
    private Integer pass11;
    @PoiExcelField(index = 36, title = "11月未通过")
    private Integer fail11;
    @PoiExcelField(index = 37, title = "12月总数")
    private Integer all12;
    @PoiExcelField(index = 38, title = "12月通过")
    private Integer pass12;
    @PoiExcelField(index = 39, title = "12月未通过")
    private Integer fail12;
}
