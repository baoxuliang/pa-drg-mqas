package com.kindo.baqc.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.baqc.model.BaNormalAnalyQo;
import com.kindo.baqc.model.BaNormalTotalKpi;
import com.kindo.baqc.model.KeyValueStringInt;
import com.kindo.baqc.model.TypeValueRatio;
import com.kindo.baqc.service.BaqcNormalAnalyService;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;

@RestController
@RequestMapping("/baqc/normal/analy")
public class BaqcNormalAnalyApi {

    @Autowired
    private BaqcNormalAnalyService service;

    @Value("${hos.memberCode}")
    private String memberCode;

    @RequestMapping(value = "/static", method = RequestMethod.GET)
    public ApiResult baqcNormalStatic(Integer dateType, Integer year, Integer quarter, Integer month,
            String[] organization) {
        BaNormalAnalyQo qo = initBaNormalAnalyQo(dateType, year, quarter, month, organization);
        if (qo == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }

        BaNormalTotalKpi kpi = service.getBaAnalyStatic(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, kpi);
    }

    @RequestMapping(value = "/type", method = RequestMethod.GET)
    public ApiResult getType(Integer dateType, Integer year, Integer quarter, Integer month, String[] organization) {
        BaNormalAnalyQo qo = initBaNormalAnalyQo(dateType, year, quarter, month, organization);
        if (qo == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }

        List<TypeValueRatio> list = service.getBaFeeType(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }

    @RequestMapping(value = "/fee", method = RequestMethod.GET)
    public ApiResult getFee(Integer dateType, Integer year, Integer quarter, Integer month, String[] organization) {
        BaNormalAnalyQo qo = initBaNormalAnalyQo(dateType, year, quarter, month, organization);
        if (qo == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }

        List<KeyValueStringInt> list = service.getCountByFee(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }

    @RequestMapping(value = "/day", method = RequestMethod.GET)
    public ApiResult getDay(Integer dateType, Integer year, Integer quarter, Integer month, String[] organization) {
        BaNormalAnalyQo qo = initBaNormalAnalyQo(dateType, year, quarter, month, organization);
        if (qo == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }

        List<KeyValueStringInt> list = service.getCountBySjzyts(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }

    private static BaNormalAnalyQo initBaNormalAnalyQo(Integer dateType, Integer year, Integer quarter, Integer month,
            String[] organization) {
        BaNormalAnalyQo qo = new BaNormalAnalyQo();
        if (dateType == null || year == null) {
            return null;
        }
        qo.setYear(year);
        switch (dateType) {
            case 1:
                break;
            case 2:
                qo.setQuarter(quarter);
                break;
            case 3:
                qo.setMonth(month);
                break;
            default:
                break;
        }
        if (organization != null) {
            if (organization.length >= 1) {
                qo.setCykbbm(organization[0]);
                if (organization.length == 2) {
                    qo.setZlzbm(organization[1]);
                }
            }
        }
        return qo;
    }
    
    @RequestMapping(value = "/sumStatic", method = RequestMethod.GET)
    public ApiResult baqcNormalSumStatic(Integer dateType, Integer year, Integer quarter, Integer month,
            String[] organization) {
        BaNormalAnalyQo qo = initBaNormalAnalyQo(dateType, year, quarter, month, organization);
        if (qo == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }

        Map<String, Integer> kpi = service.baqcNormalSumStatic(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, kpi);
    }
    
}
