package com.kindo.baqc.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaErrorSummary;
import com.kindo.baqc.model.BaSummaryCykbMonth;
import com.kindo.baqc.model.BaSummaryQo;
import com.kindo.baqc.model.TypeCount;
import com.kindo.baqc.model.TypeDeptCount;
import com.kindo.baqc.model.TypeOneDeptCount;

public interface BaqcSummaryMapper {

    Date getMaxSynDate(String memberCode);

    List<BaSummaryCykbMonth> listPageDeptSummary(@Param("param") BaSummaryQo param, @Param("page") Pageable page);

    BaErrorSummary getBaqcStatic(@Param("param") BaSummaryQo param);

    Integer countBaErrType(@Param("param") BaSummaryQo param);

    List<TypeCount> getBaErrTypeCount(@Param("param") BaSummaryQo param);
    
    List<TypeOneDeptCount> getBaErrTypeOnedeptCount(@Param("param") BaSummaryQo param);

    List<TypeDeptCount> getBaErrTypeDeptCount(@Param("param") BaSummaryQo param);
}
