package com.kindo.baqc.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class BaInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3421990660760935140L;

    private String id;
    private String memberCode;
    private String memberName;
    private String zyh;
    private Integer zycs;
    private String ylfkfs;
    private String jkkh;
    private String bah;
    private String xm;
    private String xb;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date csrq;
    private Integer nl;
    private String gj;
    private Double bzyzsnl;
    private String xsecstz;
    private String xserytz;
    private String csd;
    private String jg;
    private String mz;
    private String sfzh;
    // private String zy;
    private String hy;
    // private String xzz;
    // private String dh;
    // private String yb1;
    // private String hkdz;
    // private String yb2;
    // private String gzdwjdz;
    // private String dwdh;
    // private String yb3;
    // private String lxrxm;
    // private String gx;
    // private String dz;
    // private String dh2;
    private String rytj;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date rysj;
    private String rykb;
    private String rykbbm;
    // private String rybfmc;
    // private String rybfbm;
    private String zkkb;
    private String zkkbbm1;
    private String zkkbbm2;
    private String zkkbbm3;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date cysj;
    private String cykb;
    private String cykbbm;
    // private String cybfmc;
    // private String cybfbm;
    private Integer sjzyts;
    private String mzzd;
    private String jbbm;
    private String ryzdmc;// add
    private String ryzdbm;// add
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ryqzrq;// add
    private String zyzd;
    private String jbdm;
    private String rybq;
    private String qtzd1;
    private String jbdm1;
    private String rybq1;
    private String qtzd2;
    private String jbdm2;
    private String rybq2;
    private String qtzd3;
    private String jbdm3;
    private String rybq3;
    private String qtzd4;
    private String jbdm4;
    private String rybq4;
    private String qtzd5;
    private String jbdm5;
    private String rybq5;
    private String qtzd6;
    private String jbdm6;
    private String rybq6;
    private String qtzd7;
    private String jbdm7;
    private String rybq7;
    private String qtzd8;
    private String jbdm8;
    private String rybq8;
    private String qtzd9;
    private String jbdm9;
    private String rybq9;
    private String qtzd10;
    private String jbdm10;
    private String rybq10;
    private String qtzd11;
    private String jbdm11;
    private String rybq11;
    private String qtzd12;
    private String jbdm12;
    private String rybq12;
    private String qtzd13;
    private String jbdm13;
    private String rybq13;
    private String qtzd14;
    private String jbdm14;
    private String rybq14;
    private String qtzd15;
    private String jbdm15;
    private String rybq15;
    private String wbyy;
    private String h23;
    // private String blzd;
    // private String jbmm;
    // private String blh;
    // private String ywgm;
    // private String gmyw;
    // private String swhzsj;
    // private String xx;
    // private String rh;
    private String kzr;
    private String kzrCode;
    private String zrys;
    private String zrysCode;
    private String zzys;
    private String zzysCode;
    private String zyys;
    private String zyysCode;
    private String zrhs;
    private String jxys;
    private String sxys;
    private String bmy;
    private String bazl;
    private String zkys;
    private String zkhs;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date zkrq;
    private String ssjczbm1;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq1;
    private String ssjb1;
    private String ssjczmc1;
    private String sz1;
    private String yz1;
    private String ez1;
    private String qkdj1;
    private String qkyhlb1;
    private String mzfs1;
    private String mzys1;
    private String ssjczbm2;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq2;
    private String ssjb2;
    private String ssjczmc2;
    private String sz2;
    private String yz2;
    private String ez2;
    private String qkdj2;
    private String qkyhlb2;
    private String mzfs2;
    private String mzys2;
    private String ssjczbm3;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq3;
    private String ssjb3;
    private String ssjczmc3;
    private String sz3;
    private String yz3;
    private String ez3;
    private String qkdj3;
    private String qkyhlb3;
    private String mzfs3;
    private String mzys3;
    private String ssjczbm4;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq4;
    private String ssjb4;
    private String ssjczmc4;
    private String sz4;
    private String yz4;
    private String ez4;
    private String qkdj4;
    private String qkyhlb4;
    private String mzfs4;
    private String mzys4;
    private String ssjczbm5;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq5;
    private String ssjb5;
    private String ssjczmc5;
    private String sz5;
    private String yz5;
    private String ez5;
    private String qkdj5;
    private String qkyhlb5;
    private String mzfs5;
    private String mzys5;
    private String ssjczbm6;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq6;
    private String ssjb6;
    private String ssjczmc6;
    private String sz6;
    private String yz6;
    private String ez6;
    private String qkdj6;
    private String qkyhlb6;
    private String mzfs6;
    private String mzys6;
    private String ssjczbm7;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq7;
    private String ssjb7;
    private String ssjczmc7;
    private String sz7;
    private String yz7;
    private String ez7;
    private String qkdj7;
    private String qkyhlb7;
    private String mzfs7;
    private String mzys7;
    private String ssjczbm8;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date ssjczrq8;
    private String ssjb8;
    private String ssjczmc8;
    private String sz8;
    private String yz8;
    private String ez8;
    private String qkdj8;
    private String qkyhlb8;
    private String mzfs8;
    private String mzys8;
    private Integer qjcs;// add
    private Integer cgcs;// add
    private String ywfjhzcss;// add
    private String fjhzcssmc;// add
    private String ywyyngr;// add
    private String sfsysslclj;// add
    private String sfwclclj;// add
    private String wwclcljyy;// add
    private String sfby;// add
    private String byyy;// add
    private String sfkzrjss;// add
    private String lyfs;
    private String yzzyYljg;
    // private String wsyYljg;
    private String sfzzyjh;
    private String md;
    // private Integer ryqT;
    // private Integer ryqXs;
    // private Integer ryqF;
    // private Integer ryhT;
    // private Integer ryhXs;
    // private Integer ryhF;
    private Double zfy;
    private Double zfje;
    private Double ylfuf;
    private Double zlczf;
    private Double hlf;
    private Double qtfy;
    private Double blzdf;
    private Double syszdf;
    private Double yxxzdf;
    private Double lczdxmf;
    private Double fsszlxmf;
    private Double wlzlf;
    private Double sszlf;
    private Double maf;
    private Double ssf;
    private Double kff;
    private Double zyzlf;
    private Double xyf;
    private Double kjywf;
    private Double zcyf;
    private Double zcyf1;
    private Double xf;
    private Double bdblzpf;
    private Double qdblzpf;
    private Double nxyzlzpf;
    private Double xbyzlzpf;
    private Double hcyyclf;
    private Double yyclf;
    private Double ycxyyclf;
    private Double qtf;
    private Double nxyzlzpf2;
    private Double ptcwf;
    private Double zzjhcwf;
    // private Double hxjsysj;
    // private String zzjhbflx1;
    // private String jzzjhbfsj1;
    // private String czzjhbfsj1;
    // private String zzjhbflx2;
    // private String jzzjhbfsj2;
    // private String czzjhbfsj2;
    // private String zzjhbflx3;
    // private String jzzjhbfsj3;
    // private String czzjhbfsj3;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date gdrq;

    private String hqmsCheck;
    private String drgCheck;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date syndate;
    private String synflag;

    private List<String> errs;
    private String drgCode;
    private String drgName;
}
