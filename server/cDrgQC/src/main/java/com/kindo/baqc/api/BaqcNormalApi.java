package com.kindo.baqc.api;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoQo;
import com.kindo.baqc.model.BaNormalInfo;
import com.kindo.baqc.service.BaqcNormalService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;

@RestController
@RequestMapping("/baqc/normal")
public class BaqcNormalApi extends BaseApi {

    @Autowired
    private BaqcNormalService service;

    @Value("${hos.memberCode}")
    private String memberCode;

    @RequestMapping(value = "/bas", method = RequestMethod.GET)
    public ApiResult queryNormalBas(BaInfoQo qo, Pageable page) {
        qo.setMemberCode(memberCode);
        List<BaNormalInfo> list = service.getBaNormalList(qo, page);

        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());

        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }

    @RequestMapping(value = "/bas/export", method = RequestMethod.GET)
    public void exportNormalBas(HttpServletResponse response, BaInfoQo qo) {
        qo.setMemberCode(memberCode);
        service.exportNormalBas(response, qo);
    }

    @RequestMapping(value = "/bas/{id}", method = RequestMethod.GET)
    public ApiResult getNormalBa(@PathVariable String id) {
        BaInfo baInfo = service.getBaNorma(id);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, baInfo);
    }
}
