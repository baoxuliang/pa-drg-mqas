package com.kindo.baqc.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class BaInfoQo {

    /**
     * 医院编码
     */
    private String memberCode;

    /**
     * 出院日期从
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date cysjBegin;

    /**
     * 出院日期至
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date cysjEnd;

    /**
     * 出院科别编码
     */
    private String cykbbm;

    /**
     * 错误类型码
     */
    private String errCode;

    /**
     * 病案号
     */
    private String bah;

    /**
     * 姓名
     */
    private String xm;

    /**
     * 患者关键字（姓名+病例号）
     */
    private String hzKeyword;

    /**
     * 手术操作关键字（编码+名称）
     */
    private String ssKeyword;

    /**
     * 主要诊断（编码+名称）
     */
    private String zyzdKeyword;

    /**
     * 离院方式
     */
    private String lyfs;

    /**
     * 主任医师
     */
    private String zrys;

    /**
     * 主治医师
     */
    private String zzys;

    /**
     * 住院医师
     */
    private String zyys;

    /**
     * 住院天数从
     */
    private Integer sjzytsBegin;

    /**
     * 住院天数到
     */
    private Integer sjzytsEnd;
}
