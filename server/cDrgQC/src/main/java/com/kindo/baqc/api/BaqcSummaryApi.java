package com.kindo.baqc.api;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaErrorSummary;
import com.kindo.baqc.model.BaSummaryCykbMonth;
import com.kindo.baqc.model.BaSummaryQo;
import com.kindo.baqc.model.TypeCount;
import com.kindo.baqc.model.TypeDeptCount;
import com.kindo.baqc.model.TypeOneDeptCount;
import com.kindo.baqc.service.BaqcSummaryService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;

@RestController
@RequestMapping("/baqc/summary")
public class BaqcSummaryApi extends BaseApi {

    @Autowired
    private BaqcSummaryService service;

    @Value("${hos.memberCode}")
    private String memberCode;

    @RequestMapping(value = "/syndate", method = RequestMethod.GET)
    public ApiResult getMaxSynDate() {
        Date date = service.getMaxSynDate(memberCode);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, date);
    }

    @RequestMapping(value = "/dept", method = RequestMethod.GET)
    public ApiResult queryDeptSummary(BaSummaryQo qo, Pageable page) {
        if (qo.getYear() == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }
        qo.setMemberCode(memberCode);
        List<BaSummaryCykbMonth> list = service.queryDeptSummary(qo, page);

        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());

        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }

    @RequestMapping(value = "/dept/export", method = RequestMethod.GET)
    public void exportDeptSummary(HttpServletResponse response, BaSummaryQo qo) {
        if (qo.getYear() == null) {
            return;
        }
        qo.setMemberCode(memberCode);
        service.exportDeptSummary(response, qo);
    }

    @RequestMapping(value = "/static", method = RequestMethod.GET)
    public ApiResult baqcStatic(BaSummaryQo qo) {
        if (qo.getYear() == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }
        qo.setMemberCode(memberCode);
        BaErrorSummary sumary = service.baqcStatic(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, sumary);
    }

    @RequestMapping(value = "/type", method = RequestMethod.GET)
    public ApiResult errByType(BaSummaryQo qo) {
        if (qo.getYear() == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }
        if (qo.getOrder() == null) {
        	qo.setOrder("DESC");
        }
        qo.setMemberCode(memberCode);
        List<TypeCount> list = service.getBaErrTypeCount(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }

    @RequestMapping(value = "/type/dept", method = RequestMethod.GET)
    public ApiResult errByTypeDept(BaSummaryQo qo) {
        if (qo.getYear() == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }
        qo.setMemberCode(memberCode);
        Map<String, Object> map = service.getBaErrTypeDeptCount(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, map);
    }
    
    @RequestMapping(value = "/type/onedept", method = RequestMethod.GET)
    public ApiResult errByTypeOnedept(BaSummaryQo qo) {
        if (qo.getYear() == null) {
            return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, null);
        }
        if (qo.getOrder() == null) {
        	qo.setOrder("DESC");
        }
        qo.setMemberCode(memberCode);
        List<TypeOneDeptCount> map = service.getBaErrTypeOnedeptCount(qo);
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, map);
    }

    @RequestMapping(value = "/type/dept/export", method = RequestMethod.GET)
    public void exportErrByTypeDept(HttpServletResponse response, BaSummaryQo qo) {
        if (qo.getYear() == null) {
            return;
        }
        qo.setMemberCode(memberCode);
        service.exportErrByTypeDept(response, qo);
    }
}
