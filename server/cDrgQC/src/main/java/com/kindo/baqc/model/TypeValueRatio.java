package com.kindo.baqc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeValueRatio {

    private String type;
    private Double value;
    private Double percent;
}
