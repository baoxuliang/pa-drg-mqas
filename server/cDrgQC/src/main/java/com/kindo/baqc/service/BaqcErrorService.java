package com.kindo.baqc.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.baqc.dao.BaqcErrorMapper;
import com.kindo.baqc.model.BaErrorInfo;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoQo;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.ErrorDetail;
import com.kindo.baqc.model.ErrorDetailQo;

@Service
public class BaqcErrorService {

    @Autowired
    private BaqcErrorMapper mapper;

    public List<BaErrorInfo> getBaErrorList(BaInfoQo qo, Pageable page) {
        List<BaErrorInfo> list = mapper.listPageBaError(qo, page);
        list.forEach(item -> {
            if (item.getErrTypes() != null && item.getErrNames() != null) {

                List<String> errTypes = Arrays.asList(item.getErrTypes().split("#"));
                List<String> errNames = Arrays.asList(item.getErrNames().split("#"));

                int size = errTypes.size();
                if (size != errNames.size()) {
                    return;
                }

                for (int i = 0; i < size; i++) {
                    errNames.set(i, new StringBuffer().append('【').append(getErrTypeName(errTypes.get(i))).append('】')
                            .append(errNames.get(i)).toString());
                }
                item.setErrs(errNames);
                item.setErrNames(String.join(";", errNames));
                item.setErrTypes(errTypes.stream().distinct().map(BaqcErrorService::getErrTypeName)
                        .collect(Collectors.joining(";")));
            }
        });
        return list;
    }

    public void exportErrorBas(HttpServletResponse response, BaInfoQo qo) {
        String fileName = "问题病案" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("application/ms-excel");
            response.setHeader("Content-disposition",
                    "attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");

            PageableSupplier<BaErrorInfo, BaInfoQo> supplier = new PageableSupplier<BaErrorInfo, BaInfoQo>();
            supplier.setFunc(this::getBaErrorList);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(BaErrorInfo.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }

    public BaInfo getBaError(String id) {
        BaInfo ba = mapper.getBaError(id);
        if (ba != null) {
            List<BaqcErrResult> errList = mapper.getErrorResult(id);
            ba.setErrs(errList.stream()
                    .map(err -> String.format("【%s】%s", getErrTypeName(err.getErrType()), err.getErrName()))
                    .collect(Collectors.toList()));
            return ba;
        }
        return null;
    }

    public static String getErrTypeName(String errType) {
        return "2".equals(errType) ? "HQMS" : "3".equals(errType) ? "DRG" : "";
    }

    public List<ErrorDetail> getErrorDetailList(ErrorDetailQo qo, Pageable page) {
        return mapper.listPageErrorDetail(qo, page);
    }
}
