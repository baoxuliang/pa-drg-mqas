package com.kindo.baqc.model;

import lombok.Data;

@Data
public class KeyValueInt {

    private Integer key;
    private Integer value;
}
