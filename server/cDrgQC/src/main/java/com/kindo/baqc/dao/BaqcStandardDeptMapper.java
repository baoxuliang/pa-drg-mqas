package com.kindo.baqc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaDropDownQo;
import com.kindo.baqc.model.BaHospitalGrid;
import com.kindo.baqc.model.BaHospitalQo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.model.BaStandardDeptGrid;
import com.kindo.baqc.model.BaStandardDeptQo;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.DrgGroupResult;

public interface BaqcStandardDeptMapper {

    List<BaStandardDeptGrid> queryListpage(@Param("param") BaStandardDeptQo param, @Param("page") Pageable page);
    
    List<BaStandardDeptGrid> queryList(@Param("param") BaStandardDeptQo param);

    BaInfoSmall queryDetail(String id);
    
    Map<String,Object> querySum(@Param("param") BaStandardDeptQo qo);
    
    List<Map<String,Object>> dropdownStatistics(@Param("param") BaStandardDeptQo qo);
}
