package com.kindo.baqc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexFieldTitle {
    private int index;
    private String field;
    private String title;
}
