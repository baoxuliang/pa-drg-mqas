package com.kindo.baqc.dao;

import java.util.List;

import com.kindo.baqc.model.IfOrganizationPart;
import com.kindo.common.model.LabelValue;

public interface BaqcCommonMapper {

    List<LabelValue> getDict(String catalog);

    List<LabelValue> getCyks(String memberCode);

    List<IfOrganizationPart> getOrga(String memberCode);
}
