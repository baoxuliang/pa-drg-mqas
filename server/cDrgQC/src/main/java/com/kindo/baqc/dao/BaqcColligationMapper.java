package com.kindo.baqc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaColligationGrid;
import com.kindo.baqc.model.BaColligationQo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.DrgGroupResult;

public interface BaqcColligationMapper {

    List<BaColligationGrid> queryListpage(@Param("param") BaColligationQo param, @Param("page") Pageable page);

    BaInfoSmall queryDetail(String id);
    
    DrgGroupResult getDrgGroupResult(String id);
    
    List<BaqcErrResult> getErrorResult(String id);
}
