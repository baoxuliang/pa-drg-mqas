package com.kindo.baqc.model;

import lombok.Data;

@Data
public class TypeOneDeptCount {

    private String cykb;
    private String cykbbm;
    private Integer count;
}
