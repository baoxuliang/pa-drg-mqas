package com.kindo.baqc.model;

import lombok.Data;

@Data
public class BaNormalAnalyStaticSum {

    private Integer num;
    private Integer pass;
    private Integer fail;
    private Integer ingruop;
}
