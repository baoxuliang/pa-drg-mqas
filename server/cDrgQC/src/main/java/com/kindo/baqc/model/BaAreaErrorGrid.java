package com.kindo.baqc.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class BaAreaErrorGrid {

	@PoiExcelField(index = 0, title = "医院名称")
    private String memberName;
	@PoiExcelField(index = 1, title = "病案号")
    private String bah;
	@PoiExcelField(index = 2, title = "出院日期", format="yyyy-MM-dd",halign="center")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date cysj;
	@PoiExcelField(index = 3, title = "住院天数")
	private Integer zyts;
	@PoiExcelField(index = 4, title = "主要诊断ICD10")
    private String zdms;
	@PoiExcelField(index = 5, title = "主要操作ICD9")
    private String cchimc1;
	@PoiExcelField(index = 6, title = "错误类型")
    private String errType;
	@PoiExcelField(index = 7, title = "错误描述")
    private String errMemo;
	@PoiExcelField(index = 8, title = "出院科室")
    private String cyks;
	@PoiExcelField(index = 9, title = "离院方式")
    private String lyfs;
    private String id;
    private String memberLevelCode1;
    private String memberLevelCode2;
}
