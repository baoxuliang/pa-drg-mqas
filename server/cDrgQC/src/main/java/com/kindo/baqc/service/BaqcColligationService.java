package com.kindo.baqc.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.baqc.dao.BaqcColligationMapper;
import com.kindo.baqc.model.BaColligationGrid;
import com.kindo.baqc.model.BaColligationQo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.DrgGroupResult;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class BaqcColligationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BaqcColligationService.class);
	 /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(BaColligationQo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		LOGGER.info("BaqcColligationService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("BaqcColligationService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("BaqcColligationService.getTheRegion,用户为管理员级别!不限区域!");
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("BaqcColligationService.getTheRegion,用户为省级别!");
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("BaqcColligationService.getTheRegion,用户为市级别!");
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("BaqcColligationService.getTheRegion,用户为县级别!");
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("BaqcColligationService.getTheRegion,用户为医院用户级别!");
			region = user.getOrgaId();
			vo.setMemberCode(region);
		}
		LOGGER.info("BaqcColligationService.getTheRegion,用户region:{}",region);
	};

    @Autowired
    private BaqcColligationMapper mapper;
    
    /** 
	 * queryListpage:综合病案表格查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @return 
	 * @since JDK 1.8 
	 **/
    public List<BaColligationGrid> queryListpage(BaColligationQo qo, Pageable page) {
        List<BaColligationGrid> list = mapper.queryListpage(qo,page);
        for(int i=0;i<list.size();i++){
        	BaColligationGrid item=list.get(i);
        	item.setLyfs(getDicValue(item.getLyfs()));
        }
        return list;
    }
    
    /** 
	 * getDicValue:获取离院方式. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param key
	 * @return 
	 * @since JDK 1.8 
	 **/
    private String getDicValue(String key){
    	 DictRemoteManager drm= new DictRemoteManager();
         Map<String, String> dicValue =  drm.getDict("CC06_00_223_00");
         for (Map.Entry<String, String> entry : dicValue.entrySet()) {
        	 if(entry.getKey().equals(key)){
        		 return entry.getValue();
        	 }
         }
         return "";
    }

    /** 
	 * queryDetail:查看详情. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param id
	 * @return 
	 * @since JDK 1.8 
	 **/
    public BaInfoSmall queryDetail(String id) {
    	BaInfoSmall ba = mapper.queryDetail(id);
    	if("N".equals(ba.getHqmsCheck()) || "N".equals(ba.getDrgCheck())){
    		ba.setBastate("9");
    	}else if("Y".equals(ba.getDrgState()) && "Y".equals(ba.getHqmsCheck()) && "Y".equals(ba.getDrgCheck())){
    		ba.setBastate("1");
    	}else if("N".equals(ba.getDrgState())){
    		ba.setBastate("0");
    	}else{
    		ba.setBastate("");
    	}
    	
        if (ba != null) {
            DrgGroupResult result = mapper.getDrgGroupResult(id);
            if (result != null) {
            	ba.setDrgCode(result.getDrgCode());
                ba.setDrgName(result.getDrgName());
            }
            
            List<BaqcErrResult> errList = mapper.getErrorResult(id);
            if(errList != null){
            	ba.setErrs(errList.stream()
                        .map(err -> String.format("【%s】%s", getErrTypeName(err.getErrType()), err.getErrName()))
                        .collect(Collectors.toList()));
            }
            return ba;
        }
        return null;
    }
    
    public static String getErrTypeName(String errType) {
        return "2".equals(errType) ? "HQMS" : "3".equals(errType) ? "DRG" : "";
    }
    
    /** 
	 * exportDrgBas:问题病案表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
	 * @since JDK 1.8 
	 **/
    public void exportDrgBas(HttpServletResponse response,BaColligationQo qo) {
    	String fileName = "综合病案" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
//    		re.setContentType("application/ms-excel");
//    		re.setHeader("Content-disposition",
//    				"attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
        	/*response.setContentType("multipart/form-data");
        	response.setCharacterEncoding("utf-8");
        	response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        	response.setHeader("Content-Disposition",
    				"attachment; filename=" + new String(fileName.getBytes(), "UTF-8") + ".xlsx");*/
        	
        	response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        	
            PageableSupplier<BaColligationGrid, BaColligationQo> supplier = new PageableSupplier<BaColligationGrid, BaColligationQo>();
            supplier.setFunc(this::queryListpage);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(BaColligationGrid.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
}
