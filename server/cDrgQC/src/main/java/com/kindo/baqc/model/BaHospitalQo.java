package com.kindo.baqc.model;

import java.util.Date;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class BaHospitalQo {

    private String city;
    private String cnty;
    private String year;
    private String quarter;
    private String month;
    private String memberTypeCode;
    private String memberLevelCode1;
    private String memberLevelCode2;
    private String memberCode;
    private String deptCode;
    private UserLoginInfo user;
}
