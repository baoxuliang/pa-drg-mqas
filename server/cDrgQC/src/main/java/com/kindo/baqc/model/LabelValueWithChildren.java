package com.kindo.baqc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.kindo.common.model.LabelValue;

import lombok.Data;

@Data
public class LabelValueWithChildren implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2774745509895288557L;

    private String label;
    private String value;
    private List<LabelValue> children = new ArrayList<LabelValue>();
}
