package com.kindo.baqc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.baqc.dao.BaqcNormalAnalyMapper;
import com.kindo.baqc.model.BaFeeType;
import com.kindo.baqc.model.BaNormalAnalyQo;
import com.kindo.baqc.model.BaNormalAnalyStaticSum;
import com.kindo.baqc.model.BaNormalTotalKpi;
import com.kindo.baqc.model.KeyValueInt;
import com.kindo.baqc.model.KeyValueStringInt;
import com.kindo.baqc.model.TypeValueRatio;

@Service
public class BaqcNormalAnalyService {

    @Autowired
    private BaqcNormalAnalyMapper mapper;

    public BaNormalTotalKpi getBaAnalyStatic(BaNormalAnalyQo qo) {
        BaNormalAnalyQo qoLast = new BaNormalAnalyQo();
        qoLast.setYear(qo.getYear() - 1);
        qoLast.setQuarter(qo.getQuarter());
        qoLast.setMonth(qo.getMonth());
        qoLast.setCykbbm(qo.getCykbbm());
        qoLast.setZlzbm(qo.getZlzbm());

        BaNormalTotalKpi kpi = mapper.getBaAnalyStatic(qo);
        kpi.setYbzb(getPerc(kpi.getYbfy(), kpi.getZfy()));
        kpi.setSwl(getPerc(kpi.getSwrs(), kpi.getCyrs()));
        kpi.setZfy10000(kpi.getZfy() != null ? (kpi.getZfy().intValue() / 100) / 100.0 : 0);

        Integer zzyrs = mapper.getZzyrs(qo);
        kpi.setZzyl(getPerc(zzyrs, kpi.getCyrs()));

        BaNormalTotalKpi kpiLast = mapper.getBaAnalyStatic(qoLast);
        kpi.setCyrsRate(getPerc(kpi.getCyrs(), kpiLast.getCyrs()));
        kpi.setZfyRate(getPerc(kpi.getZfy(), kpiLast.getZfy()));
        kpi.setPjzyrRate(getPerc(kpi.getPjzyr(), kpiLast.getPjzyr()));
        kpi.setCjfyRate(getPerc(kpi.getCjfy(), kpiLast.getCjfy()));

        return kpi;
    }
    
    public Map<String, Integer> baqcNormalSumStatic(BaNormalAnalyQo qo) {
    	Map<String, Integer> result = new HashMap<>(); 
    	BaNormalAnalyStaticSum list = mapper.getBaAnalySum(qo);
        result.put("sum", list.getNum());
        result.put("hgbas", list.getPass());
        result.put("rzbas", list.getIngruop());
        result.put("wtbas", list.getFail());
        return result;
    }

    public List<TypeValueRatio> getBaFeeType(BaNormalAnalyQo qo) {
        BaFeeType feeType = mapper.getBaFeeType(qo);

        List<TypeValueRatio> list = new ArrayList<TypeValueRatio>();
        if (feeType != null) {
            list.add(new TypeValueRatio("药品类", feeType.getYpfy(), getPerc(feeType.getYpfy(), feeType.getZfy())));
            list.add(new TypeValueRatio("综合服务类", feeType.getZhfy(), getPerc(feeType.getZhfy(), feeType.getZfy())));
            list.add(new TypeValueRatio("诊断类", feeType.getZdfy(), getPerc(feeType.getZdfy(), feeType.getZfy())));
            list.add(new TypeValueRatio("治疗类", feeType.getZlfy(), getPerc(feeType.getZlfy(), feeType.getZfy())));
            list.add(new TypeValueRatio("血液和血液品类", feeType.getXyfy(), getPerc(feeType.getXyfy(), feeType.getZfy())));
            list.add(new TypeValueRatio("材料类", feeType.getClfy(), getPerc(feeType.getClfy(), feeType.getZfy())));
            list.add(new TypeValueRatio("其他", feeType.getQtfy(), getPerc(feeType.getQtfy(), feeType.getZfy())));
        }
        return list;
    }

    public List<KeyValueStringInt> getCountByFee(BaNormalAnalyQo qo) {
        List<KeyValueInt> list = mapper.getCountByFee(qo);
        List<KeyValueStringInt> retList = list.stream().filter(item -> item.getKey() != null)
                .map(item -> new KeyValueStringInt("" + item.getKey() * 10000, item.getValue()))
                .collect(Collectors.toList());
        return retList;
    }

    public List<KeyValueStringInt> getCountBySjzyts(BaNormalAnalyQo qo) {
        List<KeyValueInt> list = mapper.getCountBySjzyts(qo);
        List<KeyValueStringInt> retList = list.stream().filter(item -> item.getKey() != null && item.getKey() <= 30)
                .map(item -> new KeyValueStringInt("" + item.getKey(), item.getValue())).collect(Collectors.toList());
        list.stream().filter(item -> item.getKey() != null && item.getKey() > 30).map(KeyValueInt::getValue)
                .reduce(Integer::sum).ifPresent(item -> retList.add(new KeyValueStringInt("30以上", item)));
        return retList;
    }

    public static Double getPerc(Number d1, Number d2) {
        if (d1 == null || d2 == null || (d2.doubleValue() > -1e-6 && d2.doubleValue() < 1e-6)) {
            return null;
        }
        return (int) (d1.doubleValue() / d2.doubleValue() * 10000 + 0.5) / 100.0;
    }
}
