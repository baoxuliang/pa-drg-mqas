package com.kindo.baqc.api;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaAreaErrorQo;
import com.kindo.baqc.model.BaDropDownQo;
import com.kindo.baqc.model.BaHospitalQo;
import com.kindo.baqc.model.BaStandardDeptGrid;
import com.kindo.baqc.model.BaStandardDeptQo;
import com.kindo.baqc.service.BaqcAreaErrorService;
import com.kindo.baqc.service.BaqcStandardDeptService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/baqc/standardDeptBas")
public class BaqcStandardDeptApi extends BaseApi {
	
	@Autowired
	BaqcStandardDeptService service;
	@Autowired
	BaqcAreaErrorService service1;
    
	 @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(BaStandardDeptQo vo,HttpServletRequest request,BaAreaErrorQo qo) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		qo.setUser(info);
    	vo.setUser(info);
    };

    /** 
	 * queryListpage:标准科室表格查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/queryListpage", method = RequestMethod.GET)
    public ApiResult queryListpage(BaStandardDeptQo qo, Pageable page, HttpServletRequest request) throws Exception {
    	service.getTheRegion(qo);
    	List<BaStandardDeptGrid> list = service.queryListpage(qo,page);
    	RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }
    
    /** 
	 * queryList:标准科室图表查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/queryList", method = RequestMethod.GET)
    public ApiResult queryList(BaStandardDeptQo qo, HttpServletRequest request) throws Exception {
    	service.getTheRegion(qo);
    	List<BaStandardDeptGrid> list = service.queryList(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    
    /** 
	 * tableExport:标准科室表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void tableExport(HttpServletResponse response,BaStandardDeptQo qo) throws Exception {
    	service.getTheRegion(qo);
    	service.exportHospitalBas(response,qo);
    }
    
    /** 
	 * dropdownStatistics:标准科室表格下拉框查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/dropdownStatistics", method = RequestMethod.GET)
    public ApiResult dropdownStatistics(BaStandardDeptQo qo) throws Exception {
    	service.getTheRegion(qo);
    	List<Map<String,Object>> list = service.dropdownStatistics(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    /** 
	 * querySum:标准科室表格合计查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/querySum", method = RequestMethod.GET)
    public ApiResult querySum(BaStandardDeptQo qo) throws Exception {
    	service.getTheRegion(qo);
    	Map<String,Object> list = service.querySum(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    /** 
	 * popupStatisticsExport:标准科室表格弹出框查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/popupStatistics/export", method = RequestMethod.GET)
    public void popupStatisticsExport(HttpServletResponse response,BaAreaErrorQo qo) throws Exception {
    	service1.getTheRegion(qo);
    	service1.exportErrsBas(response, qo);
    }
    
}
