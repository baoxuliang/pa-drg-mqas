package com.kindo.baqc.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.baqc.dao.BaqcAreaErrorMapper;
import com.kindo.baqc.dao.BaqcColligationMapper;
import com.kindo.baqc.dao.BaqcHospitalMapper;
import com.kindo.baqc.model.BaAreaErrorGrid;
import com.kindo.baqc.model.BaAreaErrorQo;
import com.kindo.baqc.model.BaColligationGrid;
import com.kindo.baqc.model.BaColligationQo;
import com.kindo.baqc.model.BaDropDownQo;
import com.kindo.baqc.model.BaHospitalGrid;
import com.kindo.baqc.model.BaHospitalQo;
import com.kindo.baqc.model.BaInfoSmall;
import com.kindo.baqc.model.BaStandardDeptQo;
import com.kindo.baqc.model.BaqcErrResult;
import com.kindo.baqc.model.DrgGroupResult;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class BaqcHospitalService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BaqcHospitalService.class);
	 /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(BaHospitalQo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		LOGGER.info("BaqcHospitalService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("BaqcHospitalService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("BaqcHospitalService.getTheRegion,用户为管理员级别!不限区域!");
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("BaqcHospitalService.getTheRegion,用户为省级别!");
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("BaqcHospitalService.getTheRegion,用户为市级别!");
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("BaqcHospitalService.getTheRegion,用户为县级别!");
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("BaqcHospitalService.getTheRegion,用户为医院用户级别!");
			region = user.getOrgaId();
			vo.setMemberCode(region);
		}
		LOGGER.info("BaqcHospitalService.getTheRegion,用户region:{}",region);
	};
	
    @Autowired
    private BaqcHospitalMapper mapper;
    
    /** 
	 * queryListpage:医院病案表格查询. 分页 <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
	 * @since JDK 1.8 
	 **/
    public List<BaHospitalGrid> queryListpage(BaHospitalQo qo, Pageable page) {
        List<BaHospitalGrid> list = mapper.queryListpage(qo,page);
        return list;
    }
    
    /** 
	 * queryListExport:医院病案表格查询. 不分页 <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
	 * @since JDK 1.8 
	 **/
    public List<BaHospitalGrid> queryListExport(BaHospitalQo qo, Pageable page) {
        List<BaHospitalGrid> list = mapper.queryListpage(qo,page);
        Map<String,Object> sum = mapper.querySum(qo);
        BaHospitalGrid item = new  BaHospitalGrid();
        item.setMemberName("合计");
        item.setSumNum(Integer.parseInt(sum.get("sumNum").toString()));
        item.setErrorNum(Integer.parseInt(sum.get("errorNum").toString()));
        item.setHqmsNum(Integer.parseInt(sum.get("hqmsNum").toString()));
        item.setDrgNum(Integer.parseInt(sum.get("drgNum").toString()));
        item.setInBa(Integer.parseInt(sum.get("inBa").toString()));
        item.setNotInBa(Integer.parseInt(sum.get("notInBa").toString()));
        list.add(item);
        return list;
    }
    
    /** 
   	 * queryList:医院病案图表查询. <br/>
   	 * @author whk00216 
   	 * @date 2018年4月11日 下午9:21:13 *
   	 * @param qo
   	 * @param request
   	 * @return 
   	 * @since JDK 1.8 
   	 **/
    public List<BaHospitalGrid> queryList(BaHospitalQo qo) {
        List<BaHospitalGrid> list = mapper.queryList(qo);
        return list;
    }
    
    /** 
   	 * querySum:医院病案表格合计查询. <br/>
   	 * @author whk00216 
   	 * @date 2018年4月11日 下午9:21:13 *
   	 * @param qo
   	 * @return 
   	 * @since JDK 1.8 
   	 **/
    public Map<String,Object> querySum(BaHospitalQo qo) {
    	Map<String,Object> list = mapper.querySum(qo);
        return list;
    }
    
    /** 
	 * dropdownStatistics:医院病案表格下拉框. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
    public List<Map<String,Object>> dropdownStatistics(BaHospitalQo qo) {
    	List<Map<String,Object>> list = mapper.dropdownStatistics(qo);
        return list;
    }
    
    /** 
	 * exportHospitalBas:医院病案表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
	 * @since JDK 1.8 
	 **/
    public void exportHospitalBas(HttpServletResponse response,BaHospitalQo qo) {
    	String fileName = "医院病案" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
        	response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<BaHospitalGrid, BaHospitalQo> supplier = new PageableSupplier<BaHospitalGrid, BaHospitalQo>();
            supplier.setFunc(this::queryListExport);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(BaHospitalGrid.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
}
