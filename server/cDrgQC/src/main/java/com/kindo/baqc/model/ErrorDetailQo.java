package com.kindo.baqc.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class ErrorDetailQo {
    private String memberCode;
    private String bah;
    private String errName;
    private String errMemo;
    private String errType;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkTimeBegin;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkTimeEnd;
}
