package com.kindo.baqc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.baqc.model.BaInfo;
import com.kindo.baqc.model.BaInfoQo;
import com.kindo.baqc.model.BaNormalInfo;
import com.kindo.baqc.model.BaqcDrgGroupResult;

public interface BaqcNormalMapper {

    List<BaNormalInfo> listPageBaNormal(@Param("param") BaInfoQo param, @Param("page") Pageable page);

    BaInfo getBaNormal(String id);

    BaqcDrgGroupResult getDrgGroupResult(String id);
}
