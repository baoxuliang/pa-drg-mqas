package com.kindo.baqc.model;

import lombok.Data;

@Data
public class TypeDeptCount {

    private String type;
    private String cykbbm;
    private Integer count;
}
