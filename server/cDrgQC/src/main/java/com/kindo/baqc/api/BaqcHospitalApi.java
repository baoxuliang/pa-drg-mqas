package com.kindo.baqc.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.baqc.model.BaAreaErrorGrid;
import com.kindo.baqc.model.BaAreaErrorQo;
import com.kindo.baqc.model.BaColligationGrid;
import com.kindo.baqc.model.BaDropDownQo;
import com.kindo.baqc.model.BaHospitalGrid;
import com.kindo.baqc.model.BaHospitalQo;
import com.kindo.baqc.service.BaqcAreaErrorService;
import com.kindo.baqc.service.BaqcHospitalService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/baqc/hospitalbas")
public class BaqcHospitalApi extends BaseApi {
	@Autowired
	BaqcHospitalService service;
    
	@Autowired
	BaqcAreaErrorService service1;
	
	 @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(BaHospitalQo vo,HttpServletRequest request,BaAreaErrorQo qo) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		qo.setUser(info);
    	vo.setUser(info);
    };

    /** 
	 * queryListpage:医院病案表格查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/queryListpage", method = RequestMethod.GET)
    public ApiResult queryListpage(BaHospitalQo qo, Pageable page, HttpServletRequest request) throws Exception {
    	service.getTheRegion(qo);
    	List<BaHospitalGrid> list = service.queryListpage(qo,page);
    	RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }
    
    /** 
	 * queryList:医院病案图表查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/queryList", method = RequestMethod.GET)
    public ApiResult queryList(BaHospitalQo qo, HttpServletRequest request) throws Exception {
    	service.getTheRegion(qo);
    	List<BaHospitalGrid> list = service.queryList(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    /** 
	 * tableExport:医院病案表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void tableExport(HttpServletResponse response,BaHospitalQo qo) throws Exception {
    	service.getTheRegion(qo);
    	service.exportHospitalBas(response,qo);
    }
    
    /** 
	 * dropdownStatistics:医院病案表格下拉框. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/dropdownStatistics", method = RequestMethod.GET)
    public ApiResult dropdownStatistics(BaHospitalQo qo) throws Exception {
    	service.getTheRegion(qo);
    	List<Map<String,Object>> list = service.dropdownStatistics(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    /** 
	 * querySum:医院病案表格合计查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/querySum", method = RequestMethod.GET)
    public ApiResult querySum(BaHospitalQo qo) throws Exception {
    	service.getTheRegion(qo);
    	Map<String,Object> list = service.querySum(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    /** 
	 * popupStatistics:医院病案表格弹出框查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/popupStatistics", method = RequestMethod.GET)
    public ApiResult popupStatistics(BaAreaErrorQo qo, Pageable page, HttpServletRequest request) throws Exception {
    	if(page.getSorts().size()==0||(page.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" memberLevelCode1 ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" , memberLevelCode2 ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		SortPair sp3= new SortPair();
    		sp3.setField(" , memberName ");
    		sp3.setAsc(true);
    		sorts.add(sp3);
    		SortPair sp4= new SortPair();
    		sp4.setField(" , CYSJ ");
    		sp4.setAsc(false);
    		sorts.add(sp4);
    		page.setSorts(sorts);
    	}
    	service1.getTheRegion(qo);
    	List<BaAreaErrorGrid> list = service1.queryListpage(qo,page);
    	RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
    }
}
