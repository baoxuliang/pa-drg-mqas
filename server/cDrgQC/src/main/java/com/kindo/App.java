package com.kindo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.kindo.uas.common.anno.IgnoreDuringScan;

/**
 * 业务模块统一入口
 */
@EnableTransactionManagement
//开始自动配置，exclude配置排除不必要的自动配置类
@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class})
// com.kindo.uas为uas包路径必须配置扫描，com.kindo.demo可配置为自己项目包路径
//@ComponentScan({"com.kindo"})
//使用以下配置 可以忽略所有集成平台自带API
@ComponentScan(value = {"com.kindo"}, excludeFilters = @ComponentScan.Filter(IgnoreDuringScan.class))
//开启缓存
@EnableCaching
//@MapperScan("com.kindo")
//@ServletComponentScan("com.kindo")
@EnableScheduling
public class App extends SpringBootServletInitializer {

    private static Class<App> appClass = App.class;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(appClass);
    }

    public static void main(String[] args) {
        SpringApplication.run(appClass, args);
    }
}

