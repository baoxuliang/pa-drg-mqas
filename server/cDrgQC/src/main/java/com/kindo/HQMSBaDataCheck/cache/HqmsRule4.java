package com.kindo.HQMSBaDataCheck.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 女性诊断编码校 验
 * @author jindoulixing
 *
 */
public class HqmsRule4 {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(HqmsRule4.class);
	
	private final static Set<String> HQMSRule4Cache_1 = new HashSet<String>();
	private final static Set<String> HQMSRule4Cache_2 = new HashSet<String>();
    
    static{
    	HQMSRule4Cache_1.add("O80");
    	HQMSRule4Cache_1.add("O81");
    	HQMSRule4Cache_1.add("O82");
    	HQMSRule4Cache_1.add("O83");
    	HQMSRule4Cache_1.add("O84");
    	
    	HQMSRule4Cache_2.add("O00");
    	HQMSRule4Cache_2.add("O01");
    	HQMSRule4Cache_2.add("O02");
    	HQMSRule4Cache_2.add("O03");
    	HQMSRule4Cache_2.add("O04");
    	HQMSRule4Cache_2.add("O05");
    	HQMSRule4Cache_2.add("O06");
    	HQMSRule4Cache_2.add("O07");
    	HQMSRule4Cache_2.add("O08");
    }
    
    public static boolean checkHqmsRule11_1(List<String> list) {
    	for (String item : HQMSRule4Cache_1) {
    		for(String code : list){
    			if (code.startsWith(item)) {
    				return true;
    			}
    		}
		}
    	// 创建集合
		/*List<String> realA = new ArrayList<String>(HQMSRule4Cache_1);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		if (realA.size() > 1) {
			return true;
		}*/
		return false;
	}
    
    public static boolean checkHqmsRule11_2(List<String> list) {
    	for (String item : HQMSRule4Cache_2) {
    		for(String code : list){
    			if (code.startsWith(item)) {
    				return true;
    			}
    		}
		}
		// 创建集合
		/*List<String> realA = new ArrayList<String>(HQMSRule4Cache_2);
		List<String> realB = new ArrayList<String>(list);
		// 求交集
		realA.retainAll(realB);
		if (realA.size() > 1) {
			return true;
		}*/
		return false;
	}
}
