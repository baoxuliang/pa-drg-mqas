package com.kindo.HQMSBaDataCheck.rule;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import com.kindo.HQMSBaDataCheck.model.HqmsBaData;


public abstract class AbstractHQMSCheckRule implements Runnable{
	
	protected HqmsBaData hqmsBaData;  
	protected CountDownLatch latch; 
	protected AtomicInteger countfail; 
    
    public void HQMSCheckRule(final CountDownLatch latch,final HqmsBaData hqmsBaData,final AtomicInteger countfail) {  
        this.hqmsBaData = hqmsBaData;  
        this.latch = latch;  
        this.countfail = countfail;
    }
    
}
