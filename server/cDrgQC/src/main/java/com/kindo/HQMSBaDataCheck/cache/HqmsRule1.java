package com.kindo.HQMSBaDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.model.Rule;
/**
 * 男性诊断编码校 验
 * @author jindoulixing
 *
 */
public class HqmsRule1 {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(HqmsRule1.class);
	
	private final static Set<String> HQMSRule1Cache = new HashSet<String>();
	
    /*private static HqmsRule1 instance = null;
    
    public static HqmsRule1 getInstance() {
        if (instance == null) {
            synchronized (HqmsRule1.class) {
                if (instance == null) {
                    instance = new HqmsRule1();
                }
            }
        }
        return instance;
    }
    
    private HqmsRule1() {
    	refreshCache();
    }*/
    
    private static void refreshCache() {
    	
    	List<Rule> list =  HQMSBaDataCheck.getHQMSDataCheckMapper().queryHqmsRule1();
    	
		for (Rule item : list) {
			HQMSRule1Cache.add(item.getZdbm());
		}
    	    
    }
    
    private void cleanupCache() {
    	HQMSRule1Cache.clear();
    }
    
    public void refreshHQMSRuleConfiguration() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static Set<String> getHQMSRule1Cache(){
    	if(HQMSRule1Cache ==null || HQMSRule1Cache.size() == 0){
    		refreshCache();
    	}
    	return HQMSRule1Cache;
    }
    
    public static boolean checkHqmsRule1(String key) {
    	if(HQMSRule1Cache ==null || HQMSRule1Cache.size() == 0){
    		refreshCache();
    	}
		for (String item : HQMSRule1Cache) {
			if (key.startsWith(item)) {
				return false;
			}
		}
		return true;
    }
}
