package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 手术治疗费必须大于等于麻醉 费与手术费之和 P763≥P764+P765（允许误差 1.2） 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule24 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule24.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Double SSZLF = hqmsBaData.getSSZLF();
			Double MAF = hqmsBaData.getMAF();
			Double SSF = hqmsBaData.getSSF();
			
			SSZLF = null == SSZLF?0:SSZLF;
			MAF = null == MAF?0:MAF;
			SSF = null == SSF?0:SSF;
			if(SSZLF < (MAF+SSF)){
				if((SSZLF + 1.2) < (MAF+SSF)){
					flag = false;
				}
			}
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("224"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule24数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
