package com.kindo.HQMSBaDataCheck.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class QCErrResult implements Comparable<QCErrResult>{
	private String UID;
	private String MEMBER_CODE;
	private String BAH;
	private int ZYCS;
	private String ERR_CODE;
	private String ERR_NAME;
	private String ERR_MEMO;
	private String ERR_TPYE;
	private Date CHECK_TIME;
	private String ID;
	
	public QCErrResult(){
	}
	
	public QCErrResult(String ID,String MEMBER_CODE,String BAH,int ZYCS, String ERR_CODE,String ERR_NAME,String ERR_MEMO,String ERR_TPYE){
		this.ID = ID;
		this.MEMBER_CODE=MEMBER_CODE;
		this.BAH =BAH;
		this.ZYCS = ZYCS;
		this.ERR_CODE = ERR_CODE;
		this.ERR_NAME = ERR_NAME;
		this.ERR_MEMO = ERR_MEMO;
		this.ERR_TPYE = ERR_TPYE;
	}
	
	@Override
	public int compareTo(QCErrResult o){ 
		return this.hashCode()-o.hashCode();
		//return this.UID.compareTo(o.UID);
    }
}
