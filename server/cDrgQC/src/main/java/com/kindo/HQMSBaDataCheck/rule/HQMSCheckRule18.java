package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 年龄小于1周岁即年龄为0时， （年龄不足1周岁的）年龄不能 为空 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule18 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule18.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Integer NL = hqmsBaData.getNL();
			
    		if(null == NL || NL <= 0){
    			Double bzyzsnl = hqmsBaData.getBZYZSNL();
    			if(null == bzyzsnl || bzyzsnl <= 0){
    				flag = false;
    			}
    		}
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("218"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule18数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
