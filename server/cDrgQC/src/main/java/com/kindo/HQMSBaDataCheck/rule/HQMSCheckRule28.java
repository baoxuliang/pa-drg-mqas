package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 生存矛盾 校验 
 * 当主要诊断或其他诊断出院情 况为“4 死亡”，或者死亡患 者尸检为“1 是”，或者最高 诊断依据为“8 尸检”，则离 院方式不能为“1 医嘱离院”， “2 医嘱转院”，“3 医嘱转 社区卫生服务机构/乡镇卫生 院”，“4 非医嘱离院”，“9 其他” 主
 * @author jindoulixing
 *
 */
public class HQMSCheckRule28 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule28.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
	    	
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("228"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule28数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
