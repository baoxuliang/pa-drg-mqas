package com.kindo.HQMSBaDataCheck.rule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 分项费用全部有值时，住院总 费用应等于各分项费用（除自 付金额,临床物理治疗费,麻醉 费,手术费,抗菌药物费用外）之 和（允许误差1.2）。 P782=P752+P754+P755+P756 +P757+P758+P759+P760+P76 1+P763+P767+P768+P769+P7 71+P772+P773+P774+P775+P 776+P777+P778+P779+P780+ P781 
 *分项费用部分有值时，住院总 费用应大于等于各分项费用 （除自付金额,临床物理治疗 费,麻醉费,手术费,抗菌药物费 用外）之和（允许误差1.2）。P782≥ P752+P754+P755+P756+P757 +P758+P759+P760+P761+P76 3+P767+P768+P769+P771+P7 72+P773+P774+P775+P776+P 777+P778+P779+P780+P781 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule20 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule20.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Double in_zfy = hqmsBaData.getZFY();
			Double l_YLFUF = hqmsBaData.getYLFUF();
			Double l_ZLCZF = hqmsBaData.getZLCZF();
			Double l_HLF = hqmsBaData.getHLF();
			Double l_QTFY = hqmsBaData.getQTFY();
			Double l_BLZDF = hqmsBaData.getBLZDF();
			Double l_SYSZDF = hqmsBaData.getSYSZDF();
			Double l_YXXZDF = hqmsBaData.getYXXZDF();
			Double l_LCZDXMF = hqmsBaData.getLCZDXMF();
			Double l_FSSZLXMF = hqmsBaData.getFSSZLXMF();
			Double l_SSZLF = hqmsBaData.getSSZLF();
			Double l_KFF = hqmsBaData.getKFF();
			Double l_ZYZLF = hqmsBaData.getZYZLF();
			Double l_XYF = hqmsBaData.getXYF();
			Double l_ZCYF = hqmsBaData.getZCYF();
			Double l_ZCYF1 = hqmsBaData.getZCYF1();
			Double l_XF = hqmsBaData.getXF();
			Double l_BDBLZPF = hqmsBaData.getBDBLZPF();
			Double l_QDBLZPF = hqmsBaData.getQDBLZPF();
			Double l_NXYZLZPF = hqmsBaData.getNXYZLZPF();
			Double l_XBYZLZPF = hqmsBaData.getXBYZLZPF();
			Double l_HCYYCLF = hqmsBaData.getHCYYCLF();
			Double l_YYCLF = hqmsBaData.getYYCLF();
			Double l_YCXYYCLF = hqmsBaData.getYCXYYCLF();
			Double l_QTF = hqmsBaData.getQTF();
			
			Double in_qtzfy = l_YLFUF + l_ZLCZF + l_HLF  + l_QTFY + l_BLZDF + l_SYSZDF + l_YXXZDF + l_LCZDXMF + l_FSSZLXMF + l_SSZLF + l_KFF + l_ZYZLF + l_XYF + l_ZCYF + l_ZCYF1 + l_XF +l_BDBLZPF + l_QDBLZPF + l_NXYZLZPF + l_XBYZLZPF + l_HCYYCLF + l_YYCLF + l_YCXYYCLF + l_QTF;
			BigDecimal bg = new BigDecimal(in_qtzfy);    
		    in_qtzfy = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	        if(null != in_zfy && in_zfy > 0 && 
	        		null != l_YLFUF && l_YLFUF > 0 && 
	        		null != l_ZLCZF && l_ZLCZF > 0 && 
	        		null != l_HLF && l_HLF > 0 && 
	        		null != l_QTFY && l_QTFY > 0 && 
	        		null != l_BLZDF && l_BLZDF > 0 && 
	        		null != l_SYSZDF && l_SYSZDF > 0 && 
	        		null != l_YXXZDF && l_YXXZDF > 0 && 
	        		null != l_LCZDXMF && l_LCZDXMF > 0 && 
	        		null != l_FSSZLXMF && l_FSSZLXMF > 0 && 
	        		null != l_SSZLF && l_SSZLF > 0 && 
	        		null != l_KFF && l_KFF > 0 && 
	        		null != l_ZYZLF && l_ZYZLF > 0 && 
	        		null != l_XYF && l_XYF > 0 && 
	        		null != l_ZCYF && l_ZCYF > 0 && 
	        		null != l_ZCYF1 && l_ZCYF1 > 0 && 
	        		null != l_XF && l_XF > 0 && 
	        		null != l_BDBLZPF && l_BDBLZPF > 0 && 
	        		null != l_QDBLZPF && l_QDBLZPF > 0 && 
	        		null != l_NXYZLZPF && l_NXYZLZPF > 0 && 
	        		null != l_XBYZLZPF && l_XBYZLZPF > 0 && 
	        		null != l_HCYYCLF && l_HCYYCLF > 0 && 
	        		null != l_YYCLF && l_YYCLF > 0 && 
	        		null != l_YCXYYCLF && l_YCXYYCLF > 0 && 
	        		null != l_QTF && l_QTF >0)
	        {
	        	if((in_zfy < in_qtzfy - 1.2) || (in_zfy > in_qtzfy + 1.2)){
					flag = false;
				}
	        } else {
	        	if(in_zfy < in_qtzfy){
	        		if((in_zfy + 1.2) < in_qtzfy){
						flag = false;
					}
	        	}
	        }
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("220"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule20数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
