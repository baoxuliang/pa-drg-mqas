package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;
import com.kindo.HQMSBaDataCheck.cache.HqmsRule3;

/**
 * 门（急）诊诊断编码、入院诊 断编码、主要诊断编码、其它 诊断编码各项编码范围应为： A～U开头和Z开头的编码；不 包括字母V、W、X、Y开头的 编码 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule9 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule9.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		if (flag) {
    			String JBBM = hqmsBaData.getJBBM();
				if (!StringUtils.isEmpty(JBBM)) {
					flag = HqmsRule3.checkHqmsRule9(JBBM);
				}
			}
    		if (flag) {
    			String RYZDBM = hqmsBaData.getRYZDBM();
				if (!StringUtils.isEmpty(RYZDBM)) {
					flag = HqmsRule3.checkHqmsRule9(RYZDBM);
				}
			}
    		if (flag) {
    			String JBDM = hqmsBaData.getJBDM();
				if (!StringUtils.isEmpty(JBDM)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM);
				}
			}
			if (flag) {
				String JBDM1 = hqmsBaData.getJBDM1();
				if (!StringUtils.isEmpty(JBDM1)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM1);
				}
			}
			if (flag) {
				String JBDM2 = hqmsBaData.getJBDM2();
				if (!StringUtils.isEmpty(JBDM2)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM2);
				}
			}
			if (flag) {
				String JBDM3 = hqmsBaData.getJBDM3();
				if (!StringUtils.isEmpty(JBDM3)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM3);
				}
			}
			if (flag) {
				String JBDM4 = hqmsBaData.getJBDM4();
				if (!StringUtils.isEmpty(JBDM4)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM4);
				}
			}
			if (flag) {
				String JBDM5 = hqmsBaData.getJBDM5();
				if (!StringUtils.isEmpty(JBDM5)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM5);
				}
			}
			if (flag) {
				String JBDM6 = hqmsBaData.getJBDM6();
				if (!StringUtils.isEmpty(JBDM6)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM6);
				}
			}
			if (flag) {
				String JBDM7 = hqmsBaData.getJBDM7();
				if (!StringUtils.isEmpty(JBDM7)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM7);
				}
			}
			if (flag) {
				String JBDM8 = hqmsBaData.getJBDM8();
				if (!StringUtils.isEmpty(JBDM8)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM8);
				}
			}
			if (flag) {
				String JBDM9 = hqmsBaData.getJBDM9();
				if (!StringUtils.isEmpty(JBDM9)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM9);
				}
			}
			if (flag) {
				String JBDM10 = hqmsBaData.getJBDM10();
				if (!StringUtils.isEmpty(JBDM10)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM10);
				}
			}
			if (flag) {
				String JBDM11 = hqmsBaData.getJBDM11();
				if (!StringUtils.isEmpty(JBDM11)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM11);
				}
			}
			if (flag) {
				String JBDM12 = hqmsBaData.getJBDM12();
				if (!StringUtils.isEmpty(JBDM12)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM12);
				}
			}
			if (flag) {
				String JBDM13 = hqmsBaData.getJBDM13();
				if (!StringUtils.isEmpty(JBDM13)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM13);
				}
			}
			if (flag) {
				String JBDM14 = hqmsBaData.getJBDM14();
				if (!StringUtils.isEmpty(JBDM14)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM14);
				}
			}
			if (flag) {
				String JBDM15 = hqmsBaData.getJBDM15();
				if (!StringUtils.isEmpty(JBDM15)) {
					flag = HqmsRule3.checkHqmsRule9(JBDM15);
				}
			}
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("209"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule9数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
