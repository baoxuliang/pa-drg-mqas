package com.kindo.HQMSBaDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.model.HQMSRuleConfig;
import com.kindo.HQMSBaDataCheck.rule.AbstractHQMSCheckRule;

public class HQMSRuleConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSRuleConfiguration.class);
	
	private final static Set<AbstractHQMSCheckRule> HQMSRuleClassCache = new HashSet<AbstractHQMSCheckRule>();
	private final static Map<String, HQMSRuleConfig> HQMSRuleConfigCache = new ConcurrentHashMap<>();
	
    private static HQMSRuleConfiguration instance = null;
    
    public static HQMSRuleConfiguration getInstance() {
        if (instance == null) {
            synchronized (HQMSRuleConfiguration.class) {
                if (instance == null) {
                    instance = new HQMSRuleConfiguration();
                }
            }
        }
        return instance;
    }
    
     
    private HQMSRuleConfiguration() {
    	cleanupCache();
    	refreshCache();
    }
    
    private void refreshCache() {
    	List<HQMSRuleConfig> list = HQMSBaDataCheck.getHQMSDataCheckMapper().queryHQMSRuleConfiguration();
    	
		for (HQMSRuleConfig item : list) {
			AbstractHQMSCheckRule rule = getRuleClass(item.getCheckClass());
			if (rule != null) {
				HQMSRuleClassCache.add(rule);
			}
			HQMSRuleConfigCache.put(item.getErrCode(), item);
			
		}
    	    
    }
    
    private void cleanupCache() {
    	HQMSRuleClassCache.clear();
    	HQMSRuleConfigCache.clear();
    }
    
    public void refreshHQMSRuleConfiguration() {
    	cleanupCache();
    	refreshCache();
    }
    
    public Set<AbstractHQMSCheckRule> getHQMSRuleClassCache(){
    	if(HQMSRuleClassCache ==null || HQMSRuleClassCache.size() == 0){
    		refreshCache();
    	}
    	return HQMSRuleClassCache;
    }
    
    private AbstractHQMSCheckRule getRuleClass(String clsName) {
		try {
			Class<?> cls = Class.forName(clsName);
			return (AbstractHQMSCheckRule) cls.newInstance();
		} catch (ClassNotFoundException e) {
			LOGGER.error("Failed to find the class:" + clsName);
			return null;
		} catch (Exception e) {
			LOGGER.error("Failed to initialize the class:" + clsName, e);
			return null;
		}
	}
   
    public HQMSRuleConfig getHQMSConfigCache(String key){
    	if(HQMSRuleConfigCache ==null || HQMSRuleConfigCache.size() == 0){
    		refreshCache();
    	}
    	return HQMSRuleConfigCache.get(key);
    }
}
