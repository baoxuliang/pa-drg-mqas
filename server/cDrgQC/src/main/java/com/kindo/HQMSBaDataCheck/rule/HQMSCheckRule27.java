package com.kindo.HQMSBaDataCheck.rule;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 质控日期不能早于入院日期
 * @author jindoulixing
 *
 */
public class HQMSCheckRule27 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule27.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
	    	Date RYSJ = hqmsBaData.getRYSJ();
	    	Date ZKRQ = hqmsBaData.getZKRQ();
	    	
	    	if(null != RYSJ && null != ZKRQ){
	    		if(RYSJ.getTime() > ZKRQ.getTime()){
	    			flag = false;
	    		}
	    	}
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("227"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule27数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
