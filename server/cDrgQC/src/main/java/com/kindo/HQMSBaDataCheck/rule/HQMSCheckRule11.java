package com.kindo.HQMSBaDataCheck.rule;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;
import com.kindo.HQMSBaDataCheck.cache.HqmsRule4;

/**
 * 当主要诊断或者其它诊断编码 出现O80-O84编码，且无流产 结局编码出现O00-O08编码 时，其它诊断编码必须有分娩 结局编码Z37 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule11 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule11.class);
	private static final String check_code = "Z37";
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {

			List<String> list = new ArrayList<String>();
			
			String in_JBDM = hqmsBaData.getJBDM();
			if(null!=in_JBDM && !in_JBDM.equals("")){
				list.add(in_JBDM.trim());
			}
			String in_JBDM1 = hqmsBaData.getJBDM1();
			if(null!=in_JBDM1 && !in_JBDM1.equals("")){
				list.add(in_JBDM1.trim());
			}
			String in_JBDM2 = hqmsBaData.getJBDM2();
			if(null!=in_JBDM2 && !in_JBDM2.equals("")){
				list.add(in_JBDM2.trim());
			}
			String in_JBDM3 = hqmsBaData.getJBDM3();
			if(null!=in_JBDM3 && !in_JBDM3.equals("")){
				list.add(in_JBDM3.trim());
			}
			String in_JBDM4 = hqmsBaData.getJBDM4();
			if(null!=in_JBDM4 && !in_JBDM4.equals("")){
				list.add(in_JBDM4.trim());
			}
			String in_JBDM5 = hqmsBaData.getJBDM5();
			if(!StringUtils.isEmpty(in_JBDM5)){
				list.add(in_JBDM5.trim());
			}
			String in_JBDM6 = hqmsBaData.getJBDM6();
			if(!StringUtils.isEmpty(in_JBDM6)){
				list.add(in_JBDM6.trim());
			}
			String in_JBDM7 = hqmsBaData.getJBDM7();
			if(!StringUtils.isEmpty(in_JBDM7)){
				list.add(in_JBDM7.trim());
			}
			String in_JBDM8 = hqmsBaData.getJBDM8();
			if(!StringUtils.isEmpty(in_JBDM8)){
				list.add(in_JBDM8.trim());
			}
			String in_JBDM9 = hqmsBaData.getJBDM9();
			if(!StringUtils.isEmpty(in_JBDM9)){
				list.add(in_JBDM9.trim());
			}
			String in_JBDM10 = hqmsBaData.getJBDM10();
			if(!StringUtils.isEmpty(in_JBDM10)){
				list.add(in_JBDM10.trim());
			}
			String in_JBDM11 = hqmsBaData.getJBDM11();
			if(!StringUtils.isEmpty(in_JBDM11)){
				list.add(in_JBDM11.trim());
			}
			String in_JBDM12 = hqmsBaData.getJBDM12();
			if(!StringUtils.isEmpty(in_JBDM12)){
				list.add(in_JBDM12.trim());
			}
			String in_JBDM13 = hqmsBaData.getJBDM13();
			if(!StringUtils.isEmpty(in_JBDM13)){
				list.add(in_JBDM13.trim());
			}
			String in_JBDM14 = hqmsBaData.getJBDM14();
			if(!StringUtils.isEmpty(in_JBDM14)){
				list.add(in_JBDM14.trim());
			}
			String in_JBDM15 = hqmsBaData.getJBDM15();
			if(!StringUtils.isEmpty(in_JBDM15)){
				list.add(in_JBDM15.trim());
			}
			
			if(HqmsRule4.checkHqmsRule11_1(list)){
				if(!HqmsRule4.checkHqmsRule11_2(list)){
					list.remove(in_JBDM);
					boolean tmp = false;
					for(String item : list) {
		    			if(item.startsWith(check_code)){
		    				tmp = true;
		    			}
					}
					flag = tmp;
				}
			}
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("211"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule11数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
