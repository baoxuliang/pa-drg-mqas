package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 非手术治疗项目费必须大于等 于临床物理治疗费 P761≥P762（允许误差1.2）  
 * @author jindoulixing
 *
 */
public class HQMSCheckRule23 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule23.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Double WLZLF = hqmsBaData.getWLZLF();
			Double FSSZLXMF = hqmsBaData.getFSSZLXMF();
			
			WLZLF = null == WLZLF?0:WLZLF;
			FSSZLXMF = null == FSSZLXMF?0:FSSZLXMF;
			
			if(FSSZLXMF < WLZLF){
				if(FSSZLXMF+1.2 < WLZLF){
					flag = false;
				}
			}
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("223"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule23数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
