package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 入院日期≤监护室进入时间≤ 监护室退出时间≤出院日期 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule19 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule19.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("219"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule19数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
