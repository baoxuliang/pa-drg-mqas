package com.kindo.HQMSBaDataCheck.rule;

import java.time.LocalDate;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;
import com.kindo.utils.DateUtil;

/**
 * 年龄等于入院日期减出生日期 （误差范围1岁）
 * @author jindoulixing
 *
 */
public class HQMSCheckRule6 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule6.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Integer in_nl = hqmsBaData.getNL();
	    	Date in_ryrq = hqmsBaData.getRYSJ();
	    	Date in_csrq = hqmsBaData.getCSRQ();
	    	if(null == in_nl){
	    		in_nl = 0;
			}
			/*long from =0l;
			long to = 0l;
			int days = 0;*/
		    if(null !=in_ryrq && null != in_csrq){
		    	/*LocalDate date1 = LocalDate.of(in_ryrq.getYear(), in_ryrq.getMonth(), in_ryrq.getDay());
		    	LocalDate date2 = LocalDate.of(in_csrq.getYear(), in_csrq.getMonth(), in_csrq.getDay());
		    	int l_nl = date2.until(date1).getYears();*/
		    	int l_nl = DateUtil.getAge(in_csrq, in_ryrq);
		    	if((in_nl < l_nl - 1) || (in_nl > l_nl + 1)){
					flag = false;
				}
		    	/*from = in_csrq.getTime();  
				to = in_ryrq.getTime();
				days = (int) ((to - from)/(1000 * 60 * 60 * 24)); 
				float fl = (float)days/365;
				int l_nl = (int) Math.ceil(fl);
				if((in_nl < l_nl - 1) || (in_nl > l_nl + 1)){
					flag = false;
				}*/	
		    }
			
	    	if(!flag){//记录校验失败信息
	    		countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData, HQMSRuleConfiguration.getInstance().getHQMSConfigCache("206"));
	    	}    	
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule6数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
