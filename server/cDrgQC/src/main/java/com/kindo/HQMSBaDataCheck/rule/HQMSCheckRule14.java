package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 抢救次数应等于抢救成功次 数，除主要诊断出院情况、其 他诊断出院情况出现“3 未 愈、4死亡、9其他”、离院方 式出现“4非医嘱离院、 5死亡、 9其他”，抢救次数可以等于抢 救成功次数加1，表示最后一次 抢救未成功 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule14 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule14.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		String lyfs = hqmsBaData.getLYFS();
			
			if(!StringUtils.isEmpty(lyfs)){
				Integer in_qjcs = hqmsBaData.getQJCS();
				Integer in_qjcgcs = hqmsBaData.getCGCS();
				if(null == in_qjcs){
					in_qjcs = 0;
				}
				if(null == in_qjcgcs){
					in_qjcgcs = 0;
				}
				if(lyfs.equals("4") || lyfs.equals("5") || lyfs.equals("9") ){
					if(in_qjcs!=(in_qjcgcs+1) && in_qjcs!=in_qjcgcs){
						flag = false;
					}
				}else{
					if(in_qjcs!=in_qjcgcs){
						flag = false;
					}
				}
			}
    		
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("214"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule14数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
