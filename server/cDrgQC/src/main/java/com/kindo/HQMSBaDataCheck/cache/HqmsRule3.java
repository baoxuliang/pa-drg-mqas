package com.kindo.HQMSBaDataCheck.cache;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 女性诊断编码校 验
 * @author jindoulixing
 *
 */
public class HqmsRule3 {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(HqmsRule3.class);
	
	private final static Set<String> HQMSRule3Cache = new HashSet<String>();
    
    static{
    	HQMSRule3Cache.add("V");
    	HQMSRule3Cache.add("W");
    	HQMSRule3Cache.add("X");
    	HQMSRule3Cache.add("Y");
    }
    
    public static boolean checkHqmsRule9(String key) {
		for (String item : HQMSRule3Cache) {
			if (key.startsWith(item)) {
				return false;
			}
		}
		return true;
    }
    
    public static boolean checkHqmsRule13(String key) {
		for (String item : HQMSRule3Cache) {
			if (key.startsWith(item)) {
				return false;
			}
		}
		return true;
    }
}
