package com.kindo.HQMSBaDataCheck;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;
import com.kindo.HQMSBaDataCheck.dao.HQMSDataCheckMapper;
import com.kindo.HQMSBaDataCheck.model.HQMSRuleConfig;
import com.kindo.HQMSBaDataCheck.model.HqmsBaData;
import com.kindo.HQMSBaDataCheck.model.QCErrResult;
import com.kindo.HQMSBaDataCheck.rule.AbstractHQMSCheckRule;

@Service
@Component
public class HQMSBaDataCheck {
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSBaDataCheck.class);
	private static ExecutorService executor = Executors.newFixedThreadPool(28);
	
	public HQMSBaDataCheck() {
    }
	
	private static HQMSDataCheckMapper hQMSDataCheckMapper;
	@Autowired
    public HQMSBaDataCheck(HQMSDataCheckMapper hQMSDataCheckMapper) {
		HQMSBaDataCheck.hQMSDataCheckMapper = hQMSDataCheckMapper;
    }
	
	private static class SingleTonBuilder {
	   private static HQMSBaDataCheck singleTon = new HQMSBaDataCheck();
	}
	public static HQMSBaDataCheck getInstance() {
	   return SingleTonBuilder.singleTon;
    }
    
    private static Set<AbstractHQMSCheckRule> hqmsRuleConfiguration = null;
	
    private static ConcurrentHashMap<String, StringBuffer> resultMap = null;
    public static void setResultMap(ConcurrentHashMap<String, StringBuffer> resultMap){
   	 	HQMSBaDataCheck.resultMap = resultMap;
    }
    
    private static volatile ConcurrentSkipListSet<QCErrResult>  failList = null;
    public static void setFailList(ConcurrentSkipListSet<QCErrResult>  failList){
   	 	HQMSBaDataCheck.failList = failList;
    }
    
	public static boolean execHQMSBACheck(final HqmsBaData hqmsBaData){
		boolean result = true;
		
		final AtomicInteger countfail = new AtomicInteger(0);
		
		hqmsRuleConfiguration = HQMSRuleConfiguration.getInstance().getHQMSRuleClassCache();
		
		final CountDownLatch latch = new CountDownLatch(hqmsRuleConfiguration.size());
		
		try {
			Iterator<AbstractHQMSCheckRule> it = hqmsRuleConfiguration.iterator();  
			while (it.hasNext()) {  
				AbstractHQMSCheckRule rule = it.next();
				rule.HQMSCheckRule(latch, hqmsBaData, countfail);
				executor.submit(rule);
			}
			
			latch.await();//主线程等待
			
			String HQMS_CHECK = "Y";
			if(countfail.get()>0){//是否需要放回校验结果
				result = false;
				HQMS_CHECK = "N";
			}
			resultMap.get(HQMS_CHECK).append("'").append(hqmsBaData.getID()).append("'").append(",");
			
			//updateHqmsCheckBaData(hqmsBaData.getID(),HQMS_CHECK);
			
		} catch (InterruptedException e) {
			result = false;
			e.printStackTrace();
		}
		
	    //executor.shutdown();
		return result;
	}
	
	public static HQMSDataCheckMapper getHQMSDataCheckMapper(){
		return hQMSDataCheckMapper;
	} 
	
	private static void updateHqmsCheckBaData(String id, String HQMS_CHECK){
		hQMSDataCheckMapper.updateHqmsCheckBaData(id,HQMS_CHECK);
	}
	
	public static void updateBatchHqmsCheckBaData(String IDS, String HQMS_CHECK){
		hQMSDataCheckMapper.updateBatchHqmsCheckBaData(IDS,HQMS_CHECK);
	}
	
	public void refreshCache() {
		HQMSRuleConfiguration.getInstance().refreshHQMSRuleConfiguration();
	}
	
	public static void insertQCErrResult(HqmsBaData hqmsBaData,HQMSRuleConfig hqmsConfig) {
		QCErrResult qCErrResult  = new QCErrResult();
		//qCErrResult.setUID(hqmsBaData.getUID());
		qCErrResult.setMEMBER_CODE(hqmsBaData.getMEMBER_CODE());
		qCErrResult.setBAH(hqmsBaData.getBAH());
		qCErrResult.setZYCS(1);
		qCErrResult.setERR_CODE(hqmsConfig.getErrCode());
		qCErrResult.setERR_NAME(hqmsConfig.getErrName());
		qCErrResult.setERR_MEMO(hqmsConfig.getErrDesc());
		qCErrResult.setERR_TPYE("2");
		qCErrResult.setCHECK_TIME(new Date());
		qCErrResult.setID(hqmsBaData.getID());
		failList.add(qCErrResult);
		/*QCErrResult record = hQMSDataCheckMapper.selectHQMSQCErrResult(hqmsBaData.getID(), hqmsConfig.getErrCode());
		if(record!=null) {
			hQMSDataCheckMapper.updateHQMSQCErrResult(qCErrResult);
		}else {
			hQMSDataCheckMapper.insertHQMSQCErrResult(qCErrResult);
		}*/
	}
	
	public static void deleteBatchQcErrResult(ConcurrentSkipListSet<QCErrResult>  failList){
		hQMSDataCheckMapper.deleteBatchQcErrResult(failList);
	}
	public static void insertBatchQCErrResultDatas(List<QCErrResult>  failList){
		hQMSDataCheckMapper.insertBatchQCErrResultDatas(failList);
	}
	
	public static void main(String[] args) {
		//String jsonStr = "{\"aPPID\":\"aaa\",\"bAH\":\"132019\",\"bA_TYPE\":\"01\",\"bDBLZPF\":0.0,\"bLZDF\":0.0,\"batchNum\":\"aaa201803051721111533\",\"cBCWF\":0.0,\"cBLX_CODE\":\"310\",\"cCHIBM1\":\"XXX00099\",\"cCHIBM2\":\"KM832701\",\"cCHIBM3\":\"HME62201\",\"cCHIBM4\":\"HJM45301\",\"cCHIBM5\":\"HJE48601\",\"cCHIBM6\":\"FJD01601\",\"cCHIBM7\":\"\",\"cCHIBM8\":\"\",\"cCHIMC1\":\"其他非手术治疗\",\"cCHIMC2\":\"压力抗栓治疗\",\"cCHIMC3\":\"经皮穿刺锁骨下静脉置管术\",\"cCHIMC4\":\"经肋间胸腔闭式引流术\",\"cCHIMC5\":\"经支气管镜支气管肺泡灌洗术\",\"cCHIMC6\":\"纤维支气管镜检查\",\"cCHIMC7\":\"\",\"cCHIMC8\":\"\",\"cSRQ\":\"1932-08-02\",\"cWHCF\":0.0,\"cYKB\":\"重症医学科\",\"cYSJ\":\"2018-03-05\",\"cYSJS\":12.0,\"eJHL\":0,\"fSSZLXMF\":17061.0,\"hCYYCLF\":92.95,\"hLF\":3522.0,\"hOS_ID\":\"0101\",\"hXJSYSJ\":442.5,\"iD\":\"cb48a248-d3d5-4f0d-8779-847f4a46010f\",\"kFF\":4392.0,\"kJYWF\":19680.26,\"lCZDXMF\":3756.0,\"lYFS\":5,\"mEDICALFEES\":[],\"mZF\":342.0,\"nL\":86,\"nXYZLZPF\":0.0,\"pTCWF\":0.0,\"qDBLZPF\":0.0,\"qJCGCS\":2,\"qJCS\":2,\"qTF\":0.0,\"qTFY\":80.0,\"rYKB\":\"重症医学科\",\"rYSJ\":\"2018-02-15\",\"rYSJS\":0,\"rYTJ\":1,\"rZCCU\":0.0,\"rZEICU\":0.0,\"rZNICU\":0.0,\"rZPICU\":0.0,\"rZQTJHS\":444.0,\"rZSICU\":0.0,\"sFZY1\":1,\"sFZY10\":1,\"sFZY11\":0,\"sFZY2\":1,\"sFZY3\":1,\"sFZY4\":1,\"sFZY5\":1,\"sFZY6\":1,\"sFZY7\":1,\"sFZY8\":1,\"sFZY9\":1,\"sJHL\":0,\"sJZYTS\":18,\"sSF\":430.0,\"sSZLF\":772.0,\"sYBM\":\"J15.9030101\",\"sYBM1\":\"J80.x000101\",\"sYBM10\":\"K40.9000101\",\"sYBM11\":\"\",\"sYBM12\":\"\",\"sYBM13\":\"\",\"sYBM14\":\"\",\"sYBM15\":\"\",\"sYBM2\":\"R65.2010101\",\"sYBM3\":\"R65.3010101\",\"sYBM4\":\"J90.x000101\",\"sYBM5\":\"D64.9000101\",\"sYBM6\":\"I10.x050301\",\"sYBM7\":\"D69.5010201\",\"sYBM8\":\"L10.9000101\",\"sYBM9\":\"E87.8010101\",\"sYMC\":\"重症肺炎\",\"sYMC1\":\"急性呼吸窘迫综合征\",\"sYMC10\":\"腹股沟疝\",\"sYMC11\":\"\",\"sYMC12\":\"\",\"sYMC13\":\"\",\"sYMC14\":\"\",\"sYMC15\":\"\",\"sYMC2\":\"脓毒性休克（感染性休克）\",\"sYMC3\":\"多器官功能障碍综合症（MODS）\",\"sYMC4\":\"胸腔积液\",\"sYMC5\":\"贫血\",\"sYMC6\":\"高血压病3级（高危）\",\"sYMC7\":\"继发性血小板减少症\",\"sYMC8\":\"天疱疮\",\"sYMC9\":\"电解质紊乱\",\"sYSZDF\":16212.2,\"tJHL\":19,\"uSERNAME\":\"三明市第一医院\",\"wLZLF\":0.0,\"xB\":1,\"xBYZLZPF\":0.0,\"xF\":936.0,\"xSECSTZ\":0,\"xSERYTZ\":0,\"xYF\":35992.72,\"yBLSH\":\"105375722\",\"yCXYYCLF\":74.1,\"yJHL\":0,\"yLFKFS\":1,\"yLFUF\":3045.0,\"yXXZDF\":6430.0,\"yYCLF\":2522.47,\"zCF\":315.0,\"zCYF\":0.0,\"zCYF1\":0.0,\"zFJE\":0.0,\"zFY\":111656.94,\"zKKB\":\"\",\"zKKB2\":\"\",\"zKKB3\":\"\",\"zLCZF\":16768.5,\"zYZLF\":0.0,\"zZJHCWF\":2700.0}";
		//JSONObject jsonobject = JSONObject.parseObject(jsonStr);
		//HqmsBaData data = null;
		//data = ((JSONObject) jsonobject).toJavaObject(HqmsBaData.class);
		//DRGDataCheck.execDRGBACheck(data);
		double s = 12345678.9012;
		System.out.println();
	}

}
