package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 西药费必须大于等于抗菌药物 费用 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule25 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule25.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Double XYF = hqmsBaData.getXYF();
			Double KJYWF = hqmsBaData.getKJYWF();
			XYF = null == XYF?0:XYF;
			KJYWF = null == KJYWF?0:KJYWF;
			
			if((XYF < KJYWF) && ((XYF + 1.2) < KJYWF)){
				flag = false;
			}
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("225"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule25数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
