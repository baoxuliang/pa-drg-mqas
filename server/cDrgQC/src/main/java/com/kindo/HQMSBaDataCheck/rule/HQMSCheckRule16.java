package com.kindo.HQMSBaDataCheck.rule;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 当其它诊断编码为Z37，新生 儿出生体重不能为空
 * @author jindoulixing
 *
 */
public class HQMSCheckRule16 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule16.class);
	private static final String check_code = "Z37";
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
			List<String> list = new ArrayList<String>();
			
			String in_JBDM1 = hqmsBaData.getJBDM1();
			if(null!=in_JBDM1 && !in_JBDM1.equals("")){
				list.add(in_JBDM1.trim());
			}
			String in_JBDM2 = hqmsBaData.getJBDM2();
			if(null!=in_JBDM2 && !in_JBDM2.equals("")){
				list.add(in_JBDM2.trim());
			}
			String in_JBDM3 = hqmsBaData.getJBDM3();
			if(null!=in_JBDM3 && !in_JBDM3.equals("")){
				list.add(in_JBDM3.trim());
			}
			String in_JBDM4 = hqmsBaData.getJBDM4();
			if(null!=in_JBDM4 && !in_JBDM4.equals("")){
				list.add(in_JBDM4.trim());
			}
			String in_JBDM5 = hqmsBaData.getJBDM5();
			if(!StringUtils.isEmpty(in_JBDM5)){
				list.add(in_JBDM5.trim());
			}
			String in_JBDM6 = hqmsBaData.getJBDM6();
			if(!StringUtils.isEmpty(in_JBDM6)){
				list.add(in_JBDM6.trim());
			}
			String in_JBDM7 = hqmsBaData.getJBDM7();
			if(!StringUtils.isEmpty(in_JBDM7)){
				list.add(in_JBDM7.trim());
			}
			String in_JBDM8 = hqmsBaData.getJBDM8();
			if(!StringUtils.isEmpty(in_JBDM8)){
				list.add(in_JBDM8.trim());
			}
			String in_JBDM9 = hqmsBaData.getJBDM9();
			if(!StringUtils.isEmpty(in_JBDM9)){
				list.add(in_JBDM9.trim());
			}
			String in_JBDM10 = hqmsBaData.getJBDM10();
			if(!StringUtils.isEmpty(in_JBDM10)){
				list.add(in_JBDM10.trim());
			}
			String in_JBDM11 = hqmsBaData.getJBDM11();
			if(!StringUtils.isEmpty(in_JBDM11)){
				list.add(in_JBDM11.trim());
			}
			String in_JBDM12 = hqmsBaData.getJBDM12();
			if(!StringUtils.isEmpty(in_JBDM12)){
				list.add(in_JBDM12.trim());
			}
			String in_JBDM13 = hqmsBaData.getJBDM13();
			if(!StringUtils.isEmpty(in_JBDM13)){
				list.add(in_JBDM13.trim());
			}
			String in_JBDM14 = hqmsBaData.getJBDM14();
			if(!StringUtils.isEmpty(in_JBDM14)){
				list.add(in_JBDM14.trim());
			}
			String in_JBDM15 = hqmsBaData.getJBDM15();
			if(!StringUtils.isEmpty(in_JBDM15)){
				list.add(in_JBDM15.trim());
			}
			
			for (String item : list) {
				if (item.startsWith(check_code)) {
					String XSECSTZ_1 = hqmsBaData.getXSECSTZ_1();
					String XSECSTZ_2 = hqmsBaData.getXSECSTZ_2();
					String XSECSTZ_3 = hqmsBaData.getXSECSTZ_3();
					String XSECSTZ_4 = hqmsBaData.getXSECSTZ_4();
					String XSECSTZ_5 = hqmsBaData.getXSECSTZ_5();
					 
					if((null == XSECSTZ_1 || XSECSTZ_1.equals("")) || XSECSTZ_1.equals("0")){
						flag = false;
					}else{
						String[] stra = new String[] {XSECSTZ_2,XSECSTZ_3,XSECSTZ_4,XSECSTZ_5};
						for(String str : stra){
							if(null != str){
								if(str.equals("") || str.equals("0")){
									flag = false;
								}
							}
						}
					}
				}
			}
    		
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("216"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule16数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
