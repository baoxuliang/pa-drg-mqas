package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;
import com.kindo.HQMSBaDataCheck.cache.HqmsRule3;

/**
 * ICD-10编码范围：V、W、X、 Y开头的 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule13 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule13.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		String H23 = hqmsBaData.getH23();
    		
    		if(!StringUtils.isEmpty(H23)){
    			flag = !HqmsRule3.checkHqmsRule13(H23);
    		}
    		
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("213"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule13数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
