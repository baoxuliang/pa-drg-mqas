package com.kindo.HQMSBaDataCheck.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 住院总费用必须大于等于住院 总费用其中自付金额 P782≥P751（允许误差1.2） 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule22 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule22.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Double in_ZFY = hqmsBaData.getZFY();
			Double in_ZFJE = hqmsBaData.getZFJE();
			
			in_ZFY = null == in_ZFY?0:in_ZFY;
			in_ZFJE = null == in_ZFJE?0:in_ZFJE;
			
			if(in_ZFY < in_ZFJE){
				if(in_ZFY+1.2 < in_ZFJE){
					flag = false;
				}
			}
			
			if (!flag) {//记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("222"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule22数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
