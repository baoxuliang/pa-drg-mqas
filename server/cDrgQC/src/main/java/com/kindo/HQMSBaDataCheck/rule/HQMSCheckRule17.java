package com.kindo.HQMSBaDataCheck.rule;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * （年龄不足1周岁的）年龄≤28 天，或入院日期减出生日期 ≤28天时，新生儿入院体重不 能为空
 * @author jindoulixing
 *
 */
public class HQMSCheckRule17 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule17.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
    		Double in_bzyzsnl = hqmsBaData.getBZYZSNL();
    		Date ryrq = hqmsBaData.getRYSJ();
	    	Date csrq = hqmsBaData.getCSRQ();
	    	
			if((null != in_bzyzsnl && in_bzyzsnl > 0)){
				if((in_bzyzsnl*30) <= 28){
					String in_xserytz = hqmsBaData.getXSERYTZ();
					if((null == in_xserytz || in_xserytz.equals("")) || in_xserytz.equals("0")){
						flag = false;
					}
				}
			}else if(null != ryrq && null != csrq){
				long from = csrq.getTime();  
				long to = ryrq.getTime();
				int days = (int) ((to - from)/(1000 * 60 * 60 * 24)); 
				if(days <= 28){
					String in_xserytz = hqmsBaData.getXSERYTZ();
					if((null == in_xserytz || in_xserytz.equals("")) || in_xserytz.equals("0")){
						flag = false;
					}
				}
			}
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("217"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule17数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
