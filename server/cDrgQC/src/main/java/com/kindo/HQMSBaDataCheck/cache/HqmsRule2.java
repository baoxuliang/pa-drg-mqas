package com.kindo.HQMSBaDataCheck.cache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.model.Rule;
/**
 * 女性诊断编码校 验
 * @author jindoulixing
 *
 */
public class HqmsRule2 {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(HqmsRule2.class);
	
	private final static Set<String> HQMSRule2Cache = new HashSet<String>();
    
    private static void refreshCache() {
    	
    	List<Rule> list =  HQMSBaDataCheck.getHQMSDataCheckMapper().queryHqmsRule2();
    	
		for (Rule item : list) {
			HQMSRule2Cache.add(item.getZdbm());
		}
    	    
    }
    
    private void cleanupCache() {
    	HQMSRule2Cache.clear();
    }
    
    public void refreshHQMSRuleConfiguration() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static Set<String> getHQMSRule2Cache(){
    	if(HQMSRule2Cache ==null || HQMSRule2Cache.size() == 0){
    		refreshCache();
    	}
    	return HQMSRule2Cache;
    }
    
    public static boolean checkHqmsRule2(String key) {
    	if(HQMSRule2Cache ==null || HQMSRule2Cache.size() == 0){
    		refreshCache();
    	}
		for (String item : HQMSRule2Cache) {
			if (key.startsWith(item)) {
				return false;
			}
		}
		return true;
    }
}
