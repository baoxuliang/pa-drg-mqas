package com.kindo.HQMSBaDataCheck.dao;

import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import org.apache.ibatis.annotations.Param;

import com.kindo.HQMSBaDataCheck.model.HQMSRuleConfig;
import com.kindo.HQMSBaDataCheck.model.QCErrResult;
import com.kindo.HQMSBaDataCheck.model.Rule;

public interface HQMSDataCheckMapper {
	
	List<Rule> queryHqmsRule1();
	
	List<Rule> queryHqmsRule2();
	
	List<HQMSRuleConfig> queryHQMSRuleConfiguration();

    int insertHQMSQCErrResult(QCErrResult qCErrResult);

    QCErrResult selectHQMSQCErrResult(@Param("ID") String ID, @Param("ERR_CODE") String ERR_CODE);

    int updateHQMSQCErrResult(QCErrResult qCErrResult);
    
    int updateHqmsCheckBaData(@Param("id")String id, @Param("HQMS_CHECK")String HQMS_CHECK);
    
    int updateBatchHqmsCheckBaData(@Param("IDS")String IDS, @Param("HQMS_CHECK")String HQMS_CHECK);
    
    int deleteBatchQcErrResult(@Param("list")ConcurrentSkipListSet<QCErrResult>  failList);
    
    int insertBatchQCErrResultDatas(@Param("list")List<QCErrResult>  failList);
    
}