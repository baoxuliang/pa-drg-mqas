package com.kindo.HQMSBaDataCheck.rule;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;

/**
 * 入院日期≤出院日期； 入院日期≤入院后确诊日期≤ 出院日期； 手术操作日期≥入院日期-1天 手术操作日期≤出院日期； 
 * @author jindoulixing
 *
 */
public class HQMSCheckRule8 extends AbstractHQMSCheckRule {  
	private static final Logger LOGGER = LoggerFactory.getLogger(HQMSCheckRule8.class);
	
    @Override  
    public void run() {
    	boolean flag = true;
    	try {
	    	Date ryrq = hqmsBaData.getRYSJ();
	    	Date cysj = hqmsBaData.getCYSJ();
	    	
	    	if (ryrq.getTime() > cysj.getTime()) {
				flag = false;
			}
	    	Date RYQZRQ = hqmsBaData.getRYQZRQ();
	    	if(null != RYQZRQ){
	    		if(ryrq.getTime() > RYQZRQ.getTime() || RYQZRQ.getTime() > cysj.getTime()){
	    			flag = false;
	    		}
	    	}
	    	
			long rytime = 0l;
			long cytime = 0l;
			int rydays = 0;
			rytime = ryrq.getTime();
			cytime = cysj.getTime();
			rydays = (int) ((rytime)/(1000 * 60 * 60 * 24)); 
			if (flag) {
				Date SSJCZRQ1 = hqmsBaData.getSSJCZRQ1();
				if (null != SSJCZRQ1) {
					long sscztime = SSJCZRQ1.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ2 = hqmsBaData.getSSJCZRQ2();
				if (null != SSJCZRQ2) {
					long sscztime = SSJCZRQ2.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ3 = hqmsBaData.getSSJCZRQ3();
				if (null != SSJCZRQ3) {
					long sscztime = SSJCZRQ3.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ4 = hqmsBaData.getSSJCZRQ4();
				if (null != SSJCZRQ4) {
					long sscztime = SSJCZRQ4.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ5 = hqmsBaData.getSSJCZRQ5();
				if (null != SSJCZRQ5) {
					long sscztime = SSJCZRQ5.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ6 = hqmsBaData.getSSJCZRQ6();
				if (null != SSJCZRQ6) {
					long sscztime = SSJCZRQ6.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ7 = hqmsBaData.getSSJCZRQ7();
				if (null != SSJCZRQ7) {
					long sscztime = SSJCZRQ7.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (flag) {
				Date SSJCZRQ8 = hqmsBaData.getSSJCZRQ8();
				if (null != SSJCZRQ8) {
					long sscztime = SSJCZRQ8.getTime();
					int ssczdays = (int) ((sscztime) / (1000 * 60 * 60 * 24));
					if (ssczdays < rydays - 1 || sscztime > cytime) {
						flag = false;
					}
				}
			}
			if (!flag) {// 记录校验失败信息
				countfail.addAndGet(1);
				HQMSBaDataCheck.insertQCErrResult(hqmsBaData,HQMSRuleConfiguration.getInstance().getHQMSConfigCache("208"));
			}
	    	
		} catch (Exception e) {
			LOGGER.error("HQMSCheckRule8数据校验异常,数据:"+hqmsBaData.toString()+"异常信息"+e.getMessage());
		}
    	
        latch.countDown();  
    }  
      
}  
