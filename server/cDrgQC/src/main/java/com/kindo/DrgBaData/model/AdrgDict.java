package com.kindo.DrgBaData.model;

import lombok.Data;

@Data
public class AdrgDict {
	
	private String MDC_CODE;
	private String MDC_NAME;
	private String ADRG_CODE;
	private String ADRG_NAME;
}
