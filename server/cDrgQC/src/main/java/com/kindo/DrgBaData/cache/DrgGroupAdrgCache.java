package com.kindo.DrgBaData.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DrgBaData.model.AdrgDict;
import com.kindo.DrgBaData.service.DrgBaDataService;

public class DrgGroupAdrgCache {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgGroupAdrgCache.class);
	
	private final static Map<String, String> adrgNameCache = new ConcurrentHashMap<>();
	private final static Map<String, String> mdcNameCache = new ConcurrentHashMap<>();
	
    /*private static DrgGroupCache instance = null;
    
    public static DrgGroupCache getInstance() {
        if (instance == null) {
            synchronized (DrgGroupCache.class) {
                if (instance == null) {
                    instance = new DrgGroupCache();
                }
            }
        }
        return instance;
    }*/
    
    private static void refreshCache() {
    	List<AdrgDict> list = DrgBaDataService.getBaDataCheckMapper().queryADrgDict();
    	
		for (AdrgDict item : list) {
			adrgNameCache.put(item.getADRG_CODE(), item.getADRG_NAME());
			mdcNameCache.put(item.getMDC_CODE(), item.getMDC_NAME());
		}
    	    
    }
    
    private void cleanupCache() {
    	adrgNameCache.clear();
    	mdcNameCache.clear();
    }
    
    public void refreshDrgGroupCache() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static String getADrgName(String key){
    	if(adrgNameCache ==null || adrgNameCache.size() == 0){
    		refreshCache();
    	}
    	return adrgNameCache.get(key);
    }
    
    public static String getMdcName(String key){
    	if(mdcNameCache ==null || mdcNameCache.size() == 0){
    		refreshCache();
    	}
    	return mdcNameCache.get(key);
    }
}
