package com.kindo.DrgBaData.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kindo.DrgBaData.model.DrgDict;
import com.kindo.DrgBaData.service.DrgBaDataService;

public class DrgGroupCache {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgGroupCache.class);
	
	private final static Map<String, String> drgCache = new ConcurrentHashMap<>();
	
    /*private static DrgGroupCache instance = null;
    
    public static DrgGroupCache getInstance() {
        if (instance == null) {
            synchronized (DrgGroupCache.class) {
                if (instance == null) {
                    instance = new DrgGroupCache();
                }
            }
        }
        return instance;
    }*/
    
    private static void refreshCache() {
    	List<DrgDict> list = DrgBaDataService.getBaDataCheckMapper().queryDrgDictData();
    	
		for (DrgDict item : list) {
			drgCache.put(item.getDRGCODE(), item.getDRGNAME());
		}
    	    
    }
    
    private void cleanupCache() {
    	drgCache.clear();
    }
    
    public void refreshDrgGroupCache() {
    	cleanupCache();
    	refreshCache();
    }
    
    public static String getDrgName(String key){
    	if(drgCache ==null || drgCache.size() == 0){
    		refreshCache();
    	}
    	return drgCache.get(key);
    }
}
