package com.kindo.DrgBaData.util;

import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kindo.DrgBaData.cache.DrgGroupAdrgCache;
import com.kindo.DrgBaData.cache.DrgGroupCache;
import com.kindo.DrgBaData.model.DrgGroupAudit;
import com.kindo.DrgBaData.model.DrgGroupBaData;

@Configurable
//@PropertySource("classpath:application.properties")
@Component
public class CDrgRRestUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(CDrgRRestUtil.class);
	
	private static String rGroupUrl;
	
	@Value("${rest.rGroupUrl}")
	public void setRGroupUrl(String rGroupUrl) {
		CDrgRRestUtil.rGroupUrl = rGroupUrl;
	}

	public static ClientHttpRequestFactory getClientHttpRequestFactory() {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setConnectTimeout(3000);
		factory.setReadTimeout(30000);
		return factory;
	}

	//国家平台手动分组(启用)
	public static void RGroup(List<DrgGroupBaData> dataList,List<DrgGroupAudit> drgResultList) throws UnsupportedEncodingException {
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Accept", "application/json; charset=UTF-8");
		headers.add("Content-Encoding", "UTF-8");
		headers.add("Content-Type", "application/json; charset=UTF-8");

		JSONObject rData = transGroupData(dataList);
		LOGGER.info("执行自动分组，调用分组器请求数据"+rData);
		HttpEntity<String> formEntity = new HttpEntity<String>(rData.toString(), headers);
		String retStr = restTemplate.postForObject(rGroupUrl, formEntity, String.class);
		String respStr = new String(retStr.getBytes("ISO-8859-1"), "UTF8");
		LOGGER.info("执行自动分组，调用分组器返回结果"+respStr);
		if (!StringUtils.isEmpty(respStr)) {
			JSONArray arr = JSONArray.parseArray(respStr);
			for (Object resultObj : arr) {
				JSONObject result = JSONObject.parseObject(resultObj.toString());
				DrgGroupAudit drgResult = new DrgGroupAudit();
				drgResult.setID(result.getString("id"));
				//drgResult.setUID(result.getString("uid"));
				drgResult.setBAH(result.getString("bah"));
				drgResult.setMEMBER_CODE(result.getString("hosId"));
				drgResult.setMEMBER_NAME(result.getString("userName"));
				drgResult.setGENERATE_DATE(result.getDate("generateDate"));
				drgResult.setZYCS(result.getInteger("zycs"));
				drgResult.setAUDIT_FLAG(0);
				drgResult.setMNL_DRG(0);
				if (!StringUtils.isEmpty(result.getString("DRGCode"))) {
					drgResult.setDRG_CODE(result.getString("DRGCode"));
					drgResult.setDRG_NAME(DrgGroupCache.getDrgName(result.getString("DRGCode")));
					drgResult.setADRG_CODE(result.getString("ADRGCode"));
					drgResult.setADRG_NAME(DrgGroupAdrgCache.getADrgName(result.getString("ADRGCode")));
					drgResult.setMDC_CODE(result.getString("MDCCode"));
					drgResult.setMDC_NAME(DrgGroupAdrgCache.getMdcName(result.getString("MDCCode")));
					drgResult.setRWT(result.getDouble("RWT"));
					drgResult.setIN_DRG(1);
				} else {
					drgResult.setIN_DRG(0);
					drgResult.setMSG(result.getString("Err"));
				}
				drgResultList.add(drgResult);
			}
		}
	}

	public static JSONObject transGroupData(List<DrgGroupBaData> dataList) {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.HALF_UP);
		JSONObject rRet = new JSONObject();
		JSONArray rArr = new JSONArray();
		for (DrgGroupBaData data : dataList) {
			JSONObject rData = new JSONObject();
			rData.put("id", data.getID());
			//rData.put("uid", data.getUID());
			rData.put("bah", data.getBAH());
			rData.put("rysj", data.getRYSJ());
			rData.put("hosId", data.getMEMBER_CODE());
			rData.put("userName", data.getMEMBER_NAME());
			rData.put("generateDate", data.getSYNDATE());
			rData.put("zycs", data.getZYCS());
			rData.put("sex", data.getXB() == null ? "NA" : data.getXB());
			if (data.getBZYZSNL() == null || data.getBZYZSNL() <= 0) {
				rData.put("age", data.getNL() == null ? "NA" : data.getNL());
			} else {
				if (data.getBZYZSNL() < 0) {
					rData.put("age", "NA");
				}
				if (data.getBZYZSNL() > 0) {
					rData.put("age", df.format(data.getBZYZSNL() / 12));
				}
			}
			rData.put("LOS", data.getSJZYTS() == null ? "NA" : data.getSJZYTS());
			rData.put("bornWt", data.getXSECSTZ_1() == null || data.getXSECSTZ_1().equals("") ? "NA" : data.getXSECSTZ_1());
			rData.put("PDX", StringUtils.isEmpty(data.getJBDM()) ? "NA" : data.getJBDM());
			rData.put("ADX1", (StringUtils.isEmpty(data.getJBDM1())) ? "NA" : data.getJBDM1());
			rData.put("ADX2", (StringUtils.isEmpty(data.getJBDM2())) ? "NA" : data.getJBDM2());
			rData.put("ADX3", (StringUtils.isEmpty(data.getJBDM3())) ? "NA" : data.getJBDM3());
			rData.put("ADX4", (StringUtils.isEmpty(data.getJBDM4())) ? "NA" : data.getJBDM4());
			rData.put("ADX5", (StringUtils.isEmpty(data.getJBDM5())) ? "NA" : data.getJBDM5());
			rData.put("ADX6", (StringUtils.isEmpty(data.getJBDM6())) ? "NA" : data.getJBDM6());
			rData.put("ADX7", (StringUtils.isEmpty(data.getJBDM7())) ? "NA" : data.getJBDM7());
			rData.put("ADX8", (StringUtils.isEmpty(data.getJBDM8())) ? "NA" : data.getJBDM8());
			rData.put("ADX9", (StringUtils.isEmpty(data.getJBDM9())) ? "NA" : data.getJBDM9());
			rData.put("ADX10", (StringUtils.isEmpty(data.getJBDM10())) ? "NA" : data.getJBDM10());
			rData.put("ADX11", (StringUtils.isEmpty(data.getJBDM11())) ? "NA" : data.getJBDM11());
			rData.put("ADX12", (StringUtils.isEmpty(data.getJBDM12())) ? "NA" : data.getJBDM12());
			rData.put("ADX13", (StringUtils.isEmpty(data.getJBDM13())) ? "NA" : data.getJBDM13());
			rData.put("ADX14", (StringUtils.isEmpty(data.getJBDM14())) ? "NA" : data.getJBDM14());
			rData.put("ADX15", (StringUtils.isEmpty(data.getJBDM15())) ? "NA" : data.getJBDM15());
			rData.put("PROC1", StringUtils.isEmpty(data.getSSJCZBM1()) ? "NA" : data.getSSJCZBM1());
			rData.put("PROC2", StringUtils.isEmpty(data.getSSJCZBM2()) ? "NA" : data.getSSJCZBM2());
			rData.put("PROC3", StringUtils.isEmpty(data.getSSJCZBM3()) ? "NA" : data.getSSJCZBM3());
			rData.put("PROC4", StringUtils.isEmpty(data.getSSJCZBM4()) ? "NA" : data.getSSJCZBM4());
			rData.put("PROC5", StringUtils.isEmpty(data.getSSJCZBM5()) ? "NA" : data.getSSJCZBM5());
			rData.put("PROC6", StringUtils.isEmpty(data.getSSJCZBM6()) ? "NA" : data.getSSJCZBM6());
			rData.put("PROC7", StringUtils.isEmpty(data.getSSJCZBM7()) ? "NA" : data.getSSJCZBM7());
			rData.put("PROC8", StringUtils.isEmpty(data.getSSJCZBM8()) ? "NA" : data.getSSJCZBM8());
			rData.put("statusOut", data.getLYFS() == null ? "NA" : data.getLYFS());
			rArr.add(rData);
		}
		rRet.put("data", rArr);
		return rRet;
	}

}
