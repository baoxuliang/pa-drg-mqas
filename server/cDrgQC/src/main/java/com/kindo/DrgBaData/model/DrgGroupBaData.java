package com.kindo.DrgBaData.model;

import java.util.Date;

import lombok.Data;

@Data
public class DrgGroupBaData {

	private static final long serialVersionUID = -1429544213057969761L;
	
	private String ID;
	private String UID;
	private String BAH;
	private Date SYNDATE;
	private Date RYSJ;
	private String MEMBER_CODE;
	private String MEMBER_NAME;
	private String XB;
	private Integer NL;
	private Double BZYZSNL;
	private String SJZYTS;
	//private String XSECSTZ;
	private String XSECSTZ_1;
	private String XSECSTZ_2;
	private String XSECSTZ_3;
	private String XSECSTZ_4;
	private String XSECSTZ_5;
	
	private String JBDM;
	private String JBDM1;
	private String JBDM2;
	private String JBDM3;
	private String JBDM4;
	private String JBDM5;
	private String JBDM6;
	private String JBDM7;
	private String JBDM8;
	private String JBDM9;
	private String JBDM10;
	private String JBDM11;
	private String JBDM12;
	private String JBDM13;
	private String JBDM14;
	private String JBDM15;
	private String SSJCZBM1;
	private String SSJCZBM2;
	private String SSJCZBM3;
	private String SSJCZBM4;
	private String SSJCZBM5;
	private String SSJCZBM6;
	private String SSJCZBM7;
	private String SSJCZBM8;
	private String LYFS;
	private Integer ZYCS;
	
}
