package com.kindo.DrgBaData.api;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DRGDataCheck.cache.DrgRuleConfiguration;
import com.kindo.DRGDataCheck.model.DrgCheckBaData;
import com.kindo.DrgBaData.model.DrgGroupBaData;
import com.kindo.DrgBaData.model.QcCheckConfig;
import com.kindo.DrgBaData.service.DrgBaDataService;
import com.kindo.HQMSBaDataCheck.cache.HQMSRuleConfiguration;
import com.kindo.HQMSBaDataCheck.model.HqmsBaData;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;

@RestController
@RequestMapping("/nologin/drgcheck")
public class DrgBaDataApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataApi.class);
	private static final int BATCH_SIZE = 1000;
	
	@Autowired
	private DrgBaDataService drgBaDataService;
	
	/**
	 * drg数据清洗,调用存储过程("0 15 10 * * ? *":每天上午的10点15触发)
	 * @return
	 */
	@RequestMapping(value = "/dataTransfer/v1", method = RequestMethod.GET)
	@Scheduled(cron = "0 5 * * * ?")
	public void drgDataTransfer() {
		try {
			drgBaDataService.execDataTransfer();
		} catch (Exception e) {
			LOGGER.error("drgDataTransfer存储过程调用异常:"+e.getMessage());
		}
	}
	
	/**
	 * HQMS校验
	 * 1000条提交一次事物
	 * @return
	 */
	@RequestMapping(value = "/hqms/v1", method = RequestMethod.GET)
	@Scheduled(cron = "0 35 * * * ?")
	public void hqmsBatchCheck() {
		try {
			List<HqmsBaData> ls = drgBaDataService.queryHqmsBaData();
			int[] idx = { 0 };
			ls.stream().collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
			.forEach(item -> {
				try {
					drgBaDataService.executeHqmsBatchCheck(item);
				} catch (Exception e) {
					LOGGER.error("hqmsBatchCheck批量执行校验异常:"+e.getMessage());
				}
			});
			
		} catch (Exception e) {
			LOGGER.error("hqmsBatchCheck执行校验异常:"+e.getMessage());
		}
	}

	/**
	 * DRG校验
	 * 1000条提交一次事物
	 * @return
	 */
	@RequestMapping(value = "/cdrg/v1", method = RequestMethod.GET)
	@Scheduled(cron = "0 45 * * * ?")
	public void cdrgBatchCheck() {
		try {
			List<DrgCheckBaData> list = drgBaDataService.queryDrgBaData();
			int[] idx = { 0 };
			list.stream().collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
			.forEach(item -> {
				try {
					drgBaDataService.executeDrgBatchCheck(item);
				} catch (Exception e) {
					LOGGER.error("cdrgBatchCheck批量执行校验异常:"+e.getMessage());
				}
			});
		} catch (Exception e) {
			LOGGER.error("cdrgBatchCheck执行校验异常:"+e.getMessage());
		}
	}
	
	/**
	 * drg入组校验
	 * 1000条一次事物提交
	 * @return
	 */
	@RequestMapping(value = "/group/v1", method = RequestMethod.GET)
	@Scheduled(cron = "0 55 * * * ?")
	public void cdrgGroup() {
		List<DrgGroupBaData> dataList = drgBaDataService.queryDrgGroupBaData();
		try {
			int total = 0;
			// 获取需要拆分的List个数 
			int loopCount = (dataList.size() % BATCH_SIZE == 0) ? (dataList.size() / BATCH_SIZE) : ((dataList.size() / BATCH_SIZE) + 1); 
			// 开始拆分 
			for (int i = 0; i < loopCount; i++) { 
				// 子List的起始值 
				int startNum = i * BATCH_SIZE; 
				// 子List的终止值 
				int endNum = (i + 1) * BATCH_SIZE; 
				// 不能整除的时候最后一个List的终止值为原始List的最后一个 
				if (i == loopCount - 1) { 
					endNum = dataList.size(); 
				} 
				// 拆分List 
				total += drgBaDataService.executeRDrgGroup(dataList.subList(startNum, endNum));
			}
		} catch (Exception e) {
			LOGGER.error("cdrgGroup批量执行自动分组异常:"+e.getMessage());
		} 
	}
	
	@RequestMapping(value = "/config/query", method = RequestMethod.GET)
	public ApiResult getQcCheckConfig(String errType) {
		
		List<QcCheckConfig> list = drgBaDataService.queryQcCheckConfig(errType);
		
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
	}
	
	@RequestMapping(value = "/config/update", method = RequestMethod.POST)
	public ApiResult updateQcCheckConfig(@RequestBody QcCheckConfig config) {
		//String id,String errCode,String errName,String errType,String errDesc,String errMemo,String enable
		int num = drgBaDataService.updateQcCheckConfig(config);
		if (num >0) {
			String type = config.getErrType();
			if(type.equals("2")){//刷新缓存
				HQMSRuleConfiguration.getInstance().refreshHQMSRuleConfiguration();
			}else if(type.equals("3")){
				DrgRuleConfiguration.getInstance().refreshDrgRuleConfiguration();
			}
			return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.UPDATE_SUCCESS,null);	
		}else{
			return new ApiResult(Constants.RESULT.FAIL, Constants.RESULT_MSG.UPDATE_FAIL,null);	
		}
	}
	
	/**
	 * drg数据清洗,调用存储过程("0 15 04 * * ? *":每天上午的4点15触发)
	 * @return
	 */
	@RequestMapping(value = "/dataClear/v1", method = RequestMethod.GET)
	@Scheduled(cron = "0 15 04 ? * *")
	public void drgDataClear() {
		try {
			drgBaDataService.execDrgDataClear();
		} catch (Exception e) {
			LOGGER.error("drgDataClear存储过程调用异常:"+e.getMessage());
		}
	}
}
