package com.kindo.DrgBaData.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DRGDataCheck.model.DrgCheckBaData;
import com.kindo.DrgBaData.model.AdrgDict;
import com.kindo.DrgBaData.model.DrgDict;
import com.kindo.DrgBaData.model.DrgGroupAudit;
import com.kindo.DrgBaData.model.DrgGroupBaData;
import com.kindo.DrgBaData.model.QcCheckConfig;
import com.kindo.HQMSBaDataCheck.model.HqmsBaData;

public interface BaDataCheckMapper {
	
	List<HqmsBaData> queryHqmsBaData();
	
	List<DrgCheckBaData> queryDrgBaData();
	
	List<DrgGroupBaData> queryDrgGroupBaData();
	
	void execDataTransfer();
	
	int insertBatchDrgResult(@Param("list")List<DrgGroupAudit> list);
	
	List<DrgDict> queryDrgDictData();
	List<AdrgDict> queryADrgDict();
	
	int updateIfBaInfoDrgStutasByIds(@Param("DRG_STATE")String DRG_STATE,@Param("IDS")String IDS);
	
	List<QcCheckConfig> selectDrgCheckConfig();
	List<QcCheckConfig> selectHqmsCheckConfig();
	
	int updateDrgCheckConfig(@Param("config")QcCheckConfig config);
	int udateHqmsCheckConfig(@Param("config")QcCheckConfig config);
	
	void execDrgDataClear();
}