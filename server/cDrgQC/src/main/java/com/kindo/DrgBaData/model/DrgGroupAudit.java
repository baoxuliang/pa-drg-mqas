package com.kindo.DrgBaData.model;

import java.util.Date;

import lombok.Data;

@Data
public class DrgGroupAudit {
	private static final long serialVersionUID = -1429544213057969761L;
	
	private String ID;
	private String UID;
	private String MEMBER_CODE;
	private String MEMBER_NAME;
	private String BAH;
	private Integer ZYCS;
	private String DRG_CODE;
	private String DRG_NAME;
	private String ADRG_CODE;
	private String ADRG_NAME;
	private String MDC_CODE;
	private String MDC_NAME;
	private Double RWT;
	private Date GENERATE_DATE;
	private Integer IN_DRG;
	private Integer AUDIT_FLAG;
	private Integer MNL_DRG;
	private String MSG;
}
