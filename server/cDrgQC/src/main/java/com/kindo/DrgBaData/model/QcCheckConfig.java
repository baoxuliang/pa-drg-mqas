package com.kindo.DrgBaData.model;

import lombok.Data;

@Data
public class QcCheckConfig {
	private static final long serialVersionUID = -1429544213057969761L;
	
	private String id;
	private String errCode;
	private String errName;
	private String errType;
	private String errDesc;
	private String errMemo;
	private String enable;
}
