package com.kindo.DrgBaData.model;

import lombok.Data;

@Data
public class DrgDict {
	
	private String DRGCODE;
	private String DRGNAME;
}
