package com.kindo.DrgBaData.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.alibaba.fastjson.JSON;
import com.kindo.DRGDataCheck.DRGDataCheck;
import com.kindo.DRGDataCheck.model.DrgCheckBaData;
import com.kindo.DrgBaData.dao.BaDataCheckMapper;
import com.kindo.DrgBaData.model.DrgGroupAudit;
import com.kindo.DrgBaData.model.DrgGroupBaData;
import com.kindo.DrgBaData.model.QcCheckConfig;
import com.kindo.DrgBaData.util.CDrgRRestUtil;
import com.kindo.HQMSBaDataCheck.HQMSBaDataCheck;
import com.kindo.HQMSBaDataCheck.model.HqmsBaData;
import com.kindo.HQMSBaDataCheck.model.QCErrResult;

@Service
public class DrgBaDataService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataService.class);
	private static final int BATCH_SIZE = 1000;
	
	
	private static BaDataCheckMapper baDataCheckMapper;
	@Autowired
	public DrgBaDataService(BaDataCheckMapper baDataCheckMapper) {
		DrgBaDataService.baDataCheckMapper = baDataCheckMapper;
    }
	
	@Autowired
	private DRGDataCheck dRGDataCheck;
	
	public List<HqmsBaData> queryHqmsBaData(){
		return baDataCheckMapper.queryHqmsBaData();
		
	}
	
	public List<DrgCheckBaData> queryDrgBaData(){
		return baDataCheckMapper.queryDrgBaData();
	}
	
	public static BaDataCheckMapper getBaDataCheckMapper(){
		return baDataCheckMapper;
	}
	
	public List<DrgGroupBaData> queryDrgGroupBaData(){
		return baDataCheckMapper.queryDrgGroupBaData();
	}
	
	public void execDataTransfer(){
		baDataCheckMapper.execDataTransfer();
	}
	
	@Transactional(value="transactionManager2")
	public int executeRDrgGroup(List<DrgGroupBaData> list) throws Exception {
		try {
			List<DrgGroupAudit> drgResultList = new ArrayList<DrgGroupAudit>();

			CDrgRRestUtil.RGroup(list, drgResultList);
			LOGGER.info("调用分组器返回数据：" + JSON.toJSONString(drgResultList));
			
			StringBuffer ingroup = new StringBuffer();
			StringBuffer nogroup = new StringBuffer();
			
			drgResultList.stream().forEach(item -> {
			    if(item.getIN_DRG()== 1){
					ingroup.append("'").append(item.getID()).append("'").append(",");
				}else if(item.getIN_DRG() == 0){
					nogroup.append("'").append(item.getID()).append("'").append(",");
				}
			});
			
			baDataCheckMapper.insertBatchDrgResult(drgResultList);
			
			if(ingroup.length()>0){
				baDataCheckMapper.updateIfBaInfoDrgStutasByIds("1",ingroup.substring(0, ingroup.length()-1).toString());
			}
			if(nogroup.length()>0){
				baDataCheckMapper.updateIfBaInfoDrgStutasByIds("0",nogroup.substring(0, nogroup.length()-1).toString());
			}
			
		} catch (Exception e) {
			LOGGER.info("批量自动分组异常：" + e.getMessage());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return list.size();
	}
	
	/**
	 * HQMS校验
	 * @throws Exception
	 */
	@Transactional(value="transactionManager2")
	public void executeHqmsBatchCheck(List<HqmsBaData> list) throws Exception {
		try {
			ConcurrentHashMap<String, StringBuffer> resultMap = new ConcurrentHashMap<String, StringBuffer>(){{
				put("Y", new StringBuffer());  
				put("N", new StringBuffer());
			}};
			HQMSBaDataCheck.setResultMap(resultMap);
			ConcurrentSkipListSet<QCErrResult> failList = new ConcurrentSkipListSet<QCErrResult>();
			HQMSBaDataCheck.setFailList(failList);
			
			list.forEach(HqmsBaData -> {
				HQMSBaDataCheck.execHQMSBACheck(HqmsBaData);
			});
			//批量更新校验状态
			resultMap.forEach((k,v)->
			{
				if(v.length()>0){
					HQMSBaDataCheck.updateBatchHqmsCheckBaData(v.substring(0, v.length()-1).toString(),k);
				}
			});
			//批量插入校验日志(先删后插)
			if(failList.size()>0){
				List<QCErrResult> arr = new ArrayList<QCErrResult>();
				arr.addAll(failList);
				HQMSBaDataCheck.deleteBatchQcErrResult(failList);
				HQMSBaDataCheck.insertBatchQCErrResultDatas(arr);
			}
				
		} catch (Exception e) {
			LOGGER.error("executeHqmsBatchCheckHQMS校验异常:"+e.getMessage());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	@Transactional(value="transactionManager2")
	public void executeDrgBatchCheck(List<DrgCheckBaData> list) throws Exception {
		try {
			ConcurrentHashMap<String, StringBuffer> resultMap = new ConcurrentHashMap<String, StringBuffer>(){{
				put("Y", new StringBuffer());  
				put("N", new StringBuffer());
			}};
			dRGDataCheck.setResultMap(resultMap);
			ConcurrentSkipListSet<com.kindo.DRGDataCheck.model.QCErrResult> failList = new ConcurrentSkipListSet<com.kindo.DRGDataCheck.model.QCErrResult>();
			dRGDataCheck.setFailList(failList);
			
			list.forEach(drgBaData -> {
				dRGDataCheck.execDRGBACheck(drgBaData);
			});
			//批量更新校验状态
			resultMap.forEach((k,v)->
			{
				if(v.length()>0){
					dRGDataCheck.updateBatchDrgCheckBaData(v.substring(0, v.length()-1).toString(),k);				
				}
			});
			//批量插入校验日志(先删后插)
			if(failList.size()>0){
				List<com.kindo.DRGDataCheck.model.QCErrResult> arr = new ArrayList<com.kindo.DRGDataCheck.model.QCErrResult>();
				arr.addAll(failList);
				dRGDataCheck.deleteBatchQcErrResult(failList);
				dRGDataCheck.insertBatchQCErrResultDatas(arr);
			}
		} catch (Exception e) {
			LOGGER.error("cdrgBatchCheck处理校验异常:"+e.getMessage());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public List<QcCheckConfig> queryQcCheckConfig(String errType){
		List<QcCheckConfig> list = null;
		if(errType.equals("3")){
			list = baDataCheckMapper.selectDrgCheckConfig();
		}
		if(errType.equals("2")){
			list = baDataCheckMapper.selectHqmsCheckConfig();
		}
		return list;
	}
	public int updateQcCheckConfig(QcCheckConfig config){
		int num = 0;
		if(config.getErrType().equals("3")){
			num = baDataCheckMapper.updateDrgCheckConfig(config);
		}
		if(config.getErrType().equals("2")){
			num = baDataCheckMapper.udateHqmsCheckConfig(config);
		}
		return num;
	}
	
	public void execDrgDataClear(){
		baDataCheckMapper.execDrgDataClear();
	}
	
}
