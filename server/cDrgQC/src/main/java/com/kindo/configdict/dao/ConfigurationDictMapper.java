package com.kindo.configdict.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.kindo.common.model.LabelValue;
import com.kindo.configdict.model.pRegionDict;

@Mapper
public interface ConfigurationDictMapper {

	List<pRegionDict> queryAreaDictOld(@Param("type")String type,@Param("parentCode")String parentCode);

	List<LabelValue> queryHospitalDict(@Param("memberCode")String memberCode);
	
	List<pRegionDict> queryAreaDictNew();

	
	 /** 
	 * queryHospitalDictByZx:根据区域去查询医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月31日 下午2:56:15 *
	 * @param member
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<LabelValue> queryHospitalDictByOrga(@Param("list")List<String> member);
}
