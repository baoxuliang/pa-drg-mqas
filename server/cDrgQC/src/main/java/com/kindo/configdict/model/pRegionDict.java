package com.kindo.configdict.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class pRegionDict {
	private String value;
	private String label;
	private String regionType;
	private Integer regionLevel;
	private String fId;
	private Integer enable;
	private List<pRegionDict> children = new ArrayList<pRegionDict>();

	public void findChildren() {
		
	}
	
};
