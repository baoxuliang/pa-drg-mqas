package com.kindo.configdict.api;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.LabelValue;
import com.kindo.configdict.model.AreaHospiQo;
import com.kindo.configdict.model.pRegionDict;
import com.kindo.configdict.service.ConfigurationDictService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.OrgaInfo;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/nologin/dict")
public class ConfigurationDictApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationDictApi.class);
	
	@Autowired
	private ConfigurationDictService configurationDictService;
	
	@Autowired
    private RedisOper redisOper;
	
	
	
	private String memberCode;
//    @ModelAttribute
//    public void initUser(HttpServletRequest request,AreaHospiQo vo) {
//    	String token = request.getHeader(AuthConstants.HEADER_TOKEN);
//    	if (token != null) {
//    		UserLoginInfo info = (UserLoginInfo) redisOper.getData("uasToken:"+token);
////    		if(null !=info){
////    			if(null != info.getOrgaType() && info.getOrgaType().equals("HOS")){
////    				memberCode = info.getOrgaId();
////    			}
////    		}
//        } 
//    };
    
	
	 @ModelAttribute
	 public void initUser(HttpServletRequest request,AreaHospiQo vo) {
		String token = request.getHeader(AuthConstants.HEADER_TOKEN);
		if (token != null) {
			UserLoginInfo user = (UserLoginInfo) redisOper.getData("uasToken:"+token);
			vo.setUser(user);
	    } 
	};
	
    /**
	 * 省市县下拉列表
	 * @param type 1-省市,2-区县
	 * @param parentCode
	 * @return
	 */
	@RequestMapping(value = "/queryArea", method = RequestMethod.GET)
	public ApiResult queryAreaDict(AreaHospiQo vo) {
		
		List<pRegionDict> list = configurationDictService.queryAreaDictNewByXz(vo);
		
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
	};
	
	
	/**
	 * 医院下拉列表 -根据行政
	 * @param type 1-省市,2-区县
	 * @param parentCode
	 * @return
	 */
	@RequestMapping(value = "/queryHospital", method = RequestMethod.GET)
	public ApiResult queryHospitalDictNewByOrga(AreaHospiQo vo) {
		
//		List<LabelValue> list = configurationDictService.queryHospitalDict(memberCode);
		// 得到用户的行政级别
		List<pRegionDict> listXz = configurationDictService.queryAreaDictNewByXz(vo);
		//封装
		List<String> memberList = new LinkedList<>();
		listXz.forEach(x->{
			if(3 == x.getRegionLevel()) {
				memberList.add(x.getValue());
			}else if(2 == x.getRegionLevel()){
				x.getChildren().forEach(y->{
					memberList.add(y.getValue());
				});
			}
		});
//		memberList.clear();
		List<LabelValue> list =configurationDictService.queryHospitalDictNewByOrga(memberList);
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
	};
	
//	/**
//	 * 医院下拉列表 医院下拉列表 -根据数据权限
//	 * @param type 1-省市,2-区县
//	 * @param parentCode
//	 * @return
//	 */
//	@RequestMapping(value = "/queryHospital", method = RequestMethod.GET)
//	public ApiResult queryHospitalDictNewByData(AreaHospiQo vo) {
//		
////		List<LabelValue> list = configurationDictService.queryHospitalDict(memberCode);
//		List<LabelValue> list = new LinkedList<>();
//		UserLoginInfo user = vo.getUser();
//		List<OrgaInfo> userDataOrgaList = null; 
//		if(vo!=null && (userDataOrgaList=user.getUserDataOrgaList()) != null) {
//			userDataOrgaList.stream()
//							.filter(x->"HOS".equals(x.getOrgaType()))
//							.sorted(Comparator.comparing(OrgaInfo::getOrgaCode))
//							.forEach(x->{
//								LabelValue sub = new LabelValue();
//								sub.setLabel(x.getOrgaName());
//								sub.setValue(x.getOrgaCode());
//								list.add(sub);
//							});
//			
//		}
//		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
//	};
	
	
	
	
	
	
	/**
	 * 省市县下拉列表
	 * @param type 1-省市,2-区县
	 * @param parentCode
	 * @return
	 */
	@RequestMapping(value = "/queryAreaOld", method = RequestMethod.GET)
	public ApiResult queryAreaDictOld(String type,String parentCode) {
		
		List<pRegionDict> list = configurationDictService.queryAreaDictOld(type,parentCode);
		
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
	};
	
	/**
	 * 医院下拉列表
	 * @param type 1-省市,2-区县
	 * @param parentCode
	 * @return
	 */
	@RequestMapping(value = "/queryHospitalOld", method = RequestMethod.GET)
	public ApiResult queryHospitalDictOld() {
		
		List<LabelValue> list = configurationDictService.queryHospitalDict(memberCode);
		
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
	};
};
