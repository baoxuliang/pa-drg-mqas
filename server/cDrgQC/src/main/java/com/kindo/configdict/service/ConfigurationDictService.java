package com.kindo.configdict.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.common.model.LabelValue;
import com.kindo.configdict.dao.ConfigurationDictMapper;
import com.kindo.configdict.model.AreaHospiQo;
import com.kindo.configdict.model.pRegionDict;
import com.kindo.uas.common.model.OrgaInfo;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class ConfigurationDictService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationDictService.class);
    
    @Autowired
    private ConfigurationDictMapper configurationDictMapper;
    
    
    
    
     /** 
     * queryAreaDictNewByXz:按行政级别来确定. <br/>
     * @author whk00196 
     * @date 2018年7月31日 下午2:12:35 *
     * @param vo
     * @return 
     * @since JDK 1.8 
     **/
    public List<pRegionDict> queryAreaDictNewByXz(AreaHospiQo vo){
    	List<pRegionDict> newdict = new ArrayList<pRegionDict>(); 
    	List<pRegionDict> ret = new LinkedList<pRegionDict>(); 
    	List<pRegionDict> ret_ = new LinkedList<pRegionDict>(); 
    	List<pRegionDict> adict = configurationDictMapper.queryAreaDictNew();
    	//获得 基本的 区域
    	for(pRegionDict dic : adict){
    		int regionLevel = dic.getRegionLevel();
    		if(regionLevel == 1 || regionLevel == 2){
    			String  code = dic.getValue().trim();
        		for(pRegionDict dis : adict){
        			if(dis.getFId().trim().equals(code)){
        				dic.getChildren().add(dis);
        			}
            	}
        		newdict.add(dic);
    		}
    	};
    	UserLoginInfo  user =  vo.getUser();
    	String orgaType = user.getOrgaType();
    	LOGGER.info("ConfigurationDictService.queryAreaDictNewByXz,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("ConfigurationDictService.queryAreaDictNewByXz,发现用户所选择区域为不限,自动匹配其权限区域");
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("ConfigurationDictService.getTheRegion,用户为管理员级别!不限区域!");
			ret = newdict;
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("ConfigurationDictService.getTheRegion,用户为省级别!不限区域!");
			ret = newdict;
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("ConfigurationDictService.getTheRegion,用户为市级别!");
			String region = user.getOrgaId();
			ret=newdict.stream()
				   .filter(x->region.equals(x.getValue()))
				   .collect(Collectors.toCollection(LinkedList::new));
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("ConfigurationDictService.getTheRegion,用户为县级别!");
			String region = user.getOrgaId();
			newdict.forEach(x->{
					  x.getChildren().forEach(y->{
						  	if(region.equals(y.getValue())) {
						  		ret_.add(y);
						  	} 
					  });  
				   });
			ret = ret_;
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("ConfigurationDictService.getTheRegion,用户为医院用户级别!");
			newdict.clear(); 
			ret = newdict;
		}
    	return ret;
	};
    
    
    public List<pRegionDict> queryAreaDictNew(AreaHospiQo vo){
    	List<pRegionDict> newdict = new ArrayList<pRegionDict>(); 
    	List<pRegionDict> adict = configurationDictMapper.queryAreaDictNew();
    	for(pRegionDict dic : adict){
    		int regionLevel = dic.getRegionLevel();
    		if(regionLevel == 1 || regionLevel == 2){
    			String  code = dic.getValue().trim();
        		for(pRegionDict dis : adict){
        			if(dis.getFId().trim().equals(code)){
        				dic.getChildren().add(dis);
        			}
            	}
        		newdict.add(dic);
    		}
    	};
    	UserLoginInfo info = vo.getUser();
    	List<OrgaInfo> orgaInfoList_ = new ArrayList<>();
    	if(info != null&&info.getUserDataOrgaList() != null) {
    		orgaInfoList_ = info.getUserDataOrgaList();
    	}
    	List<OrgaInfo> orgaInfoList = orgaInfoList_;
    	//确定用户是否含有市级别的
    	List<pRegionDict> newdictCity = newdict.stream()
    		   .filter(x->{
    			   boolean flag = false;
    			   if("2".equals(x.getRegionLevel().toString())) {
    				  for( OrgaInfo tmp : orgaInfoList) {
    					  if(tmp.getOrgaCode().equals(x.getValue())) {
    						  flag = true;
    					  }
    				  } 
    			   }
    			   return flag;
    		   }).collect(Collectors.toCollection(LinkedList::new));
    	List<pRegionDict> newdictFinal  = new LinkedList<>();
    	List<pRegionDict> newdictFinal_  = new LinkedList<>();
    	//如果 只为县级别
    	if(newdictCity.size()==0) {
    		for(pRegionDict sub : newdict) {
    			sub.getChildren().stream()
    							 .forEach(x->{
    								  for( OrgaInfo tmp : orgaInfoList) {
    	    	    					  if(tmp.getOrgaCode().equals(x.getValue())) {
    	    	    						  newdictFinal_.add(x);
    	    	    					  }
    	    	    				  } 
    							 });
    		}
    		
    		return newdictFinal_;
    	}
    	//县级别的
    	newdictFinal = newdictCity.stream()
    									.peek(x->{
										List<pRegionDict> sub = x.getChildren()
																.stream()
																.filter(y -> {
																	boolean flag = false;
																	  for( OrgaInfo tmp : orgaInfoList) {
												    					  if(tmp.getOrgaCode().equals(y.getValue())) {
												    						  flag = true;
												    					  }
												    				  }
																	return flag;
																}).collect(Collectors.toCollection(LinkedList::new));
    										x.setChildren(sub);
    									}).collect(Collectors.toCollection(LinkedList::new));
    	
    	return newdictFinal;
		//return configurationDictMapper.queryAreaDict(type,parentCode);
	}
    
    public List<pRegionDict> queryAreaDictOld(String type,String parentCode){
    	List<pRegionDict> newdict = new ArrayList<pRegionDict>(); 
    	List<pRegionDict> adict = configurationDictMapper.queryAreaDictOld(type,parentCode);
    	for(pRegionDict dic : adict){
    		int regionLevel = dic.getRegionLevel();
    		if(regionLevel == 1 || regionLevel == 2){
    			String  code = dic.getValue().trim();
        		for(pRegionDict dis : adict){
        			if(dis.getFId().trim().equals(code)){
        				dic.getChildren().add(dis);
        			}
            	}
        		newdict.add(dic);
    		}
    	}
    	
    	return newdict;
		//return configurationDictMapper.queryAreaDict(type,parentCode);
	}
    
    public List<LabelValue> queryHospitalDict(String memberCode){
		return configurationDictMapper.queryHospitalDict(memberCode);
	}


	
	 /** 
	 * queryHospitalDictByZx:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月31日 下午2:54:32 *
	 * @param member 
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<LabelValue> queryHospitalDictNewByOrga(List<String> member) {
		return configurationDictMapper.queryHospitalDictByOrga(member);
	};
    
}
