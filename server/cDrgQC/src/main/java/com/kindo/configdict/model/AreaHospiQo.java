
/** 
* Project Name:cDrgQC <br/> 
* File Name:AreaHospiQo.java <br/>
* Package Name:com.kindo.configdict.model <br/>
* Date:2018年7月30日下午7:58:27 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.configdict.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: AreaHospiQo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月30日 下午7:58:27 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class AreaHospiQo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 7910728515787350556L;
	
	UserLoginInfo user;

};
