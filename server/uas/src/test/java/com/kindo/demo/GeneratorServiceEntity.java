package com.kindo.demo;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.Test;

/**
 * 代码生成器
 * 需要生成代码的表 必须有注释！
 */
public class GeneratorServiceEntity {
    /**
     * 作者署名
     */
    private static final String AUTHOR = "xhd";

    /**
     * 数据库连接URL
     */
    private static final String DB_URL = "jdbc:oracle:thin:@192.168.1.31:1521:ORCL";

    /**
     * 数据库连接用户名
     */
    private static final String USERNAME = "hnsb0503";

    /**
     * 数据库连接密码
     */
    private static final String PASSWORD = "hnsb0503";

    /**
     * 数据库连接驱动
     */
    private static final String DRIVER_NAME = "oracle.jdbc.driver.OracleDriver";

    /**
     * 数据库类型
     */
    private static final DbType DB_TYPE = DbType.ORACLE;

    /**
     * 代码文件生成路径
     */
    private static final String OUTPUT_DIR = "d:\\codeGen";

    /**
     * 生成文件所在包名
     */
    private static final String PACKAGE_NAME = "com.kindo.demo.momb";

    /**
     * WEB层超类
     */
    private static final String SUPER_CONTROLLER = "com.kindo.uas.common.model.BaseController";

    /**
     * 实体类超类
     */
    private static final String SUPER_ENTITY = "com.kindo.uas.common.model.BaseQuery";

    /**
     * 需要生成的表名称
     */
    private static final String[] TABLE_NAMES = {"DEMO_USER"};

    @Test
    public void generateCode() {
        boolean serviceNameStartWithI = false;//user -> UserService, 设置成true: user -> IUserService
        generateByTables(serviceNameStartWithI, PACKAGE_NAME, TABLE_NAMES);
    }

    private void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
        GlobalConfig config = new GlobalConfig();
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DB_TYPE)
                .setUrl(DB_URL)
                .setUsername(USERNAME)
                .setPassword(PASSWORD)
                .setDriverName(DRIVER_NAME);
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                .setSuperControllerClass(SUPER_CONTROLLER)
                .setRestControllerStyle(true)
                .setSuperEntityClass(SUPER_ENTITY)
                .setCapitalMode(true)
                .setEntityLombokModel(false)
                .setDbColumnUnderline(true)
                .setNaming(NamingStrategy.underline_to_camel)
                .setInclude(tableNames);//修改替换成你需要的表名，多个表名传数组
//                .setTablePrefix("KBMS_");   //要移除的前缀
        config.setActiveRecord(false)
                .setAuthor(AUTHOR)
                .setOutputDir(OUTPUT_DIR)
                .setFileOverride(true)
                .setEnableCache(false);
        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
        }
        new AutoGenerator()
                .setTemplateEngine(new FreemarkerTemplateEngine())
                .setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setController("controller")
                                .setEntity("entity")
                                .setMapper("dao")
                )
                .execute();
    }

}
