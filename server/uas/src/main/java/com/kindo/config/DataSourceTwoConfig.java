package com.kindo.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.MybatisConfiguration;
import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 数据源相关配置 可以参照此 配置自己的数据源
 * MapperScan注解中basePackages应该配置dao层文件所在包，不同数据源的dao层文件应该在不同包中
 * 不同数据源的mapper.xml文件应该在不同文件夹中
 * 不同数据源的JavaBean名称(@Bean注解中的name)应该不同
 *
 * @author Xu Haidong
 * @date 2018/5/25
 */
@Configuration
public class DataSourceTwoConfig {

    /**
     * 默认使用Druid连接池
     * 自动载入application.yml中的datasource2下的配置
     *
     * @return
     * @throws SQLException
     */
    @ConfigurationProperties(prefix = "datasource2")
    @Bean(name = "datasource2")
    public DataSource dataSource2() throws SQLException {
        return DruidDataSourceBuilder.create().build();
    }

}
