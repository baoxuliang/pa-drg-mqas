package com.kindo;

import com.kindo.uas.common.anno.IgnoreDuringScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 程序入口
 * 金豆进程平台相关类都在com.kindo.uas目录下，如果要使用请扫描
 * 当一个Tomcat下运行多个基于集成平台的工程时，只需要有一个工程包含集成平台的API
 * 此时可以配置@ComponentScan(excludeFilters = @ComponentScan.Filter(value = IgnoreDuringScan.class))
 * 来忽略掉带@IgnoreDuringScan注解的类。集成平台的所有WebApi类都包含此注解。
 * 修改banner.txt文件可以自定义启动banner
 * spring.properties中配置了禁用jndi，建议保留。
 *
 * @author Xu Haidong
 * @date 2018/5/31
 */
//开启事务
@EnableTransactionManagement
//开始自动配置，exclude配置排除不必要的自动配置类
@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class})
// com.kindo.uas为uas包路径必须配置扫描，com.kindo.demo可配置为自己项目包路径
@ComponentScan({"com.kindo"})
//使用以下配置 可以忽略所有集成平台自带API
//@ComponentScan(value = {"com.kindo.uas", "com.kindo.demo"}, excludeFilters = @ComponentScan.Filter(IgnoreDuringScan.class))
//开启缓存
@EnableCaching
public class App extends SpringBootServletInitializer {

    private static Class<App> appClass = App.class;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(appClass);
    }

    public static void main(String[] args) {
        SpringApplication.run(appClass, args);
    }
}
