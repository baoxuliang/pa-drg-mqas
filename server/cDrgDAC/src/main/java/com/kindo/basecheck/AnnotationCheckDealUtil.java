package com.kindo.basecheck;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.kindo.basecheck.annotation.DateTimeAnnotation;
import com.kindo.basecheck.annotation.DoubleAnnotation;
import com.kindo.basecheck.annotation.IsEmptyAnnotation;
import com.kindo.basecheck.annotation.MaxSize;

public class AnnotationCheckDealUtil {
	private static final Logger log = LoggerFactory.getLogger(AnnotationCheckDealUtil.class);
	private static final String falls_field = "falls_field";
	/**
	 * 不建议使用,性能比较差  
	 * 
	 * 建议用下面方式,在程序中自己实现,例如:
	 * Map<Boolean, List<IfBaInfoBase>> retAMap = list.parallelStream().peek(item -> item.setMISS(AnnotationCheckDealUtil.validate(item).get("falls_field").toString()))
	 *   .collect(Collectors.partitioningBy(item ->StringUtils.isEmpty(item.getMISS())));
	 * 
	 * @param list
	 * @return
	 */
	@Deprecated
	public static <T> Map<Boolean, List<T>> checkDatas(List<T> list) {
		Map<Boolean, List<T>> retAMap = list.parallelStream().peek(item -> {
			String value = AnnotationCheckDealUtil.validate(item).get("falls_field").toString();
			try {
				Method m = item.getClass().getDeclaredMethod("setMISS", String.class);  
				m.invoke(item, value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).collect(Collectors.partitioningBy(item ->{
			boolean flag = true;
			try {
				Method m = item.getClass().getDeclaredMethod("getMISS", null);  
				flag = StringUtils.isEmpty(m.invoke(item,null));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return flag;
		}));
		return retAMap;
    }
    /** 
     * 注解验证电泳方法 
     *  
     * @param bean 验证的实体 
     * @return 
     */  
    @SuppressWarnings("unchecked")  
    public static Map<String, Object> validate(Object bean) {  
        final Map<String, Object> result = new HashMap<String, Object>();
        result.put(falls_field, new StringBuffer());
        result.put("result", true);  
        Class<?> cls = bean.getClass();  
          
        // 检测field是否存在  
        try {  
            // 获取实体字段集合  
            Field[] fields = cls.getDeclaredFields();  
            for (Field f : fields) {  
                // 通过反射获取该属性对应的值  
                f.setAccessible(true);  
                // 获取字段值  
                Object value = f.get(bean);  
                // 获取字段上的注解集合  
                Annotation[] arrayAno = f.getAnnotations();  
                for (Annotation annotation : arrayAno) {  
                    // 获取注解类型（注解类的Class）  
                    Class<?> clazz = annotation.annotationType();  
                    // 获取注解类中的方法集合  
                    Method[] methodArray = clazz.getDeclaredMethods();  
                    for (Method method : methodArray) {  
                        // 获取方法名  
                        String methodName = method.getName();  
                        // 过滤错误提示方法的调用  
                        if(methodName.equals("message")) {  
                            continue;  
                        }  
                        // 初始化注解验证的方法处理类 （我的处理方法卸载本类中）  
                        Object obj = AnnotationCheckDealUtil.class.newInstance();  
                        // 获取方法  
                        try {  
                            // 根据方法名获取该方法  
                            Method m = obj.getClass().getDeclaredMethod(methodName, Object.class, Field.class,Map.class);  
                            // 调用该方法  
                            m.invoke(obj, value, f,result);
                        } catch (Exception e) {  
                            e.printStackTrace();  
                            log.info("找不到该方法:"+methodName);  
                        }  
                    }  
                }  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
            log.info("验证出错");  
        }  
        return result;  
    }  
  
    /** 
     * 验证是否空值 
     *  
     * @param value 参数值 
     * @param field 字段 
     * @return 
     */  
    public Map<String, Object> isEmpty(Object value, Field field,Map<String, Object> result) {  
        IsEmptyAnnotation annotation = field.getAnnotation(IsEmptyAnnotation.class);
        if(annotation.isEmpty()){
        	if(value == null || value.equals("")) {  
            	StringBuffer s = (StringBuffer) result.get(falls_field);
            	s.append(field.getName()).append(",");
            	result.put("result", false);  
            }
        }
        return result;
    }  
      
    /** 
     * 验证最大值 
     *  
     * @param value 参数值 
     * @param field 字段 
     * @return 
     */  
    public Map<String, Object> max(Object value, Field field,Map<String, Object> result) {  
        MaxSize annotation = field.getAnnotation(MaxSize.class); 
        if(value != null && value.toString().length() > annotation.max()) {  
        	StringBuffer s = (StringBuffer) result.get(falls_field);
        	s.append(field.getName()).append(",");
        	result.put("result", false);
        }  
        return result;  
    }
    
	public void patternDate(Object value, Field field, Map<String, Object> result) {
		DateTimeAnnotation annotation = field.getAnnotation(DateTimeAnnotation.class);
		String pt = annotation.patternDate();
		SimpleDateFormat format = new SimpleDateFormat(pt);
		try {
			format.setLenient(false);
			if (value instanceof String) {
				format.parse((String) value);
			}else if (value instanceof Date) {
				format.format(value);
			}
		} catch (ParseException e) {
			StringBuffer s = (StringBuffer) result.get(falls_field);
        	s.append(field.getName()).append(",");
        	result.put("result", false);
		}
	}
	
	public void regDouble(Object value, Field field, Map<String, Object> result) {
		boolean flag = false;
		DoubleAnnotation annotation = field.getAnnotation(DoubleAnnotation.class);
		String pt = annotation.regDouble();
		Pattern pattern = Pattern.compile(pt);
		if (value instanceof String) {
			if (!pattern.matcher((String) value).matches()) {
				flag = true;
			}
		} else if (value instanceof Double) {
			String tmp = String.valueOf(value);
			if (!pattern.matcher(tmp).matches()) {
				flag = true;
			}
		}
		if (flag) {
			StringBuffer s = (StringBuffer) result.get(falls_field);
			s.append(field.getName()).append(",");
			result.put("result", false);
		}
	}
}
