package com.kindo.basecheck.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;


/** 
 * 空指针验证类 
 */  
@Documented  
@Inherited  
@Target(ElementType.FIELD)  
@Retention(RetentionPolicy.RUNTIME)  
public @interface DateTimeAnnotation {  
	String patternDate() default "yyyy-MM-dd HH:mm:ss";
      
    public String message() default "不合格的日期格式！";  
}  
