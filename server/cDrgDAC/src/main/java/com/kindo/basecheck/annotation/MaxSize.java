package com.kindo.basecheck.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/** 
 * 最大长度验证类 
 */  
@Documented  
@Inherited  
@Target(ElementType.FIELD)  
@Retention(RetentionPolicy.RUNTIME)  
public @interface MaxSize {  
    public int max() default 150;  
  
    public String message() default "长度太长";  
}  