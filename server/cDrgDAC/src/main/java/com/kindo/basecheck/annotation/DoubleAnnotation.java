package com.kindo.basecheck.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;


/** 
 * 空指针验证类 
 */  
@Documented  
@Inherited  
@Target(ElementType.FIELD)  
@Retention(RetentionPolicy.RUNTIME)  
public @interface DoubleAnnotation {  
	String regDouble() default "^[-//+]?//d+(//.//d*)?|//.//d+$";
      
    public String message() default "double格式不匹配！";  
}  
