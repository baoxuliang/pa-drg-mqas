package com.kindo.basecheck;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

public class JsonCheckDealUtil {
	private static final Logger log = LoggerFactory.getLogger(JsonCheckDealUtil.class);
	/**
	 * json字段值基础校验
	 * @param list:json数组
	 * @param values:字符串数组:new String[]{"MEMBER_CODE:30", "MEMBER_NAME=60"}
	 * values中,数组元素中,每个元素包含校验的字段和匹配值,列如冒号分隔【:】,冒号前面是字段名,后面是校验规则,表示同时校验非空和长度
	 * 等于号分隔【=】,表示只对等于号后面的长度进行校验
	 * &号分隔【&】,对日期校验,列如【CSRQ&yyyy-MM-dd HH:mm:ss】
	 * #号分隔【#】,是正则校验匹配,需要可以加上
	 * @return
	 */
	public static Map<Boolean, List<JSONObject>> checkJsonDatas(List<Object> list, String [] values) {
        return list.parallelStream().map(item -> (JSONObject) item)
                .peek(item -> item.put("MISS",
                		Stream.of(values).parallel().filter(field ->{
                			String[] sarr = field.split(":"); 
                			String[] sarr1 = field.split("=");
                			String[] sarr2 = field.split("&");
                			String[] sarr3 = field.split("#");
                			boolean flag = false;
                			if(sarr.length>1){
                				int lenght = Integer.parseInt(sarr[1]);
                		    	String value = item.getString(sarr[0]);
                		    	flag = StringUtils.isEmpty(value) || value.length() > lenght;
                			}
                			if(sarr1.length>1){
                				int lenght = Integer.parseInt(sarr1[1]);
                				String value = item.getString(sarr1[0]);
                				if(!StringUtils.isEmpty(value)){
                					flag = value.length() > lenght;
                				}
                			}
                			if(sarr2.length>1){
                				String value = item.getString(sarr2[0]);
                				SimpleDateFormat format = new SimpleDateFormat(sarr2[1]);
                    			try {
                    				format.setLenient(false);
                    				format.parse(value);
                    			} catch (ParseException e) {
                    				flag = true;
                    			}
                			}
                			if(sarr3.length>1){
                				String value = item.getString(sarr3[0]);
                				Pattern pattern = Pattern.compile(sarr3[1]);
            					if (!pattern.matcher(value).matches()) {
            						flag = true;
            					}
                			}
                			return flag;
                		})
                .map(field -> {
                	if(field.split(":").length > 1){
                		return field.split(":")[0];
                	}
                	if(field.split("=").length > 1){
                		return field.split("=")[0];
                	}
                	if(field.split("&").length > 1){
                		return field.split("&")[0];
                	}
                	if(field.split("#").length > 1){
                		return field.split("#")[0];
                	}
                	return field;
                })
                .collect(Collectors.joining(","))))
                .collect(Collectors.partitioningBy(item -> StringUtils.isEmpty(item.getString("MISS"))));
    }
}
