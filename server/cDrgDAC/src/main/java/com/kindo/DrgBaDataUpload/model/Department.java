package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 标准科室
 * @author jindoulixing
 */
@Data
public class Department implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6687402309353782892L;

    private String UID;
    
    private String ID;

    private String DEPARTMENT_CODE;

    private String DEPARTMENT_NAME;

    private String PARENT_CODE;

    private Date SYNDATE;

    //private String SYNFLAG;
}
