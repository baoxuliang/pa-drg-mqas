package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ICD9 implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2871441327384548928L;
    
    private String UID;
    private String ID;
    private String MEMBER_CODE;
    private String MEMBER_NAME;
    private String DISTRICT_CODE;
    private String ICD_CODE;
    private String ICD_APPEND_CODE;
    private String ICD_NAME;
    private String IS_HOSPITAL_CODE;
    
    private Date SYNDATE;
   
}
