package com.kindo.DrgBaDataUpload.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kindo.DrgBaDataUpload.dao.DrgBaInfoMapper;
import com.kindo.DrgBaDataUpload.model.FailDetail;
import com.kindo.DrgBaDataUpload.model.IfBaInfo;
import com.kindo.DrgBaDataUpload.model.QCErrResult;
import com.kindo.DrgBaDataUpload.model.Result;
import com.kindo.basecheck.JsonCheckDealUtil;

@Service
public class DrgBaDataUploadV2Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataUploadV2Service.class);
    private static final int BATCH_SIZE = 1000;
    private static ExecutorService executor = Executors.newFixedThreadPool(6);
    
    @Autowired
    private DrgBaInfoMapper drgBaInfoMapper;

    // 病案数据批量入库
    public Result saveIfBaInfoDatas(String bastr, Result result) {
    	JSONArray jsonArr = null;
		try {
			jsonArr = JSONObject.parseArray(bastr);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			return result;
		}

		if (jsonArr == null || jsonArr.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			return result;
		}
		LOGGER.info("saveIfBaInfoDatas数据校验接收数据条数:"+jsonArr.size());
		String values[] = new String[]{"MEMBER_CODE:30","MEMBER_NAME:80","ZYH:30","ZYCS:3","YLFKFS:1","BAH:20","XM:40",
				"XB:1","RYSJ:24","RYKB:30","RYKBBM:10","CYSJ:24","CYKB:30","CYKBBM:10","SJZYTS:4","MZZD:150",
				"JBBM:20","ZYZD:150","JBDM:20","RYBQ:1","ZFY:12","RYBQ4=1","QTZD5=150","JBDM4=20",
				"JKKH=50","CSRQ=24","NL=3","GJ=40","BZYZSNL=5","XSERYTZ=6","CSD=300","JG=300","MZ=20","SFZH=18",
				"HY=1","RYTJ=1","ZKKB=30","ZKKBBM1=10","ZKKBBM2=10","ZKKBBM3=10","RYZDMC=150","RYZDBM=20","RYQZRQ=24","QTZD1=150",
				"JBDM1=20","RYBQ1=1","QTZD2=150","JBDM2=20","RYBQ2=1","QTZD3=150","JBDM3=20","RYBQ3=1","QTZD4=150","JBDM5=20",
				"RYBQ5=1","QTZD6=150","JBDM6=20","RYBQ6=1","QTZD7=150","JBDM7=20","RYBQ7=1","QTZD8=150","JBDM8=20","RYBQ8=1",
				"QTZD9=150","JBDM9=20","RYBQ9=1","QTZD10=150","JBDM10=20","RYBQ10=1","QTZD11=150","JBDM11=20","RYBQ11=1",
				"QTZD12=150","JBDM12=20","RYBQ12=1","QTZD13=150","JBDM13=20","RYBQ13=1","QTZD14=150","JBDM14=20","RYBQ14=1",
				"QTZD15=150","JBDM15=20","RYBQ15=1","WBYY=150","H23=20","KZR=40","KZR_CODE=10","ZRYS=40","ZRYS_CODE=10",
				"ZZYS=40","ZZYS_CODE=10","ZYYS=40","ZYYS_CODE=10","ZRHS=40","JXYS=40","SXYS=40","BMY=40","BAZL=1",
				"ZKYS=40","ZKHS=40","ZKRQ=24","SSJCZBM1=20","SSJCZRQ1=24","SSJB1=1","SSJCZMC1=150","SZ1=40","YZ1=40",
				"EZ1=40","QKYHDJ1=2","MZFS1=6","MZYS1=40","SSJCZBM2=20","SSJCZRQ2=24","SSJB2=1","SSJCZMC2=150",
				"SZ2=40","YZ2=40","EZ2=40","QKYHDJ2=2","MZFS2=6","MZYS2=40","SSJCZBM3=20","SSJCZRQ3=24","SSJB3=1",
				"SSJCZMC3=150","SZ3=40","YZ3=40","EZ3=40","QKYHDJ3=2","MZFS3=6","MZYS3=40","SSJCZBM4=20","SSJCZRQ4=24",
				"SSJB4=1","SSJCZMC4=150","SZ4=40","YZ4=40","EZ4=40","QKYHDJ4=2","MZFS4=6","MZYS4=40","SSJCZBM5=20",
				"SSJCZRQ5=24","SSJB5=1","SSJCZMC5=150","SZ5=40","YZ5=40","EZ5=40","QKYHDJ5=2","MZFS5=6","MZYS5=40",
				"SSJCZBM6=20","SSJCZRQ6=24","SSJB6=1","SSJCZMC6=150","SZ6=40","YZ6=40","EZ6=40","QKYHDJ6=2","MZFS6=6",
				"MZYS6=40","SSJCZBM7=20","SSJCZRQ7=24","SSJB7=1","SSJCZMC7=150","SZ7=40","YZ7=40","EZ7=40","QKYHDJ7=2",
				"MZFS7=6","MZYS7=40","SSJCZBM8=20","SSJCZRQ8=24","SSJB8=1","SSJCZMC8=150","SZ8=40","YZ8=40","EZ8=40",
				"QKYHDJ8=2","MZFS8=6","MZYS8=40","QJCS=3","CGCS=3",
				"LYFS=1","YZZY_YLJG=100","SFZZYJH=1","MD=100",
				"ZFY=13","ZFJE=13","YLFUF=13","ZLCZF=13","HLF=13","QTFY=13","BLZDF=13","SYSZDF=13","YXXZDF=13","LCZDXMF=13",
				"FSSZLXMF=13","WLZLF=13","SSZLF=13","MZF=13","SSF=13","KFF=13","ZYZLF=13","XYF=13","KJYWF=13","ZCYF=13",
				"ZCYF1=13","XF=13","BDBLZPF=13","QDBLZPF=13","NXYZLZPF=13","XBYZLZPF=13","HCYYCLF=13","YYCLF=13","YCXYYCLF=13",
				"QTF=13","GDRQ=24","XSECSTZ_1=6","XSECSTZ_2=6","XSECSTZ_3=6","XSECSTZ_4=6","XSECSTZ_5=6","DISTRICT_CODE:12"}; 
		long ssql = System.currentTimeMillis();
		Map<Boolean, List<JSONObject>> retAMap = JsonCheckDealUtil.checkJsonDatas(jsonArr, values);
		
    	try {
    		result.getFAILS().addAll(retAMap.get(false).parallelStream()
                    .map(item -> new FailDetail(item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"), "非空或超长字段"+item.getString("FAILS")))
                    .collect(Collectors.toList()));
            final List<IfBaInfo> dataList =  new ArrayList<IfBaInfo>();
            retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 IfBaInfo bainfo = JSONObject.toJavaObject(item, IfBaInfo.class);
			    	 bainfo.setID(bainfo.getMEMBER_CODE()+"$"+bainfo.getDISTRICT_CODE()+"$"+bainfo.getBAH()+"$"+bainfo.getZYCS());
			    	 dataList.add(bainfo);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("FAILS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfBaInfoDatas病案数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfBaInfoDatas病案数据参数格式错误,数据为:"+item);
				}
            });
            
            List<IfBaInfo> ll = moveReduplicate(dataList);
            
            int num = result.getFAILS().size();
            if(num > 0){
            	result.setCODE(201);
            	result.setFAILNUM(num);
                result.setMSG("部分成功(返回校验失败信息)");
            }else{
            	result.setCODE(200);
                result.setMSG("上传数据成功");
            }
            long esql = System.currentTimeMillis();
    		LOGGER.info("数据校验耗时==================="+(esql - ssql) +"ms");
            LOGGER.info("saveIfBaInfoDatas校验结果成功条数:"+retAMap.get(true).size());
            
			executor.submit(new Thread(()-> {
				try {
					int[] idx = { 0 };
					int[] count = { 0 };
					dataList.stream().collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
					        .forEach(item -> {
					        	try {
					        		drgBaInfoMapper.deleteBatchQcErrResult(item);
					        	    drgBaInfoMapper.deleteBatchDrgAudit(item);
									drgBaInfoMapper.deleteBatchDrgBaInfos(item);
									count[0] += drgBaInfoMapper.insertBatchDrgBaInfoBaseDatas(item);
								} catch (Exception e) {
									LOGGER.error("saveIfBaInfoDatas病案数据批量入库失败:"+e.getMessage());
									LOGGER.error("saveIfBaInfoDatas数据批量入库异常,入库数据为:"+JSONObject.toJSONString(item));
								}
					        });
					LOGGER.info("saveIfBaInfoDatas校验结果成功入库条数:"+count[0]);
				} catch (Exception e) {
					LOGGER.error("saveIfBaInfoDatas病案数据入库失败:"+e.getMessage());
					LOGGER.error("saveIfBaInfoDatas数据入库异常,数据为:"+JSONObject.toJSONString(retAMap.get(true)));
				}
			}
			));
            
            executor.submit(new Thread(()-> {
				try {
					int[] idx1 = { 0 };
					retAMap.get(false).stream()
					.map(item -> new QCErrResult(java.util.UUID.randomUUID().toString(),item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"),item.getString("MEMBER_CODE"),item.getString("BAH"),Integer.valueOf(item.getString("ZYCS")),"1","非空,超长,数据格式校验",item.getString("FAILS"),"1"))
					.collect(Collectors.groupingBy(item -> idx1[0]++ / BATCH_SIZE, Collectors.toList())).values()
					.forEach(item -> {
						try {
							LOGGER.error("deleteBatchQCErrResultDatas非空和超长校验失败记录数据库:"+item);
							drgBaInfoMapper.deleteBatchQCErrResultDatas(item);
							drgBaInfoMapper.insertBatchQCErrResultDatas(item);
						} catch (Exception e) {
							LOGGER.error("saveIfBaInfoDatas非空和超长校验失败记录数据库异常:"+e.getMessage());
							LOGGER.error("saveIfBaInfoDatas非空和超长校验失败记录数据库异常,数据为:"+JSONObject.toJSONString(retAMap.get(false)));
						}
					});
				} catch (Exception e) {
					LOGGER.error("saveIfBaInfoDatas基础校验失败记录数据库异常:"+e.getMessage());
					LOGGER.error("saveIfBaInfoDatas基础校验失败记录数据库异常,数据为:"+JSONObject.toJSONString(retAMap.get(false)));
				}
			}
            ));
        } catch (Exception e) {
        	result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfBaInfoDatas病案数据处理异常:"+e.getMessage());
        }
    	
        return result;
    }

    public List<IfBaInfo> moveReduplicate(List<IfBaInfo> list) {
    	return list.parallelStream().filter(distinctByKey(IfBaInfo::getID)).collect(Collectors.toList());
    }
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
