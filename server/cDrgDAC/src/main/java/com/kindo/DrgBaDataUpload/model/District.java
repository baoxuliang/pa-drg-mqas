package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 院区信息
 * @author jindoulixing
 *
 */
@Data
public class District implements Serializable {
    private static final long serialVersionUID = 199118143577287116L;

    private String UID;
    
    private String ID;
    
    private String MEMBER_CODE;

    private String MEMBER_NAME;

    private String DISTRICT_CODE;

    private String DISTRICT_NAME;
  
}
