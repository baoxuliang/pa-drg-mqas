package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 科室组/大科室信息
 * @author jindoulixing
 *
 */
@Data
public class IfDepartmentGroup implements Serializable {
    private static final long serialVersionUID = 199118143577287116L;

    private String UID;
    
    private String ID;
    
    private String MEMBER_CODE;

    private String DISTRICT_CODE;

    private String B_DEPARTMENT_CODE;

    private String B_DEPARTMENT_NAME;

}
