package com.kindo.DrgBaDataUpload.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.kindo.DrgBaDataUpload.model.Result;
import com.kindo.DrgBaDataUpload.service.DrgBaDataUploadService;

@RestController
@RequestMapping("/nologin/drgUpload")
public class DrgBaDataUploadApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataUploadApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private DrgBaDataUploadService drgBaDataUploadService;
	/**
	 * 病案信息
	 * @param bainfo
	 * @return
	 */
	@RequestMapping(value = "/ifBaInfo/v1", method = RequestMethod.POST)
	public Result ifBaInfo(@RequestBody (required=false) String bainfo) {
		Result result = new Result();
		if(null == bainfo){
			result.setMSG("参数值为空");
			result.setCODE(101);
			return result;
		}
		
		/*String bastr = null;
		try {
			bastr = GZIPString.unCompress(bainfo);
		} catch (IOException e) {
			result.setMSG("参数解压失败");
			result.setCODE(102);
			LOGGER.info("绩效平台[drgUpload/ifBaInfo]解压字符串异常："+e.getMessage());
		}*/
		//LOGGER.info("绩效平台[drgUpload/ifBaInfo]接收到数据bainfo："+bastr);
		drgBaDataUploadService.saveIfBaInfoDatas(bainfo,result);
		
		LOGGER.info("绩效平台[drgUpload/ifBaInfo]处理完成:"+result);
		return result;
	}
	/**
	 * 医院信息
	 * @param str
	 * @return
	 */
    @RequestMapping(value = "/hospital/v1", method = RequestMethod.POST)
    public Result hospital(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/hospital]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/hospital]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/hospital]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveHospitals(list,result);

        LOGGER.info("绩效平台[drgUpload/hospital]处理完成"+result);
        return result;
    }
    /**
     * 3.2	院区信息
     * @param str
     * @return
     */
    @RequestMapping(value = "/district/v1", method = RequestMethod.POST)
    public Result district(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/hospital]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/district]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/district]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveDistrict(list,result);

        LOGGER.info("绩效平台[drgUpload/district]处理完成"+result);
        return result;
    }
    /**
     * 3.3	科室组/大科室信息
     * @param str
     * @return
     */
    @RequestMapping(value = "/bdept/v1", method = RequestMethod.POST)
    public Result bdept(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/hospital]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/bdept]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/bdept]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveIfDepartmentGroup(list,result);

        LOGGER.info("绩效平台[drgUpload/bdept]处理完成"+result);
        return result;
    }
    /**
     * 3.4	标准科室
     * @param str
     * @return
     */
    @RequestMapping(value = "/s_department/v1", method = RequestMethod.POST)
    public Result department(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/department]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/department]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/department]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveDepartments(list,result);

        LOGGER.error("绩效平台[drgUpload/department]处理完成"+result);
        return result;
    }
    /**
     * 3.5	科室匹配
     * @param str
     * @return
     */
    @RequestMapping(value = "/departmentMatch/v1", method = RequestMethod.POST)
    public Result departmentMatch(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/departmentMatch]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/departmentMatch]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/departmentMatch]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveDepartmentMatchs(list,result);

        LOGGER.info("绩效平台[drgUpload/departmentMatch]处理完成"+result);
        return result;
    }
    /**
     * 3.6	院内科室
     * @param str
     * @return
     */
    @RequestMapping(value = "/h_dept/v1", method = RequestMethod.POST)
    public Result h_dept(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/h_dept]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/h_dept]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/h_dept]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveIfDepartmentHos(list,result);

        LOGGER.info("绩效平台[drgUpload/h_dept]处理完成"+result);
        return result;
    }
    
    /**
     * 3.7	治疗组
     * @param str
     * @return
     */
    @RequestMapping(value = "/doctorGroup/v1", method = RequestMethod.POST)
    public Result doctorGroup(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/h_dept]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/h_dept]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/h_dept]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveIfDoctorGroup(list,result);

        LOGGER.info("绩效平台[drgUpload/h_dept]处理完成"+result);
        return result;
    }
    
    @RequestMapping(value = "/orga/v1", method = RequestMethod.POST)
    public Result orga(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/orga]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/orga]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/orga]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveOrgas(list,result);

        LOGGER.info("绩效平台[drgUpload/orga]处理完成"+result);
        return result;
    }
    /**
     * 员工信息
     * @param str
     * @return
     */
    @RequestMapping(value = "/staff/v1", method = RequestMethod.POST)
    public Result staff(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/staff]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/staff]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/staff]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveStaffs(list,result);

        LOGGER.info("绩效平台[drgUpload/staff]处理完成"+result);
        return result;
    }

    /**
     * 3.10	临床诊断
     * @param str
     * @return
     */
    @RequestMapping(value = "/ccdt/v1", method = RequestMethod.POST)
    public Result ccdt(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/ccdt]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/ccdt]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/ccdt]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveIfccdt(list,result);

        LOGGER.info("绩效平台[drgUpload/ccdt]处理完成"+result);
        return result;
    }
    
    /**
     * cchi
     * @param str
     * @return
     */
    @RequestMapping(value = "/cchi/v1", method = RequestMethod.POST)
    public Result cchi(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/cchi]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
            result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/cchi]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/cchi]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveIfcchi(list,result);

        LOGGER.info("绩效平台[drgUpload/cchi]处理完成"+result);
        return result;
    }
    
    @RequestMapping(value = "/chargeDetail/v1", method = RequestMethod.POST)
    public Result chargeDetail(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/chargeDetail]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
        	result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/chargeDetail]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/chargeDetail]处理完成"+result);
			return result;
		}

		if (list.size() > ReceiveSize) {
			result.setMSG("单次接收数据不能超过:"+ReceiveSize);
			result.setCODE(105);
			LOGGER.info("绩效平台[drgUpload/chargeDetail]处理完成:"+result);
			return result;
		}
		
        drgBaDataUploadService.saveChargeDetails(list,result);

        LOGGER.info("绩效平台[drgUpload/chargeDetail]处理完成"+result);
        return result;
    }

    @RequestMapping(value = "/icd10/v1", method = RequestMethod.POST)
    public Result icd10(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/department]接收到数据：" + str);
        Result result = new Result();
        /*String jsonStr = null;
        try {
            jsonStr = GZIPString.unCompress(str);
        } catch (IOException e) {
        	result.setMSG("参数解压失败");
			result.setCODE(102);
			return result;
        }*/
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/icd10]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/icd10]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveICD10s(list,result);

        LOGGER.info("绩效平台[drgUpload/icd10]处理完成"+result);
        return result;
    }
    
    @RequestMapping(value = "/balance/v1", method = RequestMethod.POST)
    public Result balance(@RequestBody String str) {
        Result result = new Result();
		if(null == str){
			result.setMSG("参数值为空");
			result.setCODE(101);
			return result;
		}
		/*String jsonStr = null;
		try {
			jsonStr = GZIPString.unCompress(str);
		} catch (IOException e) {
			result.setMSG("参数解压失败");
			result.setCODE(102);
			LOGGER.info("绩效平台[drgUpload/balance]解压字符串异常："+e.getMessage());
			return result;
		}*/
		//LOGGER.debug("绩效平台[drgUpload/hospital]接收到数据：" + jsonStr);
		
		List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/balance]处理完成:"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/balance]处理完成:"+result);
			return result;
		}
		if (list.size() > ReceiveSize) {
			result.setMSG("单次接收数据不能超过:"+ReceiveSize);
			result.setCODE(105);
			LOGGER.error("绩效平台[drgUpload/balance]处理完成:"+result);
			return result;
		}
        drgBaDataUploadService.saveBalances(list,result);
        LOGGER.info("绩效平台[drgUpload/balance]处理完成:"+JSONObject.toJSONString(result));
        return result;
    }
    
    @RequestMapping(value = "/icd9/v1", method = RequestMethod.POST)
    public Result icd9(@RequestBody String str) {
        //LOGGER.debug("绩效平台[drgUpload/department]接收到数据：" + str);
        Result result = new Result();
        List<Object> list = null;
		try {
			list = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgUpload/icd9]处理完成"+result);
			return result;
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgUpload/icd9]处理完成"+result);
			return result;
		}

        drgBaDataUploadService.saveICD9(list,result);

        LOGGER.info("绩效平台[drgUpload/icd9]处理完成"+result);
        return result;
    }
}
