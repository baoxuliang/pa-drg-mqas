package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 3.6	院内科室
 * @author jindoulixing
 */
@Data
public class IfDepartmentHos implements Serializable {

    private static final long serialVersionUID = 6687402309353782892L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String DISTRICT_CODE;

    private String B_DEPARTMENT_CODE;
    
    private String H_DEPARTMENT_CODE;
    private String H_DEPARTMENT_NAME;
    private String STATUS;
    private String TYPE;

    private Date SYNDATE;

    //private String SYNFLAG;
}
