package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 3.7	治疗组
 * @author jindoulixing
 */
@Data
public class IfDoctorGroup implements Serializable {

    private static final long serialVersionUID = 6687402309353782892L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String DISTRICT_CODE;

    private String B_DEPARTMENT_CODE;
    
    private String H_DEPARTMENT_CODE;
    private String DOCTOR_CODE;
    private String DOCTOR_NAME;

    private Date SYNDATE;

    //private String SYNFLAG;
}
