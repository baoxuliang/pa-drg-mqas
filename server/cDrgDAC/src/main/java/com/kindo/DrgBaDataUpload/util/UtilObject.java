package com.kindo.DrgBaDataUpload.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.kindo.DrgBaDataUpload.model.FailDetail;

public class UtilObject {
	/** 
     * 判断对象或对象数组中每一个对象是否为空: 对象为null，字符序列长度为0，集合类、Map为empty 
     *  
     * @param obj 
     * @return 
     */  
    public static boolean isNullOrEmpty(Object obj) {  
        if (obj == null)  
            return true;  
  
        if (obj instanceof CharSequence)  
            return ((CharSequence) obj).length() == 0;  
  
        if (obj instanceof Collection)  
            return ((Collection) obj).isEmpty();  
  
        if (obj instanceof Map)  
            return ((Map) obj).isEmpty();  
  
        if (obj instanceof Object[]) {  
            Object[] object = (Object[]) obj;  
            if (object.length == 0) {  
                return true;  
            }  
            boolean empty = true;  
            for (int i = 0; i < object.length; i++) {  
                if (!isNullOrEmpty(object[i])) {  
                    empty = false;  
                    break;  
                }  
            }  
            return empty;  
        }  
        return false;  
    }
    /** 
     *  
     * @param obj 
     * @return 
     */  
    public static boolean checkNullOrEmpty(JSONObject obj,List<String> fl,final FailDetail sb) {
    	boolean  falg = true;
    	StringBuffer ss =  new StringBuffer();
    	for (String s : fl) { 
    		if(isNullOrEmpty(obj.getString(s))){
    			falg = false;
    			ss.append(s).append(",");
        	}
    	}
    	sb.setDESC(ss.toString());
	    return falg;
    }  
}
