package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FailDetail implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8878377737911488626L;

	private String ID;
	
	private String DESC;
	
}
