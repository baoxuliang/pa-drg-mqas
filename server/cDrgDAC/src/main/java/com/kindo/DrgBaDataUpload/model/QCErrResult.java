package com.kindo.DrgBaDataUpload.model;

import java.util.Date;

import lombok.Data;

@Data
public class QCErrResult {
	private String UID;
	private String MEMBER_CODE;
	private String BAH;
	private int ZYCS;
	private String ERR_CODE;
	private String ERR_NAME;
	private String ERR_MEMO;
	private String ERR_TPYE;
	private Date CHECK_TIME;
	private String ID;
	
	public QCErrResult(String UID,String ID,String MEMBER_CODE,String BAH,int ZYCS, String ERR_CODE,String ERR_NAME,String ERR_MEMO,String ERR_TPYE){
		this.UID = UID;
		this.ID = ID;
		this.MEMBER_CODE=MEMBER_CODE;
		this.BAH =BAH;
		this.ZYCS = ZYCS;
		this.ERR_CODE = ERR_CODE;
		this.ERR_NAME = ERR_NAME;
		this.ERR_MEMO = ERR_MEMO;
		this.ERR_TPYE = ERR_TPYE;
	}
}
