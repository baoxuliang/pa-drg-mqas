package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8609584659719572867L;

	/**
	 * 状态码
	 */
	private Integer CODE;
	
	/**
	 * 提示信息
	 */
	private String MSG;
	
	/**
	 * failNum>0 details有值
	 */
	private Integer FAILNUM;
	
	/**
	 * 详细信息
	 */
	private List<FailDetail> FAILS = new ArrayList<FailDetail>();
		
}
