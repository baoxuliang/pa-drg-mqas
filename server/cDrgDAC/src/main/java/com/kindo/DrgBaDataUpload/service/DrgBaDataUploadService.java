package com.kindo.DrgBaDataUpload.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kindo.DrgBaDataUpload.dao.DrgBaInfoMapper;
import com.kindo.DrgBaDataUpload.model.ChargeDetail;
import com.kindo.DrgBaDataUpload.model.Department;
import com.kindo.DrgBaDataUpload.model.DepartmentMatch;
import com.kindo.DrgBaDataUpload.model.District;
import com.kindo.DrgBaDataUpload.model.FailDetail;
import com.kindo.DrgBaDataUpload.model.Hospital;
import com.kindo.DrgBaDataUpload.model.ICD10;
import com.kindo.DrgBaDataUpload.model.ICD9;
import com.kindo.DrgBaDataUpload.model.IfBaInfo;
import com.kindo.DrgBaDataUpload.model.IfBalance;
import com.kindo.DrgBaDataUpload.model.IfDepartmentGroup;
import com.kindo.DrgBaDataUpload.model.IfDepartmentHos;
import com.kindo.DrgBaDataUpload.model.IfDoctorGroup;
import com.kindo.DrgBaDataUpload.model.IfFailLog;
import com.kindo.DrgBaDataUpload.model.Ifccdt;
import com.kindo.DrgBaDataUpload.model.Ifcchi;
import com.kindo.DrgBaDataUpload.model.Orga;
import com.kindo.DrgBaDataUpload.model.QCErrResult;
import com.kindo.DrgBaDataUpload.model.Result;
import com.kindo.DrgBaDataUpload.model.Staff;

@Service
public class DrgBaDataUploadService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataUploadService.class);
    private static final int BATCH_SIZE = 1000;
    private static ExecutorService executor = Executors.newFixedThreadPool(18);
    
    @Autowired
    private DrgBaInfoMapper drgBaInfoMapper;

    // 病案数据批量入库
    public Result saveIfBaInfoDatas(String bastr, Result result) {
    	JSONArray jsonArr = null;
		try {
			jsonArr = JSONObject.parseArray(bastr);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			return result;
		}

		if (jsonArr == null || jsonArr.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			return result;
		}
		LOGGER.info("saveIfBaInfoDatas数据校验接收数据条数:"+jsonArr.size());
    	try {
    		long ssql = System.currentTimeMillis();
            Map<Boolean, List<JSONObject>> retAMap = jsonArr.parallelStream()
                    .map(item -> (JSONObject) item)
                    .peek(item -> item.put("FAILS",
                    		Stream.of("MEMBER_CODE:30","MEMBER_NAME:80","ZYH:30","ZYCS:3","YLFKFS:1","BAH:20","XM:40",
                    				"XB:1","RYSJ:24","RYKB:30","RYKBBM:10","CYSJ:24","CYKB:30","CYKBBM:10","SJZYTS:4","MZZD:150",
                    				"JBBM:20","ZYZD:150","JBDM:20","RYBQ:1","ZFY:12","RYBQ4=1","QTZD5=150","JBDM4=20",
                    				"JKKH=50","CSRQ=24","NL=3","GJ=40","BZYZSNL=5","XSERYTZ=6","CSD=300","JG=300","MZ=20","SFZH=18",
                    				"HY=1","RYTJ=1","ZKKB=30","ZKKBBM1=10","ZKKBBM2=10","ZKKBBM3=10","RYZDMC=150","RYZDBM=20","RYQZRQ=24","QTZD1=150",
                    				"JBDM1=20","RYBQ1=1","QTZD2=150","JBDM2=20","RYBQ2=1","QTZD3=150","JBDM3=20","RYBQ3=1","QTZD4=150","JBDM5=20",
                    				"RYBQ5=1","QTZD6=150","JBDM6=20","RYBQ6=1","QTZD7=150","JBDM7=20","RYBQ7=1","QTZD8=150","JBDM8=20","RYBQ8=1",
                    				"QTZD9=150","JBDM9=20","RYBQ9=1","QTZD10=150","JBDM10=20","RYBQ10=1","QTZD11=150","JBDM11=20","RYBQ11=1",
                    				"QTZD12=150","JBDM12=20","RYBQ12=1","QTZD13=150","JBDM13=20","RYBQ13=1","QTZD14=150","JBDM14=20","RYBQ14=1",
                    				"QTZD15=150","JBDM15=20","RYBQ15=1","WBYY=150","H23=20","KZR=40","KZR_CODE=10","ZRYS=40","ZRYS_CODE=10",
                    				"ZZYS=40","ZZYS_CODE=10","ZYYS=40","ZYYS_CODE=10","ZRHS=40","JXYS=40","SXYS=40","BMY=40","BAZL=1",
                    				"ZKYS=40","ZKHS=40","ZKRQ=24","SSJCZBM1=20","SSJCZRQ1=24","SSJB1=1","SSJCZMC1=150","SZ1=40","YZ1=40",
                    				"EZ1=40","QKYHDJ1=2","MZFS1=6","MZYS1=40","SSJCZBM2=20","SSJCZRQ2=24","SSJB2=1","SSJCZMC2=150",
                    				"SZ2=40","YZ2=40","EZ2=40","QKYHDJ2=2","MZFS2=6","MZYS2=40","SSJCZBM3=20","SSJCZRQ3=24","SSJB3=1",
                    				"SSJCZMC3=150","SZ3=40","YZ3=40","EZ3=40","QKYHDJ3=2","MZFS3=6","MZYS3=40","SSJCZBM4=20","SSJCZRQ4=24",
                    				"SSJB4=1","SSJCZMC4=150","SZ4=40","YZ4=40","EZ4=40","QKYHDJ4=2","MZFS4=6","MZYS4=40","SSJCZBM5=20",
                    				"SSJCZRQ5=24","SSJB5=1","SSJCZMC5=150","SZ5=40","YZ5=40","EZ5=40","QKYHDJ5=2","MZFS5=6","MZYS5=40",
                    				"SSJCZBM6=20","SSJCZRQ6=24","SSJB6=1","SSJCZMC6=150","SZ6=40","YZ6=40","EZ6=40","QKYHDJ6=2","MZFS6=6",
                    				"MZYS6=40","SSJCZBM7=20","SSJCZRQ7=24","SSJB7=1","SSJCZMC7=150","SZ7=40","YZ7=40","EZ7=40","QKYHDJ7=2",
                    				"MZFS7=6","MZYS7=40","SSJCZBM8=20","SSJCZRQ8=24","SSJB8=1","SSJCZMC8=150","SZ8=40","YZ8=40","EZ8=40",
                    				"QKYHDJ8=2","MZFS8=6","MZYS8=40","QJCS=3","CGCS=3",
                    				"LYFS=1","YZZY_YLJG=100","SFZZYJH=1","MD=100",
                    				"ZFY=13","ZFJE=13","YLFUF=13","ZLCZF=13","HLF=13","QTFY=13","BLZDF=13","SYSZDF=13","YXXZDF=13","LCZDXMF=13",
                    				"FSSZLXMF=13","WLZLF=13","SSZLF=13","MZF=13","SSF=13","KFF=13","ZYZLF=13","XYF=13","KJYWF=13","ZCYF=13",
                    				"ZCYF1=13","XF=13","BDBLZPF=13","QDBLZPF=13","NXYZLZPF=13","XBYZLZPF=13","HCYYCLF=13","YYCLF=13","YCXYYCLF=13",
                    				"QTF=13","GDRQ=24","XSECSTZ_1=6","XSECSTZ_2=6","XSECSTZ_3=6","XSECSTZ_4=6","XSECSTZ_5=6","DISTRICT_CODE:12")
                            .filter(field -> {
                            	String[] sarr = field.split(":"); 
                            	String[] sarr1 = field.split("=");
                            	boolean falg = false;
                            	if(sarr.length>1){
                            		int lenght = Integer.parseInt(sarr[1]);
                                	String ss = item.getString(sarr[0]);
                                	falg = StringUtils.isEmpty(ss) || ss.length() > lenght;
                            	}
                            	if(sarr1.length>1){
                            		int lenght = Integer.parseInt(sarr1[1]);
                            		String ss = item.getString(sarr1[0]);
                            		if(!StringUtils.isEmpty(ss)){
                            			falg = ss.length() > lenght;
                            		}
                            	}
                            	return falg;
                            })
                            .map(field -> {
                            	if(field.split(":").length > 1){
                            		return field.split(":")[0];
                            	}
                            	if(field.split("=").length > 1){
                            		return field.split("=")[0];
                            	}
                            	return field;
                            })
                    .collect(Collectors.joining(","))))
                    .peek(item -> {if(!item.getString("FAILS").isEmpty())result.getFAILS().add(new FailDetail(item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"), "非空或超长字段"+item.getString("FAILS")));})
                    .collect(Collectors.partitioningBy(item -> item.getString("FAILS").isEmpty()));
            
            final List<IfBaInfo> dataList =  new ArrayList<IfBaInfo>();
            retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 IfBaInfo bainfo = JSONObject.toJavaObject(item, IfBaInfo.class);
			    	 bainfo.setID(bainfo.getMEMBER_CODE()+"$"+bainfo.getDISTRICT_CODE()+"$"+bainfo.getBAH()+"$"+bainfo.getZYCS());
			    	 dataList.add(bainfo);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("FAILS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfBaInfoDatas病案数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfBaInfoDatas病案数据参数格式错误,数据为:"+item);
				}
            });
            int num = result.getFAILS().size();
            if(num > 0){
            	result.setCODE(201);
            	result.setFAILNUM(num);
                result.setMSG("部分成功(返回校验失败信息)");
            }else{
            	result.setCODE(200);
                result.setMSG("上传数据成功");
            }
            long esql = System.currentTimeMillis();
    		LOGGER.info("数据校验耗时==================="+(esql - ssql) +"ms");
            LOGGER.info("saveIfBaInfoDatas校验结果成功条数:"+retAMap.get(true).size());
            
        	/*final List<IfBaInfo> dataList =  retAMap.get(true).stream().map(item -> JSONObject.toJavaObject(item, IfBaInfo.class))
					.peek(item -> item.setUID(java.util.UUID.randomUUID().toString())).collect(Collectors.toList());*/
			executor.submit(new Thread(()-> {
				try {
					int[] idx = { 0 };
					int[] count = { 0 };
					dataList.stream().collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
					        .forEach(item -> {
					        	try {
					        		drgBaInfoMapper.deleteBatchQcErrResult(item);
					        	    drgBaInfoMapper.deleteBatchDrgAudit(item);
									//drgBaInfoMapper.deleteBatchDrgBaInfos(item);
									count[0] += drgBaInfoMapper.insertBatchDrgBaInfoBaseDatas(item);
								} catch (Exception e) {
									LOGGER.error("saveIfBaInfoDatas病案数据批量入库失败:"+e.getMessage());
									LOGGER.error("saveIfBaInfoDatas数据批量入库异常,入库数据为:"+JSONObject.toJSONString(item));
								}
					        });
					LOGGER.info("saveIfBaInfoDatas校验结果成功入库条数:"+count[0]);
				} catch (Exception e) {
					LOGGER.error("saveIfBaInfoDatas病案数据入库失败:"+e.getMessage());
					LOGGER.error("saveIfBaInfoDatas数据入库异常,数据为:"+JSONObject.toJSONString(retAMap.get(true)));
				}
			}
			));
            
            executor.submit(new Thread(()-> {
				try {
					int[] idx1 = { 0 };
					retAMap.get(false).stream()
					.map(item -> new QCErrResult(java.util.UUID.randomUUID().toString(),item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"),item.getString("MEMBER_CODE"),item.getString("BAH"),Integer.valueOf(item.getString("ZYCS")),"1","非空,超长,数据格式校验",item.getString("FAILS"),"1"))
					.collect(Collectors.groupingBy(item -> idx1[0]++ / BATCH_SIZE, Collectors.toList())).values()
					.forEach(item -> {
						try {
							LOGGER.error("deleteBatchQCErrResultDatas非空和超长校验失败记录数据库:"+item);
							drgBaInfoMapper.deleteBatchQCErrResultDatas(item);
							drgBaInfoMapper.insertBatchQCErrResultDatas(item);
						} catch (Exception e) {
							LOGGER.error("saveIfBaInfoDatas非空和超长校验失败记录数据库异常:"+e.getMessage());
							LOGGER.error("saveIfBaInfoDatas非空和超长校验失败记录数据库异常,数据为:"+JSONObject.toJSONString(retAMap.get(false)));
						}
					});
				} catch (Exception e) {
					LOGGER.error("saveIfBaInfoDatas基础校验失败记录数据库异常:"+e.getMessage());
					LOGGER.error("saveIfBaInfoDatas基础校验失败记录数据库异常,数据为:"+JSONObject.toJSONString(retAMap.get(false)));
				}
			}
            ));
        } catch (Exception e) {
        	result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfBaInfoDatas病案数据处理异常:"+e.getMessage());
        }
    	
        return result;
    }

    public Result saveHospitals(List<Object> list,Result result) {
        try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "MEMBER_LEVEL_CODE_1:1","MEMBER_LEVEL_CODE_2:1", "MEMBER_TYPE_CODE:2", "REGION_CODE:6"}; 

			Map<Boolean, List<JSONObject>> retAMap = checkDatas(list, values);
			
			final List<Hospital> dataList =  new ArrayList<Hospital>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 Hospital hospital = JSONObject.toJavaObject(item, Hospital.class);
			    	 dataList.add(hospital);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("MEMBER_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveHospitals医院信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveHospitals医院信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList, drgBaInfoMapper::deleteBatchHospitals,drgBaInfoMapper::insertBatchHospitals);

			/*saveDatas(retAMap.get(true), Hospital.class, drgBaInfoMapper::deleteBatchHospitals,
			        drgBaInfoMapper::insertBatchHospitals);*/
			createFailResult(retAMap.get(false),result,"IF_HOSPITAL");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveHospitals医院信息数据处理异常:"+e.getMessage());
		}
        
        return result;
    }
    
    public Result saveDistrict(List<Object> list,Result result) {
        try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "DISTRICT_CODE:12","DISTRICT_NAME:30"}; 

			Map<Boolean, List<JSONObject>> retAMap = checkDatas(list, values);
			
			final List<District> dataList =  new ArrayList<District>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 District district = JSONObject.toJavaObject(item, District.class);
			    	 dataList.add(district);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveDistrict院区信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveDistrict院区信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList, drgBaInfoMapper::deleteBatchDistrict,drgBaInfoMapper::insertBatchDistrict);

			/*saveDatas(retAMap.get(true), Hospital.class, drgBaInfoMapper::deleteBatchHospitals,
			        drgBaInfoMapper::insertBatchHospitals);*/
			createFailResult(retAMap.get(false),result,"if_district");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveDistrict院区信息数据处理异常:"+e.getMessage());
		}
        
        return result;
    }
    
    public Result saveIfDepartmentGroup(List<Object> list,Result result) {
        try {
			String values[] = new String[]{"MEMBER_CODE:30", "DISTRICT_CODE=12", "B_DEPARTMENT_CODE:6","B_DEPARTMENT_NAME:20"}; 

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);
			
			final List<IfDepartmentGroup> dataList =  new ArrayList<IfDepartmentGroup>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 IfDepartmentGroup ifDepartmentGroup = JSONObject.toJavaObject(item, IfDepartmentGroup.class);
			    	 dataList.add(ifDepartmentGroup);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("B_DEPARTMENT_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfDepartmentGroup科室组信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfDepartmentGroup科室组信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList, drgBaInfoMapper::deleteBatchIfDepartmentGroup,drgBaInfoMapper::insertBatchIfDepartmentGroup);

			/*saveDatas(retAMap.get(true), Hospital.class, drgBaInfoMapper::deleteBatchHospitals,
			        drgBaInfoMapper::insertBatchHospitals);*/
			createFailResult(retAMap.get(false),result,"if_department_group");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfDepartmentGroup科室组信息数据处理异常:"+e.getMessage());
		}
        
        return result;
    }
    
    public Result saveDepartments(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"DEPARTMENT_CODE:6", "DEPARTMENT_NAME:20","PARENT_CODE:6"};

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<Department> dataList =  new ArrayList<Department>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 Department department = JSONObject.toJavaObject(item, Department.class);
			    	 dataList.add(department);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("DEPARTMENT_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveDepartments标准科室信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveDepartments标准科室信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList,drgBaInfoMapper::deleteBatchDepartments, drgBaInfoMapper::insertBatchDepartments);
			
			/*saveDatas(retAMap.get(true), Department.class, drgBaInfoMapper::deleteBatchDepartments,
			        drgBaInfoMapper::insertBatchDepartments);*/
			createFailResult(retAMap.get(false),result,"IF_DEPARTMENT");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveDepartments标准科室信息数据处理异常:"+e.getMessage());
		}
        
        return result;
    }

    public Result saveDepartmentMatchs(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "DISTRICT_CODE=12","H_DEPARTMENT_CODE:10","S_DEPARTMENT_CODE:6"};

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<DepartmentMatch> dataList =  new ArrayList<DepartmentMatch>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 DepartmentMatch department = JSONObject.toJavaObject(item, DepartmentMatch.class);
			    	 dataList.add(department);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("HOSPITAL_DEPARTMENT_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveDepartmentMatchs科室匹配信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveDepartmentMatchs科室匹配信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList,drgBaInfoMapper::deleteBatchDepartmentMatchs,drgBaInfoMapper::insertBatchDepartmentMatchs);
			
			/*saveDatas(retAMap.get(true), DepartmentMatch.class, drgBaInfoMapper::deleteBatchDepartmentMatchs,
			        drgBaInfoMapper::insertBatchDepartmentMatchs);*/

			createFailResult(retAMap.get(false),result,"IF_DEPARTMENT_MAP");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveDepartmentMatchs科室匹配信息数据处理异常:"+e.getMessage());
		}
        return result;
    }

    public Result saveIfDepartmentHos(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "DISTRICT_CODE=12","B_DEPARTMENT_CODE=6", "H_DEPARTMENT_CODE:6","H_DEPARTMENT_NAME:20","STATUS:1","TYPE:2"};

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<IfDepartmentHos> dataList =  new ArrayList<IfDepartmentHos>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 IfDepartmentHos ifDepartmentHos = JSONObject.toJavaObject(item, IfDepartmentHos.class);
			    	 dataList.add(ifDepartmentHos);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("MEMBER_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfDepartmentHos院内科室信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfDepartmentHos院内科室信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList,drgBaInfoMapper::deleteBatchIfDepartmentHos,drgBaInfoMapper::insertBatchIfDepartmentHos);
			
			/*saveDatas(retAMap.get(true), DepartmentMatch.class, drgBaInfoMapper::deleteBatchDepartmentMatchs,
			        drgBaInfoMapper::insertBatchDepartmentMatchs);*/

			createFailResult(retAMap.get(false),result,"if_department_hos");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfDepartmentHos院内科室信息数据处理异常:"+e.getMessage());
		}
        return result;
    }
    
    public Result saveIfDoctorGroup(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "DISTRICT_CODE=12","B_DEPARTMENT_CODE=6","H_DEPARTMENT_CODE:6", "DOCTOR_CODE:6","DOCTOR_NAME:40"};

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<IfDoctorGroup> dataList =  new ArrayList<IfDoctorGroup>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 IfDoctorGroup ifDoctorGroup = JSONObject.toJavaObject(item, IfDoctorGroup.class);
			    	 dataList.add(ifDoctorGroup);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("MEMBER_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfDoctorGroup治疗组信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfDoctorGroup治疗组信息数据参数格式错误,数据为:"+item);
				}
	        });
			
			saveDatas(dataList,drgBaInfoMapper::deleteBatchIfDoctorGroup,drgBaInfoMapper::insertBatchIfDoctorGroup);
			
			/*saveDatas(retAMap.get(true), DepartmentMatch.class, drgBaInfoMapper::deleteBatchDepartmentMatchs,
			        drgBaInfoMapper::insertBatchDepartmentMatchs);*/

			createFailResult(retAMap.get(false),result,"if_doctor_group");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfDoctorGroup治疗组信息数据处理异常:"+e.getMessage());
		}
        return result;
    }
    
    public Result saveOrgas(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:10", "MEMBER_NAME:30", "ORIGINAL_CODE:12", "ORIGINAL_NAME:30","ORIGINAL_HOSPITAL_CODE:20","STATUS:1","TYPE:2"};

			Map<Boolean, List<JSONObject>> retAMap = checkDatas(list, values);
			
			final List<Orga> dataList =  new ArrayList<Orga>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 Orga orga = JSONObject.toJavaObject(item, Orga.class);
			    	 orga.setUID(java.util.UUID.randomUUID().toString());
			    	 dataList.add(orga);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveOrgas组织机构信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveOrgas组织机构信息数据参数格式错误,数据为:"+item);
				}
	        });
			saveDatas(dataList, drgBaInfoMapper::deleteBatchOrgas, drgBaInfoMapper::insertBatchOrgas);
			
			createFailResult(retAMap.get(false),result,"IF_ORGANIZATION");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveOrgas组织机构信息数据处理异常:"+e.getMessage());
		}
        return result;
    }

    public Result saveStaffs(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80","DISTRICT_CODE=12","B_DEPARTMENT_CODE=6","H_DEPARTMENT_CODE:6","DOCTOR_CODE=6", "USER_CODE:10", "USER_NAME:40","USER_SEX=1","USER_PHONE=11","USER_TITLE=30","USER_TYPE:1"};//"USER_SEX", 

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);
			
			final List<Staff> dataList =  new ArrayList<Staff>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 Staff staff = JSONObject.toJavaObject(item, Staff.class);
			    	 dataList.add(staff);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("USER_CODE"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveStaffs员工信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveStaffs员工信息数据参数格式错误,数据为:"+item);
				}
	        });
			saveDatas(dataList, drgBaInfoMapper::deleteBatchStaffs,drgBaInfoMapper::insertBatchStaffs);

			createFailResult(retAMap.get(false),result,"IF_STAFF");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveStaffs员工信息数据处理异常:"+e.getMessage());
		}
        return result;
    }
    
    public Result saveIfccdt(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "DISTRICT_CODE:12", "BAH:20","ZYCS:3", "CCDT_MAIN_CODE:10","CCDT_MAIN_NAME:100","CCDT_CODE_1=10","CCDT_NAME_1=100","CCDT_TREAT_1=1","CCDT_CODE_2=10","CCDT_NAME_2=100","CCDT_TREAT_2=1","CCDT_CODE_3=10","CCDT_NAME_3=100","CCDT_TREAT_3=1","CCDT_CODE_4=10","CCDT_NAME_4=100","CCDT_TREAT_4=1"
					,"CCDT_CODE_5=10","CCDT_NAME_5=100","CCDT_TREAT_5=1","CCDT_CODE_6=10","CCDT_NAME_6=100","CCDT_TREAT_6=1","CCDT_CODE_7=10","CCDT_NAME_7=100","CCDT_TREAT_7=1","CCDT_CODE_8=10","CCDT_NAME_8=100","CCDT_TREAT_8=1"
					,"CCDT_CODE_9=10","CCDT_NAME_9=100","CCDT_TREAT_9=1","CCDT_CODE_10=10","CCDT_NAME_10=100","CCDT_TREAT_10=1","CCDT_CODE_11=10","CCDT_NAME_11=100","CCDT_TREAT_11=1","CCDT_CODE_12=10","CCDT_NAME_12=100","CCDT_TREAT_12=1"
					,"CCDT_CODE_13=10","CCDT_NAME_13=100","CCDT_TREAT_13=1","CCDT_CODE_14=10","CCDT_NAME_14=100","CCDT_TREAT_14=1","CCDT_CODE_15=10","CCDT_NAME_15=100","CCDT_TREAT_15=1"};//"USER_SEX", 

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);
			
			final List<Ifccdt> dataList =  new ArrayList<Ifccdt>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 Ifccdt ifccdt = JSONObject.toJavaObject(item, Ifccdt.class);
			    	 ifccdt.setID(ifccdt.getMEMBER_CODE()+"$"+ifccdt.getDISTRICT_CODE()+"$"+ifccdt.getBAH()+"$"+ifccdt.getZYCS());
			    	 dataList.add(ifccdt);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfccdt临床诊断信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfccdt临床诊断信息数据参数格式错误,数据为:"+item);
				}
	        });
	        saveDatasTwo(dataList, drgBaInfoMapper::deleteBatchIfccdt,drgBaInfoMapper::insertBatchIfccdt);

			createFailResultTwo(retAMap.get(false),result,"if_ccdt");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfccdt临床诊断信息数据处理异常:"+e.getMessage());
		}
        return result;
    }

    public Result saveIfcchi(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "DISTRICT_CODE:12", "BAH:20","ZYCS:3","CCHI_CODE_1=10","CCHI_NAME_1=100","CCHI_CODE_2=10","CCHI_NAME_2=100"
					,"CCHI_CODE_3=10","CCHI_NAME_3=100","CCHI_CODE_4=10","CCHI_NAME_4=100","CCHI_CODE_5=10","CCHI_NAME_5=100","CCHI_CODE_6=10","CCHI_NAME_6=100"
					,"CCHI_CODE_7=10","CCHI_NAME_7=100","CCHI_CODE_8=10","CCHI_NAME_8=100"};//"USER_SEX", 

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);
			
			final List<Ifcchi> dataList =  new ArrayList<Ifcchi>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 Ifcchi ifcchi = JSONObject.toJavaObject(item, Ifcchi.class);
			    	 ifcchi.setID(ifcchi.getMEMBER_CODE()+"$"+ifcchi.getDISTRICT_CODE()+"$"+ifcchi.getBAH()+"$"+ifcchi.getZYCS());
			    	 dataList.add(ifcchi);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveIfcchiCCHI信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveIfcchiCCHI信息数据参数格式错误,数据为:"+item);
				}
	        });
	        saveDatasTwo(dataList, drgBaInfoMapper::deleteBatchIfcchi,drgBaInfoMapper::insertBatchIfcchi);

			createFailResultTwo(retAMap.get(false),result,"if_cchi");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveIfcchiCCHI信息数据处理异常:"+e.getMessage());
		}
        return result;
    }
    
    public Result saveChargeDetails(List<Object> list,Result result) {
    	try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "DISTRICT_CODE:12","BAH:20", "ZYCS:3", "CFH:20","CFXH:2", "SFM:30",
			        "CFRQ=24", "SFRQ:24", "KLKSDM=20", "KLKSMC=30", "KLYSDM=10", "KLYSMC=10", "ZXKSDM=20", "ZXKSMC=30", "ZXYSDM=10", "ZXYSMC=10",
			        "JYLX:1", "JSZT:1", "XMBM:20", "XMMC:300", "XMDJ=10", "XMSL=10", "XMJE=13","XMLB:2"};

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);
			
			final List<ChargeDetail> dataList =  new ArrayList<ChargeDetail>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 ChargeDetail chargeDetail = JSONObject.toJavaObject(item, ChargeDetail.class);
			    	 chargeDetail.setID(chargeDetail.getMEMBER_CODE()+"$"+chargeDetail.getDISTRICT_CODE()+"$"+chargeDetail.getBAH()+"$"+chargeDetail.getZYCS());
			    	 dataList.add(chargeDetail);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveChargeDetails费用明细数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveChargeDetails费用明细数据参数格式错误,数据为:"+item);
				}
	        });
			/*final List<ChargeDetail> dataList =  retAMap.get(true).stream()
					.map(item -> JSONObject.toJavaObject(item, ChargeDetail.class))
					.collect(Collectors.toList());*/
	        saveDatasTwo(dataList,drgBaInfoMapper::deleteBatchChargeDetails,drgBaInfoMapper::insertBatchChargeDetails);
			//saveDatas(retAMap.get(true), ChargeDetail.class, drgBaInfoMapper::deleteBatchChargeDetails,drgBaInfoMapper::insertBatchChargeDetails);
			createFailResultTwo(retAMap.get(false),result,"IF_FEEDETAIL");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveChargeDetails费用明细信息数据处理异常:"+e.getMessage());
		}
        return result;
    }

    public Result saveICD9(List<Object> list,Result result) {
		try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80", "DISTRICT_CODE:12", "ICD_CODE:10","ICD_APPEND_CODE=20", "ICD_NAME:150", "IS_HOSPITAL_CODE:1"};//"ICD_APPEND_CODE", 

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<ICD9> dataList =  new ArrayList<ICD9>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 ICD9 iCD9 = JSONObject.toJavaObject(item, ICD9.class);
			    	 dataList.add(iCD9);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveICD9信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveICD9信息数据参数格式错误,数据为:"+item);
				}
	        });
	        
			saveDatas(dataList, drgBaInfoMapper::deleteBatchICD9,drgBaInfoMapper::insertBatchICD9);
			
			createFailResult(retAMap.get(false),result,"IF_ICD9");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveICD9信息数据处理异常:"+e.getMessage());
		}
        
        return result;
    }
    
    public Result saveICD10s(List<Object> list,Result result) {
		try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80","DISTRICT_CODE:12", "ICD_CODE:10","ICD_APPEND_CODE=20","ICD_NAME:150", "IS_HOSPITAL_CODE:1"};// "ICD_APPEND_CODE",

			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<ICD10> dataList =  new ArrayList<ICD10>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 ICD10 iCD10 = JSONObject.toJavaObject(item, ICD10.class);
			    	 dataList.add(iCD10);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveICD10s信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveICD10s信息数据参数格式错误,数据为:"+item);
				}
	        });
	        
			saveDatas(dataList, drgBaInfoMapper::deleteBatchICD10s,drgBaInfoMapper::insertBatchICD10s);
			
			createFailResult(retAMap.get(false),result,"IF_ICD10");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveICD10s信息数据处理异常:"+e.getMessage());
		}
        
        return result;
    }
    
    public Result saveBalances(List<Object> list,Result result) {
		try {
			String values[] = new String[]{"MEMBER_CODE:30", "MEMBER_NAME:80",  "DISTRICT_CODE:12","BAH:20","ZYCS:3","JSFS:1"
					,"YLFKFS=1","JSLX:1","JSSJ:24","ZFY:21","ZHZF=21","ZFZE=21","YHJE=21","GFJE=21","ABLZF=21","QFJE=21","YLZF=21","GRZFJE=21","OVERTAKE_OWNCOST=21","HOS_COST=21","GWYJZJE=21","YBDEBZ=21","TCZF=21"};
			
			Map<Boolean, List<JSONObject>> retAMap = checkDatasTwo(list, values);

			final List<IfBalance> dataList =  new ArrayList<IfBalance>();
	        retAMap.get(true).stream().forEach(item ->{
			     try {
			    	 IfBalance ifBalance = JSONObject.toJavaObject(item, IfBalance.class);
			    	 ifBalance.setID(ifBalance.getMEMBER_CODE()+"$"+ifBalance.getDISTRICT_CODE()+"$"+ifBalance.getBAH()+"$"+ifBalance.getZYCS());
			    	 dataList.add(ifBalance);
				} catch (Exception e) {
					result.getFAILS().add(new FailDetail(item.getString("ID"), "参数数据格式错误:"+e.getMessage()));
					//数据格式异常添加校验失败列表
					item.put("MISS", e.getMessage());
					retAMap.get(false).add(item);
					LOGGER.error("saveBalances结算信息数据参数格式错误:"+e.getMessage());
					LOGGER.error("saveBalances结算信息数据参数格式错误,数据为:"+item);
				}
	        });
			/*final List<IfBalance> dataList =  retAMap.get(true).stream()
					.map(item -> JSONObject.toJavaObject(item, IfBalance.class))
					.collect(Collectors.toList());*/
	        saveDatasTwo(dataList,drgBaInfoMapper::deleteBatchIfBalances,drgBaInfoMapper::insertBatchIfBalances);
			//saveDatas(retAMap.get(true), IfBalance.class, drgBaInfoMapper::deleteBatchIfBalances,drgBaInfoMapper::insertBatchIfBalances);
			createFailResultTwo(retAMap.get(false),result,"IF_BALANCE");
		} catch (Exception e) {
			result.setCODE(500);
            result.setMSG("处理失败");
        	LOGGER.error("saveSettlement结算信息数据处理异常:"+e.getMessage());
		}
        return result;
    }

    private Result createFailResult(List<JSONObject> list,Result result,String tableName) {
    	result.getFAILS().addAll(list.parallelStream()
                .map(item -> new FailDetail(item.getString("ID"),"非空,超长,数据格式校验"+item.getString("MISS")))
                .collect(Collectors.toList()));
        int num = list.size();
        if(num > 0){
        	result.setCODE(201);
        	result.setFAILNUM(num);
            result.setMSG("部分成功(返回校验失败信息)");
            List<IfFailLog> loglist = list.stream().map(item -> new IfFailLog(java.util.UUID.randomUUID().toString(),java.util.UUID.randomUUID().toString(),tableName,"非空,超长,数据格式校验",item.getString("MISS"),item.getString("MEMBER_CODE"),"",0,1))
            		//result.getFAILS().stream().map(item -> new IfFailLog(item.getID(),tableName,item.getDESC(),"非空,超长,数据格式校验"))
			.collect(Collectors.toList());
            saveFailLogs(loglist,drgBaInfoMapper::insertBatchIfFailLogDatas);
        }else{
        	result.setCODE(200);
        	result.setFAILNUM(0);
            result.setMSG("已接收");
        }
        return result;
    }
    
    private Result createFailResultTwo(List<JSONObject> list,Result result,String tableName) {
    	result.getFAILS().addAll(list.stream()
                .map(item -> new FailDetail(item.getString("ID"),"非空,超长,数据格式校验"+item.getString("MISS")))
                .collect(Collectors.toList()));
        int num = list.size();
        if(num > 0){
        	result.setCODE(201);
        	result.setFAILNUM(num);
            result.setMSG("部分成功(返回校验失败信息)");
            List<IfFailLog> loglist = list.stream().map(item -> new IfFailLog(java.util.UUID.randomUUID().toString(),item.getString("MEMBER_CODE")+"$"+item.getString("DISTRICT_CODE")+"$"+item.getString("BAH")+"$"+item.getString("ZYCS"),tableName,"非空,超长,数据格式校验",item.getString("MISS"),item.getString("MEMBER_CODE"),item.getString("BAH"),Integer.valueOf(null==item.getString("ZYCS")?"0":item.getString("ZYCS")),1))
            		//result.getFAILS().stream().map(item -> new IfFailLog(item.getID(),tableName,item.getDESC(),"非空,超长,数据格式校验"))
			.collect(Collectors.toList());
            saveFailLogs(loglist,drgBaInfoMapper::insertBatchIfFailLogDatas);
        }else{
        	result.setCODE(200);
        	result.setFAILNUM(0);
            result.setMSG("已接收");
        }
        return result;
    }
    
    private Map<Boolean, List<JSONObject>> checkDatas(List<Object> list, String [] values) {
        return list.parallelStream().map(item -> (JSONObject) item)
                .peek(item -> item.put("MISS",
                		Stream.of(values).parallel().filter(field ->{
                			String[] sarr = field.split(":"); 
                        	int lenght = Integer.parseInt(sarr[1]);
                        	String ss = item.getString(sarr[0]);
                        	return StringUtils.isEmpty(ss) || ss.length() > lenght;
                		})
                .map(field -> field.split(":")[0])
                .collect(Collectors.joining(","))))
                .collect(Collectors.partitioningBy(item -> StringUtils.isEmpty(item.getString("MISS"))));
    }
    
    private Map<Boolean, List<JSONObject>> checkDatasTwo(List<Object> list, String [] values) {
        return list.parallelStream().map(item -> (JSONObject) item)
                .peek(item -> item.put("MISS",
                		Stream.of(values).parallel().filter(field ->{
                			String[] sarr = field.split(":"); 
                			String[] sarr1 = field.split("=");
                			boolean falg = false;
                			if(sarr.length>1){
                				int lenght = Integer.parseInt(sarr[1]);
                		    	String ss = item.getString(sarr[0]);
                		    	falg = StringUtils.isEmpty(ss) || ss.length() > lenght;
                			}
                			if(sarr1.length>1){
                				int lenght = Integer.parseInt(sarr1[1]);
                				String ss = item.getString(sarr1[0]);
                				if(!StringUtils.isEmpty(ss)){
                					falg = ss.length() > lenght;
                				}
                			}
                			return falg;
                		})
                .map(field -> {
                	if(field.split(":").length > 1){
                		return field.split(":")[0];
                	}
                	if(field.split("=").length > 1){
                		return field.split("=")[0];
                	}
                	return field;
                })
                .collect(Collectors.joining(","))))
                .collect(Collectors.partitioningBy(item -> StringUtils.isEmpty(item.getString("MISS"))));
    }
    
    private <T> void saveDatas(List<JSONObject> list, Class<T> T, Consumer<List<T>> delOper,
            Consumer<List<T>> insOper) {
    	executor.submit(new Thread(()-> {
    			try {
					int[] idx = { 0 };
					list.stream().map(item -> JSONObject.toJavaObject(item, T))
					    .collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
					    .forEach(item -> {
					        try {
								//delOper.accept(item);
								insOper.accept(item);
							} catch (Exception e) {
								LOGGER.error(T.getName()+"数据批量入库异常,数据为:"+JSONObject.toJSONString(item));
								LOGGER.error(T.getName()+"数据批量入库异常:"+e.getMessage());
							}
					    });
				} catch (Exception e) {
					LOGGER.error(T.getName()+"数据入库异常,数据为:"+JSONObject.toJSONString(list));
					LOGGER.error(T.getName()+"数据入库异常:"+e.getMessage());
				}
    		})
         );
    }
    
    private <T> void saveDatas(List<T> list, Consumer<List<T>> delOper,Consumer<List<T>> insOper) {
    	executor.submit(new Thread(()-> {
    			try {
					int[] idx = { 0 };
					list.stream()
					    .collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
					    .forEach(item -> {
					        try {
								//delOper.accept(item);
								insOper.accept(item);
							} catch (Exception e) {
								LOGGER.error(insOper.getClass()+"数据批量入库异常,数据为:"+JSONObject.toJSONString(item));
								LOGGER.error(insOper.getClass()+"数据批量入库异常:"+e.getMessage());
							}
					    });
				} catch (Exception e) {
					LOGGER.error(insOper.getClass()+"数据入库异常,数据为:"+JSONObject.toJSONString(list));
					LOGGER.error(insOper.getClass()+"数据入库异常:"+e.getMessage());
				}
    		})
         );
    }
    
    private <T> void saveDatasTwo(List<T> list, Consumer<List<T>> delOper,Consumer<List<T>> insOper) {
    	executor.submit(new Thread(()-> {
    			try {
					int[] idx = { 0 };
					list.stream()
					    .collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
					    .forEach(item -> {
					        try {
								delOper.accept(item);
								insOper.accept(item);
							} catch (Exception e) {
								LOGGER.error(insOper.getClass()+"数据批量入库异常,数据为:"+JSONObject.toJSONString(item));
								LOGGER.error(insOper.getClass()+"数据批量入库异常:"+e.getMessage());
							}
					    });
				} catch (Exception e) {
					LOGGER.error(insOper.getClass()+"数据入库异常,数据为:"+JSONObject.toJSONString(list));
					LOGGER.error(insOper.getClass()+"数据入库异常:"+e.getMessage());
				}
    		})
         );
    }
    
    private <T> void saveFailLogs(List<T> list,Consumer<List<T>> delOper) {
    	executor.submit(new Thread(()-> {
    			try {
					int[] idx = { 0 };
					list.stream().collect(Collectors.groupingBy(item -> idx[0]++ / BATCH_SIZE, Collectors.toList())).values()
					.forEach(item -> {
					    try {
							delOper.accept(item);
						} catch (Exception e) {
							LOGGER.error(delOper.getClass()+"校验失败批量记录数据库异常,数据为:"+JSONObject.toJSONString(item));
							LOGGER.error(delOper.getClass()+"校验失败批量记录数据库异常:"+e.getMessage());
						}
					});
				} catch (Exception e) {
					LOGGER.error(delOper.getClass()+"校验失败记录数据库异常:"+e.getMessage());
				}
    		})
    	 );
    }
    
    /*public void dis3() {
        users.parallelStream().filter(distinctByKey(User::getId))
            .forEach(System.out::println);
    }


    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }*/
}
