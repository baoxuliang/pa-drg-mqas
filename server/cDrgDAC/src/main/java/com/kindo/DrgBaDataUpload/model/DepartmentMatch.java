package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 科室匹配
 * @author jindoulixing
 */
@Data
public class DepartmentMatch implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -397033595820099908L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String MEMBER_NAME;
    
    private String DISTRICT_CODE;
    
    private String H_DEPARTMENT_CODE;
    
    private String S_DEPARTMENT_CODE;

    //private String HOSPITAL_DEPARTMENT_CODE;

    //private String STANARD_DEPARTMENT_CODE;

    private Date SYNDATE;

    //private String SYNFLAG;
}
