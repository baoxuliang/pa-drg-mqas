package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
/**
 * 员工信息
 * @author jindoulixing
 *
 */
@Data
public class Staff implements Serializable {
    private static final long serialVersionUID = -2766423484099437766L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String MEMBER_NAME;
    
    private String DISTRICT_CODE;
    private String B_DEPARTMENT_CODE;
    private String H_DEPARTMENT_CODE;
    private String DOCTOR_CODE;

    private String USER_CODE;

    private String USER_NAME;

    private String USER_SEX;

    private String USER_PHONE;

    private String USER_TITLE;

    private String USER_TYPE;

    private Date SYNDATE;

    //private String SYNFLAG;
}
