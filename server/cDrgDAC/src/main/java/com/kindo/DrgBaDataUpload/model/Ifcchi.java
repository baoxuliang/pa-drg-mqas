package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
/**
 * CCHI
 * @author jindoulixing
 *
 */
@Data
public class Ifcchi implements Serializable {
    private static final long serialVersionUID = -2766423484099437766L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;
    private String MEMBER_NAME;
    private String DISTRICT_CODE;
    private String BAH;
    private Integer ZYCS;
    private String CCHI_CODE_1;
    private String CCHI_NAME_1;
    private String CCHI_CODE_2;
    private String CCHI_NAME_2;
    private String CCHI_CODE_3;
    private String CCHI_NAME_3;
    private String CCHI_CODE_4;
    private String CCHI_NAME_4;
    private String CCHI_CODE_5;
    private String CCHI_NAME_5;
    private String CCHI_CODE_6;
    private String CCHI_NAME_6;
    private String CCHI_CODE_7;
    private String CCHI_NAME_7;
    private String CCHI_CODE_8;
    private String CCHI_NAME_8;
    
    private Date SYNDATE;

    //private String SYNFLAG;
}
