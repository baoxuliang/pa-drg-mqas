package com.kindo.DrgBaDataUpload.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IfFailLog {
	private static final long serialVersionUID = -1429544213057969761L;
	
	private String UID;
	private String ID;
	private String TABLE_NAME;
	private String ERROR_NAME;
	private String ERROR_DESC;
	private String MEMBER_CODE;
	private String BAH;
	private Integer ZYCS;
	private Integer ERROR_TYPE;
	private Date GENERATE_DATE;

	
	public IfFailLog(String UID,String ID,String TABLE_NAME,String ERROR_NAME,String ERROR_DESC,String MEMBER_CODE,String BAH,Integer ZYCS,Integer ERROR_TYPE){
		this.UID = UID;
		this.ID = ID;
		this.TABLE_NAME=TABLE_NAME;
		this.ERROR_NAME =ERROR_NAME;
		this.ERROR_DESC = ERROR_DESC;
		this.MEMBER_CODE = MEMBER_CODE;
		this.BAH = BAH;
		this.ZYCS = ZYCS;
		this.ERROR_TYPE = ERROR_TYPE;
	}
}
