package com.kindo.DrgBaDataUpload.model;
/**
 * 3.15	结算信息
 */
import java.util.Date;

import lombok.Data;

@Data
public class IfBalance {
	 private String UID;
	 private String ID;
	 private String MEMBER_CODE;
	 private String MEMBER_NAME;
	 private String DISTRICT_CODE;
	 private String BAH;
	 private Integer ZYCS;
	 private String YLFKFS;
	 private String JSLX;
	 private Date JSSJ;
	 private Double ZFY;
	 private Double ZHZF;
	 private Double ZFZE;
	 private Double YHJE;
	 private Double GFJE;
	 private Double ABLZF;
	 private Double QFJE;
	 private Double YLZF;
	 private Double GRZFJE;
	 private Double OVERTAKE_OWNCOST;
	 private Double HOS_COST;
	 private Double GWYJZJE;
	 private Double YBDEBZ;
	 private Double TCZF;
	 private String JSFS;
	 private Date SYNDATE;
	 //private String SYNFLAG;
}
