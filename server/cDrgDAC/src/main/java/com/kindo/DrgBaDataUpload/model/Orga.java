package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Orga implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4437535159608487722L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String MEMBER_NAME;

    private String ORIGINAL_CODE;

    private String ORIGINAL_NAME;
    
    private String ORIGINAL_HOSPITAL_CODE;
    
    private String PARENT_CODE;

    private Date SYNDATE;

    private String STATUS;
    
    private String TYPE;
    
    //private String SYNFLAG;
}
