package com.kindo.DrgBaDataUpload.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DrgBaDataUpload.model.ChargeDetail;
import com.kindo.DrgBaDataUpload.model.Department;
import com.kindo.DrgBaDataUpload.model.DepartmentMatch;
import com.kindo.DrgBaDataUpload.model.District;
import com.kindo.DrgBaDataUpload.model.Hospital;
import com.kindo.DrgBaDataUpload.model.ICD10;
import com.kindo.DrgBaDataUpload.model.ICD9;
import com.kindo.DrgBaDataUpload.model.IfBaInfo;
import com.kindo.DrgBaDataUpload.model.IfBalance;
import com.kindo.DrgBaDataUpload.model.IfDepartmentGroup;
import com.kindo.DrgBaDataUpload.model.IfDepartmentHos;
import com.kindo.DrgBaDataUpload.model.IfDoctorGroup;
import com.kindo.DrgBaDataUpload.model.IfFailLog;
import com.kindo.DrgBaDataUpload.model.Ifccdt;
import com.kindo.DrgBaDataUpload.model.Ifcchi;
import com.kindo.DrgBaDataUpload.model.Orga;
import com.kindo.DrgBaDataUpload.model.QCErrResult;
import com.kindo.DrgBaDataUpload.model.Staff;

public interface DrgBaInfoMapper {

	// 批量插入病案数据基础表
    int insertBatchDrgBaInfoBaseDatas(@Param("list") List<IfBaInfo> dataList);
    // 批量插入病案数据
    int insertBatchDrgBaInfoDatas(@Param("list") List<IfBaInfo> dataList);
    // 批量删除病案数据
    int deleteBatchDrgBaInfos(@Param("list") List<IfBaInfo> dataList);
    // 批量删除校验日志数据
    int deleteBatchQcErrResult(@Param("list") List<IfBaInfo> dataList);
    // 批量删除DRG入组数据
    int deleteBatchDrgAudit(@Param("list") List<IfBaInfo> dataList);
    

    // 批量插入医院信息
    int insertBatchHospitals(@Param("list") List<Hospital> dataList);
    // 批量删除医院信息
    int deleteBatchHospitals(@Param("list") List<Hospital> dataList);

    // 批量插入院区信息
    int insertBatchDistrict(@Param("list") List<District> dataList);
    // 批量删除院区信息
    int deleteBatchDistrict(@Param("list") List<District> dataList);
    
    // 批量插入科室组/大科室信息
    int insertBatchIfDepartmentGroup(@Param("list") List<IfDepartmentGroup> dataList);
    // 批量删除科室组/大科室信息
    int deleteBatchIfDepartmentGroup(@Param("list") List<IfDepartmentGroup> dataList);
    
    // 批量插入标准科室
    int insertBatchDepartments(@Param("list") List<Department> dataList);
    // 批量删除标准科室
    int deleteBatchDepartments(@Param("list") List<Department> dataList);

    // 批量插入科室匹配
    int insertBatchDepartmentMatchs(@Param("list") List<DepartmentMatch> dataList);
    // 批量删除科室匹配
    int deleteBatchDepartmentMatchs(@Param("list") List<DepartmentMatch> dataList);

    // 批量插入院内科室
    int insertBatchIfDepartmentHos(@Param("list") List<IfDepartmentHos> dataList);
    // 批量删除院内科室
    int deleteBatchIfDepartmentHos(@Param("list") List<IfDepartmentHos> dataList);

    // 批量插入治疗组
    int insertBatchIfDoctorGroup(@Param("list") List<IfDoctorGroup> dataList);
    // 批量删除治疗组
    int deleteBatchIfDoctorGroup(@Param("list") List<IfDoctorGroup> dataList);

    // 批量插入组织机构
    int insertBatchOrgas(@Param("list") List<Orga> dataList);
    // 批量删除组织机构
    int deleteBatchOrgas(@Param("list") List<Orga> dataList);

    // 批量插入员工信息
    int insertBatchStaffs(@Param("list") List<Staff> dataList);
    // 批量删除员工信息
    int deleteBatchStaffs(@Param("list") List<Staff> dataList);
    
    // 批量插入临床诊断
    int insertBatchIfccdt(@Param("list") List<Ifccdt> dataList);
    // 批量删除临床诊断
    int deleteBatchIfccdt(@Param("list") List<Ifccdt> dataList);

    // 批量插入CCHi
    int insertBatchIfcchi(@Param("list") List<Ifcchi> dataList);
    // 批量删除CCHI
    int deleteBatchIfcchi(@Param("list") List<Ifcchi> dataList);

    // 批量插入费用明细
    int insertBatchChargeDetails(@Param("list") List<ChargeDetail> dataList);
    // 批量删除费用明细
    int deleteBatchChargeDetails(@Param("list") List<ChargeDetail> dataList);
    
    // 批量插入医院ICD9
    int insertBatchICD9(@Param("list") List<ICD9> dataList);
    // 批量删除医院ICD9
    int deleteBatchICD9(@Param("list") List<ICD9> dataList);

    // 批量插入医院ICD10
    int insertBatchICD10s(@Param("list") List<ICD10> dataList);
    // 批量删除医院ICD10
    int deleteBatchICD10s(@Param("list") List<ICD10> dataList);
    
    //校验未通过日志记录
    int insertBatchIfFailLogDatas(@Param("list") List<IfFailLog> dataList);
    
    int insertBatchQCErrResultDatas(@Param("list") List<QCErrResult> dataList);
    int deleteBatchQCErrResultDatas(@Param("list") List<QCErrResult> dataList);

    // 批量插入结算信息
    int insertBatchIfBalances(@Param("list") List<IfBalance> dataList);
    // 批量插入结算信息
    int deleteBatchIfBalances(@Param("list") List<IfBalance> dataList);
    
}
