package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 医院信息
 * @author jindoulixing
 *
 */
@Data
public class Hospital implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 199118143577287116L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String MEMBER_NAME;

    private String MEMBER_LEVEL_CODE_1;

    private String MEMBER_LEVEL_CODE_2;

    private String MEMBER_TYPE_CODE;

    private String REGION_CODE;

    private Date SYNDATE;

    //private String SYNFLAG;
}
