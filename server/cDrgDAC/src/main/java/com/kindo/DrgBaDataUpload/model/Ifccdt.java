package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
/**
 * 临床诊断术语
 * @author jindoulixing
 *
 */
@Data
public class Ifccdt implements Serializable {
    private static final long serialVersionUID = -2766423484099437766L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;
    private String MEMBER_NAME;
    private String DISTRICT_CODE;
    private String BAH;
    private Integer ZYCS;
    private String CCDT_MAIN_CODE;
    private String CCDT_MAIN_NAME;
    private String CCDT_CODE_1;
    private String CCDT_NAME_1;
    private Integer CCDT_TREAT_1;
    private String CCDT_CODE_2;
    private String CCDT_NAME_2;
    private Integer CCDT_TREAT_2;
    private String CCDT_CODE_3;
    private String CCDT_NAME_3;
    private Integer CCDT_TREAT_3;
    private String CCDT_CODE_4;
    private String CCDT_NAME_4;
    private Integer CCDT_TREAT_4;
    private String CCDT_CODE_5;
    private String CCDT_NAME_5;
    private Integer CCDT_TREAT_5;
    private String CCDT_CODE_6;
    private String CCDT_NAME_6;
    private Integer CCDT_TREAT_6;
    private String CCDT_CODE_7;
    private String CCDT_NAME_7;
    private Integer CCDT_TREAT_7;
    private String CCDT_CODE_8;
    private String CCDT_NAME_8;
    private Integer CCDT_TREAT_8;
    private String CCDT_CODE_9;
    private String CCDT_NAME_9;
    private Integer CCDT_TREAT_9;
    private String CCDT_CODE_10;
    private String CCDT_NAME_10;
    private Integer CCDT_TREAT_10;
    private String CCDT_CODE_11;
    private String CCDT_NAME_11;
    private Integer CCDT_TREAT_11;
    private String CCDT_CODE_12;
    private String CCDT_NAME_12;
    private Integer CCDT_TREAT_12;
    private String CCDT_CODE_13;
    private String CCDT_NAME_13;
    private Integer CCDT_TREAT_13;
    private String CCDT_CODE_14;
    private String CCDT_NAME_14;
    private Integer CCDT_TREAT_14;
    private String CCDT_CODE_15;
    private String CCDT_NAME_15;
    private Integer CCDT_TREAT_15;
    
    private Date SYNDATE;

    //private String SYNFLAG;
}
