package com.kindo.DrgBaDataUpload.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
/**
 * 3.14	费用明细/每日清单
 * @author jindoulixing
 *
 */
@Data
public class ChargeDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2941901697520181419L;

    private String UID;
    
    private String ID;

    private String MEMBER_CODE;

    private String MEMBER_NAME;
    
    private String DISTRICT_CODE;

    private String BAH;

    private String ZYCS;

    private String CFH;
    
    private String CFXH;

    private String SFM;

    private Date CFRQ;

    private Date SFRQ;

    private String KLKSDM;

    private String KLKSMC;

    private String KLYSDM;

    private String KLYSMC;

    private String ZXKSDM;

    private String ZXKSMC;

    private String ZXYSDM;

    private String ZXYSMC;

    private String JYLX;

    private String JSZT;

    private String XMLB;

    private String XMBM;

    private String XMMC;

    private Double XMDJ;

    private Double XMSL;

    private Double XMJE;

    private Date SYNDATE;

    //private String SYNFLAG;
}
