package com.kindo.drgbadataview.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DrgBaDataUpload.model.ChargeDetail;
import com.kindo.DrgBaDataUpload.model.Department;
import com.kindo.DrgBaDataUpload.model.DepartmentMatch;
import com.kindo.DrgBaDataUpload.model.District;
import com.kindo.DrgBaDataUpload.model.Hospital;
import com.kindo.DrgBaDataUpload.model.ICD10;
import com.kindo.DrgBaDataUpload.model.ICD9;
import com.kindo.DrgBaDataUpload.model.IfBaInfoBase;
import com.kindo.DrgBaDataUpload.model.IfBalance;
import com.kindo.DrgBaDataUpload.model.IfDepartmentGroup;
import com.kindo.DrgBaDataUpload.model.IfDepartmentHos;
import com.kindo.DrgBaDataUpload.model.IfDoctorGroup;
import com.kindo.DrgBaDataUpload.model.Ifccdt;
import com.kindo.DrgBaDataUpload.model.Ifcchi;
import com.kindo.DrgBaDataUpload.model.Staff;

public interface DrgBaDataViewMapper {

	//从视图获取病案数据
	List<IfBaInfoBase> queryBaBaInfoViewData(@Param("date")String date);
	
	List<Hospital> queryHospitalViewData();
	
	List<District> queryDistrictViewData();
	
	List<IfDepartmentGroup> queryIfDepartmentGroupViewData();
	
	List<Department> queryDepartmentViewData();
	
	List<DepartmentMatch> queryDepartmentMatchViewData();
	
	List<IfDepartmentHos> queryIfDepartmentHosViewData();
	
	List<IfDoctorGroup> queryIfDoctorGroupViewData();
	
	List<Staff> queryStaffViewData();
	
	List<Ifccdt> queryIfccdtViewData(@Param("date")String date);
	
	List<Ifcchi> queryIfcchiViewData(@Param("date")String date);
	
	List<ICD10> queryICD10ViewData();
	
	List<ICD9> queryICD9ViewData();
	
	List<ChargeDetail> queryChargeDetailViewData(@Param("date")String date);
	
	List<IfBalance> queryIfBalanceViewData(@Param("date")String date);
}
