package com.kindo.drgbadataview.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.kindo.DrgBaDataUpload.model.ChargeDetail;
import com.kindo.DrgBaDataUpload.model.Department;
import com.kindo.DrgBaDataUpload.model.DepartmentMatch;
import com.kindo.DrgBaDataUpload.model.District;
import com.kindo.DrgBaDataUpload.model.Hospital;
import com.kindo.DrgBaDataUpload.model.ICD10;
import com.kindo.DrgBaDataUpload.model.ICD9;
import com.kindo.DrgBaDataUpload.model.IfBaInfoBase;
import com.kindo.DrgBaDataUpload.model.IfBalance;
import com.kindo.DrgBaDataUpload.model.IfDepartmentGroup;
import com.kindo.DrgBaDataUpload.model.IfDepartmentHos;
import com.kindo.DrgBaDataUpload.model.IfDoctorGroup;
import com.kindo.DrgBaDataUpload.model.Ifccdt;
import com.kindo.DrgBaDataUpload.model.Ifcchi;
import com.kindo.DrgBaDataUpload.model.Result;
import com.kindo.DrgBaDataUpload.model.Staff;
import com.kindo.DrgBaDataUpload.service.DrgBaDataUploadService;
import com.kindo.drgbadataview.service.DrgBaDataViewService;

@RestController
@RequestMapping("/nologin/drgview")
public class DrgBaDataViewApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataViewApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private DrgBaDataUploadService drgBaDataUploadService;
	
	@Autowired
	private DrgBaDataViewService drgBaDataViewService;
	
	@RequestMapping(value = "/ifBaInfo/v1", method = RequestMethod.GET)
	@Scheduled(cron = "0 15 01 ? * *")
	public void ifBaInfoView() {
		Result result = new Result();
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        Date d=new Date();  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(d);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        d = calendar.getTime();
        
		List<IfBaInfoBase> list = drgBaDataViewService.queryBaBaInfoViewData(sdf.format(d));
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		drgBaDataUploadService.saveIfBaInfoDatas(str,result);
		
		LOGGER.info("绩效平台[drgview/ifBaInfo]处理完成:"+result);
	}

    @RequestMapping(value = "/hospital/v1", method = RequestMethod.GET)
    public void hospital() {
    	Result result = new Result();
		
		List<Hospital> list = drgBaDataViewService.queryHospitalViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/hospital]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/hospital]处理完成"+result);
		}
		
		drgBaDataUploadService.saveHospitals(listjson,result);
		
		LOGGER.info("绩效平台[drgview/hospital]处理完成:"+result);
    }

    @RequestMapping(value = "/district/v1", method = RequestMethod.GET)
    public void district() {
    	Result result = new Result();
        
		List<District> list = drgBaDataViewService.queryDistrictViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/District]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/District]处理完成"+result);
		}
		
		drgBaDataUploadService.saveDistrict(listjson,result);
		
		LOGGER.info("绩效平台[drgview/District]处理完成:"+result);
    }
    
    @RequestMapping(value = "/bdept/v1", method = RequestMethod.GET)
    public void bdept() {
    	Result result = new Result();
        
		List<IfDepartmentGroup> list = drgBaDataViewService.queryIfDepartmentGroupViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/IfDepartmentGroup]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/IfDepartmentGroup]处理完成"+result);
		}
		
		drgBaDataUploadService.saveIfDepartmentGroup(listjson,result);
		
		LOGGER.info("绩效平台[drgview/IfDepartmentGroup]处理完成:"+result);
    }
    
    @RequestMapping(value = "/s_department/v1", method = RequestMethod.GET)
    public void s_department() {
    	Result result = new Result();
        
		List<Department> list = drgBaDataViewService.queryDepartmentViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/Department]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/Department]处理完成"+result);
		}
		
		drgBaDataUploadService.saveDepartments(listjson,result);
		
		LOGGER.info("绩效平台[drgview/Department]处理完成:"+result);
    }
    
    @RequestMapping(value = "/departmentMatch/v1", method = RequestMethod.GET)
    public void departmentMatch() {
    	Result result = new Result();
        
		List<DepartmentMatch> list = drgBaDataViewService.queryDepartmentMatchViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/DepartmentMatch]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/DepartmentMatch]处理完成"+result);
		}
		
		drgBaDataUploadService.saveDepartmentMatchs(listjson,result);
		
		LOGGER.info("绩效平台[drgview/DepartmentMatch]处理完成:"+result);
    }
 
    @RequestMapping(value = "/h_dept/v1", method = RequestMethod.GET)
    public void h_dept() {
    	Result result = new Result();
        
		List<IfDepartmentHos> list = drgBaDataViewService.queryIfDepartmentHosViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/IfDepartmentHos]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/IfDepartmentHos]处理完成"+result);
		}
		
		drgBaDataUploadService.saveIfDepartmentHos(listjson,result);
		
		LOGGER.info("绩效平台[drgview/IfDepartmentHos]处理完成:"+result);
    }
    
    @RequestMapping(value = "/doctorGroup/v1", method = RequestMethod.GET)
    public void doctorGroup() {
    	Result result = new Result();
        
		List<IfDoctorGroup> list = drgBaDataViewService.queryIfDoctorGroupViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/IfDoctorGroup]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/IfDoctorGroup]处理完成"+result);
		}
		
		drgBaDataUploadService.saveIfDoctorGroup(listjson,result);
		
		LOGGER.info("绩效平台[drgview/IfDoctorGroup]处理完成:"+result);
    }
    
    @RequestMapping(value = "/staff/v1", method = RequestMethod.GET)
    public void staff() {
    	Result result = new Result();
		
		List<Staff> list = drgBaDataViewService.queryStaffViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/Staff]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/Staff]处理完成"+result);
		}
		
		drgBaDataUploadService.saveStaffs(listjson,result);
		
		LOGGER.info("绩效平台[drgview/Staff]处理完成:"+result);
    }
    
    @RequestMapping(value = "/ccdt/v1", method = RequestMethod.GET)
    @Scheduled(cron = "0 05 00 ? * *")
    public void ccdtView() {
    	Result result = new Result();
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        Date d=new Date();  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(d);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        d = calendar.getTime();
        
		List<Ifccdt> list = drgBaDataViewService.queryIfccdtViewData(sdf.format(d));
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/Ifccdt]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/Ifccdt]处理完成"+result);
		}
		
		drgBaDataUploadService.saveIfccdt(listjson,result);
		
		LOGGER.info("绩效平台[drgview/Ifccdt]处理完成:"+result);
    }
    
    @RequestMapping(value = "/cchi/v1", method = RequestMethod.GET)
    @Scheduled(cron = "0 35 00 ? * *")
    public void cchiView() {
    	Result result = new Result();
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        Date d=new Date();  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(d);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        d = calendar.getTime();
        
		List<Ifcchi> list = drgBaDataViewService.queryIfcchiViewData(sdf.format(d));
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/Ifcchi]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/Ifcchi]处理完成"+result);
		}
		
		drgBaDataUploadService.saveIfcchi(listjson,result);
		
		LOGGER.info("绩效平台[drgview/Ifcchi]处理完成:"+result);
    }
    
    @RequestMapping(value = "/icd10/v1", method = RequestMethod.GET)
    public void icd10() {
    	Result result = new Result();
        
		List<ICD10> list = drgBaDataViewService.queryICD10ViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/ICD10]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/ICD10]处理完成"+result);
		}
		
		drgBaDataUploadService.saveICD10s(listjson,result);
		
		LOGGER.info("绩效平台[drgview/ICD10]处理完成:"+result);
    }
    
    @RequestMapping(value = "/icd9/v1", method = RequestMethod.GET)
    public void icd9() {
    	Result result = new Result();
        
		List<ICD9> list = drgBaDataViewService.queryICD9ViewData();
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/ICD9]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/ICD9]处理完成"+result);
		}
		
		drgBaDataUploadService.saveICD9(listjson,result);
		
		LOGGER.info("绩效平台[drgview/ICD9]处理完成:"+result);
    }
    
    @RequestMapping(value = "/chargeDetail/v1", method = RequestMethod.GET)
    @Scheduled(cron = "0 05 02 ? * *")
    public void chargeDetailView() {
    	Result result = new Result();
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        Date d=new Date();  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(d);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        d = calendar.getTime();
        
		List<ChargeDetail> list = drgBaDataViewService.queryChargeDetailViewData(sdf.format(d));
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/ChargeDetail]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/ChargeDetail]处理完成"+result);
		}
		
		drgBaDataUploadService.saveChargeDetails(listjson,result);
		
		LOGGER.info("绩效平台[drgview/ChargeDetail]处理完成:"+result);
    }
    
    @RequestMapping(value = "/balance/v1", method = RequestMethod.GET)
    @Scheduled(cron = "0 55 02 ? * *")
    public void balanceView() {
    	Result result = new Result();
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        Date d=new Date();  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(d);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        d = calendar.getTime();

        List<IfBalance> list = drgBaDataViewService.queryIfBalanceViewData(sdf.format(d));
		
		com.google.gson.Gson s = new com.google.gson.Gson();
		String str = s.toJson(list);
		
		List<Object> listjson = null;
		try {
			listjson = JSONObject.parseArray(str);
		} catch (Exception e) {
			result.setMSG("参数JSON格式错误，解析失败"+e.getMessage());
			result.setCODE(103);
			LOGGER.error("绩效平台[drgview/IfBalance]处理完成"+result);
		}
		if (list == null || list.size() == 0) {
			result.setMSG("参数JSON为空或空串");
			result.setCODE(104);
			LOGGER.error("绩效平台[drgview/IfBalance]处理完成"+result);
		}
		
		drgBaDataUploadService.saveBalances(listjson,result);
		
		LOGGER.info("绩效平台[drgview/IfBalance]处理完成:"+result);
    }
}
