package com.kindo.drgbadataview.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.DrgBaDataUpload.model.ChargeDetail;
import com.kindo.DrgBaDataUpload.model.Department;
import com.kindo.DrgBaDataUpload.model.DepartmentMatch;
import com.kindo.DrgBaDataUpload.model.District;
import com.kindo.DrgBaDataUpload.model.Hospital;
import com.kindo.DrgBaDataUpload.model.ICD10;
import com.kindo.DrgBaDataUpload.model.ICD9;
import com.kindo.DrgBaDataUpload.model.IfBaInfoBase;
import com.kindo.DrgBaDataUpload.model.IfBalance;
import com.kindo.DrgBaDataUpload.model.IfDepartmentGroup;
import com.kindo.DrgBaDataUpload.model.IfDepartmentHos;
import com.kindo.DrgBaDataUpload.model.IfDoctorGroup;
import com.kindo.DrgBaDataUpload.model.Ifccdt;
import com.kindo.DrgBaDataUpload.model.Ifcchi;
import com.kindo.DrgBaDataUpload.model.Staff;
import com.kindo.drgbadataview.dao.DrgBaDataViewMapper;

@Service
public class DrgBaDataViewService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DrgBaDataViewService.class);
    
    @Autowired
    private DrgBaDataViewMapper drgBaDataViewMapper;
    
    public List<IfBaInfoBase> queryBaBaInfoViewData(String date){
		return drgBaDataViewMapper.queryBaBaInfoViewData(date);
	}
    public List<Hospital> queryHospitalViewData(){
		return drgBaDataViewMapper.queryHospitalViewData();
	}
    public List<District> queryDistrictViewData(){
		return drgBaDataViewMapper.queryDistrictViewData();
	}
    
    public List<IfDepartmentGroup> queryIfDepartmentGroupViewData(){
		return drgBaDataViewMapper.queryIfDepartmentGroupViewData();
	}
    
    public List<Department> queryDepartmentViewData(){
		return drgBaDataViewMapper.queryDepartmentViewData();
	}
    
    public List<DepartmentMatch> queryDepartmentMatchViewData(){
		return drgBaDataViewMapper.queryDepartmentMatchViewData();
	}
    
    public List<IfDepartmentHos> queryIfDepartmentHosViewData(){
		return drgBaDataViewMapper.queryIfDepartmentHosViewData();
	}
    
    public List<IfDoctorGroup> queryIfDoctorGroupViewData(){
    	
		return drgBaDataViewMapper.queryIfDoctorGroupViewData();
	}
    
    public List<Staff> queryStaffViewData(){
    	
		return drgBaDataViewMapper.queryStaffViewData();
	}
   
    public List<Ifccdt> queryIfccdtViewData(String date){
    	
		return drgBaDataViewMapper.queryIfccdtViewData(date);
	}
    
    public List<Ifcchi> queryIfcchiViewData(String date){
    	
		return drgBaDataViewMapper.queryIfcchiViewData(date);
	}
    
    public List<ICD10> queryICD10ViewData(){
    	
		return drgBaDataViewMapper.queryICD10ViewData();
	}
    
    public List<ICD9> queryICD9ViewData(){
    	
		return drgBaDataViewMapper.queryICD9ViewData();
	}
    
    public List<ChargeDetail> queryChargeDetailViewData(String date){
    	
		return drgBaDataViewMapper.queryChargeDetailViewData(date);
	}
    
    public List<IfBalance> queryIfBalanceViewData(String date){
    	
		return drgBaDataViewMapper.queryIfBalanceViewData(date);
	}
}
