package com.kindo.DataCenter.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.DataCenter.dao.DateCenterMapper;
import com.kindo.DataCenter.model.DataCenterQueryObj;
import com.kindo.DataCenter.model.HospitalReceiveResult;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class DataCenterService {

	@Autowired
	private DateCenterMapper mapper;
	
	public List<HospitalReceiveResult> queryHospitalResultByGrid(DataCenterQueryObj obj,Pageable page){
	       List<HospitalReceiveResult> list = mapper.queryHospitalResultByGrid(obj, page);
	       if(list!=null){
	             list.forEach(item->{
	                 item.setLevel1(getLevel1(item.getLevel1()));
	                 item.setLevel2(getLevel2(item.getLevel2()));
	                 item.setMemberType(getType(item.getMemberType()));
	                });
	        }
	        return list;
	}
	
	
	public String getLevel1(String code){
        DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("OT_02");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
              if(entry.getKey().equals(code)){
                  return entry.getValue();
              }
        }
        return "";
    }
	
	   public String getLevel2(String code){
	        DictRemoteManager drm= new DictRemoteManager();
	        Map<String, String> dicValue =  drm.getDict("OT_03");
	        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
	              if(entry.getKey().equals(code)){
	                  return entry.getValue();
	              }
	        }
	        return "";
	    }
	   
       public String getType(String code){
           DictRemoteManager drm= new DictRemoteManager();
           Map<String, String> dicValue =  drm.getDict("OT_04");
           for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
                 if(entry.getKey().equals(code)){
                     return entry.getValue();
                 }
           }
           return "";
       }
	
	/**
	 * 医院接收数据导出
	 */
    public void exportHospitalResultByGrid(HttpServletResponse response,DataCenterQueryObj obj) {
        String fileName = "医院信息" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<HospitalReceiveResult, DataCenterQueryObj> supplier = new PageableSupplier<HospitalReceiveResult, DataCenterQueryObj>();
            supplier.setFunc(this::queryHospitalResultByGrid);
            supplier.setParam(obj);

            POIExcelUtils.createExcel(HospitalReceiveResult.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
    
    /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(DataCenterQueryObj vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
		}else if("SHWJW".equals(orgaType)) {
		}else if("SWJW".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setMemberCode(region);
		}
	};
}
