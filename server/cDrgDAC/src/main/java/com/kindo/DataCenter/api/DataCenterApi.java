package com.kindo.DataCenter.api;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kindo.DataCenter.model.DataCenterQueryObj;
import com.kindo.DataCenter.model.HospitalReceiveResult;
import com.kindo.DataCenter.service.DataCenterService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.ApiConstants;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

/**
 * 
 * @author WHK00170
 * 医院信息
 */
@RestController
@RequestMapping("/dataCenter/hospital")
public class DataCenterApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(DataCenterApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private DataCenterService service;
	 @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(DataCenterQueryObj vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    	vo.setUser(info);
    };
	
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public ApiResult queryHospitalResultByGrid(DataCenterQueryObj obj, Pageable page){
		try {
			service.getTheRegion(obj);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		
	    List<HospitalReceiveResult> list = service.queryHospitalResultByGrid(obj, page);
	    
	    RowsWithTotal rt = new RowsWithTotal();
	    rt.setRows(list);
	    rt.setTotal(page.getTotal());
	    return new ApiResult(ApiConstants.Code.SUCCESS, ApiConstants.Msg.SUCCESS, rt);
	}
	
	@RequestMapping(value = "/export", method = RequestMethod.GET)
	public void exportHospitalResultByGrid(HttpServletResponse response, DataCenterQueryObj obj){
		try {
			service.getTheRegion(obj);
			service.exportHospitalResultByGrid(response, obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
