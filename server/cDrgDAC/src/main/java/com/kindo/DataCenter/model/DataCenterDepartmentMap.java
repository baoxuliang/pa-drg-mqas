package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DataCenterDepartmentMap implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = 2794362508776754780L;
    @PoiExcelField(index = 0, title = "医院代码")
    private String memberCode;
    @PoiExcelField(index = 1, title = "医院名称")
    private String memberName;
    @PoiExcelField(index = 2, title = "院内科室代码")
    private String hdepartmentCode;
    @PoiExcelField(index = 3, title = "院内科室名称")
    private String hdepartmentName;
    @PoiExcelField(index = 4, title = "标准科室代码")
    private String sdepartmentCode;
    @PoiExcelField(index = 5, title = "标准科室名称")
    private String sdepartmentName;
    @PoiExcelField(index = 6, title = "数据接收时间", format = "yyyy-MM-dd HH:mm:ss", width = 1000)
    private Date syndate;
}
