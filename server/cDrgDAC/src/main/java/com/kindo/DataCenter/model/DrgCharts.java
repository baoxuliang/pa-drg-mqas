package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DrgCharts implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6188433152178701097L;
	private Integer drgzs;
	private Double sjxhzs;
	private Double medswzs;
	private Double rw1num;
	private Double threefour;
	private String times;
}
