package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DataCenterMinbainfo implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = 7824879896591399668L;
    private String id;
    @PoiExcelField(index = 0, title = "医院代码")
    private String hosId;
    @PoiExcelField(index = 1, title = "医院名称")
    private String userName;
    @PoiExcelField(index = 2, title = "病案号")
    private String bah;
    @PoiExcelField(index = 3, title = "住院次数")
    private String zycs;
    @PoiExcelField(index = 4, title = "性别")
    private String xb;
    @PoiExcelField(index = 5, title = "年龄")
    private String nl;
    @PoiExcelField(index = 6, title = "入院途径")
    private String rytj;
    @PoiExcelField(index = 7, title = "入院时间")
    private String rysj;
    @PoiExcelField(index = 8, title = "出院科别")
    private String cykb;
    @PoiExcelField(index = 9, title = "出院时间")
    private String cysj;
    @PoiExcelField(index = 10, title = "离院方式")
    private String lyfs;
    
    private String batype;
    
    
    @PoiExcelField(index = 11, title = "总费用")
    private String zfy;
    @PoiExcelField(index = 12, title = "数据接收时间", format ="yyyy-MM-dd HH:mm:ss", width = 1000)
    private Date syndate;

}
