package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;
@Data
public class DataCenterQueryFail implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = -1165004449158771944L;
    
    private String memberCode;
    
    private String bah;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    private UserLoginInfo user;
    
    private String city;
    
    private String cnty;
}
