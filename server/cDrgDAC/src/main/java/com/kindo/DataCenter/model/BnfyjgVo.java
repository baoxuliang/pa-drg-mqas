
/** 
* Project Name:cDrgDAC <br/> 
* File Name:BnfyjgVo.java <br/>
* Package Name:com.kindo.DataCenter.model <br/>
* Date:2018年8月23日下午5:28:22 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.DataCenter.model;

import java.io.Serializable;

import lombok.Data;

/** 
* ClassName: BnfyjgVo-大饼-费用结构 <br/>
* date: 2018年8月23日 下午5:28:22 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class BnfyjgVo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -7616746872578677876L;

	double ypfy;//药品
	double zhfy;//综合服务
	double zdfy;//诊断
	double zlfy;//治疗
	double xyfy;//血液和血液品类
	double clfy;//材料类
	double kffy;//康复类
	double zylfy;//中医类
	double qtfy;//其他
	double zfy;//总费用
	
};
