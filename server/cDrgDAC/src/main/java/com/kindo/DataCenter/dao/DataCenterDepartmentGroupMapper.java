package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterDepartmentGroup;
import com.kindo.DataCenter.model.DataCenterQueryGroup;
import com.kindo.aria.base.Pageable;

public interface DataCenterDepartmentGroupMapper {
    
    List<DataCenterDepartmentGroup> queryDepartmentGroup(@Param("param") DataCenterQueryGroup param, @Param("page") Pageable page);

}
