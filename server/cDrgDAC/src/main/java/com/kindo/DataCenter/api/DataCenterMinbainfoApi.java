package com.kindo.DataCenter.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DataCenter.model.DataCenterBaInfo;
import com.kindo.DataCenter.model.DataCenterMinbainfo;
import com.kindo.DataCenter.model.DataCenterQueryMinbainfo;
import com.kindo.DataCenter.model.DataCenterQueryObj;
import com.kindo.DataCenter.service.DataCenterMinbainfoService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.ApiConstants;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

/**
 * 
 * @author WHK00170
 * 最小数据集
 *
 */
@RestController
@RequestMapping("/dataCenter/minBainfo")
public class DataCenterMinbainfoApi {
    
    @Autowired
    private DataCenterMinbainfoService service;
    @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
   public void initUser(DataCenterQueryMinbainfo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
   	vo.setUser(info);
   };
    
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult queryMinbainfo(DataCenterQueryMinbainfo db, Pageable page) {
    	try {
			service.getTheRegion(db);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
        List<DataCenterMinbainfo> list = service.queryMinbainfo(db, page);
        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(ApiConstants.Code.SUCCESS, "查询成功", rt);
    }
    
    @RequestMapping(value = "/queryInfo/{id}", method = RequestMethod.GET)
    public ApiResult queryInfo(HttpServletRequest request, @PathVariable String id) {
        if(StringUtils.isEmpty(id)) {
            return new ApiResult(ApiConstants.Code.FAIL, "参数缺失", "");
        }
        DataCenterBaInfo ret = service.quertInfoById(id);
        return new ApiResult(ApiConstants.Code.SUCCESS, "查询成功", ret);
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void esportMinbainfo(HttpServletResponse response, DataCenterQueryMinbainfo db) {
    	try {
			service.getTheRegion(db);
			service.exportMinbainfo(response, db);
		} catch (Exception e) {
			e.printStackTrace();
		}
       
    }
}
