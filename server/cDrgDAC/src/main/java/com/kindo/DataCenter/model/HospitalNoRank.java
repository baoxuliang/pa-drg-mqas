package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class HospitalNoRank implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5340137690846629739L;
	private String index;
	@PoiExcelField(index = 0, title = "医院名称")
	private String hosname;
	@PoiExcelField(index = 1, title = "综合绩效")
	private Double zhjx;
	@PoiExcelField(index = 2, title = "专业总数")
	private Integer zyzs;
	@PoiExcelField(index = 3, title = "缺失专业数")
	private Integer qszyzs;
	@PoiExcelField(index = 4, title = "DRG组数")
	private Integer drgzs;
	private String hosid;
}
