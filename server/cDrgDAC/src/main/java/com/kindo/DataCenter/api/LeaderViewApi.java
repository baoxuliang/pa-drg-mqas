package com.kindo.DataCenter.api;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DataCenter.model.BnfyjgVo;
import com.kindo.DataCenter.model.DataCenterIndex;
import com.kindo.DataCenter.model.DataCenterQo;
import com.kindo.DataCenter.model.DeptDrgIn;
import com.kindo.DataCenter.model.DeptRank;
import com.kindo.DataCenter.model.HospitalMDC;
import com.kindo.DataCenter.model.HospitalRank;
import com.kindo.DataCenter.service.LeaderViewService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/dataCenter/leaderView")
public class LeaderViewApi extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(LeaderViewApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	LeaderViewService service;
	
	@Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
   public void initUser(DataCenterQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
   	vo.setUser(info);
   };
    
	
	 /** 
	 * QueryColligateQuota:综合指标和Drg指标. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/QueryColligateQuota", method = RequestMethod.GET)
    public ApiResult QueryColligateQuota(DataCenterQo qo) throws Exception {
    	service.getTheRegion(qo);
    	DataCenterIndex list = service.QueryColligateQuota(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    
     /** 
     * queryBnfyjg:大饼图-病案费用结构. <br/>
     * @author whk00196 
     * @date 2018年8月23日 下午5:16:09 *
     * @param qo
     * @param request
     * @return 
     * @since JDK 1.8 
     **/
    @RequestMapping(value = "/bnfyjg", method = { RequestMethod.GET })
	public ApiResult queryBnfyjg(DataCenterQo qo,  HttpServletRequest request) {
    	BnfyjgVo result = null;
		try {
			LOGGER.info("[/dataCenter/leaderView/bnfyjg] LeaderViewApi.queryBnfyjg 接受查询请求,请求参数为:{}", qo);
			service.getTheRegion(qo);
			result = this.service.queryBnfyjg(qo);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/dataCenter/leaderView/bnfyjg] LeaderViewApi.queryBnfyjg 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
    /** 
	 * QueryDrgCharts:图表. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/QueryDrgCharts", method = RequestMethod.GET)
    public ApiResult QueryDrgCharts(DataCenterQo qo) throws Exception {
    	service.getTheRegion(qo);
    	Map<String, Object> result = service.QueryDrgCharts(qo);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, result);
    }
    
    /** 
   	 * QueryDrgNum:缺失和新增drg组数. <br/>
   	 * @author whk00216 
   	 * @date 2018年4月11日 下午9:21:13 *
   	 * @param qo
   	 * @return 
     * @throws Exception 
   	 * @since JDK 1.8 
   	 **/
       @RequestMapping(value = "/QueryDrgNum", method = RequestMethod.GET)
       public ApiResult QueryDrgNum(DataCenterQo qo) throws Exception {
    	   service.getTheRegion(qo);
       	Map<String, Object> result = service.QueryDrgNum(qo);
       	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, result);
       }
    
    /** 
	 * deptIndex:科室排名
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/deptIndex", method = RequestMethod.GET)
    public ApiResult deptIndex(DataCenterQo qo, Pageable page) throws Exception {
    	service.getTheRegion(qo);
    	List<DeptRank> list = service.deptIndex(qo,page);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    
    /** 
	 * hospitalExport:科室排名表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/Dept/export", method = RequestMethod.GET)
    public void deptExport(HttpServletResponse response,DataCenterQo qo) throws Exception {
    	service.getTheRegion(qo);
    	 service.deptExport(response, qo);
    }
    
    /** 
	 * queryDrg:科室Drg组数查询
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/deptQueryDrg", method = RequestMethod.GET)
	public ApiResult queryDrg (DataCenterQo qo, Pageable page) throws Exception{
    	service.getTheRegion(qo);
		List<DeptDrgIn> list = service.queryDrg(qo,page);
		RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
	}
    
    /** 
	 * DrgExport:科室Drg组数查询导出
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
       @RequestMapping(value = "/Drg/export", method = RequestMethod.GET)
   	public void DrgExport (DataCenterQo qo, HttpServletResponse response) throws Exception{
    	   service.getTheRegion(qo);
   		service.DrgExport(qo, response);
   	}
       
       /** 
   	 * HosMDCExport:医院MDC组数查询导出
   	 * @author whk00216 
   	 * @date 2018年4月11日 下午9:21:13 *
   	 * @param qo
   	 * @param response
   	 * @return 
     * @throws Exception 
   	 * @since JDK 1.8 
   	 **/
    @RequestMapping(value = "/HosMDC/export", method = RequestMethod.GET)
  	public void HosMDCExport (DataCenterQo qo, HttpServletResponse response) throws Exception{
    	service.getTheRegion(qo);
  		service.MdcExport(qo, response);
  	}
       
       /** 
   	 * queryHosMDC:医院MDC组数查询
   	 * @author whk00216 
   	 * @date 2018年4月11日 下午9:21:13 *
   	 * @param qo
   	 * @param response
   	 * @return 
     * @throws Exception 
   	 * @since JDK 1.8 
   	 **/
    @RequestMapping(value = "/queryHosMDC", method = RequestMethod.GET)
  	public ApiResult queryMDC (DataCenterQo qo,  Pageable page) throws Exception{
    	service.getTheRegion(qo);
    	List<HospitalMDC> list = service.queryMDC(qo,page);
    	RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
  	}
    
    /** 
	 * hospitalIndex:医院排名
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/hospitalIndex", method = RequestMethod.GET)
    public ApiResult hospitalIndex(DataCenterQo qo, Pageable page) throws Exception {
    	service.getTheRegion(qo);
    	List<HospitalRank> list = service.hospitalIndex(qo,page);
    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, list);
    }
    
    /** 
	 * hospitalExport:医院排名表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
     * @throws Exception 
	 * @since JDK 1.8 
	 **/
    @RequestMapping(value = "/hospital/export", method = RequestMethod.GET)
    public void hospitalExport(HttpServletResponse response,DataCenterQo qo) throws Exception {
    	service.getTheRegion(qo);
    	service.hospitalExport(response, qo);
    }
}
