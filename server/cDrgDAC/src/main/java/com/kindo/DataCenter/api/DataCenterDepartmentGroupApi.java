package com.kindo.DataCenter.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DataCenter.model.DataCenterDepartmentGroup;
import com.kindo.DataCenter.model.DataCenterQueryGroup;
import com.kindo.DataCenter.service.DataCenterDepartmentGroupService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.ApiConstants;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

/**
 * 
 * @author WHK00170
 * 科室组信息
 *
 */
@RestController
@RequestMapping("/dataCenter/departmentGroup")
public class DataCenterDepartmentGroupApi {
    
    @Autowired
    private DataCenterDepartmentGroupService service;
    @Autowired
	private RedisOper redisOper;
	
	@ModelAttribute
   public void initUser(DataCenterQueryGroup vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
   	vo.setUser(info);
   };
    
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult queryDepartmentGroup(DataCenterQueryGroup dg, Pageable page) {
    	try {
			service.getTheRegion(dg);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
        List<DataCenterDepartmentGroup> list = service.queryDepartmentGroup(dg, page);
        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(ApiConstants.Code.SUCCESS, "查询成功", rt);
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void esportDepartmentGroup(HttpServletResponse response, DataCenterQueryGroup dg) {
    	try {
			service.getTheRegion(dg);
			service.exportDepartmentGroup(response, dg);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
