package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DeptDrgIn implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5813189293525725745L;
	@PoiExcelField(index = 0, title = "DRG编码")
	private String drgcode;
	@PoiExcelField(index = 1, title = "DRG名称")
	private String drgname;
	@PoiExcelField(index = 2, title = "入组病案数")
	private Integer rzbls;
	@PoiExcelField(index = 3, title = "相对权重")
	private Double drgrwt;
	@PoiExcelField(index = 4, title = "CMI值")
	private Double cmi;
	@PoiExcelField(index = 5, title = "时间消耗指数")
	private Double timesi;
	@PoiExcelField(index = 6, title = "平均住院日(天)")
	private Double pjzyr;
	@PoiExcelField(index = 7, title = "费用消耗指数")
	private Double costsi;
	@PoiExcelField(index = 8, title = "次均费用(元)")
	private Double cjfy;
	@PoiExcelField(index = 9, title = "死亡人数")
	private Integer swnum;
	@PoiExcelField(index = 10, title = "风险等级")
	private String risk;
}
