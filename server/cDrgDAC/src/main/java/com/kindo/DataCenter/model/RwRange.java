package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class RwRange implements Serializable {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = -2284733354491053657L;

	private String ID;
	
	/**
	 * 序号
	 */
	private String XH;
	
	/**
	 * 
	 */
	private String YNBLFH1;
	
	private Integer YNBLVAL1;
	
	private String YNBLFH2;
	
	private Integer YNBLVAL2;
}
