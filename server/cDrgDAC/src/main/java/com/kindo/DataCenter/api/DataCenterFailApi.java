package com.kindo.DataCenter.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DataCenter.model.DataCenterFail;
import com.kindo.DataCenter.model.DataCenterQueryFail;
import com.kindo.DataCenter.service.DataCenterFailService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.ApiConstants;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

/**
 * 失败病案信息
 * @author WHK00170
 *
 */
@RestController
@RequestMapping("/dataCenter/fail")
public class DataCenterFailApi {
    
    
    @Autowired
    private DataCenterFailService service; 
    @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
   public void initUser(DataCenterQueryFail vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
   	vo.setUser(info);
   };
    
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult queryFail(DataCenterQueryFail df, Pageable page) {
    	try {
			service.getTheRegion(df);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
        List<DataCenterFail> list = service.queryFail(df, page);
        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(ApiConstants.Code.SUCCESS, "查询成功", rt);
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void exportFail(HttpServletResponse response, DataCenterQueryFail df) {
    	try {
			service.getTheRegion(df);
			service.exportDepartmentMap(response, df);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
