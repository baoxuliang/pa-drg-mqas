package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class DataCenterQueryMinbainfo implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = 7209102516466159803L;
    
    private String memberCode;
    
    private String bah;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    private String city;
    
    private String cnty;
    
    private UserLoginInfo user;
}
