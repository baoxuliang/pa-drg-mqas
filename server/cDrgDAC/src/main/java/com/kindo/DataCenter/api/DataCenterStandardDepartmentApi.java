package com.kindo.DataCenter.api;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DataCenter.model.DataCenterQueryStandardDepartment;
import com.kindo.DataCenter.model.DataCenterStandardDepartment;
import com.kindo.DataCenter.service.DataCenterStandardDepartmentService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.ApiConstants;

/**
 * 
 * @author WHK00170
 * 标准科室信息
 *
 */
@RestController
@RequestMapping("/dataCenter/standardDepartment")
public class DataCenterStandardDepartmentApi {
    
    @Autowired
    private DataCenterStandardDepartmentService service;
    
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult queryStandardDepartment(DataCenterQueryStandardDepartment ds, Pageable page) {
        List<DataCenterStandardDepartment> list = service.queryStandardDepartment(ds, page);
        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(ApiConstants.Code.SUCCESS, "查询成功", rt);
    }
    
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void StandardDepartment(HttpServletResponse response, DataCenterQueryStandardDepartment ds) {
        service.exportStandardDepartment(response, ds);
    }

}
