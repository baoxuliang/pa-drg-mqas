package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterHospitalArea;
import com.kindo.DataCenter.model.DataCenterQueryArea;
import com.kindo.aria.base.Pageable;


public interface DataCenterHospitalAreaMapper {
    
    List<DataCenterHospitalArea> queryHospitalArea(@Param("param") DataCenterQueryArea param, @Param("page") Pageable page);


}
