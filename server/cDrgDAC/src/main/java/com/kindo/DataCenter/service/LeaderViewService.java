package com.kindo.DataCenter.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.kindo.DataCenter.dao.LeaderViewMapper;
import com.kindo.DataCenter.model.BnfyjgVo;
import com.kindo.DataCenter.model.DataCenterColligation;
import com.kindo.DataCenter.model.DataCenterIndex;
import com.kindo.DataCenter.model.DataCenterQo;
import com.kindo.DataCenter.model.DeptDrgIn;
import com.kindo.DataCenter.model.DeptRank;
import com.kindo.DataCenter.model.DrgCharts;
import com.kindo.DataCenter.model.HospitalMDC;
import com.kindo.DataCenter.model.HospitalNoRank;
import com.kindo.DataCenter.model.HospitalRank;
import com.kindo.DataCenter.model.QcEcharts;
import com.kindo.DataCenter.model.RwRange;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;



@Service
public class LeaderViewService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LeaderViewService.class);
	
//	private Integer isHosUser =0;
	/** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(DataCenterQo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		LOGGER.info("LeaderViewService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("LeaderViewService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("LeaderViewService.getTheRegion,用户为管理员级别!不限区域!");
//			isHosUser = 0;
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("LeaderViewService.getTheRegion,用户为省级别!");
//			isHosUser = 0;
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("LeaderViewService.getTheRegion,用户为市级别!");
			region = user.getOrgaId();
//			isHosUser = 0;
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("LeaderViewService.getTheRegion,用户为县级别!");
			region = user.getOrgaId();
//			isHosUser = 0;
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("LeaderViewService.getTheRegion,用户为医院用户级别!");
			region = user.getOrgaId();
//			isHosUser = 1;
			vo.setIsHosUser(1);
			vo.setMemberCode(region);
		}
		LOGGER.info("LeaderViewService.getTheRegion,用户region:{}",region);
	};
	
	@Autowired
    private LeaderViewMapper mapper;
	
	
	/**
	 * 组装参数
	 * @param list_range
	 * @param Qo
	 * @return
	 */
	public DataCenterQo getHosParameter(List<RwRange> list_range,DataCenterQo Qo){
		String re1 = (list_range.get(0).getYNBLVAL1()!=null ? "rwt>="+list_range.get(0).getYNBLVAL1():"") +
				(list_range.get(0).getYNBLVAL2()!=null ? " and rwt<"+list_range.get(0).getYNBLVAL2():"");
		String re2 = (list_range.get(1).getYNBLVAL1()!=null ? "rwt>="+list_range.get(1).getYNBLVAL1():"") +
				(list_range.get(1).getYNBLVAL2()!=null ? " and rwt<"+list_range.get(1).getYNBLVAL2():"");
		String re3 = (list_range.get(2).getYNBLVAL1()!=null ? "rwt>="+list_range.get(2).getYNBLVAL1():"") +
				(list_range.get(2).getYNBLVAL2()!=null ? " and rwt<"+list_range.get(2).getYNBLVAL2():"");
		String re4 = (list_range.get(3).getYNBLVAL1()!=null ? "rwt>="+list_range.get(3).getYNBLVAL1():"") +
				(list_range.get(3).getYNBLVAL2()!=null ? " and rwt<"+list_range.get(3).getYNBLVAL2():"");
		Qo.setRe1(re1);
		Qo.setRe2(re2);
		Qo.setRe3(re3);
		Qo.setRe4(re4);
		return Qo;
	}
	
	/**
	 * 获取导出列头权重范围
	 * @param list_range
	 * @return
	 */
	public String[] getExportTitle(List<RwRange> list_range){
		String[] res = new String[4];
		if(list_range.get(0).getYNBLVAL1() == null && list_range.get(0).getYNBLVAL2() == null){
			res[0] = "";
        } else if(list_range.get(0).getYNBLVAL1() == null && list_range.get(0).getYNBLVAL2() != null){
        	res[0] = "RW<"+list_range.get(0).getYNBLVAL2();
        } else if(list_range.get(0).getYNBLVAL1() != null && list_range.get(0).getYNBLVAL2() == null){
        	res[0] = "RW>="+list_range.get(0).getYNBLVAL1();
        } else {
        	res[0] = list_range.get(0).getYNBLVAL1()+"<=RW<"+list_range.get(0).getYNBLVAL2();
        }
        
        if(list_range.get(1).getYNBLVAL1() == null && list_range.get(1).getYNBLVAL2() == null){
        	res[1] = "";
        } else if(list_range.get(1).getYNBLVAL1() == null && list_range.get(1).getYNBLVAL2() != null){
        	res[1] = "RW<"+list_range.get(1).getYNBLVAL2();
        } else if(list_range.get(1).getYNBLVAL1() != null && list_range.get(1).getYNBLVAL2() == null){
        	res[1] = "RW>="+list_range.get(1).getYNBLVAL1();
        } else {
        	res[1] = list_range.get(1).getYNBLVAL1()+"<=RW<"+list_range.get(1).getYNBLVAL2();
        }
        
        if(list_range.get(2).getYNBLVAL1() == null && list_range.get(2).getYNBLVAL2() == null){
        	res[2] = "";
        } else if(list_range.get(2).getYNBLVAL1() == null && list_range.get(2).getYNBLVAL2() != null){
        	res[2] = "RW<"+list_range.get(2).getYNBLVAL2();
        } else if(list_range.get(2).getYNBLVAL1() != null && list_range.get(2).getYNBLVAL2() == null){
        	res[2] = "RW>="+list_range.get(2).getYNBLVAL1();
        } else {
        	res[2] = list_range.get(2).getYNBLVAL1()+"<=RW<"+list_range.get(2).getYNBLVAL2();
        }
        
        if(list_range.get(3).getYNBLVAL1() == null && list_range.get(3).getYNBLVAL2() == null){
        	res[3] = "";
        } else if(list_range.get(3).getYNBLVAL1() == null && list_range.get(3).getYNBLVAL2() != null){
        	res[3] = "RW<"+list_range.get(3).getYNBLVAL2();
        } else if(list_range.get(3).getYNBLVAL1() != null && list_range.get(3).getYNBLVAL2() == null){
        	res[3] = "RW>="+list_range.get(3).getYNBLVAL1();
        } else {
        	res[3] = list_range.get(3).getYNBLVAL1()+"<=RW<"+list_range.get(3).getYNBLVAL2();
        }
        return res;
	}
	
	/** 
	 * QueryColligateQuota:综合指标和Drg指标. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public DataCenterIndex QueryColligateQuota(DataCenterQo qo){
		/**
	     * 同期
	     */
		DataCenterIndex result = new DataCenterIndex();
		DataCenterColligation list = mapper.QueryColligateQuota(qo);
		if(list!=null){
			result.setSwl(getPerc(list.getSWRS(),list.getBanum()));
			result.setLzzzyl(getPerc(list.getLzzryrs(),list.getBanum()));
			result.setBanum(list.getBanum());
			result.setCjfy(list.getCjfy());
			result.setPjzyts(list.getPjzyts());
			Double zfy = list.getZfy();
			result.setUnit("元");
			if(zfy != null){
				if(zfy>=0 && zfy<100000){
					result.setZfy(getTwoDot(zfy));
					result.setUnit("元");
				}else if(zfy>=100000 && zfy<1000000){
					result.setZfy(getTwoDot(zfy/10000));
					result.setUnit("万元");
				}else if(zfy>=1000000 && zfy<10000000){
					result.setZfy(getTwoDot(zfy/100000));
					result.setUnit("十万");
				}else if(zfy>=10000000 && zfy<100000000){
					result.setZfy(getTwoDot(zfy/1000000));
					result.setUnit("百万");
				}else if(zfy>=100000000){
					result.setZfy(getTwoDot(zfy/10000000));
					result.setUnit("千万");
				}
			}
		}
		DataCenterColligation head = mapper.QueryDrgHead(qo);
		if(head!=null){
			result.setDrgnum(head.getDrgnum());
			result.setErrornum(head.getErrornum());
			result.setHqmsnum(head.getHqmsnum());
			result.setIngruop(head.getIngruop());
			result.setNormalnum(head.getNormalnum());
			result.setSumnum(head.getSumnum());
		}
		//////RW
		List<RwRange> list_range = mapper.queryRwRange();
		DataCenterColligation RwRange =null;
		if(list_range.size() >= 4){
			qo = getHosParameter(list_range, qo);
			RwRange =	mapper.QueryRw(qo);
			if(RwRange !=null){
				result.setRw1num(RwRange.getRw1num());
				result.setRw2num(RwRange.getRw2num());
				result.setRw3num(RwRange.getRw3num());
				result.setRw4num(RwRange.getRw4num());
			}
		}
		////////
		DataCenterColligation drg = mapper.drgQuota(qo);
		if(drg!=null){
			result.setDrgzs(drg.getDrgzs());
			result.setSjxhzs(drg.getSjxhzs());
			result.setFyxhzs(drg.getFyxhzs());
			result.setDrgcmizs(drg.getDrgcmizs());
			result.setLowswzs(getTwoDot(drg.getLowswzs()));
			result.setMedswzs(getTwoDot(drg.getMedswzs()));
			/*result.setRw1num(drg.getRw1num());
			result.setRw2num(drg.getRw2num());
			result.setRw3num(drg.getRw3num());
			result.setRw4num(drg.getRw4num());*/
			result.setThreenum(drg.getThreenum());
			result.setFournum(drg.getFournum());
			result.setThreefour(drg.getThreefour());
		}
		DataCenterColligation feiyong = mapper.feiyong(qo);
		if(feiyong!=null){
			result.setYpfy(feiyong.getYpfy());
			result.setZhfy(feiyong.getZhfy());
			result.setZdfy(feiyong.getZdfy());
			result.setZlfy(feiyong.getZlfy());
			result.setXyfy(feiyong.getXyfy());
			result.setClfy(feiyong.getClfy());
			result.setQtfy(feiyong.getQtfy());
		}
		/**
	     * 上期
	     */
		qo= getLastPeriodParam(qo);
		DataCenterColligation listLastYear = mapper.QueryColligateQuota(qo);
		if(listLastYear!=null){
			if(list.getBanum()==0||listLastYear.getBanum()==0){
				result.setSwlLastPeriod(0.0);
			}else{
				result.setSwlLastPeriod(getIncPerc((list.getSWRS()/(double)list.getBanum()),(listLastYear.getSWRS()/(double)listLastYear.getBanum())));
			}
			if(list.getBanum()==0||listLastYear.getBanum()==0){
				result.setLzzzylLastPeriod(0.0);
			}else{
				result.setLzzzylLastPeriod(getIncPerc((list.getLzzryrs()/(double)list.getBanum()),(listLastYear.getLzzryrs()/(double)listLastYear.getBanum())));
			}
			
			result.setBanumLastPeriod(getIncPerc(list.getBanum(),listLastYear.getBanum()));
			result.setCjfyLastPeriod(getIncPerc(list.getCjfy(),listLastYear.getCjfy()));
			result.setPjzytsLastPeriod(getIncPerc(list.getPjzyts(),listLastYear.getPjzyts()));
			result.setZfyLastPeriod(getIncPerc(list.getZfy(),listLastYear.getZfy()));
		}
		
		//////RW
		if(list_range.size() >= 4){
			DataCenterColligation RwRangeLastYear =	mapper.QueryRw(qo);
			if(RwRangeLastYear !=null && RwRange !=null){
				result.setRw1numLastPeriod(getIncPerc(RwRange.getRw1num(),RwRangeLastYear.getRw1num()));
				result.setRw2numLastPeriod(getIncPerc(RwRange.getRw2num(),RwRangeLastYear.getRw2num()));
				result.setRw3numLastPeriod(getIncPerc(RwRange.getRw3num(),RwRangeLastYear.getRw3num()));
				result.setRw4numLastPeriod(getIncPerc(RwRange.getRw4num(),RwRangeLastYear.getRw4num()));
			}else {
				result.setRw1numLastPeriod(0.00);
				result.setRw2numLastPeriod(0.00);
				result.setRw3numLastPeriod(0.00);
				result.setRw4numLastPeriod(0.00);
			}
		}
		////////
		
		DataCenterColligation drgLastYear = mapper.drgQuota(qo);
		if(drgLastYear!=null&&drg!=null){
			result.setDrgzsLastPeriod(getIncPerc(drg.getDrgzs(),drgLastYear.getDrgzs()));
			result.setSjxhzsLastPeriod(getIncPerc(drg.getSjxhzs(),drgLastYear.getSjxhzs()));
			result.setFyxhzsLastPeriod(getIncPerc(drg.getFyxhzs(),drgLastYear.getFyxhzs()));
			result.setDrgcmizsLastPeriod(getIncPerc(drg.getDrgcmizs(),drgLastYear.getDrgcmizs()));
			result.setLowswzsLastPeriod(getIncPerc(drg.getLowswzs(),drgLastYear.getLowswzs()));
			result.setMedswzsLastPeriod(getIncPerc(drg.getMedswzs(),drgLastYear.getMedswzs()));
			/*result.setRw1numLastPeriod(getIncPerc(drg.getRw1num(),drgLastYear.getRw1num()));
			result.setRw2numLastPeriod(getIncPerc(drg.getRw2num(),drgLastYear.getRw2num()));
			result.setRw3numLastPeriod(getIncPerc(drg.getRw3num(),drgLastYear.getRw3num()));
			result.setRw4numLastPeriod(getIncPerc(drg.getRw4num(),drgLastYear.getRw4num()));*/
			result.setThreenumLastPeriod(getIncPerc(drg.getThreenum(),drgLastYear.getThreenum()));
			result.setFournumLastPeriod(getIncPerc(drg.getFournum(),drgLastYear.getFournum()));
			result.setThreefourLastPeriod(getIncPerc(drg.getThreefour(),drgLastYear.getThreefour()));
		}
		return result;
	}
	
	/** 
	 * deptIndex:科室排名
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<DeptRank> deptIndex (DataCenterQo qo, Pageable page){
		return mapper.deptIndex(qo);
	}  
	
	/** 
	 * deptIndex:科室排名导出
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<DeptRank> deptIndexExport (DataCenterQo qo, Pageable page){
		List<DeptRank> list =  mapper.deptIndex(qo);
		if(list!=null){
			for(int i=0;i<list.size();i++){
				DeptRank item = list.get(i);
				item.setIndex("TOP"+(i+1));
			}
		}
		return list;
	}  
	
	/** 
	 * QueryDrgCharts:drg图表
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> QueryDrgCharts (DataCenterQo qo){
		List<RwRange> list_range = mapper.queryRwRange();
		qo= getHosParameter(list_range, qo);
		
		List<DrgCharts> list = new ArrayList<>();
		List<QcEcharts> list1 = new ArrayList<>();
		QcEcharts qc = mapper.QueryQcCharts(qo);
		DrgCharts dc = mapper.drgQuoCharts(qo);
		dc.setTimes(getUnit(qo));
		if(list_range.size() >= 4){
			Integer RwRange =	mapper.QueryFirstRw(qo);
			if(RwRange !=null){
				dc.setRw1num((double) RwRange);
			}
		}
		list.add(dc);
		list1.add(qc);
		qo= getLastPeriodParam(qo);
		DrgCharts dc1 = mapper.drgQuoCharts(qo);
		QcEcharts qc1 = mapper.QueryQcCharts(qo);
		dc1.setTimes(getUnit(qo));
		
		if(list_range.size() >= 4){
			Integer RwRange1 =	mapper.QueryFirstRw(qo);
			if(RwRange1 !=null){
				dc1.setRw1num((double) RwRange1);
			}
		}
		
		list.add(dc1);
		list1.add(qc1);
		qo= getLastPeriodParam(qo);
		DrgCharts dc2 = mapper.drgQuoCharts(qo);
		QcEcharts qc2 = mapper.QueryQcCharts(qo);
		dc2.setTimes(getUnit(qo));
		if(list_range.size() >= 4){
			Integer RwRange2 =	mapper.QueryFirstRw(qo);
			if(RwRange2 !=null){
				dc2.setRw1num((double) RwRange2);
			}
		}
		list.add(dc2);
		list1.add(qc2);
		qo= getLastPeriodParam(qo);
		DrgCharts dc3 = mapper.drgQuoCharts(qo);
		QcEcharts qc3 = mapper.QueryQcCharts(qo);
		dc3.setTimes(getUnit(qo));
		if(list_range.size() >= 4){
			Integer RwRange3 =	mapper.QueryFirstRw(qo);
			if(RwRange3 !=null){
				dc3.setRw1num((double) RwRange3);
			}
		}
		list.add(dc3);
		list1.add(qc3);
		qo= getLastPeriodParam(qo);
		DrgCharts dc4 = mapper.drgQuoCharts(qo);
		QcEcharts qc4 = mapper.QueryQcCharts(qo);
		dc4.setTimes(getUnit(qo));
		if(list_range.size() >= 4){
			Integer RwRange4 =	mapper.QueryFirstRw(qo);
			if(RwRange4 !=null){
				dc4.setRw1num((double) RwRange4);
			}
		}
		list.add(dc4);
		list1.add(qc4);
		Map<String, Object> result = new HashMap<>();
		
    	result.put("drgchart", list);
    	result.put("qcchart", list1);
		return result;
	}  
	
	/** 
	 * QueryQcCharts 检验失败图表
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<QcEcharts> QueryQcCharts (DataCenterQo qo){
		
		List<QcEcharts> list = new ArrayList<>();
		QcEcharts dc = mapper.QueryQcCharts(qo);
		if(!StringUtils.isEmpty(qo.getQuarter())){
		     dc.setTimes(qo.getQuarter());
        }else if(!StringUtils.isEmpty(qo.getMonth())){
        	 dc.setTimes(qo.getMonth());
        }else if(!StringUtils.isEmpty(qo.getYear())){
        	 dc.setTimes(qo.getYear());
        }
		list.add(dc);
		qo= getLastPeriodParam(qo);
		QcEcharts dc1 = mapper.QueryQcCharts(qo);
		if(!StringUtils.isEmpty(qo.getQuarter())){
		     dc1.setTimes(qo.getQuarter());
       }else if(!StringUtils.isEmpty(qo.getMonth())){
       	 dc1.setTimes(qo.getMonth());
       }else if(!StringUtils.isEmpty(qo.getYear())){
       	 dc1.setTimes(qo.getYear());
       }
		list.add(dc1);
		qo= getLastPeriodParam(qo);
		QcEcharts dc2 = mapper.QueryQcCharts(qo);
		if(!StringUtils.isEmpty(qo.getQuarter())){
		     dc2.setTimes(qo.getQuarter());
       }else if(!StringUtils.isEmpty(qo.getMonth())){
       	 dc2.setTimes(qo.getMonth());
       }else if(!StringUtils.isEmpty(qo.getYear())){
       	 dc2.setTimes(qo.getYear());
       }
		list.add(dc2);
		qo= getLastPeriodParam(qo);
		QcEcharts dc3 = mapper.QueryQcCharts(qo);
		if(!StringUtils.isEmpty(qo.getQuarter())){
		     dc3.setTimes(qo.getQuarter());
       }else if(!StringUtils.isEmpty(qo.getMonth())){
       	 dc3.setTimes(qo.getMonth());
       }else if(!StringUtils.isEmpty(qo.getYear())){
       	 dc3.setTimes(qo.getYear());
       }
		list.add(dc3);
		qo= getLastPeriodParam(qo);
		QcEcharts dc4 = mapper.QueryQcCharts(qo);
		if(!StringUtils.isEmpty(qo.getQuarter())){
		     dc4.setTimes(qo.getQuarter());
       }else if(!StringUtils.isEmpty(qo.getMonth())){
       	     dc4.setTimes(qo.getMonth());
       }else if(!StringUtils.isEmpty(qo.getYear())){
       	     dc4.setTimes(qo.getYear());
       }
		list.add(dc4);
		return list;
	}  
	
	/** 
	 * QueryDrgNum 缺失和新增drg组数
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String,Object> QueryDrgNum (DataCenterQo qo){
		qo= setLastPeriod(qo);
		Map<String,Object> list = new HashMap<>();
		list.put("drgadd", mapper.drgAdd(qo));
		list.put("drgreduce", mapper.drgReduce(qo));
		list.put("drgReduceDetail", mapper.drgReduceDetail(qo));
		list.put("drgAddDetail", mapper.drgAddDetail(qo));
		return list;
	}  
	
	/** 
	 * hospitalIndex:医院排名
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<HospitalRank> hospitalIndex (DataCenterQo qo, Pageable page){
		return mapper.hospitalIndex(qo);
	}
	
	/** 
	 * hospitalIndex:医院排名导出
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<HospitalRank> hospitalIndexExport (DataCenterQo qo, Pageable page){
		List<HospitalRank> list = mapper.hospitalIndex(qo);
		
		if(list!=null){
			for(int i=0;i<list.size();i++){
				HospitalRank item = list.get(i);
				item.setIndex("TOP"+(i+1));
			}
		}
		return list;
	}
	
	/** 
	 * hospitalIndex:医院用户医院排名导出
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<HospitalNoRank> hospitalNoIndexExport (DataCenterQo qo, Pageable page){
		List<HospitalNoRank> list = mapper.hospitalNoIndex(qo);
		return list;
	}
	
	  /** 
		 * hospitalExport:医院排名表格导出. <br/>
		 * @author whk00216 
		 * @date 2018年4月11日 下午9:21:13 *
		 * @param qo
		 * @param response
		 * @return 
		 * @since JDK 1.8 
		 **/
	    public void hospitalExport(HttpServletResponse response,DataCenterQo qo) {
	    	String fileName = "医院排名" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
	        try {
	        	response.setContentType("multipart/form-data;charset=UTF-8");
	            response.setCharacterEncoding("UTF-8");
	            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
	            try {
	                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
	                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
	            } catch (UnsupportedEncodingException e) {
	                e.printStackTrace();
	            }
	            if(qo.getIsHosUser() == 1){
	            	PageableSupplier<HospitalNoRank, DataCenterQo> supplier = new PageableSupplier<HospitalNoRank, DataCenterQo>();
		            supplier.setFunc(this::hospitalNoIndexExport);
		            supplier.setParam(qo);

		            POIExcelUtils.createExcel(HospitalNoRank.class, supplier, null, response.getOutputStream());
	            }else{
		            PageableSupplier<HospitalRank, DataCenterQo> supplier = new PageableSupplier<HospitalRank, DataCenterQo>();
		            supplier.setFunc(this::hospitalIndexExport);
		            supplier.setParam(qo);
	
		            POIExcelUtils.createExcel(HospitalRank.class, supplier, null, response.getOutputStream());
	            }
	        } catch (IOException e) {}
	    }
	    
	    /** 
		 * hospitalExport:医院排名表格导出. <br/>
		 * @author whk00216 
		 * @date 2018年4月11日 下午9:21:13 *
		 * @param qo
		 * @param response
		 * @return 
		 * @since JDK 1.8 
		 **/
	    public void deptExport(HttpServletResponse response,DataCenterQo qo) {
	    	String fileName = "科室排名" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
	        try {
	        	response.setContentType("multipart/form-data;charset=UTF-8");
	            response.setCharacterEncoding("UTF-8");
	            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
	            try {
	                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
	                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
	            } catch (UnsupportedEncodingException e) {
	                e.printStackTrace();
	            }
	            PageableSupplier<DeptRank, DataCenterQo> supplier = new PageableSupplier<DeptRank, DataCenterQo>();
	            supplier.setFunc(this::deptIndexExport);
	            supplier.setParam(qo);

	            POIExcelUtils.createExcel(DeptRank.class, supplier, null, response.getOutputStream());
	        } catch (IOException e) {}
	    }
	    
    /** 
	 * queryDrg:科室Drg组数查询
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<DeptDrgIn> queryDrg (DataCenterQo qo, Pageable page){
		return mapper.queryDrg(qo,page);
	}
	
	/** 
	 * queryDrg:科室Drg组数查询
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<DeptDrgIn> queryDrgExport (DataCenterQo qo, Pageable page){
		List<DeptDrgIn> list = mapper.queryDrg(qo,page);
		if(list!=null){
			 list.forEach(item->{
		        	item.setRisk(getDicValue(item.getRisk()));
		        });
		}
		return list;
	}
	
	 /** 
		 * queryMDC:医院MDC组数查询
		 * @author whk00216 
		 * @date 2018年4月11日 下午9:21:13 *
		 * @param qo
		 * @param page
		 * @return 
		 * @since JDK 1.8 
		 **/
		public List<HospitalMDC> queryMDC (DataCenterQo qo, Pageable page){
			return mapper.queryMDC(qo,page);
		}
		
		/** 
		 * MdcExport:医院MDC组数查询导出
		 * @author whk00216 
		 * @date 2018年4月11日 下午9:21:13 *
		 * @param qo
		 * @param page
		 * @return 
		 * @since JDK 1.8 
		 **/
		public void MdcExport (DataCenterQo qo, HttpServletResponse response){
			String fileName = "MDC统计分析" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
	        try {
	        	response.setContentType("multipart/form-data;charset=UTF-8");
	            response.setCharacterEncoding("UTF-8");
	            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
	            try {
	                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
	                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
	            } catch (UnsupportedEncodingException e) {
	                e.printStackTrace();
	            }
	            PageableSupplier<HospitalMDC, DataCenterQo> supplier = new PageableSupplier<HospitalMDC, DataCenterQo>();
	            supplier.setFunc(this::queryMDC);
	            supplier.setParam(qo);

	            POIExcelUtils.createExcel(HospitalMDC.class, supplier, null, response.getOutputStream());
	        } catch (IOException e) {}
		}
	
	/** 
	 * DrgExport:科室Drg组数查询导出
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @return 
	 * @since JDK 1.8 
	 **/
	public void DrgExport (DataCenterQo qo, HttpServletResponse response){
		String fileName = "DRG组数排名" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
        	response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<DeptDrgIn, DataCenterQo> supplier = new PageableSupplier<DeptDrgIn, DataCenterQo>();
            supplier.setFunc(this::queryDrgExport);
            supplier.setParam(qo);

            POIExcelUtils.createExcel(DeptDrgIn.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
	}
	
	/** 
	 * getDicValue:风险等级. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param key
	 * @return 
	 * @since JDK 1.8 
	 **/
    private String getDicValue(String key){
    	 DictRemoteManager drm= new DictRemoteManager();
         Map<String, String> dicValue =  drm.getDict("ML_RISK");
         for (Map.Entry<String, String> entry : dicValue.entrySet()) {
        	 if(entry.getKey().equals(key)){
        		 return entry.getValue();
        	 }
         }
         return "";
    }
	
	public static Double getTwoDot(Number d1) {
		if(d1 !=null){
            return (double)Math.round(d1.doubleValue() * 100)/100;
		}
		return null;
    }
	
	public static Double getPerc(Number d1, Number d2) {
        if (d1 == null || d2 == null || (d2.doubleValue() > -1e-6 && d2.doubleValue() < 1e-6)) {
            return null;
        }
        return (int) (d1.doubleValue() / d2.doubleValue() * 10000 + 0.5) / 100.0;
    }
	
	private static Double getIncPerc(Number d1, Number d2) {
        if (d1 == null || d2 == null || (d2.doubleValue() > -1e-6 && d2.doubleValue() < 1e-6)|| (d1.doubleValue() > -1e-6 && d1.doubleValue() < 1e-6)) {
            return 0.0;
        }
        return (double)Math.round(((d1.doubleValue() - d2.doubleValue())/ d2.doubleValue() * 100) * 100)/100;
    }
	
	private DataCenterQo setLastPeriod(DataCenterQo qo) {
        Integer dateType  = 0;
        int year=1;
        if(!StringUtils.isEmpty(qo.getQuarter())){
        	dateType=2;
        	year = Integer.parseInt(qo.getQuarter().substring(0, 4));
        }else if(!StringUtils.isEmpty(qo.getMonth())){
        	dateType=3;
        	year = Integer.parseInt(qo.getMonth().substring(0, 4));
        }else if(!StringUtils.isEmpty(qo.getYear())){
        	year = Integer.parseInt(qo.getYear().substring(0, 4));
        	dateType=1;
        }

        switch (dateType) {
            case 1:
            	qo.setYearLastPeriod((Integer.parseInt(qo.getYear())-1)+"");
                break;
            case 2:
                int quarter = Integer.parseInt(qo.getQuarter().substring(4));
                if (quarter == 1) {
                	qo.setQuarterLastPeriod(new StringBuffer().append(year - 1).append(4).toString());
                } else {
                	qo.setQuarterLastPeriod(new StringBuffer().append(year).append(quarter - 1).toString());
                }
                break;
            case 3:
                int month = Integer.parseInt(qo.getMonth().substring(4));
                if (month == 1) {
                	qo.setMonthLastPeriod(new StringBuffer().append(year - 1).append(12).toString());
                } else {
                	qo.setMonthLastPeriod(new StringBuffer().append(year).append(month > 10 ? "" : "0").append(month - 1).toString());
                }
                break;
            default:
                break;
        }
        return qo;
    }
	
	private String getUnit(DataCenterQo qo){
		String unit="";
		if(!StringUtils.isEmpty(qo.getQuarter())){
			unit = qo.getQuarter().substring(0,4)+"年"+ qo.getQuarter().substring(4)+"季度";
      }else if(!StringUtils.isEmpty(qo.getMonth())){
    	    Integer month = Integer.parseInt(qo.getMonth().substring(4));
    	    unit = qo.getMonth().substring(0,4)+"年"+ month+"月";
      }else if(!StringUtils.isEmpty(qo.getYear())){
    	    unit = qo.getYear()+"年";
      }
		return unit;
	}
	
	private DataCenterQo getLastPeriodParam(DataCenterQo qo) {
        Integer dateType  = 0;
        int year=1;
        if(!StringUtils.isEmpty(qo.getQuarter())){
        	dateType=2;
        	year = Integer.parseInt(qo.getQuarter().substring(0, 4));
        }else if(!StringUtils.isEmpty(qo.getMonth())){
        	dateType=3;
        	year = Integer.parseInt(qo.getMonth().substring(0, 4));
        }else if(!StringUtils.isEmpty(qo.getYear())){
        	year = Integer.parseInt(qo.getYear().substring(0, 4));
        	dateType=1;
        }

        switch (dateType) {
            case 1:
            	qo.setYear((Integer.parseInt(qo.getYear())-1)+"");
                break;
            case 2:
                int quarter = Integer.parseInt(qo.getQuarter().substring(4));
                if (quarter == 1) {
                	qo.setQuarter(new StringBuffer().append(year - 1).append(4).toString());
                } else {
                	qo.setQuarter(new StringBuffer().append(year).append(quarter - 1).toString());
                }
                break;
            case 3:
                int month = Integer.parseInt(qo.getMonth().substring(4));
                if (month == 1) {
                	qo.setMonth(new StringBuffer().append(year - 1).append(12).toString());
                } else {
                	qo.setMonth(new StringBuffer().append(year).append(month > 10 ? "" : "0").append(month - 1).toString());
                }
                break;
            default:
                break;
        }
        return qo;
    }

	
	 /** 
	 * queryBnfyjg:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年8月23日 下午5:15:50 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	public BnfyjgVo queryBnfyjg(DataCenterQo qo) {
		BnfyjgVo bnfyjgVo = mapper.queryBnfyjg(qo);
		return bnfyjgVo;
	};
    
    
	
}
