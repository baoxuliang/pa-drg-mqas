package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class HospitalMDC implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2509774757158579782L;
	@PoiExcelField(index = 0, title = "MDC编码")
	private String jbmmdccode;
	@PoiExcelField(index = 1, title = "MDC名称")
	private String jbmmdcname;
	@PoiExcelField(index = 2, title = "入组病案数")
	private Integer jbmmdcnum;
	@PoiExcelField(index = 3, title = "标准DRG组数")
	private Integer jbmbdrgnum;
	@PoiExcelField(index = 4, title = "实际DRG组数")
	private Integer jbmdrgnum;
	@PoiExcelField(index = 5, title = "总权重")
	private Double jbnrwt;
	@PoiExcelField(index = 6, title = "CMI值")
	private Double jbmcmi;
	@PoiExcelField(index = 7, title = "时间消耗指数")
	private Double jbmtimesi;
	@PoiExcelField(index = 8, title = "平均住院日(天)")
	private Double jbmpjzyr;
	@PoiExcelField(index = 9, title = "费用消耗指数")
	private Double jbmcostsi;
	@PoiExcelField(index = 10, title = "次均费用(元)")
	private Double jbmcjfy;
	@PoiExcelField(index = 11, title = "死亡人数")
	private Integer jbmswnum;
	@PoiExcelField(index = 12, title = "死亡率(%)")
	private Double jbmswl;
}
