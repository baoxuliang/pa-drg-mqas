package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class DataCenterQueryMap implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = 2494963063738875777L;
    
    private String memberCode;

    private String sdepartmentName;

    private String hdepartmentName;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    
    private String city;
    
    private String cnty;
    
    private UserLoginInfo user;

}
