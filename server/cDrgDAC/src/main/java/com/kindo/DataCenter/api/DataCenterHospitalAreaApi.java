package com.kindo.DataCenter.api;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.DataCenter.model.DataCenterHospitalArea;
import com.kindo.DataCenter.model.DataCenterQueryArea;
import com.kindo.DataCenter.model.DataCenterQueryObj;
import com.kindo.DataCenter.service.DataCenterHospitalAreaService;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.uas.common.constant.ApiConstants;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

/**
 * 
 * @author WHK00170
 * 院区信息
 *
 */
@RestController
@RequestMapping("/dataCenter/hospitalArea")
public class DataCenterHospitalAreaApi {
    
    @Autowired
    private DataCenterHospitalAreaService service;
    @Autowired
	 private RedisOper redisOper;
	
	@ModelAttribute
   public void initUser(DataCenterQueryObj vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
   	vo.setUser(info);
   };
    
    
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult queryHospitalArea(DataCenterQueryArea qa, Pageable page) {
    	try {
			service.getTheRegion(qa);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
        List<DataCenterHospitalArea> list = service.queryHospitalArea(qa, page);
        RowsWithTotal rt = new RowsWithTotal();
        rt.setRows(list);
        rt.setTotal(page.getTotal());
        return new ApiResult(ApiConstants.Code.SUCCESS, "查询成功", rt);
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void exportHospitalArea(HttpServletResponse response, DataCenterQueryArea da) {
    	try {
			service.getTheRegion(da);
			service.exportHospitalArea(response, da);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

}
