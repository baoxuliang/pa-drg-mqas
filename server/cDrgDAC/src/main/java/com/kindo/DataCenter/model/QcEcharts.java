package com.kindo.DataCenter.model;



import lombok.Data;

@Data
public class QcEcharts {
	private String times;
    private Integer hqmsnum;
    private Integer drgnum;
}
