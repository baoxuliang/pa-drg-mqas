package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterDepartmentMap;
import com.kindo.DataCenter.model.DataCenterQueryMap;
import com.kindo.aria.base.Pageable;

public interface DataCenterDepartmentMapMapper {
    
    List<DataCenterDepartmentMap> queryDepartmentMap(@Param("param") DataCenterQueryMap param, @Param("page") Pageable page);

}
