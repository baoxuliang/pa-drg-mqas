package com.kindo.DataCenter.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.BnfyjgVo;
import com.kindo.DataCenter.model.DataCenterColligation;
import com.kindo.DataCenter.model.DataCenterQo;
import com.kindo.DataCenter.model.DeptDrgIn;
import com.kindo.DataCenter.model.DeptRank;
import com.kindo.DataCenter.model.DrgCharts;
import com.kindo.DataCenter.model.DrgDetail;
import com.kindo.DataCenter.model.HospitalMDC;
import com.kindo.DataCenter.model.HospitalNoRank;
import com.kindo.DataCenter.model.HospitalRank;
import com.kindo.DataCenter.model.QcEcharts;
import com.kindo.DataCenter.model.RwRange;
import com.kindo.aria.base.Pageable;

public interface LeaderViewMapper {
	DataCenterColligation QueryColligateQuota(@Param("param") DataCenterQo qo);  
	
	DataCenterColligation QueryDrgHead (@Param("param") DataCenterQo qo);  
	
	DataCenterColligation drgQuota (@Param("param") DataCenterQo qo);
	
	DataCenterColligation QueryRw (@Param("param") DataCenterQo qo);
	
	Integer QueryFirstRw(@Param("param") DataCenterQo qo);
	
	List<RwRange> queryRwRange();
	
	DrgCharts drgQuoCharts (@Param("param") DataCenterQo qo);  
	
	QcEcharts QueryQcCharts (@Param("param") DataCenterQo qo);  
	
	DataCenterColligation feiyong (@Param("param") DataCenterQo qo);  
	
	List<DeptRank> deptIndex (@Param("param") DataCenterQo qo);  
	
	List<HospitalRank> hospitalIndex (@Param("param") DataCenterQo qo);  
	
	List<HospitalNoRank> hospitalNoIndex (@Param("param") DataCenterQo qo);  
	
	List<DeptDrgIn> queryDrg (@Param("param") DataCenterQo qo, @Param("page") Pageable page);  
	
	List<HospitalMDC> queryMDC (@Param("param") DataCenterQo qo, @Param("page") Pageable page); 
	
	List<DrgDetail> drgReduceDetail (@Param("param") DataCenterQo qo);  
	
	List<DrgDetail> drgAddDetail (@Param("param") DataCenterQo qo);
	
	Integer drgAdd (@Param("param") DataCenterQo qo);
	
	Integer drgReduce (@Param("param") DataCenterQo qo);

	
	 /** 
	 * queryBnfyjg:大饼-费用结构. <br/>
	 * @author whk00196 
	 * @date 2018年8月23日 下午5:36:55 *
	 * @param qo
	 * @return 
	 * @since JDK 1.8 
	 **/
	BnfyjgVo queryBnfyjg(@Param("param")DataCenterQo qo);
}
