package com.kindo.DataCenter.model;



import lombok.Data;

@Data
public class DrgDetail {
	private String drgcode;
    private String drgname;
    private Integer drgnum;
}
