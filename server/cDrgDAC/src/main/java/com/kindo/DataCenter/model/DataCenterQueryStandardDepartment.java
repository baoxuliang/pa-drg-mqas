package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class DataCenterQueryStandardDepartment implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = -493147216100494077L;

    private String departmentCode;
    
    private String departmentName;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    
}
