package com.kindo.DataCenter.model;



import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class DataCenterQo {
	private String city;
    private String cnty;
    private String memberTypeCode;
    private String memberLevelCode1;
    private String memberLevelCode2;
    private String memberCode;
    private String year;
    private String quarter;
    private String month;
    private String deptCode;
    private String yearLastPeriod;
    private String quarterLastPeriod;
    private String monthLastPeriod;
    private UserLoginInfo user;
    /**
	 * 公式1
	 */
	private String re1;
	
	/**
	 * 公式2
	 */
	private String re2;
	
	/**
	 * 公式3
	 */
	private String re3;
	
	/**
	 * 公式4
	 */
	private String re4;
	
	int isHosUser;
    
};
