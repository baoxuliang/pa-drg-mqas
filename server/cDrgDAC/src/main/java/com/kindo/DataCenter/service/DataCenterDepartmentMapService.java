package com.kindo.DataCenter.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.DataCenter.dao.DataCenterDepartmentMapMapper;
import com.kindo.DataCenter.model.DataCenterDepartmentMap;
import com.kindo.DataCenter.model.DataCenterQueryMap;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class DataCenterDepartmentMapService {

    @Autowired
    private DataCenterDepartmentMapMapper mpr;
    
    public List<DataCenterDepartmentMap> queryDepartmentMap(DataCenterQueryMap dm, Pageable page) {
        return mpr.queryDepartmentMap(dm, page);
    }
    
    public void exportDepartmentMap(HttpServletResponse response, DataCenterQueryMap dm) {
        String fileName = "科室匹配信息" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<DataCenterDepartmentMap, DataCenterQueryMap> supplier = new PageableSupplier<DataCenterDepartmentMap, DataCenterQueryMap>();
            supplier.setFunc(this::queryDepartmentMap);
            supplier.setParam(dm);

            POIExcelUtils.createExcel(DataCenterDepartmentMap.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
    
    /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(DataCenterQueryMap vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
		}else if("SHWJW".equals(orgaType)) {
		}else if("SWJW".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setMemberCode(region);
		}
	};
}
