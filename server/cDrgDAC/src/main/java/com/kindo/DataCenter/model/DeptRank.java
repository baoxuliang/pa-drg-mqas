package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DeptRank implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8390396041979826334L;
	@PoiExcelField(index = 0, title = "排名")
	private String index;
	@PoiExcelField(index = 1, title = "科室名称")
	private String deptname;
	@PoiExcelField(index = 2, title = "综合绩效")
	private Double zhjx;
	@PoiExcelField(index = 3, title = "DRG组数")
	private Integer drgzs;
	private String deptcode;
}
