package com.kindo.DataCenter.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class DataCenterQueryObj {

	/**
	 * 医院编码
	 */
	private String memberCode;
	
	/**
	 * 数据接收开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dataReceiveStartTime;
	
	/**
	 * 数据接收结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dataReceiveEndTime;
	
	private String regionCode;
	
	/**
     * 行政区代码前4位
     */
    private String region;
    
    private String city;
    
    private String cnty;
    
    private UserLoginInfo user;

}
