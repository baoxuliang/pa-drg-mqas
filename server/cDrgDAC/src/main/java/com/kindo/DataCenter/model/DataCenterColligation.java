package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;

@Data
public class DataCenterColligation implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3855613962876794998L;
	private Integer banum;
	private Double zfy;
	private Double pjzyts;
	private Double cjfy;
    private Double swl;
    private Double lzzzyl;
    private Integer SWRS;
    private Integer lzzryrs;
    
    private Integer sumnum;
    private Integer normalnum;
    private Integer errornum;
    private Integer hqmsnum;
    private Integer drgnum;
    private Integer ingruop;
    
    private Double sjxhzs;
    private Double fyxhzs;
    private Double drgcmizs;
    private Double lowswzs;
    private Double medswzs;
    private Double rw1num;
    private Double rw2num;
    private Double rw3num;
    private Double rw4num;
    private Integer threenum;
    private Integer fournum;
    private Double threefour;
    private Integer drgzs;
    
    private Double ypfy;
    private Double zhfy;
    private Double zdfy;
    private Double zlfy;
    private Double xyfy;
    private Double clfy;
    private Double qtfy;
}
