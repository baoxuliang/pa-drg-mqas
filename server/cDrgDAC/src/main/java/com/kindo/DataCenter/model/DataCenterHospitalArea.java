package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DataCenterHospitalArea implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = -5730491781523736452L;
    @PoiExcelField(index = 0, title = "医院代码")
    private String memberCode;
    @PoiExcelField(index = 1, title = "医院代码")
    private String memberName;
    @PoiExcelField(index = 2, title = "院区代码")
    private String districtCode;
    @PoiExcelField(index = 3, title = "院区名称")
    private String districtName;
    @PoiExcelField(index = 4, title = "数据接收时间", format ="yyyy-MM-dd HH:mm:ss", width = 1000)
    private Date syndate;
}
