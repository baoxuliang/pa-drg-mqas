package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;
@Data
public class DataCenterFail implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = 6912120439512669896L;
    
    @PoiExcelField(index = 0, title = "医院代码")
    private String memberCode;
    @PoiExcelField(index = 1, title = "医院名称")
    private String memberName;
    @PoiExcelField(index = 2, title = "病案号")
    private String bah;
    @PoiExcelField(index = 3, title = "住院次数")
    private String zycs;
    @PoiExcelField(index = 4, title = "失败原因")
    private String errName;
    @PoiExcelField(index = 5, title = "数据接收时间", format ="yyyy-MM-dd HH:mm:ss", width = 1000)
    private Date checkTime;

}
