package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterQueryStandardDepartment;
import com.kindo.DataCenter.model.DataCenterStandardDepartment;
import com.kindo.aria.base.Pageable;

public interface DataCenterStandardDepartmentMapper {
    
    List<DataCenterStandardDepartment> queryStandardDepartment(@Param("param") DataCenterQueryStandardDepartment param, @Param("page") Pageable page);

}
