package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterFail;
import com.kindo.DataCenter.model.DataCenterQueryFail;
import com.kindo.aria.base.Pageable;

public interface DataCenterFailMapper {
    
    List<DataCenterFail> queryFail(@Param("param") DataCenterQueryFail param, @Param("page") Pageable page);

}
