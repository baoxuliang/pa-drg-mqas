package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterBaInfo;
import com.kindo.DataCenter.model.DataCenterMinbainfo;
import com.kindo.DataCenter.model.DataCenterQueryMinbainfo;
import com.kindo.aria.base.Pageable;

public interface DataCenterMinbainfoMapper {
    
    List<DataCenterMinbainfo> queryMinbainfo(@Param("param") DataCenterQueryMinbainfo param, @Param("page") Pageable page);
    
    DataCenterBaInfo queryInfoById(@Param("id") String id);
    
    

}
