package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.DataCenter.model.DataCenterHospitalDepartment;
import com.kindo.DataCenter.model.DataCenterQueryHospitalDepartment;
import com.kindo.aria.base.Pageable;

public interface DataCenterHospitalDepartmentMapper {
    
    List<DataCenterHospitalDepartment> queryNormalDepartment(@Param("param") DataCenterQueryHospitalDepartment param, @Param("page") Pageable page);

}
