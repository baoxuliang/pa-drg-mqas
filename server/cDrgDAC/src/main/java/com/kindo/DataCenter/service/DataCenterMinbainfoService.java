package com.kindo.DataCenter.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.DataCenter.dao.DataCenterMinbainfoMapper;
import com.kindo.DataCenter.model.DataCenterBaInfo;
import com.kindo.DataCenter.model.DataCenterMinbainfo;
import com.kindo.DataCenter.model.DataCenterQueryMinbainfo;
import com.kindo.DataCenter.model.DataCenterQueryObj;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class DataCenterMinbainfoService {
    
    @Autowired
    private DataCenterMinbainfoMapper mpr;
    
    public List<DataCenterMinbainfo> queryMinbainfo(DataCenterQueryMinbainfo db, Pageable page) {
        List<DataCenterMinbainfo> list = mpr.queryMinbainfo(db, page);
        if(list!=null){
            list.forEach(item->{
                item.setLyfs(getLyfs(item.getLyfs()));
                item.setXb(getSex(item.getXb()));
                item.setRytj(getRytj(item.getRytj()));
               });
       }
        return list;
    }

    public DataCenterBaInfo quertInfoById(String id) {
        return mpr.queryInfoById(id);
    }
    
    public String getLyfs(String code){
        DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("CC06_00_223_00");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
              if(entry.getKey().equals(code)){
                  return entry.getValue();
              }
        }
        return "";
    }
    
    public String getRytj(String code){
        DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("CC06_00_237_00");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
              if(entry.getKey().equals(code)){
                  return entry.getValue();
              }
        }
        return "";
    }
    
    public String getSex(String code){
        DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("GB_T2261_1");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
              if(entry.getKey().equals(code)){
                  return entry.getValue();
              }
        }
        return "";
    }
    
    public void exportMinbainfo(HttpServletResponse response, DataCenterQueryMinbainfo db) {
        String fileName = "最小数据集病案信息" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<DataCenterMinbainfo, DataCenterQueryMinbainfo> supplier = new PageableSupplier<DataCenterMinbainfo, DataCenterQueryMinbainfo>();
            supplier.setFunc(this::queryMinbainfo);
            supplier.setParam(db);

            POIExcelUtils.createExcel(DataCenterMinbainfo.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
    
    /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(DataCenterQueryMinbainfo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
		  ) {
			return;
		}
	
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
		}else if("SHWJW".equals(orgaType)) {
		}else if("SWJW".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			region = user.getOrgaId();
			vo.setMemberCode(region);
		}
	};
}
