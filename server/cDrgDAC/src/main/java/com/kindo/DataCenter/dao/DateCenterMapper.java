package com.kindo.DataCenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.kindo.DataCenter.model.DataCenterQueryObj;
import com.kindo.DataCenter.model.HospitalReceiveResult;
import com.kindo.aria.base.Pageable;

public interface DateCenterMapper {
	
	List<HospitalReceiveResult> queryHospitalResultByGrid (@Param("param") DataCenterQueryObj param, @Param("page") Pageable page); 
	
}
