package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class DataCenterStandardDepartment implements Serializable{/**
     * 
     */
    private static final long serialVersionUID = -2586513236962896037L;
    @PoiExcelField(index = 0, title = "科室编码")
    private String departmentCode;
    @PoiExcelField(index = 1, title = "科室名称")
    private String departmentName;
    @PoiExcelField(index = 2, title = "上级科室编码")
    private String parentCode;
    @PoiExcelField(index = 3, title = "数据接收时间", format ="yyyy-MM-dd HH:mm:ss", width = 1000)
    private Date syndate;

}
