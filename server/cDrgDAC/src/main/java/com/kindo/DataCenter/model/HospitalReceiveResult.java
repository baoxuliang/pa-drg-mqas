package com.kindo.DataCenter.model;

import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class HospitalReceiveResult {
	
	/**
	 * 医疗机构编码
	 */
    @PoiExcelField(index = 0, title = "医院代码")
	private String memberCode;
	
	/**
	 * 医疗机构名称
	 */
    @PoiExcelField(index = 1, title = "医院名称")
	private String memberName;
	
	/**
	 * 院区编码
	 */
	//private String hospitalAreaCode;
	
	/**
	 * 院区名称
	 */
	//private String hospitalAreaName;
	
	/**
	 * 所属区域
	 */
	//private String area;
	
	/**
	 * 医疗机构等级（1:一级 2：二级 3：三级）	
	 */
    @PoiExcelField(index = 3, title = "医院级别")
	private String level1;
	
	/**
	 * 医疗机构等级（1:特 2：甲 3：乙 4：丙）	
	 */
    @PoiExcelField(index = 4, title = "医院等级")
	private String level2;
	
	/**
	 * 医院类型
	 */
	@PoiExcelField(index = 2, title = "医院类型")
	private String memberType;
	
	/**
	 * 行政区域划分
	 */
	private String regionalism;
	
	@PoiExcelField(index = 5, title = "所属行政区域")
	private String regionName;
	
	/**
	 * 状态
	 */
	private String state;
	
	/**
	 * 失败原因
	 */
	//private String failCause;
	
	/**
	 * 数据接收时间(年月日)
	 */
	@PoiExcelField(index = 6, title = "数据接收时间",format = "yyyy-MM-dd HH:mm:ss", width = 1000)
	private Date dataReceiveTime;
	

}
