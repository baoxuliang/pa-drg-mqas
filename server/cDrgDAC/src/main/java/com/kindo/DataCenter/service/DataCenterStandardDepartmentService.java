package com.kindo.DataCenter.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.DataCenter.dao.DataCenterStandardDepartmentMapper;
import com.kindo.DataCenter.model.DataCenterQueryStandardDepartment;
import com.kindo.DataCenter.model.DataCenterStandardDepartment;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;

@Service
public class DataCenterStandardDepartmentService {

    @Autowired
    private DataCenterStandardDepartmentMapper mpr;
    
    public List<DataCenterStandardDepartment> queryStandardDepartment(DataCenterQueryStandardDepartment ds, Pageable page) {
        return mpr.queryStandardDepartment(ds, page);
    }
    
    public void exportStandardDepartment(HttpServletResponse response, DataCenterQueryStandardDepartment ds) {
        String fileName = "标准科室信息" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
            response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<DataCenterStandardDepartment, DataCenterQueryStandardDepartment> supplier = new PageableSupplier<DataCenterStandardDepartment, DataCenterQueryStandardDepartment>();
            supplier.setFunc(this::queryStandardDepartment);
            supplier.setParam(ds);

            POIExcelUtils.createExcel(DataCenterStandardDepartment.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
}
