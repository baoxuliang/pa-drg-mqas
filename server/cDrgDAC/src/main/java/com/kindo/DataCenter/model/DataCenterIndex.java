package com.kindo.DataCenter.model;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;

@Data
public class DataCenterIndex implements Serializable {

	
	/**
	 * 本期
	 */
	private static final long serialVersionUID = -7308674761850952478L;
	
	private Integer banum;
	private Double zfy;
	private Double pjzyts;
	private Double cjfy;
    private Double swl;
    private Double lzzzyl;
    private Integer sumnum;
    private Integer normalnum;
    private Integer errornum;
    private Integer hqmsnum;
    private Integer drgnum;
    private Integer ingruop;
    
    private Double sjxhzs;
    private Double fyxhzs;
    private Double drgcmizs;
    private Double lowswzs;
    private Double medswzs;
    private Double rw1num;
    private Double rw2num;
    private Double rw3num;
    private Double rw4num;
    private Integer threenum;
    private Integer fournum;
    private Double threefour;
    private Integer drgzs;
    
    private Double ypfy;
    private Double zhfy;
    private Double zdfy;
    private Double zlfy;
    private Double xyfy;
    private Double clfy;
    private Double qtfy;
    
    private String unit;
    
    /**
     * 上期
     */
    private Double banumLastPeriod;
	private Double zfyLastPeriod;
	private Double pjzytsLastPeriod;
	private Double cjfyLastPeriod;
    private Double swlLastPeriod;
    private Double lzzzylLastPeriod;
    
    private Double sjxhzsLastPeriod;
    private Double fyxhzsLastPeriod;
    private Double drgcmizsLastPeriod;
    private Double lowswzsLastPeriod;
    private Double medswzsLastPeriod;
    private Double rw1numLastPeriod;
    private Double rw2numLastPeriod;
    private Double rw3numLastPeriod;
    private Double rw4numLastPeriod;
    private Double threenumLastPeriod;
    private Double fournumLastPeriod;
    private Double threefourLastPeriod;
    private Double drgzsLastPeriod;
}
