package com.kindo.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.MybatisConfiguration;
import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import com.kindo.plugin.PagePlugin;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 数据源相关配置 可以参照此 配置自己的数据源
 * MapperScan注解中basePackages应该配置dao层文件所在包，不同数据源的dao层文件应该在不同包中
 * 不同数据源的mapper.xml文件应该在不同文件夹中 不同数据源的JavaBean名称(@Bean注解中的name)应该不同
 *
 * @author Xu Haidong
 * @date 2018/5/25
 */
@Configuration
@MapperScan(basePackages = { "com.kindo.DrgBaDataUpload.dao*","com.kindo.DataCenter.dao*" }, sqlSessionTemplateRef = "sqlSessionTemplate3")
public class DataSourceThreeConfig {
	private static final String MAPPER_PATH_1 = "classpath:/mybatis/DrgBaData/*.xml";
    private static final String MAPPER_PATH_2 = "classpath:/mybatis/DataCenter/*.xml";

	/**
	 * 默认使用Druid连接池 自动载入application.yml中的datasource2下的配置
	 *
	 * @return
	 * @throws SQLException
	 */
	@ConfigurationProperties(prefix = "datasource3")
	@Bean(name = "datasource3")
	public DataSource dataSource() throws SQLException {
		return DruidDataSourceBuilder.create().build();
	}

	/**
	 * sessionFactory
	 *
	 * @param dataSource
	 * @param paginationInterceptor
	 * @param globalConfiguration
	 * @return
	 * @throws Exception
	 */
	@Bean(name = "sessionFactory3")
	public SqlSessionFactory sqlSessionFactory(@Qualifier(value = "datasource3") DataSource dataSource,
			PaginationInterceptor paginationInterceptor,
			 PagePlugin pagePlugin,
			@Qualifier(value = "globalConfiguration3") GlobalConfiguration globalConfiguration) throws Exception {

		MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
		bean.setDataSource(dataSource);

		Resource[] resources1 = new PathMatchingResourcePatternResolver().getResources(MAPPER_PATH_1);
		Resource[] resources2 = new PathMatchingResourcePatternResolver().getResources(MAPPER_PATH_2);
		List list = new ArrayList(Arrays.asList(resources1));
		list.addAll(Arrays.asList(resources2));
		Resource[] resources = new Resource[list.size()];
		list.toArray(resources);
		bean.setMapperLocations(resources);

		// bean.setMapperLocations(new
		// PathMatchingResourcePatternResolver().getResources(MAPPER_PATH));
		// 加载分页拦截器，可以自动识别不同数据库，分页拦截器已在集成平台自动配置。如果不想使用，可以注释掉以下两行代码
		Interceptor[] interceptors = new Interceptor[] { pagePlugin };
		bean.setPlugins(interceptors);
		bean.setGlobalConfig(globalConfiguration);
		MybatisConfiguration configuration = new MybatisConfiguration();
		configuration.setJdbcTypeForNull(JdbcType.NULL);
		// 是否开启自动驼峰命名规则（camel case）映射，即从经典数据库列名 A_COLUMN 到经典
		// Java 属性名 aColumn 的类似映射
		configuration.setMapUnderscoreToCamelCase(true);
		bean.setConfiguration(configuration);
		return bean.getObject();
	}

	/**
	 * 自动载入application.yml中的globalConfig3下的配置
	 *
	 * @return
	 */
	@ConfigurationProperties(prefix = "global-config3")
	@Bean(name = "globalConfiguration3")
	public GlobalConfiguration globalConfiguration() {
		return new GlobalConfiguration();
	}

	/**
	 * 事务
	 *
	 * @param dataSource
	 * @return
	 */
	@Bean(name = "transactionManager3")
	public DataSourceTransactionManager dataSourceTransactionManager(@Qualifier("datasource3") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	/**
	 * sqlSessionTemplate
	 *
	 * @param sqlSessionFactory
	 * @return
	 * @throws Exception
	 */
	@Bean(name = "sqlSessionTemplate3")
	public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sessionFactory3") SqlSessionFactory sqlSessionFactory)
			throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}
