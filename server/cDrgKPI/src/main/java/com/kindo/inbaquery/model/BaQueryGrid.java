package com.kindo.inbaquery.model;

import java.util.Date;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class BaQueryGrid {
	@PoiExcelField(index = 0, title = "医院名称")
    private String memberName;
	@PoiExcelField(index = 1, title = "病案号")
    private String bah;
	@PoiExcelField(index = 2, title = "DRG编码")
    private String drgCode;
	@PoiExcelField(index = 3, title = "DRG名称")
	private String drgName;
	@PoiExcelField(index = 4, title = "相对权重",halign="right")
    private String rwt;
	@PoiExcelField(index = 5, title = "主要诊断")
    private String zyzd;
	@PoiExcelField(index = 6, title = "主要操作")
    private String zycz;
	@PoiExcelField(index = 7, title = "住院天数")
    private Integer zyts;
	@PoiExcelField(index = 8, title = "出院日期", format="yyyy-MM-dd",halign="center")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date cyrq;
	@PoiExcelField(index = 9, title = "住院总费用")
	private Double zfy;
    private String id;
}
