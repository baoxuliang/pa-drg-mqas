package com.kindo.inbaquery.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.inbaquery.model.BaQueryGrid;
import com.kindo.inbaquery.model.BaQueryQo;

@Mapper
public interface BaQueryMapper {
	List<BaQueryGrid> queryListpage(@Param("param") BaQueryQo qo, @Param("page")Pageable pagination);
	
}
