package com.kindo.inbaquery.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

@Data
public class BaQueryQo {

	private String city;
    private String cnty;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bgTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    private String memberCode;
    private String drgValue;
    private String zysdValue;
    private String zyczValue;
    private UserLoginInfo user;
}
