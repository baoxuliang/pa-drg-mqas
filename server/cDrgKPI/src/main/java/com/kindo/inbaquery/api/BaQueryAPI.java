package com.kindo.inbaquery.api;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.inbaquery.model.BaQueryQo;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.inbaquery.model.BaQueryGrid;
import com.kindo.inbaquery.service.BaQueryService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/drgkpi/inbas")
public class BaQueryAPI extends BaseApi{
	 @Autowired
	    BaQueryService service;
	    
	    @Autowired
		 private RedisOper redisOper;
		
		@ModelAttribute
	    public void initUser(BaQueryQo vo,HttpServletRequest request) {
			UserLoginInfo info = null;
			String token = null;
			try {
				token=request.getHeader(AuthConstants.HEADER_TOKEN);
				info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	    	vo.setUser(info);
	    };
	    
	    /** 
		 * queryListpage:入组病案表格查询. <br/>
		 * @author whk00216 
		 * @date 2018年4月11日 下午9:21:13 *
		 * @param qo
		 * @param page
		 * @param request
		 * @return 
	     * @throws Exception 
		 * @since JDK 1.8 
		 **/
	    @RequestMapping(value = "/queryListpage", method = RequestMethod.GET)
	    public ApiResult queryListpage(BaQueryQo qo, Pageable page, HttpServletRequest request) throws Exception {
	    	service.getTheRegion(qo);
	    	List<BaQueryGrid> list = service.queryListpage(qo,page);
	    	RowsWithTotal rt = new RowsWithTotal();
	        rt.setRows(list);
	        rt.setTotal(page.getTotal());
	    	return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.SUCCESS, rt);
	    }
	    
	    /** 
		 * export:入组病案表格导出. <br/>
		 * @author whk00216 
		 * @date 2018年4月11日 下午9:21:13 *
		 * @param qo
		 * @param response
		 * @return 
	     * @throws Exception 
		 * @since JDK 1.8 
		 **/
	    @RequestMapping(value = "/export", method = RequestMethod.GET)
	    public void export(BaQueryQo qo, HttpServletResponse response) throws Exception {
	    	service.getTheRegion(qo);
	    	service.exportInBas(response, qo);
	    }
}
