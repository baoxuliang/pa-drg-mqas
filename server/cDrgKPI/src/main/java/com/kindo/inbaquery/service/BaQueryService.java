package com.kindo.inbaquery.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.inbaquery.dao.BaQueryMapper;
import com.kindo.inbaquery.model.BaQueryGrid;
import com.kindo.inbaquery.model.BaQueryQo;
import com.kindo.qyjxpj.model.QyjxpjQo;
import com.kindo.qyjxpj.service.DeptjxpjService;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class BaQueryService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BaQueryService.class);
	
	@Autowired
	private BaQueryMapper mapper;
	

	 /** 
		 * getTheRegion:得到用户的地区/医院. <br/>
		 * @author whk00196 
		 * @date 2018年7月17日 下午9:00:45 *
		 * @param vo 
		 * @throws Exception 
		 * @since JDK 1.8 
		 **/
		public void  getTheRegion(BaQueryQo vo) throws Exception {
			UserLoginInfo user = null;
			user =  vo.getUser();
			if(user == null) {
				throw new Exception("用户未登陆或者已失效");
			}
			if(
			!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
			&&
			(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
			&&
			(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
			  ) {
				return;
			}
		
			LOGGER.info("BaQueryService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
			LOGGER.info("BaQueryService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
			String orgaType = user.getOrgaType();
			String region = null;
			if("".equals(orgaType)||"ROOT".equals(orgaType)) {
				LOGGER.info("BaQueryService.getTheRegion,用户为管理员级别!不限区域!");
			}else if("SHWJW".equals(orgaType)) {
				LOGGER.info("BaQueryService.getTheRegion,用户为省级别!");
			}else if("SWJW".equals(orgaType)) {
				LOGGER.info("BaQueryService.getTheRegion,用户为市级别!");
				region = user.getOrgaId();
				vo.setCity(region);
			}else if("XWJW".equals(orgaType)) {
				LOGGER.info("BaQueryService.getTheRegion,用户为县级别!");
				region = user.getOrgaId();
				vo.setCnty(region);
			}else if("HOS".equals(orgaType)) {
				LOGGER.info("BaQueryService.getTheRegion,用户为医院用户级别!");
				region = user.getOrgaId();
				vo.setMemberCode(region);
			}
			LOGGER.info("BaQueryService.getTheRegion,用户region:{}",region);
		};
	
	 /** 
	 * queryListpage:入组病案表格查询. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param page
	 * @param request
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<BaQueryGrid> queryListpage(BaQueryQo qo, Pageable page) {
        List<BaQueryGrid> list = mapper.queryListpage(qo,page);
        return list;
    }
	
	/** 
	 * exportInBas:入组病案表格导出. <br/>
	 * @author whk00216 
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param qo
	 * @param response
	 * @return 
	 * @since JDK 1.8 
	 **/
	public void exportInBas(HttpServletResponse response,BaQueryQo qo) {
    	String fileName = "入组病案" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        try {
        	response.setContentType("multipart/form-data;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            try {
                String formatFileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PageableSupplier<BaQueryGrid, BaQueryQo> supplier = new PageableSupplier<BaQueryGrid, BaQueryQo>();
            supplier.setFunc(this::queryListpage);
            supplier.setParam(qo);
            POIExcelUtils.createExcel(BaQueryGrid.class, supplier, null, response.getOutputStream());
        } catch (IOException e) {}
    }
	
}
