package com.kindo.pz.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.pz.model.Ynblrwt;

public interface YnblrwtMapper {
    int deleteByPrimaryKey(String id);

    int insert(Ynblrwt record);

    Ynblrwt selectByPrimaryKey(String id);

    List<Ynblrwt> selectAll();

    int updateByPrimaryKey(Ynblrwt record);
    
    List<Ynblrwt> listPageYnblrwt();
    boolean updateValue(@Param("param")Ynblrwt record);
};