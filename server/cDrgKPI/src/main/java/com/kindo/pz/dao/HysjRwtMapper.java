
/** 
* Project Name:cDrgKPI <br/> 
* File Name:HysjRwt.java <br/>
* Package Name:com.kindo.pz.dao <br/>
* Date:2018年7月6日下午2:04:42 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.model.HysjRwtVo;

/** 
* ClassName: HysjRwt <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 下午2:04:42 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
public interface HysjRwtMapper {
	List<HysjRwtVo>  listPageHysjRwtVo(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	List<Map<String,Object>> getRwtParm();
	boolean updateRwt(@Param("param")Map<String, Object> paramMap);
	
	boolean updateRwtType(String type);
	
	boolean updateRwtDt(Date dt);

	
	 /** 
	 * queryDrgName:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月25日 下午7:25:52 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryDrgName(@Param("param")Map<String, Object> paramMap);
};
