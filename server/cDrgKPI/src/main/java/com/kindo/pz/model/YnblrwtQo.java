
/** 
* Project Name:cDrgKPI <br/> 
* File Name:YnblrwtQo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日下午5:17:38 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;
import java.util.List;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: YnblrwtQo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 下午5:17:38 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class YnblrwtQo implements Serializable {
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 5418939350902738014L;
	List<Ynblrwt> list;
	String memberCode;
	UserLoginInfo user;
};
