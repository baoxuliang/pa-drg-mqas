
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZhjxzspzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:13:42 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.pz.dao.ZhjxzspzMapper;
import com.kindo.pz.model.ZhjxzspzQo;
import com.kindo.pz.model.ZhjxzspzVo;

/** 
* ClassName: ZhjxzspzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:13:42 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class ZhjxzspzService {
	
	@Autowired
	ZhjxzspzMapper mapr;
	 /** 
	 * getData:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:15:29 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> getData() {
		List<ZhjxzspzVo> list = mapr.listPageZhjxzspzVo();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", list.size());
		return map;
	};

	
	 /** 
	 * updateData:. <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:18:28 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @since JDK 1.8 
	 **/
	public boolean updateData(ZhjxzspzQo record) {
		record.getList().forEach(x->mapr.updateQz(x));
		return true;
	}

}
