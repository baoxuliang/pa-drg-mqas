
/** 
* Project Name:cDrgKPI <br/> 
* File Name:BzkshzpzMapper.java <br/>
* Package Name:com.kindo.pz.dao <br/>
* Date:2018年7月11日下午5:25:32 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.model.BzkshzpzViewVo;
import com.kindo.pz.model.BzkshzpzVo;

/** 
* ClassName: BzkshzpzMapper <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月11日 下午5:25:32 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
public interface BzkshzpzMapper {

	
	 /** 
	 * listPageBzkshzpzVo:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月11日 下午8:29:33 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<BzkshzpzVo> listPageBzkshzpzVo(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryView:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月11日 下午8:36:27 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<BzkshzpzViewVo> queryView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	 
};
