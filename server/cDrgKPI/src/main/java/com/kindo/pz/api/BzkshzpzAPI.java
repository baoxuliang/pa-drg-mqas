
/** 
* Project Name:cDrgKPI <br/> 
* File Name:BzkshzpzAPI.java <br/>
* Package Name:com.kindo.pz.api <br/>
* Date:2018年6月25日上午9:49:21 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.pz.model.BzkshzpzQo;
import com.kindo.pz.service.BzkshzpzService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/** 
* ClassName: BzkshzpzAPI <br/> 标准科室汇总配置
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:49:21 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@RestController
@RequestMapping("/pz/bzkshzpz")
public class BzkshzpzAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(BzkshzpzAPI.class);
	@Autowired
	private BzkshzpzService service;

	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	 @Autowired
	 private RedisOper redisOper;
	  
	@ModelAttribute
    public void initUser(BzkshzpzQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info = (UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(BzkshzpzQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};
	
	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(BzkshzpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/bzkshzpz/listpage] BzkshzpzAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageBzkshzpz(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/listpage] BzkshzpzAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	@RequestMapping(value = "/view", method = { RequestMethod.GET })
	public ApiResult queryView(BzkshzpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/bzkshzpz/view] BzkshzpzAPI.queryView 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "yyCode参数为空");
			}
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageView(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/view] BzkshzpzAPI.queryView 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	@RequestMapping(value = "/exportview", method = { RequestMethod.GET })
	public void exportview(BzkshzpzQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/bzkshzpz/exportview] BzkshzpzAPI.exportview 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				LOGGER.error("[/bzjxpj/bzkshzpz/exportview] BzkshzpzAPI.exportview 接受导出请求,参数异常,信息为:{}","yyCode参数为空" );
				return;
			}
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportView(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/exportlistpage] BzkshzpzAPI.exportview 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};

	
	
	

};
