
/** 
* Project Name:cDrgKPI <br/> 
* File Name:RwtVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日下午2:41:07 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: RwtVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 下午2:41:07 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class HysjRwtVo {
	@PoiExcelField(index=0,title="DRG编码",halign="left")
	String drgCode;
	@PoiExcelField(index=1,title="DRG名称",halign="left")
	String drgName;
	@PoiExcelField(index=2,title="国家权重",halign="right")
	Double crwt;
	@PoiExcelField(index=3,title="地区权重",halign="right")
	Double prwt;
};
