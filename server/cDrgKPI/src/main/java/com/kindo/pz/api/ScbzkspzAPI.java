
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ScbzkspzAPI.java <br/>
* Package Name:com.kindo.pz.api <br/>
* Date:2018年6月25日上午9:49:21 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.pz.model.BzkshzpzQo;
import com.kindo.pz.service.ScbzkspzService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/** 
* ClassName: ScbzkspzAPI <br/> 上传标准科室
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:49:21 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@RestController
@RequestMapping("/pz/bzkshzpz/upload")
public class ScbzkspzAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScbzkspzAPI.class);
	@Autowired
	private ScbzkspzService service;
	
	 @Autowired
	 private RedisOper redisOper;
	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	@ModelAttribute
    public void initUser(BzkshzpzQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info = (UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	vo.setUser(info);
    };
	@ModelAttribute
	public String getMemberCode(BzkshzpzQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(BzkshzpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/bzkshzpz/upload/listpage] ScbzkspzAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageScbzkspzVo(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/upload/listpage] ScbzkspzAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	@RequestMapping(value = "/exportlistpage", method = { RequestMethod.GET })
	public void exportListPage(BzkshzpzQo vo, HttpServletResponse resp, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/bzkshzpz/upload/exportlistpage] ScbzkspzAPI.exprotlistpage 接受导出请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exprotlistpage(paramMap, resp);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/upload/exportlistpage] ScbzkspzAPI.exportview 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/upload", method = { RequestMethod.POST })
	public ApiResult upload(BzkshzpzQo vo, Pageable pagination, HttpServletRequest rt
			,MultipartFile file) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/bzkshzpz/upload/upload] ScbzkspzAPI.upload 接受上传更新请求,请求参数为:{}", vo);
			if(vo.getUser()==null) {
				return new ApiResult(Constants.RESULT.AUTH_FAIL, Constants.RESULT_MSG.AUTH_FAIL, "用户未登陆或者已失效");
				
			}
			paramMap = Tools.convertBean2Map(vo);
			paramMap.put("userName",vo.getUser().getEmplName());
			flag = this.service.uploadFile(paramMap, pagination,file);
			return new ApiResult(flag?Constants.RESULT.SUCCESS.intValue():Constants.RESULT.FAIL, "更新"+(flag?"成功":"失败"), flag);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/upload/upload] ScbzkspzAPI.upload 接受上传更新请求,发生异常,信息为:{}",
					e.getMessage());
			if(e.getMessage().indexOf("读取的excel表") != -1) {
				return new ApiResult(758, "模板不合格,请使用标准科室模板", e.getMessage()); 
			}
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	@RequestMapping(value = "/download", method = { RequestMethod.GET })
	public void download(BzkshzpzQo vo, HttpServletResponse re, HttpServletRequest rt) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/bzkshzpz/upload/download] ScbzkspzAPI.download 接受导出请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			flag = service.download(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/pz/bzkshzpz/upload/download] ScbzkspzAPI.download 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};

	
	
	

};
