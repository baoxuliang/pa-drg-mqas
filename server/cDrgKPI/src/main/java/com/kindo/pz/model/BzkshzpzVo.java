
/** 
* Project Name:cDrgKPI <br/> 
* File Name:BzkshzpzVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月11日下午8:24:54 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/** 
* ClassName: BzkshzpzVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月11日 下午8:24:54 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class BzkshzpzVo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 4444027734100764494L;
	
	String id;
	String yyName;
	String yyCode;
	int ydjNum;
	int wdjNum;
	Date operDt;
};
