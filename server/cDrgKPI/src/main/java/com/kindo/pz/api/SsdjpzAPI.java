
/** 
* Project Name:cDrgKPI <br/> 
* File Name:SsdjpzAPI.java <br/>
* Package Name:com.kindo.pz.api <br/>
* Date:2018年6月25日上午9:49:21 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.api;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.pz.model.BzkshzpzQo;
import com.kindo.pz.model.SsdjpzQo;
import com.kindo.pz.service.SsdjpzService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/** 
* ClassName: ZkgzpzAPI <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:49:21 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@RestController
@RequestMapping("/pz/ssdjpz")
public class SsdjpzAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(SsdjpzAPI.class);
	@Autowired
	private SsdjpzService service;

	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	 @Autowired
	 private RedisOper redisOper;
	  
	@ModelAttribute
    public void initUser(BzkshzpzQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info = (UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(SsdjpzQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(SsdjpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/ssdjpz/listpage] SsdjpzAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			if(vo.getSsfj()!=null&&!"".equals(vo.getSsfj())) {
				if("E".equals(vo.getSsfj())) {
					vo.setSsfj("C,D");
				}
				paramMap.put("ssfjList", Arrays.asList(vo.getSsfj().split(",")));
			}
//			Arrays.asList(new String[3]);
			result = this.service.listPageSsdjpz(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/ssdjpz/listpage] SsdjpzAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/cchi", method = { RequestMethod.GET })
	public ApiResult queryCCHI(SsdjpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[pz/ssdjpz/cchi] SsdjpzAPI.queryCCHI 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.queryCCHI(paramMap);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/ssdjpz/cchi] SsdjpzAPI.queryCCHI 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};

	@RequestMapping(value = "/up", method = { RequestMethod.PUT })
	public ApiResult updateSSFJ(@RequestBody SsdjpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/ssdjpz/up] SsdjpzAPI.updateSSFJ 接受更新请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getCchiCode())||StringUtils.isEmpty(vo.getSsfj())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "关键参数为空");
			}
			paramMap = Tools.convertBean2Map(vo);
			flag = this.service.updateSSFJ(paramMap);
			return new ApiResult(flag?Constants.RESULT.SUCCESS.intValue():Constants.RESULT.FAIL, "更新"+(flag?"成功":"失败"), flag);
		} catch (Exception e) {
			LOGGER.error("[/pz/ssdjpz/up] SsdjpzAPI.updateSSFJ 接受更新请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/tj", method = { RequestMethod.GET })
	public ApiResult queryTj(SsdjpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[pz/ssdjpz/tj] SsdjpzAPI.queryTj 接受统计查询请求,请求参数为:{}", vo);
			result = this.service.queryTj();
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/ssdjpz/tj] SsdjpzAPI.queryTj 接受统计查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};

};
