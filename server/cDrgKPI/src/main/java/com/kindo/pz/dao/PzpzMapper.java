
/** 
* Project Name:cDrgKPI <br/> 
* File Name:Pzpz.java <br/>
* Package Name:com.kindo.pz.dao <br/>
* Date:2018年7月13日上午11:07:44 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.model.PzpzVo;
import com.kindo.pz.model.ScbzksVo;

/** 
* ClassName: Pzpz <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月13日 上午11:07:44 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
public interface PzpzMapper {
   
		 /** 
		 * selectAllBzks:得到所有的标准科室编码. <br/>
		 * @author whk00196 
		 * @date 2018年7月13日 上午11:14:38 *
		 * @return 
		 * @since JDK 1.8 
		 **/
		List<PzpzVo> selectAllBzks();
		
		
		 /** 
		 * deleteMap:删除某个医院与标准科室的对照关系. <br/>
		 * @author whk00196 
		 * @date 2018年7月13日 上午11:17:17 *
		 * @param yyCode
		 * @return 
		 * @since JDK 1.8 
		 **/
		boolean deleteMap(@Param("yyCode")String yyCode);

		
		 /** 
		 * insertList:插入标准科室与医院科室的 对照关系. <br/>
		 * @author whk00196 
		 * @date 2018年7月13日 上午11:21:45 *
		 * @param tmp
		 * @return 
		 * @since JDK 1.8 
		 **/
		int  insertList(@Param("list")List<PzpzVo> tmp);
		
		String getYyName(String yyCode);
		
		
		 /** 
		 * queryMapping:获得某个医院的 的标准科室对照关系!. <br/>
		 * @author whk00196 
		 * @date 2018年7月19日 下午8:43:29 *
		 * @param paramMap
		 * @param pagination
		 * @return 
		 * @since JDK 1.8 
		 **/
		List<PzpzVo> queryMapping(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
};
