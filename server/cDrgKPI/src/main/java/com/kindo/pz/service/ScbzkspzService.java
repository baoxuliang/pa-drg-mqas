
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ScbzksService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:36:35 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.bzjxpj.service.MdcjxpjService;
import com.kindo.pz.dao.ScbzkspzMapper;
import com.kindo.pz.model.ScbzksVo;
import com.kindo.uas.common.util.POIExcelUtils;

/** 
* ClassName: ScbzksService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:36:35 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class ScbzkspzService {
	@Autowired
	ScbzkspzMapper mapr;
	private static final Logger LOGGER = LoggerFactory.getLogger(ScbzkspzService.class);
	 /** 
	 * listPageScbzkspz:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:38:46 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageScbzkspzVo(Map<String, Object> paramMap, Pageable pagination) {
		List<ScbzksVo> list = mapr.listPageScbzksVo(pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};

	
	 /** 
	 * uploadFile:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:41:58 *
	 * @param paramMap
	 * @param pagination
	 * @param file 
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	@Transactional(value="transactionManager2",transactionManager="transactionManager2"
			,rollbackFor= {Exception.class},propagation=Propagation.REQUIRED)
	public boolean uploadFile(Map<String, Object> paramMap, Pageable pagination, MultipartFile file) throws Exception {
		List<ScbzksVo>  list = null;
		List<ScbzksVo>  listReady = null;
		List<ScbzksVo>  existsList = new LinkedList<>();
		List<ScbzksVo>  notExistsList = new LinkedList<>();
		Date dt = new Date();
//		String userName = (String) paramMap.get("userName");
		int ysSize = 0,yxSize = 0,bcuSize=0,czNoMapperSize=0,czMapperSize=0;
		// 从Excel中得到数据
		list=POIExcelUtils.readExcel(file, ScbzksVo.class);
		ysSize = list.size();
		//去重
		listReady=list.stream()
					  .distinct()
					  .filter(x->!StringUtils.isEmpty(x.getStdDeptCode())&&!StringUtils.isEmpty(x.getStdDeptName()))
					  .collect(Collectors.toList());
//		Map<String,ScbzksVo> ms = new HashMap<>(listReady);
		yxSize=	 listReady.size(); 
		
		//存在的但是并没有关联关系的
		List<ScbzksVo>  existsNoOrmList = new LinkedList<>();
		//从excel中得到 ScbzksVo
		//得到表中已存在的和不存在的
		existsList=listReady.stream()
			.filter(x->{
				boolean ret =this.mapr.exists(x.getStdDeptCode());
				if(!ret) {
					notExistsList.add(x);
				}
				return ret;
			})
			.collect(Collectors.toList());
		bcuSize=notExistsList.size();
		//得到已存在的没有关联关系的 改  科室名称!
		existsNoOrmList=existsList.parallelStream()
				  .filter(x->
				  !mapr.noOrm(x.getStdDeptCode())
				  )
				  .collect(Collectors.toList());
		czNoMapperSize = existsNoOrmList.size();
		czMapperSize =  existsList.size()-czNoMapperSize;
		//没存在的进行新增
		int len =notExistsList.size();
		List<ScbzksVo>  tmp = new LinkedList<>();
		
		for(int i=0;i<len;i++) {
			notExistsList.get(i).setSynDate(dt);
			tmp.add(notExistsList.get(i));
			if(tmp.size()==500||i==len-1) {
				 mapr.insertList(tmp);
				 tmp = new LinkedList<>();
			}
		};
		//存在的但是没有关联关系的更新
		existsNoOrmList.forEach(x->{
			x.setSynDate(dt);
			mapr.updateStdDeptName(x);}
		);
		LOGGER.info("ScbzkspzService.uploadFile:对上传的标准科室进行处理,本次共上传的标准科室数为:{}"
				+ ",\n经过去除排除无效数据之后的数目为:{}"
				+ ",\n新增的标准数目为:{},已经存在于表并已有对接关系的为:{}"
				+ ",已经存在于表但是没有对接关系的为:{}",ysSize,yxSize,bcuSize
				,czMapperSize,czNoMapperSize);
		return true;
	};


	
	 /** 
	 * download:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:42:56 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean download(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
//		String fileName = "标准科室模板" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		String fileName = "标准科室模板";
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<ScbzksVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return new ArrayList<>();
		});
		flag = POIExcelUtils.createExcel(ScbzksVo.class, supplier, null, re.getOutputStream());
		return flag;
	};

	 /** 
		 * exprotlistpage:(这里用一句话描述这个方法的作用). <br/>
		 * @author whk00196 
		 * @date 2018年6月25日 上午10:43:32 *
		 * @param paramMap
		 * @param request
		 * @return 
		 * @since JDK 1.8 
		 **/
		public boolean exprotlistpage(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
			boolean flag = false;
			String fileName = "标准科室" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
			
			re.setContentType("multipart/form-data;charset=UTF-8");
			re.setCharacterEncoding("UTF-8");
			re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            String formatFileName = URLEncoder.encode(fileName, "UTF-8");
            re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
                
			
			PageableSupplier<ScbzksVo, Map<String, Object>> supplier = new PageableSupplier<>();
			supplier.setParam(paramMap);
			supplier.setFunc((params, pagination) -> {
				return this.mapr.listPageScbzksVo(pagination);
			});
			flag = POIExcelUtils.createExcel(ScbzksVo.class, supplier, null, re.getOutputStream());
			return flag;
		};
	
		public static void main(String[] args) {
//			Class<Collection<DeptHosjxpj>> type=(Class<Collection<DeptHosjxpj>>) new ArrayList<DeptHosjxpj>().getClass();
//			System.out.println(type.getClass());
		};

};
