package com.kindo.pz.model;

import java.io.Serializable;

public class Ynblrwt implements Serializable {
    private String id;

    private Integer xh;

    private String ynblFh1;

    private Double ynblVal1;

    private String ynblFh2;

    private Double ynblVal2;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Integer getXh() {
        return xh;
    }

    public void setXh(Integer xh) {
        this.xh = xh;
    }

    public String getYnblFh1() {
        return ynblFh1;
    }

    public void setYnblFh1(String ynblFh1) {
        this.ynblFh1 = ynblFh1 == null ? null : ynblFh1.trim();
    }

    public Double getYnblVal1() {
        return ynblVal1;
    }

    public void setYnblVal1(Double ynblVal1) {
        this.ynblVal1 = ynblVal1;
    }

    public String getYnblFh2() {
        return ynblFh2;
    }

    public void setYnblFh2(String ynblFh2) {
        this.ynblFh2 = ynblFh2 == null ? null : ynblFh2.trim();
    }

    public Double getYnblVal2() {
        return ynblVal2;
    }

    public void setYnblVal2(Double ynblVal2) {
        this.ynblVal2 = ynblVal2;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", xh=").append(xh);
        sb.append(", ynblFh1=").append(ynblFh1);
        sb.append(", ynblVal1=").append(ynblVal1);
        sb.append(", ynblFh2=").append(ynblFh2);
        sb.append(", ynblVal2=").append(ynblVal2);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

	
//	 /** 
//	 * MISSING:(这里用一句话描述这个方法的作用). <br/>
//	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
//	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
//	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
//	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
//	 * @author whk00196 
//	 * @date 2018年7月9日 上午9:54:55 * 
//	 * @since JDK 1.8 
//	 **/
//	public static void main(String[] args) {
////		Ynblrwt
//	}
}