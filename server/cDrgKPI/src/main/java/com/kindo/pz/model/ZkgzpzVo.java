
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZkgzpzVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日上午11:02:31 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: ZkgzpzVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 上午11:02:31 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class ZkgzpzVo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -4018391860441490552L;
	String id;
	@PoiExcelField(index=0,title="规则类型",halign="left")
	String gzType;
	@PoiExcelField(index=1,title="规则名称",halign="left")
	String gzName;
	@PoiExcelField(index=2,title="错误描述",halign="left")
	String gzMs;
	@PoiExcelField(index=3,title="规则状态",halign="left")
	String gzZt;
};
