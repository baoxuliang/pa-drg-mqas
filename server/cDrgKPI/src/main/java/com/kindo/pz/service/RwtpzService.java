
/** 
* Project Name:cDrgKPI <br/> 
* File Name:RwtpzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:08:28 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.pz.dao.HysjRwtMapper;
import com.kindo.pz.model.HysjRwtVo;

/** 
* ClassName: RwtpzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:08:28 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class RwtpzService {
	@Autowired
	HysjRwtMapper mapr;
	
	 /** 
	 * listPageRwtpz:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:09:56 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageRwtpz(Map<String, Object> paramMap, Pageable pagination) {
		List<HysjRwtVo> list = mapr.listPageHysjRwtVo(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};

	
	public boolean exportlistpage(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "权重配置" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<HysjRwtVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.listPageHysjRwtVo(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(HysjRwtVo.class, supplier, null, re.getOutputStream());
		return flag;
	};
	
	
	 /** 
	 * getRwtParm:得到相关信息. <br/>
	 * @author whk00196 
	 * @date 2018年7月24日 上午10:44:55 *
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> getRwtParm() {
		List<Map<String,Object>> list = mapr.getRwtParm();
		List<Map<String,Map<String,Object>>> ret = new ArrayList<>();
		Map<String,Map<String,Object>> tmp =  new HashMap<>();
		list.forEach( x->{
			  tmp.put((String) x.get("key"), x);
//				x.keySet().forEach(k->{
//					tmp.put(k, x);
//				});
//				ret.add(tmp);
		});
		ret.add(tmp);
		Map<String,Object> ms = new HashMap<>();
		ms.put("type",ret.get(0).get("RWT_SET_TYPE").get("value"));
		String date = (String)ret.get(0).get("RWT_SET_TIME").get("value");
		ms.put("date",date.substring(0, date.lastIndexOf(".")));
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", ms);
		return map;
	};
	 /** 
	 * useInSystem:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:11:13 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @since JDK 1.8 
	 **/
	@Transactional(value="transactionManager2",transactionManager="transactionManager2"
			,rollbackFor= {Exception.class},propagation=Propagation.REQUIRED)
	public boolean useInSystem(Map<String, Object> paramMap, HttpServletResponse re) {
		 String type = (String) paramMap.get("rwtType");
		 Date dt = new Date();
		 mapr.updateRwtDt(dt);
		 mapr.updateRwtType(type);
		 mapr.updateRwt(paramMap);
		return true;
	}


	
	 /** 
	 * queryDrgName:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:12:59 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<Map<String, Object>> queryDrgName(Map<String, Object> paramMap, Pageable pagination) {
		return mapr.queryDrgName(paramMap);
	}

}
