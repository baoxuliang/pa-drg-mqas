
/** 
* Project Name:cDrgKPI <br/> 
* File Name:PzpzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:44:24 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.pz.dao.PzpzMapper;
import com.kindo.pz.model.HysjRwtVo;
import com.kindo.pz.model.PzpzVo;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.util.POIExcelUtils;

/** 
* ClassName: PzpzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:44:24 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class PzpzService {
	@Autowired
	PzpzMapper mapr;
	private static final Logger LOGGER = LoggerFactory.getLogger(PzpzService.class);
	
	public String getUserYyCode(UserLoginInfo user) {
		String yyCode = null;
		if(user == null || !"HOS".equals(user.getOrgaType())) {
			return null;
		}
		yyCode = user.getOrgaId();
		return yyCode;
	};
	
	 /** 
	 * listPagePzpz:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:46:30 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPagePzpz(Map<String, Object> paramMap, Pageable pagination) {
		List<PzpzVo> list = mapr.queryMapping(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};

	
	 /** 
	 * exprotlistpage:用. <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:46:46 *
	 * @param paramMap
	 * @param request
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
//	@Deprecated
	public boolean exprotlistpage(Map<String, Object> paramMap,  HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "医院标准科室对照关系_" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
        PageableSupplier<PzpzVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryMapping(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(PzpzVo.class, supplier, null, re.getOutputStream());
		return flag;
	}


	
	 /** 
	 * uploadFile:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:46:56 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	@Transactional(value="transactionManager2",transactionManager="transactionManager2"
			,rollbackFor= {Exception.class},propagation=Propagation.REQUIRED)
	public boolean uploadFile(Map<String, Object> paramMap,  MultipartFile file) throws Exception {
		List<PzpzVo>  listRet = null;
		String yyCode = (String) paramMap.get("yyCode");
		int ysSize = 0,doSize = 0;
		String yyName = mapr.getYyName(yyCode);
		//得到上传的list集合
		listRet=POIExcelUtils.readExcel(file, PzpzVo.class);
		ysSize = listRet.size();
//		StringUtils.isEmpty(str)
		//忽略掉为空的 重复的
		listRet=listRet.stream()
			   .filter(x->!(StringUtils.isEmpty(x.getHosDepartCode()))
					   	  &&!(StringUtils.isEmpty(x.getHosDepartName()))
					   	  &&!(StringUtils.isEmpty(x.getStdDepartCode()))
					   	  &&!(StringUtils.isEmpty(x.getStdDepartName()))
					   	  )
			   .distinct()
			   .collect(Collectors.toList());
		doSize=listRet.size();
		if(doSize > 0) {
			//删除已有的数据
			this.mapr.deleteMap(yyCode);
		}
		String operName = (String) paramMap.get("userName");
		int len = listRet.size();
		List<PzpzVo> tmp = new LinkedList<>();
		 Date operDt = new Date();
		//新增集合中的数据
		for(int i=0;i<len;i++) {
			PzpzVo sub = listRet.get(i);
			sub.setOperDt(operDt);
			sub.setOperName(operName);
			sub.setYyCode(yyCode);
			sub.setYyName(yyName);
			tmp.add(sub);
			if(tmp.size()==500||i==len-1) {
				 mapr.insertList(tmp);
				 tmp = new LinkedList<>();
			}
		};
		LOGGER.info("PzpzService.uploadFile,共计上传:{}条记录!,经过排除真正有效的记录数为:{};操作员:{};",ysSize,doSize,operName);
		return true;
	};


	
	 /** 
	 * download:下载配置模板. <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:47:01 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean download(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
//		String fileName = "医院标准科室对照模板_" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		String fileName = "医院标准科室对照模板";
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
        PageableSupplier<PzpzVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.selectAllBzks();
		});
		flag = POIExcelUtils.createExcel(PzpzVo.class, supplier, null, re.getOutputStream());
		return flag;
	};

}
