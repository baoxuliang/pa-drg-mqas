
/** 
* Project Name:cDrgKPI <br/> 
* File Name:PzpzVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月13日上午11:08:50 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.util.Date;

import com.kindo.uas.common.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: PzpzVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月13日 上午11:08:50 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class PzpzVo {
 @PoiExcelField(index=0,title="标准科室编码",checkExists=true) 
 String stdDepartCode;
 @PoiExcelField(index=1,title="标准科室名称",checkExists=true) 
 String stdDepartName;
 @PoiExcelField(index=2,title="院内科室编码",checkExists=true) 
 String hosDepartCode;
 @PoiExcelField(index=3,title="院内科室名称",checkExists=true) 
 String hosDepartName;
 
 @PoiExcelField(index=4,title="院区代码",checkExists=true) 
 String yqCode;
 
 String yyCode;
 String yyName;
// @PoiExcelField(index=5,title="上传人",checkExists=true) 
 String operName;
// @PoiExcelField(index=6,title="上传时间",checkExists=true) 
 Date operDt;

	 /** 
	 * equals:(这里用一句话描述这个方法的作用).<br/> 
	 * @author whk00196 
	 * @date 2018年7月25日 下午6:05:58 *
	 * @param obj
	 * @return 
	 * @since JDK 1.8 
	 **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PzpzVo other = (PzpzVo) obj;
		if (hosDepartCode == null) {
			if (other.hosDepartCode != null)
				return false;
		} else if (!hosDepartCode.equals(other.hosDepartCode))
			return false;
		if (stdDepartCode == null) {
			if (other.stdDepartCode != null)
				return false;
		} else if (!stdDepartCode.equals(other.stdDepartCode))
			return false;
		if (yqCode == null) {
			if (other.yqCode != null)
				return false;
		} else if (!yqCode.equals(other.yqCode))
			return false;
		return true;
	}
	
	 /** 
	 * hashCode:(这里用一句话描述这个方法的作用).<br/> 
	 * @author whk00196 
	 * @date 2018年7月25日 下午6:05:58 *
	 * @return 
	 * @since JDK 1.8 
	 **/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hosDepartCode == null) ? 0 : hosDepartCode.hashCode());
		result = prime * result + ((stdDepartCode == null) ? 0 : stdDepartCode.hashCode());
		result = prime * result + ((yqCode == null) ? 0 : yqCode.hashCode());
		return result;
	}
	 
	 
};
