
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZkgzpzMapper.java <br/>
* Package Name:com.kindo.pz.dao <br/>
* Date:2018年7月6日上午11:07:42 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.model.ZkgzpzVo;

/** 
* ClassName: ZkgzpzMapper <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 上午11:07:42 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
public interface ZkgzpzMapper {
	List<ZkgzpzVo>  listPageZkgzpzVo(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * listPageExportZkgzpzVo:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月6日 上午11:26:40 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<ZkgzpzVo> listPageExportZkgzpzVo(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	boolean updateRule(@Param("param")Map<String, Object> paramMap);
};
