
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZkgzpzAPI.java <br/>
* Package Name:com.kindo.pz.api <br/>
* Date:2018年6月25日上午9:49:21 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.pz.model.ZkgzpzQo;
import com.kindo.pz.service.ZkgzpzService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.intf.BaseApiInterface;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/** 
* ClassName: ZkgzpzAPI <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:49:21 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@RestController
@RequestMapping("/pz/zkgzpz")
public class ZkgzpzAPI extends BaseApi  {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZkgzpzAPI.class);
	@Autowired
	private ZkgzpzService service;

	@Autowired
	 private RedisOper redisOper;
	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
//	@ModelAttribute
    public void initUser(ZkgzpzQo vo,HttpServletRequest request) {
    	UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info = (UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(ZkgzpzQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(ZkgzpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/zkgzpz/listpage] ZkgzpzAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageZkgzpz(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/zkgzpz/listpage] ZkgzpzAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/exportlistpage", method = { RequestMethod.GET })
	public void exportlistpage(ZkgzpzQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/zkgzpz/exportlistpage] ZkgzpzAPI.exportlistpage 接受导出请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportlistpage(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/pz/zkgzpz/exportlistpage] ZkgzpzAPI.exportlistpage 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};

	@RequestMapping(value = "/up", method = { RequestMethod.POST,RequestMethod.PUT })
	public ApiResult updateRule(@RequestBody ZkgzpzQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		boolean flag = false;
		try {
			LOGGER.info("[/pz/zkgzpz/up] ZkgzpzAPI.updateRule 接受更新请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getCzType())
			   ||StringUtils.isEmpty(vo.getId())
			   ||StringUtils.isEmpty(vo.getGzType())
				) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, 
						"czType,id,gzType参数为空");
			}
			paramMap = Tools.convertBean2Map(vo);
			flag = this.service.updateRule(paramMap,pagination);
			return new ApiResult(flag?Constants.RESULT.SUCCESS.intValue():Constants.RESULT.FAIL, "更新"+(flag?"成功":"失败"), flag);
		} catch (Exception e) {
			LOGGER.error("[/pz/zkgzpz/up] ZkgzpzAPI.updateRule 接受更新请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	}
	
	 /** 
	 * getRedisOper:(这里用一句话描述这个方法的作用).<br/> 
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/> 
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/> 
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 * @author whk00196 
	 * @date 2018年7月10日 下午7:29:10 *
	 * @return 
	 * @since JDK 1.8 
	 **/

	

};
