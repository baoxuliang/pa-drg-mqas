
/** 
* Project Name:cDrgKPI <br/> 
* File Name:BzkshzpzViewVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月11日下午8:33:33 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: BzkshzpzViewVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月11日 下午8:33:33 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class BzkshzpzViewVo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -7846663488925064767L;
	@PoiExcelField(index=0,title="科室编码",halign="left")
	String stdDepartCode;
	@PoiExcelField(index=1,title="科室名称",halign="left")
	String stdDepartName;
	@PoiExcelField(index=2,title="院内科室编码",halign="left")
	String hosDepartCode;
	@PoiExcelField(index=3,title="院内科室名称",halign="left")
	String hosDepartName;
	@PoiExcelField(index=4,title="上传人",halign="left")
	String operName;
	@PoiExcelField(index=5,title="上传时间",halign="left")
	Date operDt;
};
