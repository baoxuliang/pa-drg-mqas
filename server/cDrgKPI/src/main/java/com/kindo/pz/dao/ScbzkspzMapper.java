package com.kindo.pz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.model.ScbzksVo;
import com.kindo.pz.model.StdDepart;

public interface ScbzkspzMapper {
    int deleteByPrimaryKey(Short uid);

    int insert(StdDepart record);

    StdDepart selectByPrimaryKey(Short uid);

    List<StdDepart> selectAll();

    int updateByPrimaryKey(StdDepart record);
    
    List<ScbzksVo> selectTheAll();

	
	 /** 
	 * listPageScbzksVo:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月12日 下午6:26:03 *
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<ScbzksVo> listPageScbzksVo(@Param("pagination")Pageable pagination);
	
	
	 /** 
	 * exists:判断 if_department 是否存在  这样的 DEPARTMENT_CODE . <br/>
	 * 查看 对应的科室编码是否存在!
	 * @author whk00196 
	 * @date 2018年7月12日 下午8:13:35 *
	 * @param code
	 * @return 
	 * @since JDK 1.8 
	 **/
	boolean  exists(@Param("ksCode")String ksCode);

	
	 /** 
	 * noOrm:判断该科室编码 是否 存在于 if_department_map 这张表中. <br/>
	 * @author whk00196 
	 * @date 2018年7月12日 下午8:49:35 *
	 * @param stdDeptCode
	 * @return 
	 * @since JDK 1.8 
	 **/
	boolean noOrm(@Param("ksCode")String ksCode);

	
	 /** 
	 * insertList:向 if_department 批量插入数据. <br/>
	 * @author whk00196 
	 * @date 2018年7月12日 下午9:07:25 *
	 * @param tmp 
	 * @since JDK 1.8 
	 **/
	int insertList(@Param("list")List<ScbzksVo> tmp);

	
	 /** 
	 * updateStdDeptName:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月12日 下午9:11:47 *
	 * @param x
	 * @return 
	 * @since JDK 1.8 
	 **/
	int updateStdDeptName(ScbzksVo x);
};