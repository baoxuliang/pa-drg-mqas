
/** 
* Project Name:cDrgKPI <br/> 
* File Name:SsdjpzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:19:38 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.dao.SsdjpzMapper;
import com.kindo.pz.model.Ssdjpz;

/** 
* ClassName: SsdjpzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:19:38 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class SsdjpzService {
	@Autowired
	SsdjpzMapper mapr;
	
	public Map<String, Object> listPageSsdjpz(Map<String, Object> paramMap, Pageable pagination) {
		List<Ssdjpz> list = mapr.listPageSsdjpz(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};
	
	public boolean updateSSFJ(Map<String, Object> paramMap) throws Exception {
		return mapr.updateSSFJ(paramMap);
	};
	
	public Map<String, Object> queryCCHI(Map<String, Object> paramMap) {
		List<Map<String,Object>> list = mapr.queryCCHI(paramMap);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		return map;
	};
	
	public Map<String, Object> queryTj() {
		List<Map<String, Object>> list = mapr.queryTj();
		Map<String, Object> map = new HashMap<String, Object>();
		// 计算三 四级
		Map<String, Object> e = new HashMap<>();

		Optional<Object> op = list.stream().filter(x -> "C".equals(x.get("ssfj")) || "D".equals(x.get("ssfj")))
				.map(x -> {
					return  x.get("gs") == null ? 0L :  x.get("gs");
				}).reduce((x, y) ->{
					Object v = 0;
					v=Long.valueOf(x.toString())+Long.valueOf(y.toString());
					return v;
				});
		Map<String, Object> ms = list.stream().findFirst().get();
		Object zs = 0;
		if (ms != null) {
			zs =  ms.get("zs");
		}
		Long gs = (Long)op.orElse(0L);
		e.put("ssfj", "E");
		e.put("gs", gs);
		e.put("zs", zs);
		e.put("rate", Long.valueOf(zs.toString()) == 0 ? 0 : Double.valueOf(gs.toString()) / Long.valueOf(zs.toString())*100);
		list.add(e);
		map.put("rows", list);
		return map;
	};
};
