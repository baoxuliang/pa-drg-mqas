
/** 
* Project Name:cDrgKPI <br/> 
* File Name:SsdjpzQo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月16日上午10:49:02 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: SsdjpzQo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月16日 上午10:49:02 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class SsdjpzQo implements Serializable {
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 4470571001946740888L;
	String memberCode;
	UserLoginInfo user;
	
	String cchiValue;
	String cchiCode;
	String ssfj;
};
