
/** 
* Project Name:cDrgKPI <br/> 
* File Name:BzkshzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:27:58 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.bzjxpj.model.Drgjxpj;
import com.kindo.bzjxpj.model.RzbasVo;
import com.kindo.pz.dao.BzkshzpzMapper;
import com.kindo.pz.model.BzkshzpzViewVo;
import com.kindo.pz.model.BzkshzpzVo;

/** 
* ClassName: BzkshzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:27:58 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class BzkshzpzService {
	
	@Autowired
	BzkshzpzMapper mapr;
	
	 /** 
	 * listPageBzkshzpz:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:30:46 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageBzkshzpz(Map<String, Object> paramMap, Pageable pagination) {
		List<BzkshzpzVo> list = mapr.listPageBzkshzpzVo(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};
	
	 /** 
	 * listPageView:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:32:43 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageView(Map<String, Object> paramMap, Pageable pagination) {
		List<BzkshzpzViewVo> list = mapr.queryView(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};


	
	 /** 
	 * exportView:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:33:28 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportView(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "标准科室汇总_查看" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
		
        PageableSupplier<BzkshzpzViewVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryView(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(BzkshzpzViewVo.class, supplier, null, re.getOutputStream());
		return flag;
	};

}
