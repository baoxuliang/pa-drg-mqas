
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZhjxzspzAPI.java <br/>
* Package Name:com.kindo.pz.api <br/>
* Date:2018年6月25日上午9:49:21 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.pz.model.ZhjxzspzQo;
import com.kindo.pz.service.ZhjxzspzService;
import com.kindo.qyjxpj.model.Hosjxpj;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

/** 
* ClassName: ZhjxzspzAPI <br/> 综合绩效指数
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:49:21 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@RestController
@RequestMapping("/pz/zhjxzspz")
public class ZhjxzspzAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZhjxzspzAPI.class);
	@Autowired
	private ZhjxzspzService service;

	 @Autowired
	 private RedisOper redisOper;
	 
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	@ModelAttribute
    public void initUser(ZhjxzspzQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info = (UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(ZhjxzspzQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/get", method = { RequestMethod.GET })
	public ApiResult getData(Pageable pagination, HttpServletRequest request) {
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/zhjxzspz/get] ZhjxzspzAPI.getData 接受查询请求");
			result = this.service.getData();
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/zhjxzspz/get] ZhjxzspzAPI.getData 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/up", method = { RequestMethod.POST ,RequestMethod.PUT})
	public ApiResult updateData(@RequestBody ZhjxzspzQo vo, HttpServletResponse re, HttpServletRequest request) {
		boolean flag = false;
		try {
			LOGGER.info("[/pz/zhjxzspz/up] ZhjxzspzAPI.updateData 接受修改请求,请求参数为:{}", vo);
			if(vo.getList()==null||vo.getList().size()!=5) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, 
						"参数为空,必须为5组权重!");
			}
			flag = service.updateData(vo);
			 return new ApiResult(flag?Constants.RESULT.SUCCESS.intValue():Constants.RESULT.FAIL, "更新"+(flag?"成功":"失败"), flag);
		} catch (Exception e) {
			LOGGER.error("[/pz/zhjxzspz/up] ZhjxzspzAPI.updateData 接受修改请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};


	
	

};
