
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ScbzksVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月12日下午6:17:22 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.uas.common.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: ScbzksVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月12日 下午6:17:22 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class ScbzksVo implements Serializable {
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -78608216220981840L;
	String uid;
	@PoiExcelField(index=0,title="标准科室编码",halign="left",checkExists=true)
	String stdDeptCode;
	@PoiExcelField(index=1,title="标准科室名称",halign="left",checkExists=true)
	String stdDeptName;
	Date  synDate;
	
	 /** 
	 * equals:(这里用一句话描述这个方法的作用).<br/> 
	 * @author whk00196 
	 * @date 2018年7月25日 下午5:55:57 *
	 * @param obj
	 * @return 
	 * @since JDK 1.8 
	 **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScbzksVo other = (ScbzksVo) obj;
		if (stdDeptCode == null) {
			if (other.stdDeptCode != null)
				return false;
		} else if (!stdDeptCode.equals(other.stdDeptCode))
			return false;
		return true;
	}
	
	 /** 
	 * hashCode:(这里用一句话描述这个方法的作用).<br/> 
	 * @author whk00196 
	 * @date 2018年7月25日 下午5:55:57 *
	 * @return 
	 * @since JDK 1.8 
	 **/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stdDeptCode == null) ? 0 : stdDeptCode.hashCode());
		return result;
	}
 
};
