
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZhjxzspzVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日下午3:46:03 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;

import lombok.Data;

/** 
* ClassName: ZhjxzspzVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 下午3:46:03 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class ZhjxzspzVo implements Serializable {
    
	/**
	* serialVersionUID:. 
	**/
	private static final long serialVersionUID = 5105392607865724221L;
	String qzCode;
    String qzValue;
};
