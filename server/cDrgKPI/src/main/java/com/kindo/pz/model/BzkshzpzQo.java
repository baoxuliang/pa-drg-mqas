
/** 
* Project Name:cDrgKPI <br/> 
* File Name:PzQo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年6月25日上午9:52:53 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: PzQo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:52:53 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class BzkshzpzQo implements Serializable {
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -8373410059013626259L;
	UserLoginInfo user;
	String memberCode;
	
	String yyCode;
	String yyName;
	
	String userName;
	
};
