package com.kindo.pz.model;

import java.io.Serializable;

import com.kindo.uas.common.excel.annotation.PoiExcelField;

import lombok.Data;

@Data
public class Ssdjpz implements Serializable {
    
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -6284517607323678111L;
	@PoiExcelField(index=0,title="CCHI",halign="left")
	private String cchiCode;
	@PoiExcelField(index=1,title="手术操作名称",halign="left")
    private String cchiName;

    private String rlhs;
    @PoiExcelField(index=2,title="技术难度",halign="left")
    private String jsnd;
    @PoiExcelField(index=3,title="风险程度",halign="left")
    private String fxcd;
    @PoiExcelField(index=4,title="手术等级",halign="left")
    private String ssfj;

    private String ycxhc;

    private String cwnr;

    private String dzhc;

    private String jjdw;

    private String jjsm;

    private String xmbs;

    private String xmnh;

    private String zt;

    private String bs;

    private Integer fz;


};