
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZkgzpzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午9:48:26 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.bzjxpj.model.Mdcjxpj;
import com.kindo.pz.dao.ZkgzpzMapper;
import com.kindo.pz.model.ZkgzpzVo;


/** 
* ClassName: ZkgzpzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:48:26 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class ZkgzpzService {
	@Autowired
	ZkgzpzMapper mapr;
	
	 /** 
	 * listPageZkgzpz:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:01:30 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageZkgzpz(Map<String, Object> paramMap, Pageable pagination) {
		List<ZkgzpzVo> list = mapr.listPageZkgzpzVo(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};

	
	 /** 
	 * exportlistpage:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:02:37 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportlistpage(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "质控规则配置" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<ZkgzpzVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.listPageExportZkgzpzVo(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(ZkgzpzVo.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 /** 
	 * updateRule:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:05:40 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public boolean updateRule(Map<String, Object> paramMap, Pageable pagination) {
		return this.mapr.updateRule(paramMap);
	};



	
};
