package com.kindo.pz.model;

import java.io.Serializable;
import java.util.Date;

public class StdDepart implements Serializable {
    private Integer uid;

    private String departmentCode;

    private String departmentName;

    private String parentCode;

    private Date syndate;

    private String synflag;

    private static final long serialVersionUID = 1L;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode == null ? null : departmentCode.trim();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName == null ? null : departmentName.trim();
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode == null ? null : parentCode.trim();
    }

    public Date getSyndate() {
        return syndate;
    }

    public void setSyndate(Date syndate) {
        this.syndate = syndate;
    }

    public String getSynflag() {
        return synflag;
    }

    public void setSynflag(String synflag) {
        this.synflag = synflag == null ? null : synflag.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uid=").append(uid);
        sb.append(", departmentCode=").append(departmentCode);
        sb.append(", departmentName=").append(departmentName);
        sb.append(", parentCode=").append(parentCode);
        sb.append(", syndate=").append(syndate);
        sb.append(", synflag=").append(synflag);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}