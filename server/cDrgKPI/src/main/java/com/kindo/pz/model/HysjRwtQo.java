
/** 
* Project Name:cDrgKPI <br/> 
* File Name:RwtQo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日下午2:13:51 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: RwtQo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 下午2:13:51 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class HysjRwtQo implements Serializable {
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 2225897634079073618L;
	String rwtType;
	String mdcCode;
	String drgCode;
	String memberCode;
	UserLoginInfo user;
};
