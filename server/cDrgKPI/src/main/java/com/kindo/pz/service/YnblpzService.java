
/** 
* Project Name:cDrgKPI <br/> 
* File Name:YnblpzService.java <br/>
* Package Name:com.kindo.pz.service <br/>
* Date:2018年6月25日上午10:22:35 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.pz.dao.YnblrwtMapper;
import com.kindo.pz.model.Ynblrwt;

/** 
* ClassName: YnblpzService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午10:22:35 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class YnblpzService {
	@Autowired
	YnblrwtMapper mapr;
	
	 /** 
	 * listPageYnblpz:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:24:49 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageYnblpz() {
		List<Ynblrwt> list = mapr.listPageYnblrwt();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", list.size());
		return map;
	};

	
	 /** 
	 * updateData:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月25日 上午10:25:20 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @since JDK 1.8 
	 **/
	public boolean updateData(List<Ynblrwt> list) {
		list.forEach(x->mapr.updateValue(x));
		return true;
	}

}
