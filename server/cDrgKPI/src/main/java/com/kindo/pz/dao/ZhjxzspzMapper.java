
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZhjxzspzMapper.java <br/>
* Package Name:com.kindo.pz.dao <br/>
* Date:2018年7月6日下午3:40:57 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kindo.pz.model.ZhjxzspzVo;

/** 
* ClassName: ZhjxzspzMapper <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 下午3:40:57 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
public interface ZhjxzspzMapper {
	List<ZhjxzspzVo>  listPageZhjxzspzVo();
	boolean updateQz(@Param("param")ZhjxzspzVo param);
};
