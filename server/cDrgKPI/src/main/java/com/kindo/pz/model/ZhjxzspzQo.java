
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZhjxzspzQo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日下午3:48:46 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;
import java.util.List;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: ZhjxzspzQo <br/>
* date: 2018年7月6日 下午3:48:46 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class ZhjxzspzQo   implements Serializable {
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 6266629319719565929L;
	List<ZhjxzspzVo> list;
	String memberCode;
	UserLoginInfo user;
};
