package com.kindo.pz.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.pz.model.Ssdjpz;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SsdjpzMapper {
    int deleteByPrimaryKey(String cchiCode);

    int insert(Ssdjpz record);

    Ssdjpz selectByPrimaryKey(String cchiCode);

    List<Ssdjpz> selectAll();

    int updateByPrimaryKey(Ssdjpz record);
    
    
     /** 
     * listPageSsdjpz:手术等级配置. <br/>
     * @author whk00196 
     * @date 2018年7月16日 上午10:38:21 *
     * @param paramMap
     * @param pagination
     * @return 
     * @since JDK 1.8 
     **/
    List<Ssdjpz> listPageSsdjpz(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
    
    
     /** 
     * queryCCHI:模糊查询. <br/>
     * @author whk00196 
     * @date 2018年7月16日 上午10:39:01 *
     * @param paramMap
     * @return 
     * @since JDK 1.8 
     **/
    List<Map<String,Object>> queryCCHI(@Param("param")Map<String, Object> paramMap);
    
    
     /** 
     * updateSSFJ:更新手术分级. <br/>
     * @author whk00196 
     * @date 2018年7月16日 上午10:42:01 *
     * @param paramMap
     * @return 
     * @since JDK 1.8 
     **/
    boolean updateSSFJ(@Param("param")Map<String, Object> paramMap);
    
    List<Map<String,Object>> queryTj();
};