
/** 
* Project Name:cDrgKPI <br/> 
* File Name:ZkgzpzVo.java <br/>
* Package Name:com.kindo.pz.model <br/>
* Date:2018年7月6日上午10:08:34 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/** 
* ClassName: ZkgzpzVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月6日 上午10:08:34 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class ZkgzpzQo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -4018391860441490552L;

	String id;
	String czType;
	String gzType;
	String gzName;
	String memberCode;
	UserLoginInfo user;
};
