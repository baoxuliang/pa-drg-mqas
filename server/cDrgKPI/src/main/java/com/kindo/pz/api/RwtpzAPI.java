
/** 
* Project Name:cDrgKPI <br/> 
* File Name:RwtpzAPI.java <br/>
* Package Name:com.kindo.pz.api <br/>
* Date:2018年6月25日上午9:49:21 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.pz.api;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.pz.model.BzkshzpzQo;
import com.kindo.pz.model.HysjRwtQo;
import com.kindo.pz.service.RwtpzService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/** 
* ClassName: RwtpzAPI <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月25日 上午9:49:21 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@RestController
@RequestMapping("/pz/rwtpz")
public class RwtpzAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(RwtpzAPI.class);
	@Autowired
	private RwtpzService service;

	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	 @Autowired
	 private RedisOper redisOper;
	  
	 
	@ModelAttribute
   public void initUser(BzkshzpzQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info = (UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(HysjRwtQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(HysjRwtQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/rwtpz/listpage] RwtpzAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageRwtpz(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/rwtpz/listpage] RwtpzAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/exportlistpage", method = { RequestMethod.GET })
	public void exportlistpage(HysjRwtQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/rwtpz/exportlistpage] RwtpzAPI.exportlistpage 接受导出请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportlistpage(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/pz/rwtpz/exportlistpage] RwtpzAPI.exportlistpage 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/use", method = { RequestMethod.POST,RequestMethod.PUT })
	public ApiResult useInSystem(@RequestBody HysjRwtQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/pz/rwtpz/use] RwtpzAPI.useInSystem 接受更新请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getRwtType())||(!"2".equals(vo.getRwtType())&&!"1".equals(vo.getRwtType()))) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "rwtType参数为空");
			}
			paramMap = Tools.convertBean2Map(vo);
			flag = service.useInSystem(paramMap, re);
			 return new ApiResult(flag?Constants.RESULT.SUCCESS.intValue():Constants.RESULT.FAIL, "更新"+(flag?"成功":"失败"), flag);
		} catch (Exception e) {
			LOGGER.error("[/pz/rwtpz/use] RwtpzAPI.useInSystem 接受更新请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/useinfo", method = {RequestMethod.GET })
	public ApiResult useInfo( HysjRwtQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/pz/rwtpz/useinfo] RwtpzAPI.useInfo 接受查询请求,请求参数为:{}", vo);
			result = service.getRwtParm();
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/rwtpz/useinfo] RwtpzAPI.useInfo 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};

	@RequestMapping(value = "/drgname", method = { RequestMethod.GET })
	public ApiResult queryDrgName(HysjRwtQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		List<Map<String, Object>> result = null;
		try {
			LOGGER.info("[/pz/rwtpz/drgname] RwtpzAPI.queryDrgName 接受查询请求,请求参数为:{}", vo);
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.queryDrgName(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/pz/rwtpz/drgname] RwtpzAPI.queryDrgName 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	

};
