package com.kindo.surgicalSkills.model;

import lombok.Data;

/**
 * 外科能力分析-表格数据返回pojo
 * @author likai
 *
 */
@Data
public class HosByGrid {

	private String memberCode;
	
	private String memberName;
	
	private Integer surgerySum;
	
	private Integer lev1_surgery;
	
	private Integer lev2_surgery;
	
	private Integer lev3_surgery;
	
	private Integer lev4_surgery;
	
	private Integer levTF_surgery;
	
	private Double levTF_surgeryRatio;
	
	private Double jsnd;
	
	private Double fxcd;
	
	private Double pjzyts;
	
	private Double pjsqzyts;
	
	private Double pjzyfy;
	
	private Double pjssfy;
	
	private Double ssfyzb;
	
	private Double yzb;
	
	private Double hczb;
}
