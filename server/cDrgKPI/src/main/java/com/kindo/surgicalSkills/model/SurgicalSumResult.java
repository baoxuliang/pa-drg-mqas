package com.kindo.surgicalSkills.model;

import lombok.Data;

@Data
public class SurgicalSumResult {

	private Integer surgeryTotal;
	
	private Double jsndTotal;
	
	private Double fxcdTotal;
}
