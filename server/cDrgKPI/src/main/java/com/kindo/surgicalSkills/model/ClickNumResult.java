package com.kindo.surgicalSkills.model;

import lombok.Data;

/**
 * 医院，科室外科能力分析-点击表格手术台数返回pojo
 * @author likai
 *
 */
@Data
public class ClickNumResult {

	private String cchiCode;
	
	private String cchiName;
	
	private Integer ssts;
	
	private String ssfj;
	
	private Double jsnd;
	
	private Double fxcd;
	
	private Double pjzyts;
	
	private Double pjsqzyts;
	
	private Double pjzyfy;
	
	private Double pjssfy;
	
	private Double ssfyzb;
	
	private Double yzb;
	
	private Double hczb;
}
