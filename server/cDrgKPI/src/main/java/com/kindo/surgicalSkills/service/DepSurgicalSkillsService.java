package com.kindo.surgicalSkills.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.difficultCases.utils.ExcelUtil;
import com.kindo.surgicalSkills.dao.DepSurgicalSkillsMapper;
import com.kindo.surgicalSkills.model.ClickNumResult;
import com.kindo.surgicalSkills.model.DepByGrid;
import com.kindo.surgicalSkills.model.HosAndDepQo;
import com.kindo.surgicalSkills.model.HosByGrid;
import com.kindo.uas.common.dict.DictRemoteManager;

@Service
public class DepSurgicalSkillsService {
	
	@Autowired
	private DepSurgicalSkillsMapper mapper;
	
	public ApiResult getDepSkillsQuery(HosAndDepQo qo){
		qo.setStdksdm(null);////图形数据不需要科室编码条件查询
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,
				mapper.getDepSkillsQuery(qo));
	}
	
	public ApiResult getDepSkillsQueryByGrid(HosAndDepQo qo,Pageable pagination){
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" surgerySum ");
    		sp1.setAsc(false);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.getDepSkillsQueryByGrid(qo,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult getDepSkillsQueryInfo(HosAndDepQo qo,Pageable pagination){
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" surgerySum ");
    		sp1.setAsc(false);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.getDepSkillsQueryInfo(qo,pagination));  
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryDepByNumClick(HosAndDepQo qo,Pageable pagination){
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.queryDepByNumClick(qo,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryDepDataSum(HosAndDepQo qo){
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,
				mapper.queryDepDataSum(qo));
	}
	
	/**
	 * 科室外科能力点击手术台数数据导出
	 */
	public void exportByNumClickData(HttpServletResponse response,HosAndDepQo qo){
		String fileName = "科室外科能力分析手术台数明细" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        String[] rowsName = new String[]{"CCHI_L","手术操作名称_L",
        		"手术台数_R","手术等级_C","技术难度_R","风险程度_R","平均住院天数_R","平均术前住院天数_R",
        		"平均住院费用_R","平均手术费用_R","手术费用占比(%)_R","药占比(%)_R","耗材占比(%)_R"};
		
        List<ClickNumResult> list = mapper.queryDepByNumClick(qo);
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        for (int i = 0; i < list.size(); i++) {
        	ClickNumResult hos = list.get(i);
            objs = new Object[rowsName.length];
            objs[0] = hos.getCchiCode();
            objs[1] = hos.getCchiName();
            objs[2] = hos.getSsts();
            objs[3] = getSsfjStr(hos.getSsfj());
            objs[4] = hos.getJsnd()!= null?fm.format(hos.getJsnd()):hos.getJsnd();
            objs[5] = hos.getFxcd()!=null?fm.format(hos.getFxcd()):hos.getFxcd();
            objs[6] = hos.getPjzyts()!=null?fm.format(hos.getPjzyts()):hos.getPjzyts();
            objs[7] = hos.getPjsqzyts()!=null?fm.format(hos.getPjsqzyts()):hos.getPjsqzyts();
            objs[8] = hos.getPjzyfy()!=null?fm.format(hos.getPjzyfy()):hos.getPjzyfy();
            objs[9] = hos.getPjssfy()!=null?fm.format(hos.getPjssfy()):hos.getPjssfy();
            objs[10] = hos.getSsfyzb()!=null?fm.format(hos.getSsfyzb()):hos.getSsfyzb();
            objs[11] = hos.getYzb()!=null?fm.format(hos.getYzb()):hos.getYzb();
            objs[12] = hos.getHczb()!=null?fm.format(hos.getHczb()):hos.getHczb();
            dataList.add(objs);
        }
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
			ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	public void exportDepSkillsQuery(HttpServletResponse response,HosAndDepQo qo){
		String fileName = "科室外科能力分析数据" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        String[] rowsName = new String[]{"标准科室_L","手术台数_R",
        		"一级手术_R","二级手术_R","三级手术_R","四级手术_R","三四级手术_R","三四级手术占比(%)_R",
        		"技术难度_R","风险程度_R","平均住院天数_R","平均术前住院天数_R",
        		"平均住院费用_R","平均手术费用_R","手术费用占比(%)_R","药占比(%)_R","耗材占比(%)_R"};
		
        List<DepByGrid> list = mapper.getDepSkillsQueryByGrid(qo);
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        for (int i = 0; i < list.size(); i++) {
        	DepByGrid hos = list.get(i);
            objs = new Object[rowsName.length];
            objs[0] = hos.getStdksmc();
            objs[1] = hos.getSurgerySum();
            objs[2] = hos.getLev1_surgery();
            objs[3] = hos.getLev2_surgery();
            objs[4] = hos.getLev3_surgery();
            objs[5] = hos.getLev4_surgery();
            objs[6] = hos.getLevTF_surgery();
            objs[7] = hos.getLevTF_surgeryRatio()!=null?fm.format(hos.getLevTF_surgeryRatio()):hos.getLevTF_surgeryRatio();
            objs[8] = hos.getJsnd()!= null?fm.format(hos.getJsnd()):hos.getJsnd();
            objs[9] = hos.getFxcd()!=null?fm.format(hos.getFxcd()):hos.getFxcd();
            objs[10] = hos.getPjzyts()!=null?fm.format(hos.getPjzyts()):hos.getPjzyts();
            objs[11] = hos.getPjsqzyts()!=null?fm.format(hos.getPjsqzyts()):hos.getPjsqzyts();
            objs[12] = hos.getPjzyfy()!=null?fm.format(hos.getPjzyfy()):hos.getPjzyfy();
            objs[13] = hos.getPjssfy()!=null?fm.format(hos.getPjssfy()):hos.getPjssfy();
            objs[14] = hos.getSsfyzb()!=null?fm.format(hos.getSsfyzb()):hos.getSsfyzb();
            objs[15] = hos.getYzb()!=null?fm.format(hos.getYzb()):hos.getYzb();
            objs[16] = hos.getHczb()!=null?fm.format(hos.getHczb()):hos.getHczb();
            dataList.add(objs);
        }
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
        	ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	/**
	 * 科室外科能力数据导出
	 * @param response
	 * @param qo
	 */
	public void exportQueryInfoByDep(HttpServletResponse response,HosAndDepQo qo){
		String fileName = "科室外科能力分析查看明细" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        String[] rowsName = new String[]{"医院名称_L","手术台数_R",
        		"一级手术_R","二级手术_R","三级手术_R","四级手术_R","三四级手术_R","三四级手术占比(%)_R",
        		"技术难度_R","风险程度_R","平均住院天数_R","平均术前住院天数_R",
        		"平均住院费用_R","平均手术费用_R","手术费用占比(%)_R","药占比(%)_R","耗材占比(%)_R"};
		
        List<HosByGrid> list = mapper.getDepSkillsQueryInfo(qo);
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        for (int i = 0; i < list.size(); i++) {
        	HosByGrid hos = list.get(i);
            objs = new Object[rowsName.length];
            objs[0] = hos.getMemberName();
            objs[1] = hos.getSurgerySum();
            objs[2] = hos.getLev1_surgery();
            objs[3] = hos.getLev2_surgery();
            objs[4] = hos.getLev3_surgery();
            objs[5] = hos.getLev4_surgery();
            objs[6] = hos.getLevTF_surgery();
            objs[7] = hos.getLevTF_surgeryRatio()!=null?fm.format(hos.getLevTF_surgeryRatio()):hos.getLevTF_surgeryRatio();
            objs[8] = hos.getJsnd()!= null?fm.format(hos.getJsnd()):hos.getJsnd();
            objs[9] = hos.getFxcd()!=null?fm.format(hos.getFxcd()):hos.getFxcd();
            objs[10] = hos.getPjzyts()!=null?fm.format(hos.getPjzyts()):hos.getPjzyts();
            objs[11] = hos.getPjsqzyts()!=null?fm.format(hos.getPjsqzyts()):hos.getPjsqzyts();
            objs[12] = hos.getPjzyfy()!=null?fm.format(hos.getPjzyfy()):hos.getPjzyfy();
            objs[13] = hos.getPjssfy()!=null?fm.format(hos.getPjssfy()):hos.getPjssfy();
            objs[14] = hos.getSsfyzb()!=null?fm.format(hos.getSsfyzb()):hos.getSsfyzb();
            objs[15] = hos.getYzb()!=null?fm.format(hos.getYzb()):hos.getYzb();
            objs[16] = hos.getHczb()!=null?fm.format(hos.getHczb()):hos.getHczb();
            dataList.add(objs);
        }
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
        	ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	/**
	 * 获取手术分级
	 * @param code
	 * @return
	 */
	public String getSsfjStr(String code){
		DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("CC06_00_255_00");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
        	  if(entry.getKey().equals(code)){
        		  return entry.getValue();
        	  }
        }
		return "";
	}
}
