package com.kindo.surgicalSkills.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.surgicalSkills.model.HosAndDepQo;
import com.kindo.surgicalSkills.service.HosSurgicalSkillsService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/drgjx/hospitalsurgical")
public class HosSurgicalSkillsDataApi {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HosSurgicalSkillsDataApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private HosSurgicalSkillsService service;
	@Autowired
	private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(HosAndDepQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		vo.setUser(info);
     };
	

	@RequestMapping(value = "/hospitalskills/query", method = RequestMethod.GET)
	public ApiResult getHosSkillsQuery(HosAndDepQo qo){
		try {
			service.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getHosSkillsQuery(qo);
	}

	@RequestMapping(value = "hospitalskills/queryByPage", method = RequestMethod.GET)
	public ApiResult getHosSkillsQueryByPage(HosAndDepQo qo,Pageable pagination){
		try {
			service.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getHosSkillsQueryByPage(qo,pagination);
	}
	
	@RequestMapping(value = "/hospitalskills/exportHosSkillsQuery", method = RequestMethod.GET)
	public void exportHosSkillsQuery(HttpServletResponse response,HosAndDepQo qo){
		try {
			service.getTheRegion(qo);
			service.exportHosSkillsQuery(response,qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/hospitalskills/queryInfo", method = RequestMethod.GET)
	public ApiResult getHosSkillsQueryInfo(HosAndDepQo qo,Pageable pagination){
		try {
			service.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getHosSkillsQueryInfo(qo,pagination);
	}
	
	@RequestMapping(value = "/hospitalskills/queryByNumClick", method = RequestMethod.GET)
	public ApiResult queryHospitalByNumClick(HosAndDepQo qo,Pageable pagination){
		try {
			service.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.queryHospitalByNumClick(qo,pagination);
	}
	
	@RequestMapping(value = "/hospitalskills/queryHosDataSum", method = RequestMethod.GET)
	public ApiResult queryHospitalDataSum(HosAndDepQo qo){
		try {
			service.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.queryHospitalDataSum(qo);
	}
	
	@RequestMapping(value = "/hospitalskills/exportByNumClick/export", method = RequestMethod.GET)
	public void exportByNumClickData(HttpServletResponse response,HosAndDepQo qo){
		try {
			service.getTheRegion(qo);
			service.exportByNumClickData(response,qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/hospitalskills/exportQueryInfoByHos/export", method = RequestMethod.GET)
	public void exportQueryInfoByHos(HttpServletResponse response,HosAndDepQo qo){
		try {
			service.getTheRegion(qo);
			service.exportQueryInfoByHos(response,qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
