package com.kindo.surgicalSkills.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.difficultCases.utils.ExcelUtil;
import com.kindo.surgicalSkills.dao.SurgicalQueryMapper;
import com.kindo.surgicalSkills.model.SurgicalClickNumResult;
import com.kindo.surgicalSkills.model.SurgicalQueryQo;
import com.kindo.surgicalSkills.model.SurgicalQueryResult;
import com.kindo.surgicalSkills.model.SurgicalSumResult;
import com.kindo.uas.common.dict.DictRemoteManager;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class SurgicalQueryService {
	
	@Autowired
	private SurgicalQueryMapper mapper;
	
	public ApiResult getSurgicalQuery(SurgicalQueryQo qo,Pageable pagination){
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" cchiCode ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.getSurgicalQueryByGrid(qo,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryByNumData(SurgicalQueryQo qo,Pageable pagination){
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" memberName ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" ,bah ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.querySurgicalQueryByNumClick(qo,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryByDataSum(SurgicalQueryQo qo){
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,
				mapper.querySurgicalQuerySum(qo));
	}
	
	public void exportSurgicalData(HttpServletResponse response,SurgicalQueryQo qo){
		String fileName = "外科能力分析-手术数据" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        String[] rowsName = new String[]{"序号_C","CCHI_L","手术操作名称_L",
        		"手术台数_R","手术等级_C","技术难度_R","风险程度_R"};
		
        List<SurgicalQueryResult> list = mapper.getSurgicalQueryByGrid(qo);
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        if(list.size() > 0){
        	for (int i = 0; i < list.size(); i++) {
            	SurgicalQueryResult hos = list.get(i);
                objs = new Object[rowsName.length];
                objs[0] = i+1;
                objs[1] = hos.getCchiCode();
                objs[2] = hos.getCchiName();
                objs[3] = hos.getSurgerySum();
                objs[4] = getSsfjStr(hos.getSurgeryLev());
                objs[5] = hos.getJsnd()!= null?fm.format(hos.getJsnd()):hos.getJsnd();
                objs[6] = hos.getFxcd()!=null?fm.format(hos.getFxcd()):hos.getFxcd();
                dataList.add(objs);
            }
            
            SurgicalSumResult sum = mapper.querySurgicalQuerySum(qo);//合计数据
            
            objs = new Object[rowsName.length];
            objs[0] = "合计";
            objs[3] = sum.getSurgeryTotal();
            
            dataList.add(new Object[rowsName.length]);
            dataList.add(objs);
            
            sheet.addMergedRegion(new CellRangeAddress(list.size()+1,list.size()+1,0,7));//横向：合并第一行的第1列到第12列  
        }
        
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
			ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	public void exportByNumData(HttpServletResponse response,SurgicalQueryQo qo){
		String fileName = "外科能力分析-手术查询手术台数明细数据" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        String[] rowsName = new String[]{"医院名称_L","病案号_L",
        		"主要诊断ICD10_L","主要操作ICD9_L","出院日期_C","住院天数_R",
        		"出院科室_L","离院方式_L"};
		
        List<SurgicalClickNumResult> list = mapper.querySurgicalQueryByNumClick(qo);
        Object[] objs = null;
        for (int i = 0; i < list.size(); i++) {
        	SurgicalClickNumResult hos = list.get(i);
            objs = new Object[rowsName.length];
            objs[0] = hos.getMemberName();
            objs[1] = hos.getBah();
            objs[2] = hos.getZyzd_icd10();
            objs[3] = hos.getZycz_icd9();
            objs[4] = hos.getCyrq();
            objs[5] = hos.getZyts();
            objs[6] = hos.getCyks();
            objs[7] = getLyfsStr(hos.getLyfs());
            dataList.add(objs);
        }
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
			ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	/**
	 * 获取离院方式
	 * @param code
	 * @return
	 */
	public String getLyfsStr(String code){
		DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("CC06_00_223_00");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
        	  if(entry.getKey().equals(code)){
        		  return entry.getValue();
        	  }
        }
		return "";
	}
	
	/**
	 * 获取手术分级
	 * @param code
	 * @return
	 */
	public String getSsfjStr(String code){
		DictRemoteManager drm= new DictRemoteManager();
        Map<String, String> dicValue =  drm.getDict("CC06_00_255_00");
        for (Map.Entry<String, String> entry : dicValue.entrySet()) { 
        	  if(entry.getKey().equals(code)){
        		  return entry.getValue();
        	  }
        }
		return "";
	}
	

	 /** 
		 * getTheRegion:得到用户的地区/医院. <br/>
		 * @author whk00196 
		 * @date 2018年7月17日 下午9:00:45 *
		 * @param vo 
		 * @throws Exception 
		 * @since JDK 1.8 
		 **/
		public void  getTheRegion(SurgicalQueryQo vo) throws Exception {
			UserLoginInfo user = null;
			user =  vo.getUser();
			if(user == null) {
				throw new Exception("用户未登陆或者已失效");
			}
			if(
			!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
			&&
			(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
			&&
			(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
			  ) {
				return;
			}
		
			String orgaType = user.getOrgaType();
			String region = null;
			if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			}else if("SHWJW".equals(orgaType)) {
			}else if("SWJW".equals(orgaType)) {
				region = user.getOrgaId();
				vo.setCity(region);
			}else if("XWJW".equals(orgaType)) {
				region = user.getOrgaId();
				vo.setCnty(region);
			}else if("HOS".equals(orgaType)) {
				region = user.getOrgaId();
				vo.setMemberCode(region);
			}
		};
}
