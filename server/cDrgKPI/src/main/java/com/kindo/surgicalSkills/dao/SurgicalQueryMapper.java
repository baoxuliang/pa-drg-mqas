package com.kindo.surgicalSkills.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.kindo.aria.base.Pageable;
import com.kindo.surgicalSkills.model.SurgicalClickNumResult;
import com.kindo.surgicalSkills.model.SurgicalQueryQo;
import com.kindo.surgicalSkills.model.SurgicalQueryResult;
import com.kindo.surgicalSkills.model.SurgicalSumResult;

@Mapper
public interface SurgicalQueryMapper {

	List<SurgicalQueryResult> getSurgicalQueryByGrid(@Param("param")SurgicalQueryQo qo); 
	
	List<SurgicalQueryResult> getSurgicalQueryByGrid(@Param("param")SurgicalQueryQo qo,@Param("pagination") Pageable pagination); 
	
	List<SurgicalClickNumResult> querySurgicalQueryByNumClick(@Param("param")SurgicalQueryQo qo,@Param("pagination") Pageable pagination);
	
	List<SurgicalClickNumResult> querySurgicalQueryByNumClick(@Param("param")SurgicalQueryQo qo);

	SurgicalSumResult querySurgicalQuerySum(@Param("param")SurgicalQueryQo qo);
	
}
