package com.kindo.surgicalSkills.model;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/**
 * 医院，科室 外科能力查询参数pojo
 * @author likai
 *
 */
@Data
public class HosAndDepQo {

	/**
	 * 行政区划类型（省:1    市:2    区:3）
	 */
	private String city;
	
	/**
	 * 行政区划代码
	 */
	private String cnty;
	
	/**
	 * 区域code
	 */
	private String qy_code;
	
	/**
	 * 年
	 */
	private String year;
	
	/**
	 * 季度
	 */
	private String quarter;
	
	/**
	 * 月份
	 */
	private String month;
	
	/**
	 * 科室编码
	 */
	private String stdksdm;
	
	/**
	 * 医疗机构编码
	 */
	private String memberCode;
	
	/**
	 * 医疗机构类型代码（0:综合医院 1:转科医院）
	 */
	private String memberTypeCode;
	
	/**
	 * 医疗机构等级（1:一级 2：二级 3：三级）
	 */
	private String memberLev1;
	
	/**
	 * 医疗机构等级（1:特 2：甲 3：乙 4：丙）
	 */
	private String memberLev2;
	
	/**
	 * 手术分级 （A-一级手术，B-二级手术，C-三级手术，D-四级手术）
	 */
	private String ssfj;
	
	private UserLoginInfo user;
	
}
