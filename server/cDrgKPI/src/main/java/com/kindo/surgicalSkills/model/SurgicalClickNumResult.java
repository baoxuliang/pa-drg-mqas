package com.kindo.surgicalSkills.model;

import lombok.Data;

/**
 * 外科能力分析-手术查询点击手术台数返回pojo
 * @author likai
 *
 */
@Data
public class SurgicalClickNumResult {
	
	private String id;

	private String memberName;
	
	private String bah;
	
	private String zyzd_icd10;
	
	private String zycz_icd9;
	
	private String cyrq;
	
	private Integer zyts;
	
	private String cyks;
	
	private String lyfs;
}
