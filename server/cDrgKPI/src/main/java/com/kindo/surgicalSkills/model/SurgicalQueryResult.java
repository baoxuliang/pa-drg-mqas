package com.kindo.surgicalSkills.model;

import lombok.Data;

@Data
public class SurgicalQueryResult {

	private String cchiCode;
	
	private String cchiName;
	
	private Integer surgerySum;
	
	private String surgeryLev;
	
	private Double jsnd;
	
	private Double fxcd;
}
