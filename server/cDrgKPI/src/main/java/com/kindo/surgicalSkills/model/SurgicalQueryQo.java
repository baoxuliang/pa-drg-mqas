package com.kindo.surgicalSkills.model;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/**
 *  外科能力分析   手术查询查询参数pojo
 * @author likai
 *
 */
@Data
public class SurgicalQueryQo {

	/**
	 * 行政区划类型（省:1    市:2    区:3）
	 */
	private String city;
	
	/**
	 * 行政区划代码
	 */
	private String cnty;
	
	/**
	 * 区域code
	 */
	private String qy_code;
	
	/**
	 * 年
	 */
	private String year;
	
	/**
	 * 季度
	 */
	private String quarter;
	
	/**
	 * 月份
	 */
	private String month;
	
	/**
	 * 医疗机构编码
	 */
	private String memberCode;
	
	
	/**
	 * cchi编码或者名称
	 */
	private String cchiCodeOrName;
	
	
	/**
	 * 手术分级 （A-一级手术，B-二级手术，C-三级手术，D-四级手术）
	 */
	private String surgeryLev;
	
	/**
	 * cchi编码
	 */
	private String cchiCode;
	
	private UserLoginInfo user;
	
}
