package com.kindo.surgicalSkills.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.kindo.aria.base.Pageable;
import com.kindo.surgicalSkills.model.ClickNumResult;
import com.kindo.surgicalSkills.model.DepByGrid;
import com.kindo.surgicalSkills.model.HosAndDepQo;
import com.kindo.surgicalSkills.model.HosByGraphics;
import com.kindo.surgicalSkills.model.HosByGrid;
import com.kindo.surgicalSkills.model.SummationResult;

@Mapper
public interface HosSurgicalSkillsMapper {

	List<HosByGraphics> getHosSkillsQuery(@Param("param")HosAndDepQo qo); 
	
	List<HosByGrid> getHosSkillsQueryByGrid(@Param("param")HosAndDepQo qo,@Param("pagination") Pageable pagination); 
	
	List<HosByGrid> getHosSkillsQueryByGrid(@Param("param")HosAndDepQo qo); 
	
	List<DepByGrid> getHosSkillsQueryInfo(@Param("param")HosAndDepQo qo,@Param("pagination") Pageable pagination);
	
	List<DepByGrid> getHosSkillsQueryInfo(@Param("param")HosAndDepQo qo);
	
	List<ClickNumResult> queryHospitalByNumClick(@Param("param")HosAndDepQo qo,@Param("pagination") Pageable pagination);
	
	List<ClickNumResult> queryHospitalByNumClick(@Param("param")HosAndDepQo qo);
	
	SummationResult queryHospitalDataSum(@Param("param")HosAndDepQo qo); 

}
