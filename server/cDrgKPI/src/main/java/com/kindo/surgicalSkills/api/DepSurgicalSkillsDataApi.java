package com.kindo.surgicalSkills.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.surgicalSkills.model.HosAndDepQo;
import com.kindo.surgicalSkills.service.DepSurgicalSkillsService;
import com.kindo.surgicalSkills.service.HosSurgicalSkillsService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/drgjx/departmentsurgical")
public class DepSurgicalSkillsDataApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(HosSurgicalSkillsDataApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private DepSurgicalSkillsService service;
	@Autowired
	private HosSurgicalSkillsService hosService;
	@Autowired
	private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(HosAndDepQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		vo.setUser(info);
     };
	

	@RequestMapping(value = "/departmentskills/query", method = RequestMethod.GET)
	public ApiResult getDepSkillsQuery(HosAndDepQo qo){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getDepSkillsQuery(qo);
	}

	@RequestMapping(value = "departmentskills/queryByPage", method = RequestMethod.GET)
	public ApiResult getDepSkillsQueryByPage(HosAndDepQo qo,Pageable pagination){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getDepSkillsQueryByGrid(qo, pagination);
	}
	
	@RequestMapping(value = "/departmentskills/exportDepSkillsQuery", method = RequestMethod.GET)
	public void exportDepSkillsQuery(HttpServletResponse response,HosAndDepQo qo){
		try {
			hosService.getTheRegion(qo);
			service.exportDepSkillsQuery(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/departmentskills/queryInfo", method = RequestMethod.GET)
	public ApiResult getDepSkillsQueryInfo(HosAndDepQo qo,Pageable pagination){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getDepSkillsQueryInfo(qo, pagination);
	}
	
	@RequestMapping(value = "/departmentskills/queryByNumClick", method = RequestMethod.GET)
	public ApiResult exportDepSkillsByNumClick(HosAndDepQo qo,Pageable pagination){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.queryDepByNumClick(qo, pagination);
	}
	
	@RequestMapping(value = "/departmentskills/queryHosDataSum", method = RequestMethod.GET)
	public ApiResult queryDepSkillsDataSum(HosAndDepQo qo){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.queryDepDataSum(qo);
	}
	
	@RequestMapping(value = "/departmentskills/exportByNumClick/export", method = RequestMethod.GET)
	public void exportDepSkillsByNumClickData(HttpServletResponse response,HosAndDepQo qo){
		try {
			hosService.getTheRegion(qo);
			service.exportByNumClickData(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/departmentskills/exportQueryInfoData/export", method = RequestMethod.GET)
	public void exportDepSkillsQueryInfoByHos(HttpServletResponse response,HosAndDepQo qo){
		try {
			hosService.getTheRegion(qo);
			service.exportQueryInfoByDep(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
