package com.kindo.surgicalSkills.model;

import lombok.Data;

/**
 * 医院外科能力分析-图形返回pojo
 * @author likai
 *
 */
@Data
public class HosByGraphics {

	private String memberName;
	
	private Integer surgerySum;
	
	private Integer lev1_surgery;
	
	private Integer lev2_surgery;
	
	private Integer lev3_surgery;
	
	private Integer lev4_surgery;
}
