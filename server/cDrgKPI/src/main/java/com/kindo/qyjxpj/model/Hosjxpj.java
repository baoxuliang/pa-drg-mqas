package com.kindo.qyjxpj.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

public class Hosjxpj implements Serializable {
	
    
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -6623175991707886210L;

	private String id;

    private String jbhdate;

    private String jbhtype;

    private String qyCode;

    private String memberCode;
    
    @PoiExcelField(index=0,title="医院名称",halign="left")
    private String memberName;

    private String memberType;

    private String memberLev1;

    private String memberLev2;
    
    @PoiExcelField(index=1,title="综合绩效",halign="right")
    private Double jbhzhzs;
    
    @PoiExcelField(index=2,title="总病案数",halign="right")
    private Integer jbhbanum;
    @PoiExcelField(index=3,title="入组病案数",halign="right")
    private Integer jbhindrgnum;

    private Integer jbhynbl;
    
    @PoiExcelField(index=4,title="MDC分类",halign="right")
    private Integer jbhmdcnum;
    @PoiExcelField(index=5,title="DRG组数",halign="right")
    private Integer jbhdrgnum;
    @PoiExcelField(index=6,title="总权重",halign="right",format="0.00")
    private Double jbhrwt;
    @PoiExcelField(index=7,title="CMI值",halign="right")
    private Double jbhcmi;
    @PoiExcelField(index=8,title="时间消耗指数",halign="right")
    private Double jbhtimesi;
    @PoiExcelField(index=9,title="平均住院日",halign="right")
    private Double jbhpjzyr;
    @PoiExcelField(index=10,title="费用消耗指数",halign="right")
    private Double jbhcostsi;
    @PoiExcelField(index=11,title="次均费用",halign="right")
    private Double jbhcjfy;

    private Integer jbhlownum;
    
    @PoiExcelField(index=12,title="低风险组死亡率%(人数)",halign="right")
    private String jbhlowswl;

    private Integer jbhmednum;
    @PoiExcelField(index=13,title="中低风险组死亡率%(人数)",halign="right")
    private String jbhmedswl;

    private Double jbhsszb;

    private Date moddate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getJbhdate() {
        return jbhdate;
    }

    public void setJbhdate(String jbhdate) {
        this.jbhdate = jbhdate == null ? null : jbhdate.trim();
    }

    public String getJbhtype() {
        return jbhtype;
    }

    public void setJbhtype(String jbhtype) {
        this.jbhtype = jbhtype == null ? null : jbhtype.trim();
    }

    public String getQyCode() {
        return qyCode;
    }

    public void setQyCode(String qyCode) {
        this.qyCode = qyCode == null ? null : qyCode.trim();
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode == null ? null : memberCode.trim();
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType == null ? null : memberType.trim();
    }

    public String getMemberLev1() {
        return memberLev1;
    }

    public void setMemberLev1(String memberLev1) {
        this.memberLev1 = memberLev1 == null ? null : memberLev1.trim();
    }

    public String getMemberLev2() {
        return memberLev2;
    }

    public void setMemberLev2(String memberLev2) {
        this.memberLev2 = memberLev2 == null ? null : memberLev2.trim();
    }

    public Double getJbhzhzs() {
        return jbhzhzs;
    }

    public void setJbhzhzs(Double jbhzhzs) {
        this.jbhzhzs = jbhzhzs;
    }

    public Integer getJbhbanum() {
        return jbhbanum;
    }

    public void setJbhbanum(Integer jbhbanum) {
        this.jbhbanum = jbhbanum;
    }

    public Integer getJbhindrgnum() {
        return jbhindrgnum;
    }

    public void setJbhindrgnum(Integer jbhindrgnum) {
        this.jbhindrgnum = jbhindrgnum;
    }

    public Integer getJbhynbl() {
        return jbhynbl;
    }

    public void setJbhynbl(Integer jbhynbl) {
        this.jbhynbl = jbhynbl;
    }

    public Integer getJbhmdcnum() {
        return jbhmdcnum;
    }

    public void setJbhmdcnum(Integer jbhmdcnum) {
        this.jbhmdcnum = jbhmdcnum;
    }

    public Integer getJbhdrgnum() {
        return jbhdrgnum;
    }

    public void setJbhdrgnum(Integer jbhdrgnum) {
        this.jbhdrgnum = jbhdrgnum;
    }

    public Double getJbhrwt() {
        return jbhrwt;
    }

    public void setJbhrwt(Double jbhrwt) {
        this.jbhrwt = jbhrwt;
    }

    public Double getJbhcmi() {
        return jbhcmi;
    }

    public void setJbhcmi(Double jbhcmi) {
        this.jbhcmi = jbhcmi;
    }

    public Double getJbhtimesi() {
        return jbhtimesi;
    }

    public void setJbhtimesi(Double jbhtimesi) {
        this.jbhtimesi = jbhtimesi;
    }

    public Double getJbhpjzyr() {
        return jbhpjzyr;
    }

    public void setJbhpjzyr(Double jbhpjzyr) {
        this.jbhpjzyr = jbhpjzyr;
    }

    public Double getJbhcostsi() {
        return jbhcostsi;
    }

    public void setJbhcostsi(Double jbhcostsi) {
        this.jbhcostsi = jbhcostsi;
    }

    public Double getJbhcjfy() {
        return jbhcjfy;
    }

    public void setJbhcjfy(Double jbhcjfy) {
        this.jbhcjfy = jbhcjfy;
    }

    public Integer getJbhlownum() {
        return jbhlownum;
    }

    public void setJbhlownum(Integer jbhlownum) {
        this.jbhlownum = jbhlownum;
    }

    public String getJbhlowswl() {
        return jbhlowswl;
    }

    public void setJbhlowswl(String jbhlowswl) {
        this.jbhlowswl = jbhlowswl;
    }

    public Integer getJbhmednum() {
        return jbhmednum;
    }

    public void setJbhmednum(Integer jbhmednum) {
        this.jbhmednum = jbhmednum;
    }

    public String getJbhmedswl() {
        return jbhmedswl;
    }

    public void setJbhmedswl(String jbhmedswl) {
        this.jbhmedswl = jbhmedswl;
    }

    public Double getJbhsszb() {
        return jbhsszb;
    }

    public void setJbhsszb(Double jbhsszb) {
        this.jbhsszb = jbhsszb;
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", jbhdate=").append(jbhdate);
        sb.append(", jbhtype=").append(jbhtype);
        sb.append(", qyCode=").append(qyCode);
        sb.append(", memberCode=").append(memberCode);
        sb.append(", memberName=").append(memberName);
        sb.append(", memberType=").append(memberType);
        sb.append(", memberLev1=").append(memberLev1);
        sb.append(", memberLev2=").append(memberLev2);
        sb.append(", jbhzhzs=").append(jbhzhzs);
        sb.append(", jbhbanum=").append(jbhbanum);
        sb.append(", jbhindrgnum=").append(jbhindrgnum);
        sb.append(", jbhynbl=").append(jbhynbl);
        sb.append(", jbhmdcnum=").append(jbhmdcnum);
        sb.append(", jbhdrgnum=").append(jbhdrgnum);
        sb.append(", jbhrwt=").append(jbhrwt);
        sb.append(", jbhcmi=").append(jbhcmi);
        sb.append(", jbhtimesi=").append(jbhtimesi);
        sb.append(", jbhpjzyr=").append(jbhpjzyr);
        sb.append(", jbhcostsi=").append(jbhcostsi);
        sb.append(", jbhcjfy=").append(jbhcjfy);
        sb.append(", jbhlownum=").append(jbhlownum);
        sb.append(", jbhlowswl=").append(jbhlowswl);
        sb.append(", jbhmednum=").append(jbhmednum);
        sb.append(", jbhmedswl=").append(jbhmedswl);
        sb.append(", jbhsszb=").append(jbhsszb);
        sb.append(", moddate=").append(moddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}