
/** 
* Project Name:cDrgKPI <br/> 
* File Name:KsjxpjAPI.java <br/>
* Package Name:com.kindo.ksjxpj.api <br/>
* Date:2018年4月11日上午10:16:19 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.qyjxpj.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.qyjxpj.model.Hosjxpj;
import com.kindo.qyjxpj.model.QyjxpjQo;
import com.kindo.qyjxpj.service.YyjxpjService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/**
 * ClassName: KsjxpjAPI <br/>
 * Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/>
 * Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
 * date: 2018年4月11日 上午10:16:19 *<br/>
 * 
 * @author whk00196
 * @version
 * @since JDK 1.8
 **/
@RestController
@RequestMapping("/qyjxpj/yy")
public class YyjxpjAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(YyjxpjAPI.class);
	@Autowired
	private YyjxpjService service;
	 @Autowired
	 private RedisOper redisOper;
	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	@ModelAttribute
    public void initUser(QyjxpjQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    	vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(QyjxpjQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/qyjxpj/yy/listpage] YyjxpjAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageHosjxpj(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/yy/listpage] YyjxpjAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	
	@RequestMapping(value = "/exportlistpage", method = { RequestMethod.GET })
	public void exportlistpage(QyjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/qyjxpj/yy/exportlistpage] YyjxpjAPI.exportlistpage 接受导出请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportlistpage(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/yy/exportlistpage] YyjxpjAPI.exportlistpage 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/zhjx", method = { RequestMethod.GET })
	public ApiResult queryZhjx(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/qyjxpj/kspj/zhjx] YyjxpjAPI.queryZhjx 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			// 得到图例上各个点
			list = this.service.queryZhjx(paramMap);
			ms.put("rows", list);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/zhjx] YyjxpjAPI.queryZhjx 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	/**
	 * queryEfficiency:效率指标. <br/>
	 * 
	 * @author whk00196
	 * @date 2018年4月11日 下午9:20:54 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/efficiency", method = { RequestMethod.GET })
	public ApiResult queryEfficiency(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> ms = new HashMap<>();
		Object polex = null, poley = null;
		try {
			LOGGER.info("[/qyjxpj/kspj/efficiency] YyjxpjAPI.queryEfficiency 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			// 得到图例上各个点
			list = this.service.queryEfficiency(paramMap);
			ms.put("rows", list);
			// 得到 标杆 数据来源 可能来源 闯哥
//			polex = list.stream().collect(Collectors.averagingDouble(x -> {
//				return x.get("valuex") == null ? 0D : Double.valueOf(x.get("valuex").toString());
//			}));
//			poley = list.stream().collect(Collectors.averagingDouble(x -> {
//				return x.get("valuey") == null ? 0D : Double.valueOf(x.get("valuey").toString());
//			}));
			ms.put("polex", "1");
			ms.put("poley", "1");
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/efficiency] YyjxpjAPI.queryEfficiency 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/cnzb", method = { RequestMethod.GET })
	public ApiResult queryCnzb(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> ms = new HashMap<>();
		Object polex = null, poley = null;
		try {
			LOGGER.info("[/qyjxpj/kspj/cnzb] YyjxpjAPI.queryCnzb 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			// 得到图例上各个点
			list = this.service.queryCnzb(paramMap);
			ms.put("rows", list);
			// 得到 标杆 数据来源 可能来源 闯哥
			polex = list.stream().collect(Collectors.averagingDouble(x -> {
				return x.get("valuex") == null ? 0D : Double.valueOf(x.get("valuex").toString());
			}));
			poley = list.stream().collect(Collectors.averagingDouble(x -> {
				return x.get("valuey") == null ? 0D : Double.valueOf(x.get("valuey").toString());
			}));
			ms.put("polex", polex);
			ms.put("poley", poley);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/cnzb] YyjxpjAPI.queryCnzb 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/aq", method = { RequestMethod.GET })
	public ApiResult queryQq(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/qyjxpj/kspj/aq] YyjxpjAPI.queryQq 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			// 得到图例上各个点
			list = this.service.queryAq(paramMap);
			String aqOrder = vo.getAqOrder();
			if("med".equals(aqOrder)||"low".equals(aqOrder)){
				list.sort((x,y)->{ 
						String xv=x.get(aqOrder).toString();
						String yv=y.get(aqOrder).toString();
						int ret = -Double.compare(Double.valueOf(xv),Double.valueOf(yv));
						return ret;
					} );
			}
				
			ms.put("rows", list);

			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/aq] YyjxpjAPI.queryQq 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/compare", method = { RequestMethod.GET })
	public ApiResult queryCompare(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/qyjxpj/kspj/compare] YyjxpjAPI.queryCompare 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getCompItems())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "compItems参数为空");
			}
			
//			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			paramMap.put("compItems", vo.getCompItems().split(","));
			// 得到图例上各个点
			list = this.service.queryCompare(paramMap);
			ms.put("rows", list);

			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/compare] YyjxpjAPI.queryCompare 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/drg", method = { RequestMethod.GET })
	public ApiResult queryDrg(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/qyjxpj/kspj/drg] YyjxpjAPI.queryDrg 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "yyCode参数为空");
			}
			
//			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryDrg(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/drg] YyjxpjAPI.queryDrg 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportdrg", method = { RequestMethod.GET })
	public void exportdrg(QyjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/qyjxpj/yy/exportdrg] HosjxpjAPI.exportdrg 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				LOGGER.error("[/qyjxpj/yy/exportdrg] HosjxpjAPI.exportdrg 接受导出请求,参数异常,信息为:{}","yyCode参数为空" );
				return;
			}
			
//			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportdrg(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/yy/exportdrg] YyjxpjAPI.exportdrg 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/rzbas", method = { RequestMethod.GET })
	public ApiResult queryRzbas(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/qyjxpj/kspj/rzbas] YyjxpjAPI.queryRzbas 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "yyCode参数为空");
			}
			
//			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryRzbas(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/rzbas] YyjxpjAPI.queryRzbas 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportrzbas", method = { RequestMethod.GET })
	public void exportrzbas(QyjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/qyjxpj/yy/exportrzbas] HosjxpjAPI.exportrzbas 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				LOGGER.error("[/qyjxpj/yy/exportrzbas] HosjxpjAPI.exportrzbas 接受导出请求,参数异常,信息为:{}","yyCode参数为空" );
				return;
			}
			
//			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportrzbas(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/yy/exportrzbas] YyjxpjAPI.exportdrg 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/view", method = { RequestMethod.GET })
	public ApiResult queryView(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/qyjxpj/kspj/view] YyjxpjAPI.queryView 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "yyCode参数为空");
			}
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryView(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/kspj/view] YyjxpjAPI.queryView 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportview", method = { RequestMethod.GET })
	public void exportview(QyjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/qyjxpj/yy/exportview] YyjxpjAPI.exportview 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getYyCode())) {
				LOGGER.error("[/qyjxpj/yy/exportview] HosjxpjAPI.exportview 接受导出请求,参数异常,信息为:{}","yyCode参数为空" );
				return;
			}
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportview(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/qyjxpj/yy/exportview] YyjxpjAPI.exportview 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
//	@RequestMapping(value = "/querydetail", method = { RequestMethod.GET })
//	public ApiResult queryDetail(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
//		Map<String, Object> paramMap = null;
//		Map<String, Object> result = null;
//		try {
//			LOGGER.info("[/qyjxpj/kspj/querydetail] YyjxpjAPI.queryDetail 接受查询请求,请求参数为:{}", vo);
//			if(StringUtils.isEmpty(vo.getId())) {
//				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "id参数为空");
//			}
//			paramMap = Tools.convertBean2Map(vo);
//			pagination=null;
//			result = this.service.listPageDeptjxpjDetail(paramMap, pagination);
//			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
//		} catch (Exception e) {
//			LOGGER.error("[/qyjxpj/kspj/querydetail] YyjxpjAPI.queryDetail 接受查询请求,发生异常,信息为:{}",
//					e.getMessage());
//			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
//		} finally {
//
//		}
//	};
	

//
//	@RequestMapping(value = "/medlowdeathrate", method = { RequestMethod.GET })
//	public ApiResult queryMedLowDeathRate(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
//		Map<String, Object> paramMap = null;
//		List<Map<String, Object>> list = null;
//		Map<String, Object> ms = new HashMap<>();
//		try {
//			LOGGER.info("[/qyjxpj/kspj/medlowdeathrate] YyjxpjAPI.queryMedLowDeathRate 接受查询请求,请求参数为:{}", vo);
//			paramMap = Tools.convertBean2Map(vo);
//			// 得到图例上各个点
//			list = this.service.queryLeftMedLowDeathRate(paramMap);
//			ms.put("rows", list);
//
//			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
//		} catch (Exception e) {
//			LOGGER.error("[/qyjxpj/kspj/medlowdeathrate] YyjxpjAPI.queryMedLowDeathRate 接受查询请求,发生异常,信息为:{}",
//					e.getMessage());
//			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
//		} finally {
//
//		}
//
//	};
//
//	@RequestMapping(value = "/lowdeathrate", method = { RequestMethod.GET })
//	public ApiResult queryLowDeathRate(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
//		Map<String, Object> paramMap = null;
//		List<Map<String, Object>> list = null;
//		Map<String, Object> ms = new HashMap<>();
//		// BigDecimal
//		try {
//			LOGGER.info("[/qyjxpj/kspj/lowdeathrate] YyjxpjAPI.queryLowDeathRate 接受查询请求,请求参数为:{}", vo);
//			paramMap = Tools.convertBean2Map(vo);
//			// 得到图例上各个点
//			list = this.service.queryRightLowdeathrate(paramMap);
//			ms.put("rows", list);
//
//			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
//		} catch (Exception e) {
//			LOGGER.error("[/qyjxpj/kspj/lowdeathrate] YyjxpjAPI.queryLowDeathRate 接受查询请求,发生异常,信息为:{}",
//					e.getMessage());
//			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
//		} finally {
//
//		}
//
//	};
//


//

//

//	

};
