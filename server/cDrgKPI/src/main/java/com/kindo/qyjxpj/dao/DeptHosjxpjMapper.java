package com.kindo.qyjxpj.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.qyjxpj.model.DeptHosjxpj;
import com.kindo.qyjxpj.model.DeptHosjxpjKsDrg;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DeptHosjxpjMapper {
    int deleteByPrimaryKey(String id);

    int insert(DeptHosjxpj record);

    DeptHosjxpj selectByPrimaryKey(String id);

    List<DeptHosjxpj> selectAll();

    int updateByPrimaryKey(DeptHosjxpj record);



	 /** 
	 * queryView:医院DRG绩效列表-查看. <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:10:41 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<DeptHosjxpj> queryView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	List<DeptHosjxpj> queryExportView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryExportViewKsDrg:queryExportViewKsDrg 科室DRG维度的 view!. <br/>
	 * @author whk00196 
	 * @date 2018年7月4日 下午4:48:03 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<DeptHosjxpjKsDrg> queryExportViewKsDrg(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
};