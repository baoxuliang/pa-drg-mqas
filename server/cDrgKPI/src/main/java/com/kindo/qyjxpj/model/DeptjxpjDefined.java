
/** 
* Project Name:cDrgKPI <br/> 
* File Name:DeptjxpjDefined.java <br/>
* Package Name:com.kindo.qyjxpj.model <br/>
* Date:2018年6月27日下午5:43:15 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.qyjxpj.model;

import java.io.Serializable;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: DeptjxpjDefined <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月27日 下午5:43:15 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class DeptjxpjDefined implements Serializable {
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -7728598983293236779L;
	String ksbm;
	@PoiExcelField(index=0,title="标准科室",halign="left")
	String ksmc;
	@PoiExcelField(index=1,title="综合能力指数",halign="right")
	Double zhjx;
	Double zhjx_;
	@PoiExcelField(index=2,title="总病案数",halign="right")
	Integer zbas;
	@PoiExcelField(index=3,title="入组病例数",halign="right")
	Integer rzbls;
	@PoiExcelField(index=4,title="DRG组数",halign="right")
	Integer drgnum;
	@PoiExcelField(index=5,title="总权重",halign="right")
	Double zrwt;
	@PoiExcelField(index=6,title="CMI值",halign="right")
	Double cmi;
	@PoiExcelField(index=7,title="时间消耗指数",halign="right")
	Double timesi;
	@PoiExcelField(index=8,title="平均住院日",halign="right")
	Double pjzyr;
	@PoiExcelField(index=9,title="费用消耗指数",halign="right")
	Double costsi;
	@PoiExcelField(index=10,title="次均费用",halign="right")
	Double cjfy;
	Integer lownum;
	@PoiExcelField(index=11,title="低风险组死亡率%(人数)",halign="right")
	Object lowswl;
	Integer mednum;
	@PoiExcelField(index=12,title="中低风险组死亡率%(人数)",halign="right")
	Object medswl;
};
