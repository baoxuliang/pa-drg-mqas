package com.kindo.qyjxpj.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Deptjxpj implements Serializable {
    private String id;

    private String jbddate;

    private String jbdtype;

    private String jbdstddeptcode;

    private String jbdstddeptname;

    private BigDecimal jbdzhzs;

    private Integer jbdbanum;

    private Integer jbdindrgnum;

    private Integer jbddrgnum;

    private BigDecimal jbdrwt;

    private BigDecimal jbdcmi;

    private BigDecimal jbdtimesi;

    private BigDecimal jbdpjzyr;

    private BigDecimal jbdcostsi;

    private BigDecimal jbdcjfy;

    private Integer jbdlownum;

    private String jbdlowswl;

    private Integer jbdmednum;

    private String jbdmedswl;

    private BigDecimal jbdsszb;

    private Date moddate;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getJbddate() {
        return jbddate;
    }

    public void setJbddate(String jbddate) {
        this.jbddate = jbddate == null ? null : jbddate.trim();
    }

    public String getJbdtype() {
        return jbdtype;
    }

    public void setJbdtype(String jbdtype) {
        this.jbdtype = jbdtype == null ? null : jbdtype.trim();
    }

    public String getJbdstddeptcode() {
        return jbdstddeptcode;
    }

    public void setJbdstddeptcode(String jbdstddeptcode) {
        this.jbdstddeptcode = jbdstddeptcode == null ? null : jbdstddeptcode.trim();
    }

    public String getJbdstddeptname() {
        return jbdstddeptname;
    }

    public void setJbdstddeptname(String jbdstddeptname) {
        this.jbdstddeptname = jbdstddeptname == null ? null : jbdstddeptname.trim();
    }

    public BigDecimal getJbdzhzs() {
        return jbdzhzs;
    }

    public void setJbdzhzs(BigDecimal jbdzhzs) {
        this.jbdzhzs = jbdzhzs;
    }

    public Integer getJbdbanum() {
        return jbdbanum;
    }

    public void setJbdbanum(Integer jbdbanum) {
        this.jbdbanum = jbdbanum;
    }

    public Integer getJbdindrgnum() {
        return jbdindrgnum;
    }

    public void setJbdindrgnum(Integer jbdindrgnum) {
        this.jbdindrgnum = jbdindrgnum;
    }

    public Integer getJbddrgnum() {
        return jbddrgnum;
    }

    public void setJbddrgnum(Integer jbddrgnum) {
        this.jbddrgnum = jbddrgnum;
    }

    public BigDecimal getJbdrwt() {
        return jbdrwt;
    }

    public void setJbdrwt(BigDecimal jbdrwt) {
        this.jbdrwt = jbdrwt;
    }

    public BigDecimal getJbdcmi() {
        return jbdcmi;
    }

    public void setJbdcmi(BigDecimal jbdcmi) {
        this.jbdcmi = jbdcmi;
    }

    public BigDecimal getJbdtimesi() {
        return jbdtimesi;
    }

    public void setJbdtimesi(BigDecimal jbdtimesi) {
        this.jbdtimesi = jbdtimesi;
    }

    public BigDecimal getJbdpjzyr() {
        return jbdpjzyr;
    }

    public void setJbdpjzyr(BigDecimal jbdpjzyr) {
        this.jbdpjzyr = jbdpjzyr;
    }

    public BigDecimal getJbdcostsi() {
        return jbdcostsi;
    }

    public void setJbdcostsi(BigDecimal jbdcostsi) {
        this.jbdcostsi = jbdcostsi;
    }

    public BigDecimal getJbdcjfy() {
        return jbdcjfy;
    }

    public void setJbdcjfy(BigDecimal jbdcjfy) {
        this.jbdcjfy = jbdcjfy;
    }

    public Integer getJbdlownum() {
        return jbdlownum;
    }

    public void setJbdlownum(Integer jbdlownum) {
        this.jbdlownum = jbdlownum;
    }

    public String getJbdlowswl() {
        return jbdlowswl;
    }

    public void setJbdlowswl(String jbdlowswl) {
        this.jbdlowswl = jbdlowswl;
    }

    public Integer getJbdmednum() {
        return jbdmednum;
    }

    public void setJbdmednum(Integer jbdmednum) {
        this.jbdmednum = jbdmednum;
    }

    public String getJbdmedswl() {
        return jbdmedswl;
    }

    public void setJbdmedswl(String jbdmedswl) {
        this.jbdmedswl = jbdmedswl;
    }

    public BigDecimal getJbdsszb() {
        return jbdsszb;
    }

    public void setJbdsszb(BigDecimal jbdsszb) {
        this.jbdsszb = jbdsszb;
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", jbddate=").append(jbddate);
        sb.append(", jbdtype=").append(jbdtype);
        sb.append(", jbdstddeptcode=").append(jbdstddeptcode);
        sb.append(", jbdstddeptname=").append(jbdstddeptname);
        sb.append(", jbdzhzs=").append(jbdzhzs);
        sb.append(", jbdbanum=").append(jbdbanum);
        sb.append(", jbdindrgnum=").append(jbdindrgnum);
        sb.append(", jbddrgnum=").append(jbddrgnum);
        sb.append(", jbdrwt=").append(jbdrwt);
        sb.append(", jbdcmi=").append(jbdcmi);
        sb.append(", jbdtimesi=").append(jbdtimesi);
        sb.append(", jbdpjzyr=").append(jbdpjzyr);
        sb.append(", jbdcostsi=").append(jbdcostsi);
        sb.append(", jbdcjfy=").append(jbdcjfy);
        sb.append(", jbdlownum=").append(jbdlownum);
        sb.append(", jbdlowswl=").append(jbdlowswl);
        sb.append(", jbdmednum=").append(jbdmednum);
        sb.append(", jbdmedswl=").append(jbdmedswl);
        sb.append(", jbdsszb=").append(jbdsszb);
        sb.append(", moddate=").append(moddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}