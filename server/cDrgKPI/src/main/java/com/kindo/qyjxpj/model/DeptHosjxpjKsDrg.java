package com.kindo.qyjxpj.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

public class DeptHosjxpjKsDrg implements Serializable {
    private String id;

    private String jbddate;

    private String jbdtype;

    private String jbdstddeptcode;
    private String jbdstddeptname;

    private String qyCode;

    private String memberCode;

    @PoiExcelField(index=0,title="医院名称",halign="left")
    private String memberName;

    private String memberType;

    private String memberLev1;

    private String memberLev2;
    @PoiExcelField(index=1,title="综合绩效",halign="right")
    private Double jbdzhzs;
    @PoiExcelField(index=2,title="总病案数",halign="right")
    private Integer jbdbanum;
    @PoiExcelField(index=3,title="入组病案数",halign="right")
    private Integer jbdindrgnum;
    @PoiExcelField(index=4,title="DRG组数",halign="right")
    private Integer jbddrgnum;
    @PoiExcelField(index=5,title="总权重",halign="right")
    private Double jbdrwt;
    @PoiExcelField(index=6,title="CMI值",halign="right")
    private Double jbdcmi;
    @PoiExcelField(index=7,title="时间消耗指数",halign="right")
    private Double jbdtimesi;
    @PoiExcelField(index=8,title="平均住院日",halign="right")
    private Double jbdpjzyr;
    @PoiExcelField(index=9,title="费用消耗指数",halign="right")
    private Double jbdcostsi;
    @PoiExcelField(index=10,title="次均费用",halign="right")
    private Double jbdcjfy;

    private Integer jbdlownum;
    @PoiExcelField(index=11,title="低风险组死亡率%(人数)",halign="right")
    private String jbdlowswl;

    private Integer jbdmednum;
    @PoiExcelField(index=12,title="中低风险组死亡率%(人数)",halign="right")
    private String jbdmedswl;

    private Date moddate;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getJbddate() {
        return jbddate;
    }

    public void setJbddate(String jbddate) {
        this.jbddate = jbddate == null ? null : jbddate.trim();
    }

    public String getJbdtype() {
        return jbdtype;
    }

    public void setJbdtype(String jbdtype) {
        this.jbdtype = jbdtype == null ? null : jbdtype.trim();
    }

    public String getJbdstddeptcode() {
        return jbdstddeptcode;
    }

    public void setJbdstddeptcode(String jbdstddeptcode) {
        this.jbdstddeptcode = jbdstddeptcode == null ? null : jbdstddeptcode.trim();
    }

    public String getJbdstddeptname() {
        return jbdstddeptname;
    }

    public void setJbdstddeptname(String jbdstddeptname) {
        this.jbdstddeptname = jbdstddeptname == null ? null : jbdstddeptname.trim();
    }

    public String getQyCode() {
        return qyCode;
    }

    public void setQyCode(String qyCode) {
        this.qyCode = qyCode == null ? null : qyCode.trim();
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode == null ? null : memberCode.trim();
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType == null ? null : memberType.trim();
    }

    public String getMemberLev1() {
        return memberLev1;
    }

    public void setMemberLev1(String memberLev1) {
        this.memberLev1 = memberLev1 == null ? null : memberLev1.trim();
    }

    public String getMemberLev2() {
        return memberLev2;
    }

    public void setMemberLev2(String memberLev2) {
        this.memberLev2 = memberLev2 == null ? null : memberLev2.trim();
    }

    public Double getJbdzhzs() {
        return jbdzhzs;
    }

    public void setJbdzhzs(Double jbdzhzs) {
        this.jbdzhzs = jbdzhzs;
    }

    public Integer getJbdbanum() {
        return jbdbanum;
    }

    public void setJbdbanum(Integer jbdbanum) {
        this.jbdbanum = jbdbanum;
    }

    public Integer getJbdindrgnum() {
        return jbdindrgnum;
    }

    public void setJbdindrgnum(Integer jbdindrgnum) {
        this.jbdindrgnum = jbdindrgnum;
    }

    public Integer getJbddrgnum() {
        return jbddrgnum;
    }

    public void setJbddrgnum(Integer jbddrgnum) {
        this.jbddrgnum = jbddrgnum;
    }

    public Double getJbdrwt() {
        return jbdrwt;
    }

    public void setJbdrwt(Double jbdrwt) {
        this.jbdrwt = jbdrwt;
    }

    public Double getJbdcmi() {
        return jbdcmi;
    }

    public void setJbdcmi(Double jbdcmi) {
        this.jbdcmi = jbdcmi;
    }

    public Double getJbdtimesi() {
        return jbdtimesi;
    }

    public void setJbdtimesi(Double jbdtimesi) {
        this.jbdtimesi = jbdtimesi;
    }

    public Double getJbdpjzyr() {
        return jbdpjzyr;
    }

    public void setJbdpjzyr(Double jbdpjzyr) {
        this.jbdpjzyr = jbdpjzyr;
    }

    public Double getJbdcostsi() {
        return jbdcostsi;
    }

    public void setJbdcostsi(Double jbdcostsi) {
        this.jbdcostsi = jbdcostsi;
    }

    public Double getJbdcjfy() {
        return jbdcjfy;
    }

    public void setJbdcjfy(Double jbdcjfy) {
        this.jbdcjfy = jbdcjfy;
    }

    public Integer getJbdlownum() {
        return jbdlownum;
    }

    public void setJbdlownum(Integer jbdlownum) {
        this.jbdlownum = jbdlownum;
    }

    public String getJbdlowswl() {
        return jbdlowswl;
    }

    public void setJbdlowswl(String jbdlowswl) {
        this.jbdlowswl = jbdlowswl;
    }

    public Integer getJbdmednum() {
        return jbdmednum;
    }

    public void setJbdmednum(Integer jbdmednum) {
        this.jbdmednum = jbdmednum;
    }

    public String getJbdmedswl() {
        return jbdmedswl;
    }

    public void setJbdmedswl(String jbdmedswl) {
        this.jbdmedswl = jbdmedswl;
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", jbddate=").append(jbddate);
        sb.append(", jbdtype=").append(jbdtype);
        sb.append(", jbdstddeptcode=").append(jbdstddeptcode);
        sb.append(", jbdstddeptname=").append(jbdstddeptname);
        sb.append(", qyCode=").append(qyCode);
        sb.append(", memberCode=").append(memberCode);
        sb.append(", memberName=").append(memberName);
        sb.append(", memberType=").append(memberType);
        sb.append(", memberLev1=").append(memberLev1);
        sb.append(", memberLev2=").append(memberLev2);
        sb.append(", jbdzhzs=").append(jbdzhzs);
        sb.append(", jbdbanum=").append(jbdbanum);
        sb.append(", jbdindrgnum=").append(jbdindrgnum);
        sb.append(", jbddrgnum=").append(jbddrgnum);
        sb.append(", jbdrwt=").append(jbdrwt);
        sb.append(", jbdcmi=").append(jbdcmi);
        sb.append(", jbdtimesi=").append(jbdtimesi);
        sb.append(", jbdpjzyr=").append(jbdpjzyr);
        sb.append(", jbdcostsi=").append(jbdcostsi);
        sb.append(", jbdcjfy=").append(jbdcjfy);
        sb.append(", jbdlownum=").append(jbdlownum);
        sb.append(", jbdlowswl=").append(jbdlowswl);
        sb.append(", jbdmednum=").append(jbdmednum);
        sb.append(", jbdmedswl=").append(jbdmedswl);
        sb.append(", moddate=").append(moddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}