
/** 
* Project Name:cDrgKPI <br/> 
* File Name:DrgzbfxService.java <br/>
* Package Name:com.kindo.bzjxpj.service <br/>
* Date:2018年4月11日下午2:51:35 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.qyjxpj.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.qyjxpj.dao.DeptHosjxpjMapper;
import com.kindo.qyjxpj.dao.HosjxpjMapper;
import com.kindo.qyjxpj.model.DeptHosjxpj;
import com.kindo.qyjxpj.model.Hosjxpj;
import com.kindo.qyjxpj.model.QyjxpjQo;
import com.kindo.qyjxpj.model.DrgnumVo;
import com.kindo.qyjxpj.model.RzbasVo;
import com.kindo.uas.common.model.UserLoginInfo;

/**
 * ClassName: DrgzbfxService <br/>
 * Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/>
 * Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
 * date: 2018年4月11日 下午2:51:35 *<br/>
 * 
 * @author whk00196
 * @version
 * @since JDK 1.8
 **/
@Service
public class YyjxpjService {
	private static final Logger LOGGER = LoggerFactory.getLogger(YyjxpjService.class);
	@Autowired
	HosjxpjMapper mapr;
	@Autowired
	DeptHosjxpjMapper mapr_;
	
	
	 /** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(QyjxpjQo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		&&
		(vo.getYyCode() == null ||  "".equals(vo.getYyCode().trim())) )
		  ) {
			return;
		}
	
		LOGGER.info("YyjxpjService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("YyjxpjService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("YyjxpjService.getTheRegion,用户为管理员级别!不限区域!");
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("YyjxpjService.getTheRegion,用户为省级别!");
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("YyjxpjService.getTheRegion,用户为市级别!");
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("YyjxpjService.getTheRegion,用户为县级别!");
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("YyjxpjService.getTheRegion,用户为医院用户级别!");
			region = user.getOrgaId();
			vo.setYyCode(region);
		}
		LOGGER.info("YyjxpjService.getTheRegion,用户region:{}",region);
	};
	
	/**
	 * listPageZlzjxpj:综合指标详情 - 分页.. <br/>
	 * 
	 * @author whk00196
	 * @date 2018年4月11日 下午4:18:08 *
	 * @param paramMap
	 * @param pagination
	 * @return
	 * @since JDK 1.8
	 **/
	public Map<String, Object> listPageHosjxpj(Map<String, Object> paramMap, Pageable pagination) {
		List<Hosjxpj> list = mapr.listPageHosjxpj(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	}

	


	
	 /** 
	 * exportlistpage:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午3:45:40 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportlistpage(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "医院DRG绩效评价" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
	
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
		
		PageableSupplier<Hosjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.listPageExportHosjxpj(params, pagination);
		});
		flag = POIExcelUtils.createExcel(Hosjxpj.class, supplier, null, re.getOutputStream());
		return flag;
	};





	
	 /** 
	 * queryZhjx:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午3:48:08 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<Map<String, Object>> queryZhjx(Map<String, Object> paramMap) {
		return mapr.queryZhjx(paramMap);
	}





	
	 /** 
	 * queryCnzb:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:01:09 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<Map<String, Object>> queryCnzb(Map<String, Object> paramMap) {
		return mapr.queryCnzb(paramMap);
	}





	
	 /** 
	 * queryEfficiency:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:01:19 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<Map<String, Object>> queryEfficiency(Map<String, Object> paramMap) {
		return mapr.queryEfficiency(paramMap);
	}





	
	 /** 
	 * queryAq:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:01:29 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<Map<String, Object>> queryAq(Map<String, Object> paramMap) {
		return mapr.queryAq(paramMap);
	}





	
	 /** 
	 * queryCompare:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:01:41 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	public List<Map<String, Object>> queryCompare(Map<String, Object> paramMap) {
		return mapr.queryCompare(paramMap);
	}





	
	 /** 
	 * queryDrg:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:01:56 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryDrg(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" drgcode ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		List<Map<String, Object>> list = mapr.queryDrg(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};





	
	 /** 
	 * exportdrg:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:02:03 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @since JDK 1.8 
	 **/
	public boolean exportdrg(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "医院DRG绩效评价_DRG组数" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
		
        PageableSupplier<DrgnumVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryExportDrg(params, pagination);
		});
		flag = POIExcelUtils.createExcel(DrgnumVo.class, supplier, null, re.getOutputStream());
		return flag;
	};





	
	 /** 
	 * queryRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:02:11 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryRzbas(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" yyName ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" ,bah ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		pagination.setSorts(sorts);
    	}
		List<Map<String, Object>> list = mapr.queryRzbas(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};






	
	 /** 
	 * exportrzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:02:17 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportrzbas(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "医院DRG绩效评价_入组病案数" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
		
        PageableSupplier<RzbasVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryExportRzbas(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(RzbasVo.class, supplier, null, re.getOutputStream());
		return flag;
	};





	
	 /** 
	 * queryView:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:02:22 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryView(Map<String, Object> paramMap, Pageable pagination) {
		List<DeptHosjxpj> list = mapr_.queryView(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};





	
	 /** 
	 * exportview:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:02:46 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportview(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "医院DRG绩效评价_查看" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
		
        PageableSupplier<DeptHosjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr_.queryExportView(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(DeptHosjxpj.class, supplier, null, re.getOutputStream());
		return flag;
	};





	
	
	
//	public Map<String, Object> listPageDeptjxpjDetail(Map<String, Object> paramMap, Pageable pagination) {
//		List<Deptjxpj> list = mapr.listPageDeptjxpjDetail(paramMap, pagination);
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("rows", list);
//		map.put("total", list.size());
//		return map;
//	};
//

//
//	@SuppressWarnings("unchecked")
//	public boolean export(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
//		boolean flag = false;
//		String fileName = "科室绩效评价" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
//		re.setContentType("application/ms-excel");
//		re.setHeader("Content-disposition",
//				"attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
//		PageableSupplier<Deptjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
////		PoiExcelSimpleCreateResolver
//		supplier.setParam(paramMap);
//		supplier.setFunc((params, pagination) -> {
//			return this.queryExport(params, pagination);
//		});
//		flag = POIExcelUtils.createExcel(Deptjxpj.class, supplier, null, re.getOutputStream());
//		return flag;
//	};
//
//
//	
//	 /** 
//	 * queryDrg:分页Drg 详情. <br/>
//	 * @author whk00196 
//	 * @date 2018年5月22日 上午10:37:51 *
//	 * @param paramMap
//	 * @return 
//	 * @since JDK 1.8 
//	 **/
//	public Map<String, Object> queryDrg(Map<String, Object> paramMap,Pageable pagination) {
//		List<Map<String, Object>> list = mapr.queryDrg(paramMap, pagination);
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("rows", list);
//		map.put("total", pagination.getTotal());
//		return map;
//	};

};
