
/** 
* Project Name:cDrgKPI <br/> 
* File Name:DrgzbfxVo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年4月11日下午4:58:47 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.qyjxpj.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;


/** 
* ClassName: BzjxpjVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年4月11日 下午4:58:47 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class QyjxpjQo implements Serializable{
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -6305968850639280807L;
	String year;
	String quarter;
	String month;
	
	
	/**
	* yljglx:医疗机构类型. 
	**/
	String yljglx;
	
	
	/**
	* lev1:医疗机构等级1. 
	**/
	String lev1;
	
	
	/**
	* lev2:医疗机构等级2. 
	**/
	String lev2;
	
	
	/**
	* city:省市 Code. 
	**/
	String city;
	
	
	/**
	* cnty:县Code. 
	**/
	String cnty;
	
	
	/**
	* qyCode:区域编码. 
	**/
	String qyCode;
	
	/**
	* yyCode:医院. 
	**/
	String yyCode;
	
	/**
	* ksCode:科室编码. 
	**/
	String ksCode;
	
	String memberCode;
	
	String compItems;
	
	
	/** 
	 * 下拉详情 唯一 ID
	**/
	String id;
	
	
	UserLoginInfo user;
	
	
	
	
	/**
	* aqOrder:安全排序. 
	**/
	String aqOrder;
	
	public QyjxpjQo() {
	}


	
	
	
	
};
