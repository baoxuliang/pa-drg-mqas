package com.kindo.qyjxpj.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.qyjxpj.model.Hosjxpj;
import com.kindo.qyjxpj.model.DrgnumVo;
import com.kindo.qyjxpj.model.RzbasVo;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface HosjxpjMapper {
    int deleteByPrimaryKey(String id);

    int insert(Hosjxpj record);

    Hosjxpj selectByPrimaryKey(String id);

    List<Hosjxpj> selectAll();

    int updateByPrimaryKey(Hosjxpj record);
    
    /** 
     * listPageHosjxpj:综合指标详情 - 分页. <br/>
     * @author whk00196 
     * @date 2018年4月11日 下午4:17:22 *
     * @param paramMap
     * @param pagination
     * @return 
     * @since JDK 1.8 
     **/
    List<Hosjxpj>  listPageHosjxpj(@Param("param")Map<String,Object> paramMap,@Param("pagination")Pageable pagination);

	
	 /** 
	 * queryZhjx:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:03:59 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryZhjx(@Param("param")Map<String, Object> paramMap);

	
	 /** 
	 * queryCnzb:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:05:01 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryCnzb(@Param("param")Map<String, Object> paramMap);

	
	 /** 
	 * queryEfficiency:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:05:55 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryEfficiency(@Param("param")Map<String, Object> paramMap);

	
	 /** 
	 * queryAq:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:06:21 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryAq(@Param("param")Map<String, Object> paramMap);

	
	 /** 
	 * queryCompare:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:06:49 *
	 * @param paramMap
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryCompare(@Param("param")Map<String, Object> paramMap);

	
	 /** 
	 * queryDrg:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:09:50 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryDrg(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 上午10:10:19 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryRzbas(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	List<RzbasVo> queryExportRzbas(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	 /** 
	 * listPageExportHosjxpj:导出. <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 下午2:53:54 *
	 * @param params
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Hosjxpj> listPageExportHosjxpj(@Param("param")Map<String,Object> paramMap,@Param("pagination")Pageable pagination);

	
	 /** 
	 * listPageExportDrg:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月26日 下午4:09:53 *
	 * @param params
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<DrgnumVo> queryExportDrg(@Param("param")Map<String,Object> paramMap,@Param("pagination")Pageable pagination);
	
	

};