
/** 
* Project Name:cDrgKPI <br/> 
* File Name:DrgzbfxVo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年4月11日下午4:58:47 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.model;

import java.io.Serializable;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;


/** 
* ClassName: BzjxpjVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年4月11日 下午4:58:47 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class BzjxpjQo implements Serializable{
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -6305968850639280807L;
	String year;
	String quarter;
	String month;
	
	
	String city;
	String cnty;
	String province;
	String mdcCode;
	String drgCode;
	
	/**
	* zbType:[0:能效指数,1:综合绩效指数,2:DRG组数,3:病例数,4:总权重,5:服务能力指数,6:服务效率指数]. 
	**/
	String zbType;
	
	

	
	String memberCode;
	
	String compItems;
	
	
	/** 
	 * 下拉详情 唯一 ID
	**/
	String id;
	
	
	UserLoginInfo user;
	
	
	public BzjxpjQo() {
	}


	
	
};
