package com.kindo.bzjxpj.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

public class DrgHosjxpj implements Serializable {
    private String jbgdate;

    private String jbgtype;

    private String jbgdrgcode;

    private String memberCode;

    private String jbgmdccode;

    private String jbgmdcname;

    private String jbgadrgcode;

    private String jbgdrgname;

    private String qyCode;
	@PoiExcelField(index=0,title="医院名称",halign="left")
    private String memberName;
	@PoiExcelField(index=1,title="入组病案数",halign="right")
    private Integer jbgindrgnum;
	@PoiExcelField(index=2,title="总权重",halign="right")
    private Double jbgrwt;

    private Double jbgcmi;
    @PoiExcelField(index=3,title="时间消耗指数",halign="right")
    private Double jbgtimesi;
    @PoiExcelField(index=4,title="平均住院日",halign="right")
    private Double jbgpjzyr;
    @PoiExcelField(index=5,title="费用消耗指数",halign="right")
    private Double jbgcostsi;

    private Double jbgfxzs;
    @PoiExcelField(index=6,title="次均费用",halign="right")
    private Double jbgcjfy;

    private Double jbghyljfy;

    private Double jbgsxzs;

 

    private Double jbghypjzyr;

    private Integer jbglownum;

    private String jbglowswl;

    private Integer jbgmednum;

    private String jbgmedswl;
    @PoiExcelField(index=7,title="死亡人数",halign="right")
    private Integer jbgswnum;
    @PoiExcelField(index=8,title="死亡率(%)",halign="right")
    private String jbgswl;

    private Date moddate;

    private static final long serialVersionUID = 1L;

    public String getJbgdate() {
        return jbgdate;
    }

    public void setJbgdate(String jbgdate) {
        this.jbgdate = jbgdate == null ? null : jbgdate.trim();
    }

    public String getJbgtype() {
        return jbgtype;
    }

    public void setJbgtype(String jbgtype) {
        this.jbgtype = jbgtype == null ? null : jbgtype.trim();
    }

    public String getJbgdrgcode() {
        return jbgdrgcode;
    }

    public void setJbgdrgcode(String jbgdrgcode) {
        this.jbgdrgcode = jbgdrgcode == null ? null : jbgdrgcode.trim();
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode == null ? null : memberCode.trim();
    }

    public String getJbgmdccode() {
        return jbgmdccode;
    }

    public void setJbgmdccode(String jbgmdccode) {
        this.jbgmdccode = jbgmdccode == null ? null : jbgmdccode.trim();
    }

    public String getJbgmdcname() {
        return jbgmdcname;
    }

    public void setJbgmdcname(String jbgmdcname) {
        this.jbgmdcname = jbgmdcname == null ? null : jbgmdcname.trim();
    }

    public String getJbgadrgcode() {
        return jbgadrgcode;
    }

    public void setJbgadrgcode(String jbgadrgcode) {
        this.jbgadrgcode = jbgadrgcode == null ? null : jbgadrgcode.trim();
    }

    public String getJbgdrgname() {
        return jbgdrgname;
    }

    public void setJbgdrgname(String jbgdrgname) {
        this.jbgdrgname = jbgdrgname == null ? null : jbgdrgname.trim();
    }

    public String getQyCode() {
        return qyCode;
    }

    public void setQyCode(String qyCode) {
        this.qyCode = qyCode == null ? null : qyCode.trim();
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public Integer getJbgindrgnum() {
        return jbgindrgnum;
    }

    public void setJbgindrgnum(Integer jbgindrgnum) {
        this.jbgindrgnum = jbgindrgnum;
    }

    public Double getJbgrwt() {
        return jbgrwt;
    }

    public void setJbgrwt(Double jbgrwt) {
        this.jbgrwt = jbgrwt;
    }

    public Double getJbgcmi() {
        return jbgcmi;
    }

    public void setJbgcmi(Double jbgcmi) {
        this.jbgcmi = jbgcmi;
    }

    public Double getJbgcostsi() {
        return jbgcostsi;
    }

    public void setJbgcostsi(Double jbgcostsi) {
        this.jbgcostsi = jbgcostsi;
    }

    public Double getJbgfxzs() {
        return jbgfxzs;
    }

    public void setJbgfxzs(Double jbgfxzs) {
        this.jbgfxzs = jbgfxzs;
    }

    public Double getJbgcjfy() {
        return jbgcjfy;
    }

    public void setJbgcjfy(Double jbgcjfy) {
        this.jbgcjfy = jbgcjfy;
    }

    public Double getJbghyljfy() {
        return jbghyljfy;
    }

    public void setJbghyljfy(Double jbghyljfy) {
        this.jbghyljfy = jbghyljfy;
    }

    public Double getJbgtimesi() {
        return jbgtimesi;
    }

    public void setJbgtimesi(Double jbgtimesi) {
        this.jbgtimesi = jbgtimesi;
    }

    public Double getJbgsxzs() {
        return jbgsxzs;
    }

    public void setJbgsxzs(Double jbgsxzs) {
        this.jbgsxzs = jbgsxzs;
    }

    public Double getJbgpjzyr() {
        return jbgpjzyr;
    }

    public void setJbgpjzyr(Double jbgpjzyr) {
        this.jbgpjzyr = jbgpjzyr;
    }

    public Double getJbghypjzyr() {
        return jbghypjzyr;
    }

    public void setJbghypjzyr(Double jbghypjzyr) {
        this.jbghypjzyr = jbghypjzyr;
    }

    public Integer getJbglownum() {
        return jbglownum;
    }

    public void setJbglownum(Integer jbglownum) {
        this.jbglownum = jbglownum;
    }

    public String getJbglowswl() {
        return jbglowswl;
    }

    public void setJbglowswl(String jbglowswl) {
        this.jbglowswl = jbglowswl;
    }

    public Integer getJbgmednum() {
        return jbgmednum;
    }

    public void setJbgmednum(Integer jbgmednum) {
        this.jbgmednum = jbgmednum;
    }

    public String getJbgmedswl() {
        return jbgmedswl;
    }

    public void setJbgmedswl(String jbgmedswl) {
        this.jbgmedswl = jbgmedswl;
    }

    public Integer getJbgswnum() {
        return jbgswnum;
    }

    public void setJbgswnum(Integer jbgswnum) {
        this.jbgswnum = jbgswnum;
    }

    public String getJbgswl() {
        return jbgswl;
    }

    public void setJbgswl(String jbgswl) {
        this.jbgswl = jbgswl;
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", jbgdate=").append(jbgdate);
        sb.append(", jbgtype=").append(jbgtype);
        sb.append(", jbgdrgcode=").append(jbgdrgcode);
        sb.append(", memberCode=").append(memberCode);
        sb.append(", jbgmdccode=").append(jbgmdccode);
        sb.append(", jbgmdcname=").append(jbgmdcname);
        sb.append(", jbgadrgcode=").append(jbgadrgcode);
        sb.append(", jbgdrgname=").append(jbgdrgname);
        sb.append(", qyCode=").append(qyCode);
        sb.append(", memberName=").append(memberName);
        sb.append(", jbgindrgnum=").append(jbgindrgnum);
        sb.append(", jbgrwt=").append(jbgrwt);
        sb.append(", jbgcmi=").append(jbgcmi);
        sb.append(", jbgcostsi=").append(jbgcostsi);
        sb.append(", jbgfxzs=").append(jbgfxzs);
        sb.append(", jbgcjfy=").append(jbgcjfy);
        sb.append(", jbghyljfy=").append(jbghyljfy);
        sb.append(", jbgtimesi=").append(jbgtimesi);
        sb.append(", jbgsxzs=").append(jbgsxzs);
        sb.append(", jbgpjzyr=").append(jbgpjzyr);
        sb.append(", jbghypjzyr=").append(jbghypjzyr);
        sb.append(", jbglownum=").append(jbglownum);
        sb.append(", jbglowswl=").append(jbglowswl);
        sb.append(", jbgmednum=").append(jbgmednum);
        sb.append(", jbgmedswl=").append(jbgmedswl);
        sb.append(", jbgswnum=").append(jbgswnum);
        sb.append(", jbgswl=").append(jbgswl);
        sb.append(", moddate=").append(moddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}