
/** 
* Project Name:cDrgKPI <br/> 
* File Name:DrgjxpjService.java <br/>
* Package Name:com.kindo.bzjxpj.service <br/>
* Date:2018年6月21日下午4:25:22 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.bzjxpj.dao.DrgHosjxpjMapper;
import com.kindo.bzjxpj.dao.DrgjxpjMapper;
import com.kindo.bzjxpj.model.BzjxpjQo;
import com.kindo.bzjxpj.model.DrgHosjxpj;
import com.kindo.bzjxpj.model.Drgjxpj;
import com.kindo.bzjxpj.model.RzbasVo;
import com.kindo.qyjxpj.model.QyjxpjQo;
import com.kindo.qyjxpj.service.DeptjxpjService;
import com.kindo.uas.common.model.UserLoginInfo;

/** 
* ClassName: DrgjxpjService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月21日 下午4:25:22 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class DrgjxpjService {
	@Autowired
	DrgjxpjMapper mapr;
	@Autowired
	DrgHosjxpjMapper mapr_;
	private static final Logger LOGGER = LoggerFactory.getLogger(DrgjxpjService.class);
	
	 /** 
		 * getTheRegion:得到用户的地区/医院. <br/>
		 * @author whk00196 
		 * @date 2018年7月17日 下午9:00:45 *
		 * @param vo 
		 * @throws Exception 
		 * @since JDK 1.8 
		 **/
		public void  getTheRegion(BzjxpjQo vo) throws Exception {
			UserLoginInfo user = null;
			user =  vo.getUser();
			if(user == null) {
				throw new Exception("用户未登陆或者已失效");
			}
			if(
			!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
			&&
			(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
			 )
			  ) {
				return;
			}
		
			LOGGER.info("DrgjxpjService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
			LOGGER.info("DrgjxpjService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
			String orgaType = user.getOrgaType();
			String region = null;
			if("".equals(orgaType)||"ROOT".equals(orgaType)) {
				LOGGER.info("DrgjxpjService.getTheRegion,用户为管理员级别!不限区域!");
			}else if("SHWJW".equals(orgaType)) {
				LOGGER.info("DrgjxpjService.getTheRegion,用户为省级别!");
				region = user.getOrgaId();
				vo.setProvince(region);
			}else if("SWJW".equals(orgaType)) {
				LOGGER.info("DrgjxpjService.getTheRegion,用户为市级别!");
				region = user.getOrgaId();
				vo.setCity(region);
			}else if("XWJW".equals(orgaType)) {
				LOGGER.info("DrgjxpjService.getTheRegion,用户为县级别!");
				region = user.getOrgaId();
				vo.setCnty(region);
			}else if("HOS".equals(orgaType)) {
				LOGGER.info("DrgjxpjService.getTheRegion,用户为医院用户级别!");
				region = user.getOrgaId();
//				vo.setYyCode(region);
			}
			LOGGER.info("DrgjxpjService.getTheRegion,用户region:{}",region);
		};
	 /** 
	 * listPageDrgjxpj:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:33:17 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageDrgjxpj(Map<String, Object> paramMap, Pageable pagination) {
		List<Drgjxpj> list = mapr.listPageDrgjxpj(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	}

	
	 /** 
	 * exportlistpage:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:33:39 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportlistpage(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "DRG绩效评价" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<Drgjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.listPageExportDrgjxpj(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(Drgjxpj.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 /** 
	 * queryView:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:35:07 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryView(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" jbgindrgnum ");
    		sp1.setAsc(false);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		List<DrgHosjxpj> list = mapr_.queryView(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map; 
	}


	
	 /** 
	 * exportview:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:35:18 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportview(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "DRG绩效评价_查看" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<DrgHosjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr_.queryExportView(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(DrgHosjxpj.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 /** 
	 * queryRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月28日 下午5:56:46 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryRzbas(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" yyName ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" ,bah ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		pagination.setSorts(sorts);
    	}
		List<Map<String, Object>> list = mapr.queryRzbas(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};


	
	 /** 
	 * exportrzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月28日 下午5:56:51 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportrzbas(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "DRG绩效评价_入组病案数" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
        PageableSupplier<RzbasVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryExportRzbas(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(RzbasVo.class, supplier, null, re.getOutputStream());
		return flag;
	};
	
};
