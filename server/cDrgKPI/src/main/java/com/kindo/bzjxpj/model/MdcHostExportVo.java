
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MdcHostExportVo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年7月3日下午9:04:53 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: MdcHostExportVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月3日 下午9:04:53 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class MdcHostExportVo {
	@PoiExcelField(index=0,title="医院名称",halign="left")
	 String yyName;
	String yyCode;
	@PoiExcelField(index=1,title="综合指标",halign="right")
	Object zhzb;
	@PoiExcelField(index=2,title="专业总数",halign="right")
	Integer zyzs;
	@PoiExcelField(index=3,title="缺失专业数",halign="right")
	Integer qsZys;
	@PoiExcelField(index=4,title="神经系统疾病",halign="right")
	Object mdcA;
	@PoiExcelField(index=5,title="内分泌代谢及营养性疾病",halign="right")
	Object mdcB;
	@PoiExcelField(index=6,title="眼和附器疾病",halign="right")
	Object mdcC;
	@PoiExcelField(index=7,title="耳鼻咽喉口腔颌面疾病",halign="right")
	Object mdcD;
	@PoiExcelField(index=8,title="呼吸系统疾病",halign="right")
	Object mdcF;
	@PoiExcelField(index=9,title="循环系统疾病",halign="right")
	Object mdcG;
	@PoiExcelField(index=10,title="血液、造血器官、免疫系统疾病",halign="right")
	Object mdcH;
	@PoiExcelField(index=11,title="消化系统疾病",halign="right")
	Object mdcI;
	@PoiExcelField(index=12,title="泌尿系统疾病",halign="right")
	Object mdcJ;
	@PoiExcelField(index=13,title="男性生殖系统疾病",halign="right")
	Object mdcK;
	@PoiExcelField(index=14,title="女性生殖系统疾病",halign="right")
	Object mdcL;
	@PoiExcelField(index=15,title="妊娠、分娩和产褥期疾病",halign="right")
	Object mdcM;
	@PoiExcelField(index=16,title="新生儿疾病",halign="right")
	Object mdcN;
	@PoiExcelField(index=17,title="肌肉骨骼系统和结缔组织疾病",halign="right")
	Object mdcO;
	@PoiExcelField(index=18,title="皮肤、皮下组织、乳腺疾病",halign="right")
	Object mdcP;
	@PoiExcelField(index=19,title="烧伤疾病",halign="right")
	Object mdcQ;
	@PoiExcelField(index=20,title="精神和行为障碍",halign="right")
	Object mdcR;
	@PoiExcelField(index=21,title="感染与寄生虫疾病",halign="right")
	Object mdcS;
	@PoiExcelField(index=22,title="损伤、中毒和药物毒性反应",halign="right")
	Object mdcT;
	@PoiExcelField(index=23,title="肿瘤疾病",halign="right")
	Object mdcU;
	@PoiExcelField(index=24,title="其他疾病或操作",halign="right")
	Object mdcZ;
	
	public final	static List<Method> getM  = new  ArrayList<>(24);
	public final static List<Method> setM  = new  ArrayList<>(24);
	static {
			try {
			Class<MdcHostExportVo> classDP = MdcHostExportVo.class;
			Method getZhzb = classDP.getMethod("getZhzb");
			Method setZhzb = classDP.getMethod("setZhzb", Object.class);
			getM.add(getZhzb);
			setM.add(setZhzb);
			Field [] fieldDP0=
					classDP.getDeclaredFields();
					List<Field> list = Arrays.stream(fieldDP0)
						  .filter(x->x.getName().indexOf("mdc")==0)
						  .collect(Collectors.toList());
					list.stream()
						.forEach(x->{
							String methodName=x.getName();
							String methodName_=methodName.substring(0, 1).toUpperCase()+methodName.substring(1);
							Method get = null,set = null;
							try {
								 get=classDP.getMethod("get"+methodName_);
								 set = classDP.getMethod("set"+methodName_, x.getType());
								 getM.add(get);
								 setM.add(set);
							} catch (NoSuchMethodException e1) {
								e1.printStackTrace();
							}
						});
					
			}catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public static void main(String[] args) {
		MdcHostExportVo vo =new MdcHostExportVo();
		System.out.println(MdcHostExportVo.getM.size());
	}
};

