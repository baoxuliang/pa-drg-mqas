package com.kindo.bzjxpj.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.bzjxpj.model.DrgHosjxpj;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DrgHosjxpjMapper {
    int deleteByPrimaryKey(@Param("jbgdate") String jbgdate, @Param("jbgtype") String jbgtype, @Param("jbgdrgcode") String jbgdrgcode, @Param("memberCode") String memberCode);

    int insert(DrgHosjxpj record);

    DrgHosjxpj selectByPrimaryKey(@Param("jbgdate") String jbgdate, @Param("jbgtype") String jbgtype, @Param("jbgdrgcode") String jbgdrgcode, @Param("memberCode") String memberCode);

    List<DrgHosjxpj> selectAll();

    int updateByPrimaryKey(DrgHosjxpj record);

	
	 /** 
	 * queryView:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月28日 下午5:55:49 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<DrgHosjxpj> queryView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryExportView:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月4日 下午8:09:26 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<DrgHosjxpj> queryExportView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
}