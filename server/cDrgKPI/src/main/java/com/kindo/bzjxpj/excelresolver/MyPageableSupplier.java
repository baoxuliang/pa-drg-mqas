
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MyPageableSupplier.java <br/>
* Package Name:com.kindo.bzjxpj.excelresolver <br/>
* Date:2018年7月5日下午3:24:25 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.excelresolver;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import com.kindo.aria.base.Pageable;

import lombok.Data;
import lombok.Setter;

/** 
* ClassName: MyPageableSupplier <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月5日 下午3:24:25 *<br/>
适用于 一次性 获取的  !
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class MyPageableSupplier<T,U> implements Supplier<T> {

	 /**
     * 获取一个元素，当可用元素为空，且数据还有记录时，获取下一批记录
     *
     * @author whk00104/金豆-小蝴蝶
     */
    @Override
    public T get() {
        if (index >= size && !isOver) {
            initData();
        }
        return index < size ? list.get(index++) : null;
    }

    /**
     * 获取下一批记录，当记录数小于分页大小时判定已获取所有数据
     *
     * @author whk00104/金豆-小蝴蝶
     */
    public void initData() {
        list = func.apply(param, page);
        size = list.size();

//        if (size < LIMIT_SIZE) {
            isOver = true;
//        }
        index = 0;
        page.setPage(page.getPage() + 1);
    }

    // 查询参数
//    @Setter
    private U param;
    // 分页查询调用方法
//    @Setter
    private BiFunction<U, Pageable, List<T>> func;

    // 查询分页大小限制
    private static final int LIMIT_SIZE = 10000;
    // 当前获取元素位置
    private int index = 0;
    // 队列大小
    private int size = 0;
    // 是否已获取所有数据
    private boolean isOver = false;
    // 分页对象。设置不进行count
    private Pageable page = new Pageable(1, LIMIT_SIZE, false);
    // 当前获取的数据
    private List<T> list;

};
