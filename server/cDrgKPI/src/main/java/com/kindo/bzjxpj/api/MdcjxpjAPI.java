
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MdczbfxAPI.java <br/>
* Package Name:com.kindo.bzjxpj.api <br/>
* Date:2018年4月11日上午11:07:12 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.bzjxpj.model.BzjxpjQo;
import com.kindo.bzjxpj.service.MdcjxpjService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.qyjxpj.model.Hosjxpj;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

import lombok.Getter;

/**
 * ClassName: DrgzbfxAPI <br/>
 * Function:.<br/>
 * Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
 * date: 2018年4月11日 上午11:07:12 *<br/>
 * 
 * @author whk00196
 * @version
 * @since JDK 1.8
 **/
@RestController
@RequestMapping("/bzjxpj/mdc")
public class MdcjxpjAPI extends BaseApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(MdcjxpjAPI.class);
	@Autowired
	private MdcjxpjService service;
	
	 @Autowired
	 private RedisOper redisOper;
	 
	@Value("${hos.memberCode}")
	private String memberCode;
	
	@ModelAttribute
    public void initUser(BzjxpjQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		vo.setUser(info);
    };
//	@ModelAttribute
	public String getMemberCode(BzjxpjQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/bzjxpj/mdc/listpage] MdcjxpjAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageMdcjxpj(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/listpage] MdcjxpjAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/exportlistpage", method = { RequestMethod.GET })
	public void exportlistpage(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/mdc/exportlistpage] MdcjxpjAPI.exportlistpage 接受导出请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportlistpage(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/exportlistpage] MdcjxpjAPI.exportlistpage 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	
	
	/**
	 * queryYy:医院维度. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/yy", method = { RequestMethod.GET })
	public ApiResult queryYy(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/bzjxpj/mdc/yy] MdcjxpjAPI.queryYy 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getZbType())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "zbType参数为空");
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			result = this.service.listPageMdcHostVo(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/yy] MdcjxpjAPI.queryYy 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/exportyy", method = { RequestMethod.GET })
	public void exportyy(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/mdc/exportyy] MdcjxpjAPI.exportyy 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getZbType())||vo.getZbType().trim().length()==0) {
				LOGGER.error("[/bzjxpj/mdc/exportyy] MdcjxpjAPI.exportyy 接受导出请求,参数异常,信息为:{}","zbType参数为空" );
				return;
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportYy(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/exportyy] MdcjxpjAPI.exportyy 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	
	
	@RequestMapping(value = "/drg", method = { RequestMethod.GET })
	public ApiResult queryDrg(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/bzjxpj/mdc/drg] MdcjxpjAPI.queryDrg 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getMdcCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "mdcCode参数为空");
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryDrg(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/drg] MdcjxpjAPI.queryDrg 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportdrg", method = { RequestMethod.GET })
	public void exportdrg(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/mdc/exportdrg] MdcjxpjAPI.exportdrg 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getMdcCode())) {
				LOGGER.error("[/bzjxpj/mdc/exportdrg] MdcjxpjAPI.exportdrg 接受导出请求,参数异常,信息为:{}","mdcCode参数为空" );
				return;
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportdrg(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/exportdrg] MdcjxpjAPI.exportdrg 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/rzbas", method = { RequestMethod.GET })
	public ApiResult queryRzbas(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/bzjxpj/mdc/rzbas] MdcjxpjAPI.queryRzbas 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getMdcCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "mdcCode参数为空");
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryRzbas(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/rzbas] MdcjxpjAPI.queryRzbas 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportrzbas", method = { RequestMethod.GET })
	public void exportrzbas(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/mdc/exportrzbas] MdcjxpjAPI.exportrzbas 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getMdcCode())) {
				LOGGER.error("[/bzjxpj/mdc/exportrzbas] MdcjxpjAPI.exportrzbas 接受导出请求,参数异常,信息为:{}","mdcCode参数为空" );
				return;
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportrzbas(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/exportrzbas] MdcjxpjAPI.exportdrg 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	@RequestMapping(value = "/view", method = { RequestMethod.GET })
	public ApiResult queryView(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/bzjxpj/mdc/view] MdcjxpjAPI.queryView 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getMdcCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "mdcCode参数为空");
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryView(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/view] MdcjxpjAPI.queryView 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportview", method = { RequestMethod.GET })
	public void exportview(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/mdc/exportview] MdcjxpjAPI.exportview 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getMdcCode())) {
				LOGGER.error("[/bzjxpj/mdc/exportview] MdcjxpjAPI.exportview 接受导出请求,参数异常,信息为:{}","mdcCode参数为空" );
				return;
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportview(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/mdc/exportview] MdcjxpjAPI.exportview 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};


};
