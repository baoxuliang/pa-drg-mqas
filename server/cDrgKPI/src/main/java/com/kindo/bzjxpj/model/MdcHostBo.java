
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MdcHostBo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年7月2日下午8:59:30 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.model;

import lombok.Data;

/** 
* ClassName: MdcHostBo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月2日 下午8:59:30 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class MdcHostBo {
	String yyCode;
	String yyName;
	String mdcCode;
	String mdcName;
	
	double zhzs;
	int drgnum;
	int bls;
	double rwt;
	double fwnlzs;
	double fwxlzs;
	double nxzs;
	
	
	double zhZhzs;
	int zhDrgnum;
	int zhBls;
	double zhRwt;
	double zhFwnlzs;
	double zhFwxlzs;
	double zhNxzs;
	
	int qsZys;
	int zyzs;
};
