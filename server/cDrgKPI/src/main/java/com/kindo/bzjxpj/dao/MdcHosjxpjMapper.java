package com.kindo.bzjxpj.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.bzjxpj.model.MdcHosjxpj;
import com.kindo.bzjxpj.model.MdcHostBo;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface MdcHosjxpjMapper {
    int deleteByPrimaryKey(@Param("jbmdate") String jbmdate, @Param("jbmtype") String jbmtype, @Param("jbmmdccode") String jbmmdccode, @Param("memberCode") String memberCode);

    int insert(MdcHosjxpj record);

    MdcHosjxpj selectByPrimaryKey(@Param("jbmdate") String jbmdate, @Param("jbmtype") String jbmtype, @Param("jbmmdccode") String jbmmdccode, @Param("memberCode") String memberCode);

    List<MdcHosjxpj> selectAll();

    int updateByPrimaryKey(MdcHosjxpj record);
    
    List<MdcHostBo> listPageMdcHostBo(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryView:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午4:28:06 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<MdcHosjxpj> queryView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryExportView:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月4日 下午8:56:45 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<MdcHosjxpj> queryExportView(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
};
