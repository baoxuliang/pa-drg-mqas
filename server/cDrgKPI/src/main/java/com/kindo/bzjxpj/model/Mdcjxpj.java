package com.kindo.bzjxpj.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

public class Mdcjxpj implements Serializable {
    private String jbmdate;

    private String jbmtype;
    @PoiExcelField(index=0,title="MDC编码",halign="left")
    private String jbmmdccode;
    @PoiExcelField(index=1,title="MDC名称",halign="left")
    private String jbmmdcname;

    private String qyCode;
    @PoiExcelField(index=2,title="入组病案数",halign="right")
    private Integer jbmmdcnum;

    private Integer jbmadrgnum;
    @PoiExcelField(index=3,title="标准DRG组数",halign="right")
    Integer jbmbdrgcount;
    @PoiExcelField(index=4,title="实际DRG组数",halign="right")
    private Integer jbmdrgnum;
    @PoiExcelField(index=5,title="总权重",halign="right")
    private Double jbmrwt;

    private Double jbmnxzs;

    private Double jbmfwnlzs;

    private Double jbmfwxlzs;
    @PoiExcelField(index=6,title="CMI值",halign="right")
    private Double jbmcmi;
    
    @PoiExcelField(index=7,title="时间消耗指数",halign="right")
    private Double jbmtimesi;
    @PoiExcelField(index=8,title="平均住院日(天)",halign="right")
    private Double jbmpjzyr;
    @PoiExcelField(index=9,title="费用消耗指数",halign="right")
    private Double jbmcostsi;

    private Double jbmfxzs;
    @PoiExcelField(index=10,title="次均费用(元)",halign="right")
    private Double jbmcjfy;

    private Double jbmhyljfy;
   
    private Double jbmsxzs;


    private Double jbmhypjzyr;

    private Integer jbmlownum;

    private String jbmlowswl;

    private Integer jbmmednum;

    private String jbmmedswl;
    @PoiExcelField(index=11,title="死亡人数",halign="right")
    private Integer jbmswnum;
    @PoiExcelField(index=12,title="死亡率(%)",halign="right")
    private String jbmswl;

    private Date moddate;

    private static final long serialVersionUID = 1L;

    public String getJbmdate() {
        return jbmdate;
    }

    public void setJbmdate(String jbmdate) {
        this.jbmdate = jbmdate == null ? null : jbmdate.trim();
    }

    public String getJbmtype() {
        return jbmtype;
    }

    public void setJbmtype(String jbmtype) {
        this.jbmtype = jbmtype == null ? null : jbmtype.trim();
    }

    public String getJbmmdccode() {
        return jbmmdccode;
    }

    public void setJbmmdccode(String jbmmdccode) {
        this.jbmmdccode = jbmmdccode == null ? null : jbmmdccode.trim();
    }

    public String getJbmmdcname() {
        return jbmmdcname;
    }

    public void setJbmmdcname(String jbmmdcname) {
        this.jbmmdcname = jbmmdcname == null ? null : jbmmdcname.trim();
    }

    public String getQyCode() {
        return qyCode;
    }

    public void setQyCode(String qyCode) {
        this.qyCode = qyCode == null ? null : qyCode.trim();
    }

    public Integer getJbmmdcnum() {
        return jbmmdcnum;
    }

    public void setJbmmdcnum(Integer jbmmdcnum) {
        this.jbmmdcnum = jbmmdcnum;
    }

    public Integer getJbmadrgnum() {
        return jbmadrgnum;
    }

    public void setJbmadrgnum(Integer jbmadrgnum) {
        this.jbmadrgnum = jbmadrgnum;
    }

    public Integer getJbmdrgnum() {
        return jbmdrgnum;
    }

    public void setJbmdrgnum(Integer jbmdrgnum) {
        this.jbmdrgnum = jbmdrgnum;
    }

    public Double getJbmrwt() {
        return jbmrwt;
    }

    public void setJbmrwt(Double jbmrwt) {
        this.jbmrwt = jbmrwt;
    }

    public Double getJbmnxzs() {
        return jbmnxzs;
    }

    public void setJbmnxzs(Double jbmnxzs) {
        this.jbmnxzs = jbmnxzs;
    }

    public Double getJbmfwnlzs() {
        return jbmfwnlzs;
    }

    public void setJbmfwnlzs(Double jbmfwnlzs) {
        this.jbmfwnlzs = jbmfwnlzs;
    }

    public Double getJbmfwxlzs() {
        return jbmfwxlzs;
    }

    public void setJbmfwxlzs(Double jbmfwxlzs) {
        this.jbmfwxlzs = jbmfwxlzs;
    }

    public Double getJbmcmi() {
        return jbmcmi;
    }

    public void setJbmcmi(Double jbmcmi) {
        this.jbmcmi = jbmcmi;
    }

    public Double getJbmcostsi() {
        return jbmcostsi;
    }

    public void setJbmcostsi(Double jbmcostsi) {
        this.jbmcostsi = jbmcostsi;
    }

    public Double getJbmfxzs() {
        return jbmfxzs;
    }

    public void setJbmfxzs(Double jbmfxzs) {
        this.jbmfxzs = jbmfxzs;
    }

    public Double getJbmcjfy() {
        return jbmcjfy;
    }

    public void setJbmcjfy(Double jbmcjfy) {
        this.jbmcjfy = jbmcjfy;
    }

    public Double getJbmhyljfy() {
        return jbmhyljfy;
    }

    public void setJbmhyljfy(Double jbmhyljfy) {
        this.jbmhyljfy = jbmhyljfy;
    }

    public Double getJbmtimesi() {
        return jbmtimesi;
    }

    public void setJbmtimesi(Double jbmtimesi) {
        this.jbmtimesi = jbmtimesi;
    }

    public Double getJbmsxzs() {
        return jbmsxzs;
    }

    public void setJbmsxzs(Double jbmsxzs) {
        this.jbmsxzs = jbmsxzs;
    }

    public Double getJbmpjzyr() {
        return jbmpjzyr;
    }

    public void setJbmpjzyr(Double jbmpjzyr) {
        this.jbmpjzyr = jbmpjzyr;
    }

    public Double getJbmhypjzyr() {
        return jbmhypjzyr;
    }

    public void setJbmhypjzyr(Double jbmhypjzyr) {
        this.jbmhypjzyr = jbmhypjzyr;
    }

    public Integer getJbmlownum() {
        return jbmlownum;
    }

    public void setJbmlownum(Integer jbmlownum) {
        this.jbmlownum = jbmlownum;
    }

    public String getJbmlowswl() {
        return jbmlowswl;
    }

    public void setJbmlowswl(String jbmlowswl) {
        this.jbmlowswl = jbmlowswl;
    }

    public Integer getJbmmednum() {
        return jbmmednum;
    }

    public void setJbmmednum(Integer jbmmednum) {
        this.jbmmednum = jbmmednum;
    }

    public String getJbmmedswl() {
        return jbmmedswl;
    }

    public void setJbmmedswl(String jbmmedswl) {
        this.jbmmedswl = jbmmedswl;
    }

    public Integer getJbmswnum() {
        return jbmswnum;
    }

    public void setJbmswnum(Integer jbmswnum) {
        this.jbmswnum = jbmswnum;
    }

    public String getJbmswl() {
        return jbmswl;
    }

    public void setJbmswl(String jbmswl) {
        this.jbmswl = jbmswl;
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", jbmdate=").append(jbmdate);
        sb.append(", jbmtype=").append(jbmtype);
        sb.append(", jbmmdccode=").append(jbmmdccode);
        sb.append(", jbmmdcname=").append(jbmmdcname);
        sb.append(", qyCode=").append(qyCode);
        sb.append(", jbmmdcnum=").append(jbmmdcnum);
        sb.append(", jbmadrgnum=").append(jbmadrgnum);
        sb.append(", jbmdrgnum=").append(jbmdrgnum);
        sb.append(", jbmrwt=").append(jbmrwt);
        sb.append(", jbmnxzs=").append(jbmnxzs);
        sb.append(", jbmfwnlzs=").append(jbmfwnlzs);
        sb.append(", jbmfwxlzs=").append(jbmfwxlzs);
        sb.append(", jbmcmi=").append(jbmcmi);
        sb.append(", jbmcostsi=").append(jbmcostsi);
        sb.append(", jbmfxzs=").append(jbmfxzs);
        sb.append(", jbmcjfy=").append(jbmcjfy);
        sb.append(", jbmhyljfy=").append(jbmhyljfy);
        sb.append(", jbmtimesi=").append(jbmtimesi);
        sb.append(", jbmsxzs=").append(jbmsxzs);
        sb.append(", jbmpjzyr=").append(jbmpjzyr);
        sb.append(", jbmhypjzyr=").append(jbmhypjzyr);
        sb.append(", jbmlownum=").append(jbmlownum);
        sb.append(", jbmlowswl=").append(jbmlowswl);
        sb.append(", jbmmednum=").append(jbmmednum);
        sb.append(", jbmmedswl=").append(jbmmedswl);
        sb.append(", jbmswnum=").append(jbmswnum);
        sb.append(", jbmswl=").append(jbmswl);
        sb.append(", moddate=").append(moddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

	
	/** 
	* @return the jbmbdrgcount 
	*/
	public Integer getJbmbdrgcount() {
		return jbmbdrgcount;
	}

	
	/**
	* @param jbmbdrgcount the jbmbdrgcount to set 
	**/
	public void setJbmbdrgcount(Integer jbmbdrgcount) {
		this.jbmbdrgcount = jbmbdrgcount;
	}
}