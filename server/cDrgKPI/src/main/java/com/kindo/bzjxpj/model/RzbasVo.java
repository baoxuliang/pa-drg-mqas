
/** 
* Project Name:cDrgKPI <br/> 
* File Name:RzbasVo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年7月2日下午3:59:37 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: RzbasVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月2日 下午3:59:37 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class RzbasVo implements Serializable {

	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = 7836369897848257802L;
	@PoiExcelField(index=0,title="医院名称",halign="left")
	String yyName;
	String yyCode;
	@PoiExcelField(index=1,title="病案号",halign="left")
	String bah;
	@PoiExcelField(index=2,title="DRG编码",halign="left")
	String drgcode;
	@PoiExcelField(index=3,title="DRG名称",halign="left")
	String drgname;
	@PoiExcelField(index=4,title="相对权重",halign="right")
	Double rwt;
	@PoiExcelField(index=5,title="主要诊断",halign="left")
	String zyzd;
	@PoiExcelField(index=6,title="主要操作",halign="left")
	String zycz;
	@PoiExcelField(index=7,title="住院天数",halign="right")
	Integer zyts;
	@PoiExcelField(index=8,title="出院日期",halign="right")
	Date cyrq;
	@PoiExcelField(index=9,title="住院总费用",halign="right")
	Double zyzfy;
//	public static void main(String[] args) {
//		//System.out.println(Date.class.isAssignableFrom(java.sql.Date.class));
//	}
	
	
};
