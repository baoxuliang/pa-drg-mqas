
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MdcjxpjService.java <br/>
* Package Name:com.kindo.bzjxpj.service <br/>
* Date:2018年6月21日下午4:25:22 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.service;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.aria.excel.resolver.PageableSupplier;
import com.kindo.aria.excel.util.POIExcelUtils;
import com.kindo.bzjxpj.dao.MdcHosjxpjMapper;
import com.kindo.bzjxpj.dao.MdcjxpjMapper;
import com.kindo.bzjxpj.excelresolver.MyPageableSupplier;
import com.kindo.bzjxpj.model.BzjxpjQo;
import com.kindo.bzjxpj.model.DrgnumVo;
import com.kindo.bzjxpj.model.MdcHosjxpj;
import com.kindo.bzjxpj.model.MdcHostBo;
import com.kindo.bzjxpj.model.MdcHostExportVo;
import com.kindo.bzjxpj.model.MdcHostVo;
import com.kindo.bzjxpj.model.MdcHostVo.MdcHostVoMdcKeyValue;
import com.kindo.qyjxpj.service.DeptjxpjService;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.bzjxpj.model.Mdcjxpj;
import com.kindo.bzjxpj.model.RzbasVo;

/** 
* ClassName: MdcjxpjService <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月21日 下午4:25:22 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Service
public class MdcjxpjService {
	@Autowired
	MdcjxpjMapper mapr;
	@Autowired
	MdcHosjxpjMapper mapr_;
	private static final Logger LOGGER = LoggerFactory.getLogger(MdcjxpjService.class);
	 
	
	
	/** 
	 * getTheRegion:得到用户的地区/医院. <br/>
	 * @author whk00196 
	 * @date 2018年7月17日 下午9:00:45 *
	 * @param vo 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public void  getTheRegion(BzjxpjQo vo) throws Exception {
		UserLoginInfo user = null;
		user =  vo.getUser();
		if(user == null) {
			throw new Exception("用户未登陆或者已失效");
		}
		if(
		!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
		&&
		(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
		 )
		  ) {
			return;
		}
	
		LOGGER.info("MdcjxpjService.getTheRegion,发现用户类型为:{};用户机构编号为:{};",user.getOrgaType(),user.getOrgaId());
		LOGGER.info("MdcjxpjService.getTheRegion,发现用户所选择区域为不限,自动匹配其权限区域");
		String orgaType = user.getOrgaType();
		String region = null;
		if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			LOGGER.info("MdcjxpjService.getTheRegion,用户为管理员级别!不限区域!");
		}else if("SHWJW".equals(orgaType)) {
			LOGGER.info("MdcjxpjService.getTheRegion,用户为省级别!");
			region = user.getOrgaId();
			vo.setProvince(region);
		}else if("SWJW".equals(orgaType)) {
			LOGGER.info("MdcjxpjService.getTheRegion,用户为市级别!");
			region = user.getOrgaId();
			vo.setCity(region);
		}else if("XWJW".equals(orgaType)) {
			LOGGER.info("MdcjxpjService.getTheRegion,用户为县级别!");
			region = user.getOrgaId();
			vo.setCnty(region);
		}else if("HOS".equals(orgaType)) {
			LOGGER.info("MdcjxpjService.getTheRegion,用户为医院用户级别!");
			region = user.getOrgaId();
//			vo.setYyCode(region);
		}
		LOGGER.info("MdcjxpjService.getTheRegion,用户region:{}",region);
	};
	
	/** 
	 * listPageMdcjxpj:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:37:01 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageMdcjxpj(Map<String, Object> paramMap, Pageable pagination) {
		List<Mdcjxpj> list = mapr.listPageMdcjxpj(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	}

	
	 /** 
	 * exportlistpage:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:38:36 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportlistpage(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "MDC绩效评价" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());

		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<Mdcjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.listPageExportMdcjxpj(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(Mdcjxpj.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 /** 
	 * queryDrg:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:41:02 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryDrg(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" drgcode ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		List<Map<String, Object>> list = mapr.queryDrg(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};


	
	 /** 
	 * exportdrg:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:41:09 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportdrg(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "MDC绩效评价_DRG组数" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
        PageableSupplier<DrgnumVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryExportDrg(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(DrgnumVo.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 /** 
	 * queryRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:41:16 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryRzbas(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" yyName ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" ,bah ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		pagination.setSorts(sorts);
    	}
		List<Map<String, Object>> list = mapr.queryRzbas(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};


	
	 /** 
	 * exportrzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:41:26 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportrzbas(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "MDC绩效评价_入组病案数" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
        PageableSupplier<RzbasVo, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr.queryExportRzbas(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(RzbasVo.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 /** 
	 * queryView:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:41:38 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> queryView(Map<String, Object> paramMap, Pageable pagination) {
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" jbmmdcnum ");
    		sp1.setAsc(false);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		List<MdcHosjxpj> list = mapr_.queryView(paramMap, pagination);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", list);
		map.put("total", pagination.getTotal());
		return map;
	};


	
	 /** 
	 * exportview:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月21日 下午4:41:45 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportview(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		String fileName = "MDC绩效评价_查看" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
		PageableSupplier<MdcHosjxpj, Map<String, Object>> supplier = new PageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return this.mapr_.queryExportView(paramMap, pagination);
		});
		flag = POIExcelUtils.createExcel(MdcHosjxpj.class, supplier, null, re.getOutputStream());
		return flag;
	};


	
	 

	/** 
	 * listPageMdcHostVo:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午4:14:55 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	public Map<String, Object> listPageMdcHostVo(Map<String, Object> paramMap, Pageable pagination) {
	
		List<MdcHostBo> listMdcHostBo = mapr_.listPageMdcHostBo(paramMap, null);
		//按照yyCode分组
		 Map<String, List<MdcHostBo>> 
		 yyCodeMap=listMdcHostBo.parallelStream()
		  .collect(Collectors.groupingBy(x->x.getYyCode()));
		 List<MdcHostVo> viewTotal = new ArrayList<>();
		 //汇总数据
		 mdcHostVoCollecot(yyCodeMap, viewTotal);
			
		 	
		 Map<String, Object> map = new HashMap<String, Object>();
		 //颜色排序
		 mdcHosVoColorSort(paramMap, pagination, viewTotal);
		 //排序
		 mdcHosVoSort(paramMap, pagination, viewTotal);
		 //分页
		 List<MdcHostVo> view =	viewTotal.stream()
				 						 .skip(pagination.getSkip())
				 						 .limit(pagination.getLimit())
				 						 .collect(Collectors.toList());
//		 MdcHostVo vo = mockMdc();
//		 view.add(vo);
		 	map.put("rows", view);
			map.put("total", viewTotal.size()); 
		return map;
	}


	
	 
	 /** 
	 * mdcHosVoColorSort:总的按照颜色排序,获得 对应的tote和sequ. <br/>
	 * @author whk00196 
	 * @date 2018年7月9日 上午11:43:06 *
	 * @param paramMap
	 * @param pagination
	 * @param viewTotal 
	 * @since JDK 1.8 
	 **/
	private void mdcHosVoColorSort(Map<String, Object> paramMap, Pageable pagination, List<MdcHostVo> viewTotal) {
		//确定专业总数 序列号
		Integer count = 0;
		viewTotal.sort((x,y)->{
					 int ret =0;
					 ret=Integer.compare(x.getZyzs(),y.getZyzs());
					 return ret;
				 });
			for(MdcHostVo vo : viewTotal) {
				count++;
				vo.setZyzsSequ(count);
			}
			
		//确定缺失专业数
			count=0;
			viewTotal.sort((x,y)->{
				 int ret =0;
				 ret=Integer.compare(x.getQsZys(),y.getQsZys());
				 return ret;
			 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setQsZysSequ(count);
		}
		//综合绩效指数
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhZhzs(),y.getZhZhzs());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhZhzsSequ(count);
		}
		//DRG组数
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhDrgnum(),y.getZhDrgnum());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhDrgnumSequ(count);
		}
		//病例数
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhBls(),y.getZhBls());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhBlsSequ(count);
		}
		//总权重
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhRwt(),y.getZhRwt());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhRwtSequ(count);
		}
		//服务能力指数
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhFwnlzs(),y.getZhFwnlzs());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhFwnlzsSequ(count);
		}
		//服务效率指数
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhFwxlzs(),y.getZhFwxlzs());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhFwxlzsSequ(count);
		}
		//能效指数
		count = 0;
		viewTotal.sort((x,y)->{
			 int ret =0;
			 ret=Double.compare(x.getZhNxzs(),y.getZhNxzs());
			 return ret;
		 });
		for(MdcHostVo vo : viewTotal) {
			count++;
			vo.setZhNxzsSequ(count);
		}
		//对比 MDC
		List<Map<String,String>> mdcAll = this.mapr.queryMdc();
		mdcAll.forEach(x->{
//			System.out.println(x);
//			x.keySet().forEach(
//					 mdcCode -> {
//						 mdcCodeColorSort(x.get(mdcCode),viewTotal);
//					 });
			mdcCodeColorSort(x.get("MDCCODE"),viewTotal);
		}
				);
	};


	
	 /** 
	 * mdcCodeColorSort:各mdc的数值颜色排序.... <br/>
	 * @author whk00196 
	 * @date 2018年7月9日 下午3:27:00 *
	 * @param mdcCode
	 * @param viewTotal 
	 * @since JDK 1.8 
	 **/
	private void mdcCodeColorSort(String mdcCode, List<MdcHostVo> viewTotal) {
		 /**zhzs drgnum bls rwt fwnlzs fwxlzs nxzs*/ 
		int sequ = 0;
		List<MdcHostVo> tmpList = null;
		//综合绩效指数
		mdcCodeSubColorSort("zhzs",mdcCode, viewTotal);
	   //DRG组数
		mdcCodeSubColorSort("drgnum",mdcCode, viewTotal);
	   //病例数
		mdcCodeSubColorSort("bls",mdcCode, viewTotal);
	   //总权重
		mdcCodeSubColorSort("rwt",mdcCode, viewTotal);
	   //服务能力指数
		mdcCodeSubColorSort("fwnlzs",mdcCode, viewTotal);
	   //服务效率指数
		mdcCodeSubColorSort("fwxlzs",mdcCode, viewTotal);
	   //能效指数
		mdcCodeSubColorSort("nxzs",mdcCode, viewTotal);
	}


	
	 /** 
	 * mdcCodeSubColorSort:按照 所选择的 zhzs drgnum之类的进行颜色 . <br/>
	 * @author whk00196 
	 * @date 2018年7月9日 下午4:08:55 *
	 * @param mdcCode
	 * @param viewTotal 
	 * @since JDK 1.8 
	 **/
	private void mdcCodeSubColorSort(String modeKey,String mdcCode, List<MdcHostVo> viewTotal) {
		int sequ;
		List<MdcHostVo> tmpList;
		sequ = 0;
		tmpList = viewTotal.stream()
				 .filter(x->x.getZbTypeList().get(modeKey).get(mdcCode)!=null)
				 .collect(Collectors.toList());
		int tote=tmpList.size();
		tmpList=tmpList.stream()
			   .sorted((x,y)->{
				   int ret = 0;
				   MdcHostVoMdcKeyValue  xV= x.getZbTypeList().get(modeKey).get(mdcCode);
				   MdcHostVoMdcKeyValue  yV= y.getZbTypeList().get(modeKey).get(mdcCode);
				   if(xV.getValue() instanceof Double) {
					 ret = Double.compare((Double)xV.getValue(),(Double)yV.getValue());  
				   }else if(xV.getValue() instanceof Integer){
					   ret = Double.compare((Integer)xV.getValue(),(Integer)yV.getValue());  
					   
				   };
				   return ret;
			   }).collect(Collectors.toList());
		for(MdcHostVo vo : tmpList) {
			sequ++;
			MdcHostVoMdcKeyValue  sub=vo.getZbTypeList().get(modeKey).get(mdcCode);
			sub.setSequ(sequ);
			sub.setTote(tote);
		};
	};


	/** 
	 * mockMdc:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月6日 下午1:56:54 *
	 * @return 
	 * @since JDK 1.8 
	 **/
	private MdcHostVo mockMdc() {
		MdcHostVo vo = new MdcHostVo();
		 vo.setQsZys(11);
		 vo.setZyzs(13);
		 vo.setYyCode("A1");
		 vo.setYyName("BD");
		 Map<String,Object> ms = new HashMap<>();
		 ms.put("MDC A", 1);
		 ms.put("MDC B", 1);
		 ms.put("MDC C", 1);
		 ms.put("MDC D", 1);
		 ms.put("MDC F", 1);
		 ms.put("MDC G", 1);
		 ms.put("MDC H", 1);
		 ms.put("MDC I", 1);
		 ms.put("MDC J", 1);
		 ms.put("MDC K", 1);
		 ms.put("MDC L", 1);
		 ms.put("MDC M", 1);
		 ms.put("MDC N", 1);
		 ms.put("MDC O", 1);
		 ms.put("MDC P", 1);
		 ms.put("MDC Q", 1);
		 ms.put("MDC R", 1);
		 ms.put("MDC S", 1);
		 ms.put("MDC T", 1);
		 ms.put("MDC U", 1);
		 ms.put("MDC Z", 1);
		return vo;
	}

	 /** 
	 * mdcHostVoCollecot:汇总数据. <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午7:36:14 *
	 * @param yyCodeMap
	 * @param viewTotal 
	 * @since JDK 1.8 
	 **/
	private void mdcHostVoCollecot(Map<String, List<MdcHostBo>> yyCodeMap, List<MdcHostVo> viewTotal) {
		yyCodeMap.keySet()
				 		 .stream()
				 		 .peek(yyCode->{
				 			List<MdcHostBo> list=yyCodeMap.get(yyCode); 
				 			MdcHostVo detail = new MdcHostVo();
				 			list.sort(Comparator.comparing(MdcHostBo::getMdcCode));
				 			//将多个MdcHostBo 汇总为一个 MdcHostVo
				 			MdcHostBo first = list.get(0);
				 			BeanUtils.copyProperties(first, detail);
				 			list.forEach(mdchostbo->{
//				 				// 7条不同维度的 数据 N多个MDC
				 				Map<String,MdcHostVoMdcKeyValue> zhzs =
				 			    detail.getZbTypeList().get("zhzs")==null?
				 			    new ConcurrentHashMap<String,MdcHostVoMdcKeyValue>(24):detail.getZbTypeList().get("zhzs");
				 			  /**zhzs drgnum bls rwt fwnlzs fwxlzs nxzs*/  
				 				Map<String,MdcHostVoMdcKeyValue> drgnum =
				 				detail.getZbTypeList().get("drgnum")==null?
				 			   new ConcurrentHashMap<>(24):detail.getZbTypeList().get("drgnum");
				 				 			    
				 			  Map<String,MdcHostVoMdcKeyValue> bls =
				 				detail.getZbTypeList().get("bls")==null?
				 				new ConcurrentHashMap<>(24):
						 		  detail.getZbTypeList().get("bls");
						 	  
				 				Map<String,MdcHostVoMdcKeyValue> rwt =  
						 		detail.getZbTypeList().get("rwt")==null?
						 		 new ConcurrentHashMap<>(24):detail.getZbTypeList().get("rwt");
						 					 	  
						 		Map<String,MdcHostVoMdcKeyValue> fwnlzs=
						 		detail.getZbTypeList().get("fwnlzs")==null?
						 		new ConcurrentHashMap<>(24):detail.getZbTypeList().get("fwnlzs");
						 		
						 		Map<String,MdcHostVoMdcKeyValue> fwxlzs=
						 		detail.getZbTypeList().get("fwxlzs")==null?
								new ConcurrentHashMap<>(24):detail.getZbTypeList().get("fwxlzs");
								
								Map<String,MdcHostVoMdcKeyValue> nxzs=
								detail.getZbTypeList().get("nxzs")==null?
								new ConcurrentHashMap<>(24):detail.getZbTypeList().get("nxzs");
				 				//不同维度中的 N多个MDC对应的数值
				 				String mdcCode = mdchostbo.getMdcCode();
				 				String mdcName = mdchostbo.getMdcName();
				 				
				 				Object zhzsV   =  mdchostbo.getZhzs();
				 				Object drgnumV   =  mdchostbo.getDrgnum();
				 				Object blsV   =  mdchostbo.getBls();
				 				Object rwtV   =  mdchostbo.getRwt();
				 				Object fwnlzsV   =  mdchostbo.getFwnlzs();
				 				Object fwxlzsV   =  mdchostbo.getFwxlzs();
				 				Object nxzsV   =  mdchostbo.getNxzs();
				 			
					 			//zhzs
					 			zhzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,zhzsV));
					 			//drgnum
					 			drgnum.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,drgnumV));
					 			
					 			//bls
					 			bls.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,blsV));
					 			//rwt
					 			rwt.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,rwtV));
					 			//fwnlzs
					 			fwnlzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,fwnlzsV));
					 			//fwxlzs
					 			fwxlzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,fwxlzsV));
					 			//nxzs
					 			nxzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,nxzsV));
					 			
					 			detail.getZbTypeList().put("zhzs", zhzs);
					 			detail.getZbTypeList().put("drgnum", drgnum);
					 			detail.getZbTypeList().put("bls", bls);
					 			detail.getZbTypeList().put("rwt", rwt);
					 			detail.getZbTypeList().put("fwnlzs", fwnlzs);
					 			detail.getZbTypeList().put("fwxlzs", fwxlzs);
					 			detail.getZbTypeList().put("nxzs", nxzs);
				 			});
				 			viewTotal.add(detail);
				 		 }).noneMatch(yyCode->yyCode==null||yyCode.trim().equals(""));
	};


	
	 /** 
	 * mdcHosVoSort:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午6:18:33 *
	 * @param paramMap
	 * @param pagination
	 * @param viewTotal 
	 * @since JDK 1.8 
	 **/
	private void mdcHosVoSort(Map<String, Object> paramMap, Pageable pagination, List<MdcHostVo> viewTotal) {
		
		if(pagination.getSorts().size()>0) {
			 SortPair sortP = pagination.getSorts().get(0);
			boolean isAsc = sortP.isAsc();
			String  sortStr = sortP.getField();
			if("zyzs".equals(sortStr)) {
				Function<MdcHostVo,Integer> keyExtractor = null;
				keyExtractor=MdcHostVo::getZyzs;
				viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
					int ret = 1;
					ret=Integer.compare(x, y);
					return isAsc?ret:-ret;
				}));
			}else if("qsZys".equals(sortStr)) {
				Function<MdcHostVo,Integer> keyExtractor = null;
				keyExtractor=MdcHostVo::getQsZys;
				viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
					int ret = 1;
					ret=Integer.compare(x, y);
					return isAsc?ret:-ret;
				}));
			}else if("zhzb".equals(sortStr)) {
				/**
				 * [0:能效指数,1:综合绩效指数,2:DRG组数,3:病例数,4:总权重,5:服务能力指数,6:服务效率指数]
				 * */
				if("0".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Double> keyExtractor = MdcHostVo::getZhNxzs;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));//1:综合绩效指数
				}else if("1".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Double> keyExtractor = MdcHostVo::getZhZhzs;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));//2:DRG组数
				}else if("2".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Integer> keyExtractor = MdcHostVo::getZhDrgnum;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));//3:病例数
				}else if("3".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Integer> keyExtractor = MdcHostVo::getZhBls;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));//4:总权重
				}else if("4".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Double> keyExtractor = MdcHostVo::getZhRwt;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));//5:服务能力指数
				}else if("5".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Double> keyExtractor = MdcHostVo::getZhFwnlzs;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));//6:服务效率指数
				}else if("6".equals(paramMap.get("zbType"))) {
					Function<MdcHostVo,Double> keyExtractor = MdcHostVo::getZhFwxlzs;
					viewTotal.sort(Comparator.comparing(keyExtractor,(x,y)->{
						int ret = 1;
						ret=Double.compare(x, y);
						return isAsc?ret:-ret;
					}));
				}
				
			}else if(sortStr!=null&&sortStr.indexOf("MDC")==0) {
				
				String key_ = null;
				/**
				 * [0:能效指数,1:综合绩效指数,2:DRG组数,3:病例数,4:总权重,5:服务能力指数,6:服务效率指数]
				 * */
				if("0".equals(paramMap.get("zbType"))) {
					key_ = "nxzs";//0:能效指数
				}else if("1".equals(paramMap.get("zbType"))) {
					key_ = "zhzs";//1:综合绩效指数
				}else if("2".equals(paramMap.get("zbType"))) {
					key_ = "drgnum";//DRG组数
				}else if("3".equals(paramMap.get("zbType"))) {
					key_ = "bls";//3:病例数
				}else if("4".equals(paramMap.get("zbType"))) {
					key_ = "rwt";//4:总权重
				}else if("5".equals(paramMap.get("zbType"))) {
					key_ = "fwnlzs";//5:服务能力指数
				}else if("6".equals(paramMap.get("zbType"))) {
					key_ = "fwxlzs";//6:服务效率指数
				}
				if(key_ == null)return;
				String key=key_;
				viewTotal.sort((x,y)->{
					int ret =0;
					Map<String, MdcHostVoMdcKeyValue>  xList = x.getZbTypeList().get(key);
					Map<String, MdcHostVoMdcKeyValue>  yList = y.getZbTypeList().get(key);
					MdcHostVoMdcKeyValue xValue=xList.get(sortStr);
					MdcHostVoMdcKeyValue yValue=yList.get(sortStr);
					if(xValue == null) {
						ret = -1;
						return isAsc?ret:-ret;
					}else if(yValue == null) {
						ret =  1;
						return isAsc?ret:-ret;
					}
					Object xv = null, yv = null;
					xv = xValue.getValue();
					yv = yValue.getValue();
					if(xv instanceof Double) {
						ret=Double.compare((Double)xv, (Double)yv);
					}else if(xv instanceof Integer) {
						ret=Integer.compare((Integer)xv, (Integer)yv);
					}
					
					return isAsc?ret:-ret;
				});
			}
			
			
			
		 }
	};
	
	 /** 
	 * exportYy:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午4:29:34 *
	 * @param paramMap
	 * @param re
	 * @return 
	 * @throws Exception 
	 * @since JDK 1.8 
	 **/
	public boolean exportYy(Map<String, Object> paramMap, HttpServletResponse re) throws Exception {
		boolean flag = false;
		/**
		 * [0:能效指数,1:综合绩效指数,2:DRG组数,3:病例数,4:总权重,5:服务能力指数,6:服务效率指数]
		 * */
		String zbType=(String) paramMap.get("zbType");
		String suff=null;
			
		String key_ = null;
		if("0".equals(zbType)) {
			suff="能效指数";
			key_="nxzs";
		}else if("1".equals(zbType)) {
			suff="综合绩效指数";
			key_="zhzs";
		}else if("2".equals(zbType)) {
			suff="DRG组数";
			key_="drgnum";
		}else if("3".equals(zbType)) {
			suff="病例数";
			key_="bls";
		}else if("4".equals(zbType)) {
			suff="总权重";
			key_="rwt";
		}else if("5".equals(zbType)) {
			suff="服务能力指数";
			key_="fwnlzs";
		}else if("6".equals(zbType)) {
			suff="服务效率指数";
			key_="fwxlzs";
		}
		if(key_==null)return true;
		String key=key_;
		String fileName = "MDC绩效评价_医院维度评价_"+suff + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		
		re.setContentType("multipart/form-data;charset=UTF-8");
		re.setCharacterEncoding("UTF-8");
		re.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        String formatFileName = URLEncoder.encode(fileName, "UTF-8");
        re.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
        
        
        
		//下面的就是作为:supplier 
		//com.kindo.aria.excel.resolver.PoiExcelSimpleCreateResolver.supplier
		MyPageableSupplier<MdcHostExportVo, Map<String, Object>> supplier = new MyPageableSupplier<>();
		supplier.setParam(paramMap);
		supplier.setFunc((params, pagination) -> {
			return exportMdcHostExportVo(paramMap, pagination,key);
		});
		flag = POIExcelUtils.createExcel(MdcHostExportVo.class, supplier, null, re.getOutputStream());
		return flag;
	}


	
	 /** 
	 * exportMdcHostExportVo:导出!. <br/>
	 * @author whk00196 
	 * @date 2018年7月5日 下午3:35:27 *
	 * @param paramMap
	 * @param pagination
	 * @param key_ 
	 * @return 
	 * @since JDK 1.8 
	 **/
	private List<MdcHostExportVo> exportMdcHostExportVo(Map<String, Object> paramMap, Pageable pagination, String key)  {
		List<MdcHostBo> listMdcHostBo = mapr_.listPageMdcHostBo(paramMap, null);
		 Map<String, List<MdcHostBo>> 
		 yyCodeMap=listMdcHostBo.parallelStream()
		  .collect(Collectors.groupingBy(x->x.getYyCode()));
		 List<MdcHostVo> viewTotal = new ArrayList<>();
		 //汇总数据
		 mdcHostVoCollecot(yyCodeMap, viewTotal);
		 //排序
		 mdcHosVoSort(paramMap, pagination, viewTotal);
		 List<MdcHostExportVo> ret = new ArrayList<>(viewTotal.size());
		 //将MdcHostVo 变为 MdcHostExportVo
		 viewTotal.forEach(x->{
			 MdcHostExportVo vo = new MdcHostExportVo();
			 BeanUtils.copyProperties(x, vo);
			 Map<String, MdcHostVoMdcKeyValue> ms  = x.getZbTypeList().get(key);
			 //注入单个MdcHostVo某 组的 各MDC数值到 vo
			 ms.keySet().forEach(mdcCode->{
			 MdcHostVoMdcKeyValue model = ms.get(mdcCode);
			 Object value = model.getValue();
				String methName = "setMdc"+mdcCode.substring(mdcCode.length()-1);
				Method metd = null; 
				try {	
					metd=MdcHostExportVo.class.getMethod(methName,Object.class);
					metd.invoke(vo, value);
				} catch (Exception e) {
					e.printStackTrace();
				}
			 });
			 Method metd = null; 
			 String methName="getZh"+key.substring(0, 1).toUpperCase()+key.substring(1, key.length());
			 try {
				metd=MdcHostVo.class.getMethod(methName);
				vo.setZhzb(metd.invoke(x));
			} catch (Exception e) {
				e.printStackTrace();
			} 
			 ret.add(vo);
		 });
		 //改精度...
		 if(!"bls".equals(key)&&!"drgnum".equals(key)) {
			 try {
				precisionMdcHostExportVo(ret);
			} catch (Exception e) {
				LOGGER.info("MdcjxpjService.precisionMdcHostExportVo,修改精度异常,信息为:{}!",e.getMessage());
			}
		 }
		 return ret;
	}


	
	 /** 
	 * precisionMdcHostExportVo:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月18日 下午4:05:58 *
	 * @param ret 
	 * @throws Exception 
	 * @throws NoSuchMethodException 
	 * @since JDK 1.8 
	 **/
	private void precisionMdcHostExportVo(List<MdcHostExportVo> ret) throws  Exception {
		int len = MdcHostExportVo.getM.size();
		ret.forEach(x->{
			try {
				for(int i = 0;i<len;i++) {
					Method g = MdcHostExportVo.getM.get(i);
					Method s = MdcHostExportVo.setM.get(i);
					Object v = g.invoke(x);
					if( v!=null && v instanceof Double) {
						s.invoke(x, (double)Math.round((Double)v*100)/100);
					}
				}	
			} catch (Exception e) {
				LOGGER.info("MdcjxpjService.precisionMdcHostExportVo,修改精度异常,信息为:{}!",e.getMessage());
			}
		
		});
	};
	public static void main(String[] args) throws Exception, SecurityException {
		Class<MdcHostExportVo> classDP = MdcHostExportVo.class;
		List<Method> getList = new LinkedList<>();
		List<Method> setList = new LinkedList<>();
		Field [] fieldDP0=
		classDP.getDeclaredFields();
		Field [] fieldDP1= classDP.getFields();
		System.out.println(fieldDP0);
		System.out.println(fieldDP1);
//		Field zhzbField = classDP.getField("yyName");
		MdcHostExportVo vo = new MdcHostExportVo();
		vo.setYyName("ag");  
//		vo.get
//		zhzbField.get(vo);
		List<Field> list = Arrays.stream(fieldDP0)
			  .parallel()
			  .filter(x->x.getName().indexOf("mdc")==0)
			  .collect(Collectors.toList());
	System.out.println(list);
		list.stream()
			.forEach(x->{
				String methodName=x.getName();
				String methodName_=methodName.substring(0, 1).toUpperCase()+methodName.substring(1);
				Method get = null,set = null;
				try {
					 get=classDP.getMethod("get"+methodName_);
					 set = classDP.getMethod("set"+methodName_, x.getType());
					 getList.add(get);
					 setList.add(set);
				} catch (NoSuchMethodException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});
	System.out.println(getList);
	}
};
   
 

////将MdcHostVo 变为 MdcHostExportVo
//viewTotal.forEach(x->{
//	 MdcHostExportVo vo = new MdcHostExportVo();
//	 BeanUtils.copyProperties(x, vo);
//	 Map<String, MdcHostVoMdcKeyValue> ms = null;
//	 ms = x.getZbTypeList().get(key);
//	 //注入单个MdcHostVo某 组的 各MDC数值到 vo
//	 ms.keySet().forEach(mdcCode->{
//		Object value = ms.get(mdcCode);
//		String methName = "setMdc"+mdcCode.substring(mdcCode.length()-1);
//		Method metd = null; 
//		try {	
//			metd=MdcHostExportVo.class.getMethod(methName,Object.class);
//			metd.invoke(vo, value);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	 });
//	 Method metd = null; 
//	 String methName="getZh"+key.substring(0, 1).toUpperCase()+key.substring(1, key.length());
//	 try {
//		metd=MdcHostVo.class.getMethod(methName);
//		vo.setZhzb(metd.invoke(x));
//	} catch (Exception e) {
//		e.printStackTrace();
//	} 
//	 ret.add(vo);
//});
//return ret;
//	public static void main(String[] args) {
//		Method metd = null; 
//		String methName="setMdcA";
//		try {	
//			metd=MdcHostExportVo.class.getMethod(methName,Object.class);
//			metd=MdcHostExportVo.class.getMethod(methName);
////			metd.invoke(obj, args)
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

///** 
//* mdcHostVoCollecot:汇总数据. <br/>
//* @author whk00196 
//* @date 2018年7月3日 下午7:36:14 *
//* @param yyCodeMap
//* @param viewTotal 
//* @since JDK 1.8 
//**/
//private void mdcHostVoCollecot(Map<String, List<MdcHostBo>> yyCodeMap, List<MdcHostVo> viewTotal) {
//	yyCodeMap.keySet()
//			 		 .stream()
//			 		 .peek(yyCode->{
//			 			List<MdcHostBo> list=yyCodeMap.get(yyCode); 
//			 			MdcHostVo detail = new MdcHostVo();
//			 			list.sort(Comparator.comparing(MdcHostBo::getMdcCode));
//			 			//将多个MdcHostBo 汇总为一个 MdcHostVo
//			 			MdcHostBo first = list.get(0);
//			 			BeanUtils.copyProperties(first, detail);
//			 			list.forEach(mdchostbo->{
//			 				// 7条不同维度的 数据 N多个MDC
//			 				Map<String,MdcHostVoMdcKeyValue> zhzs =
//			 			    detail.getZbTypeList().get("zhzs")==null?
//			 			    new ConcurrentHashMap<String,MdcHostVoMdcKeyValue>(24):detail.getZbTypeList().get("zhzs");
//			 			    
//			 				Map<String,MdcHostVoMdcKeyValue> drgnum =
//			 				detail.getZbTypeList().get("drgnum")==null?
//			 			   new ConcurrentHashMap<>(24):detail.getZbTypeList().get("drgnum");
//			 				 			    
//			 			  Map<String,MdcHostVoMdcKeyValue> bls =
//			 				detail.getZbTypeList().get("bls")==null?
//			 				new ConcurrentHashMap<>(24):
//					 		  detail.getZbTypeList().get("bls");
//					 	  
//			 				Map<String,MdcHostVoMdcKeyValue> rwt =  
//					 		detail.getZbTypeList().get("rwt")==null?
//					 		 new ConcurrentHashMap<>(24):detail.getZbTypeList().get("rwt");
//					 					 	  
//					 		Map<String,MdcHostVoMdcKeyValue> fwnlzs=
//					 		detail.getZbTypeList().get("fwnlzs")==null?
//					 		new ConcurrentHashMap<>(24):detail.getZbTypeList().get("fwnlzs");
//					 		
//					 		Map<String,MdcHostVoMdcKeyValue> fwxlzs=
//					 		detail.getZbTypeList().get("fwxlzs")==null?
//							new ConcurrentHashMap<>(24):detail.getZbTypeList().get("fwnlzs");
//							
//							Map<String,MdcHostVoMdcKeyValue> nxzs=
//							detail.getZbTypeList().get("nxzs")==null?
//							new ConcurrentHashMap<>(24):detail.getZbTypeList().get("nxzs");
//			 				//不同维度中的 N多个MDC对应的数值
//			 				String mdcCode = mdchostbo.getMdcCode();
//			 				String mdcName = mdchostbo.getMdcName();
//			 				
//			 				Object zhzsV   =  mdchostbo.getZhzs();
//			 				Object drgnumV   =  mdchostbo.getDrgnum();
//			 				Object blsV   =  mdchostbo.getBls();
//			 				Object rwtV   =  mdchostbo.getRwt();
//			 				Object fwnlzsV   =  mdchostbo.getFwnlzs();
//			 				Object fwxlzsV   =  mdchostbo.getFwxlzs();
//			 				Object nxzsV   =  mdchostbo.getNxzs();
//			 			
//				 			//zhzs
//				 			zhzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,zhzsV));
//				 			//drgnum
//				 			drgnum.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,drgnumV));
//				 			
//				 			//bls
//				 			bls.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,blsV));
//				 			//rwt
//				 			rwt.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,rwtV));
//				 			//fwnlzs
//				 			fwnlzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,fwnlzsV));
//				 			//fwxlzs
//				 			fwxlzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,fwxlzsV));
//				 			//nxzs
//				 			nxzs.put(mdcCode,detail.new MdcHostVoMdcKeyValue(mdcCode,mdcName,nxzsV));
//				 			
//				 			detail.getZbTypeList().put("zhzs", zhzs);
//				 			detail.getZbTypeList().put("drgnum", drgnum);
//				 			detail.getZbTypeList().put("bls", bls);
//				 			detail.getZbTypeList().put("rwt", rwt);
//				 			detail.getZbTypeList().put("fwnlzs", fwnlzs);
//				 			detail.getZbTypeList().put("fwxlzs", fwxlzs);
//				 			detail.getZbTypeList().put("nxzs", nxzs);
//			 			});
//			 			viewTotal.add(detail);
//			 		 }).noneMatch(yyCode->yyCode==null||yyCode.trim().equals(""));
//}

//// 7条不同维度的 数据 N多个MDC
//	Map<String,Object> zhzs =
// detail.getZbTypeList().get("zhzs")==null?
// new ConcurrentHashMap<String,Object>(24):detail.getZbTypeList().get("zhzs");
// 
//	Map<String,Object> drgnum =
//	detail.getZbTypeList().get("drgnum")==null?
//new ConcurrentHashMap<>(24):detail.getZbTypeList().get("drgnum");
//	 			    
//Map<String,Object> bls =
//	detail.getZbTypeList().get("bls")==null?
//	new ConcurrentHashMap<>(24):
//	  detail.getZbTypeList().get("bls");
//
//	Map<String,Object> rwt =  
//	detail.getZbTypeList().get("rwt")==null?
//	 new ConcurrentHashMap<>(24):detail.getZbTypeList().get("rwt");
//				 	  
//	Map<String,Object> fwnlzs=
//	detail.getZbTypeList().get("fwnlzs")==null?
//	new ConcurrentHashMap<>(24):detail.getZbTypeList().get("fwnlzs");
//	
//	Map<String,Object> fwxlzs=
//	detail.getZbTypeList().get("fwxlzs")==null?
//new ConcurrentHashMap<>(24):detail.getZbTypeList().get("fwnlzs");
//
//Map<String,Object> nxzs=
//detail.getZbTypeList().get("nxzs")==null?
//new ConcurrentHashMap<>(24):detail.getZbTypeList().get("nxzs");
//	//不同维度中的 N多个MDC对应的数值
//	String mdcCode = mdchostbo.getMdcCode();
//	String mdcName = mdchostbo.getMdcName();
//	
//	Object zhzsV   =  mdchostbo.getZhzs();
//	Object drgnumV   =  mdchostbo.getDrgnum();
//	Object blsV   =  mdchostbo.getBls();
//	Object rwtV   =  mdchostbo.getRwt();
//	Object fwnlzsV   =  mdchostbo.getFwnlzs();
//	Object fwxlzsV   =  mdchostbo.getFwxlzs();
//	Object nxzsV   =  mdchostbo.getNxzs();
//
//	//zhzs
//	zhzs.put(mdcCode,zhzsV);
//	//drgnum
//	drgnum.put(mdcCode,drgnumV);
//	
//	//bls
//	bls.put(mdcCode,blsV);
//	//rwt
//	rwt.put(mdcCode,rwtV);
//	//fwnlzs
//	fwnlzs.put(mdcCode,fwnlzsV);
//	//fwxlzs
//	fwxlzs.put(mdcCode,fwxlzsV);
//	//nxzs
//	nxzs.put(mdcCode,nxzsV);
//	
//	detail.getZbTypeList().put("zhzs", zhzs);
//	detail.getZbTypeList().put("drgnum", drgnum);
//	detail.getZbTypeList().put("bls", bls);
//	detail.getZbTypeList().put("rwt", rwt);
//	detail.getZbTypeList().put("fwnlzs", fwnlzs);
//	detail.getZbTypeList().put("fwxlzs", fwxlzs);
//	detail.getZbTypeList().put("nxzs", nxzs);
//	
//"0".equals(zbType)?"能效指数":
//	"1".equals(zbType)?"综合绩效指数":
//	"2".equals(zbType)?"DRG组数":
//	"3".equals(zbType)?"病例数":
//	"4".equals(zbType)?"总权重":
//	"5".equals(zbType)?"服务能力指数":
//	"6".equals(zbType)?"服务效率指数":"";