package com.kindo.bzjxpj.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.bzjxpj.model.Drgjxpj;
import com.kindo.bzjxpj.model.RzbasVo;
import com.kindo.qyjxpj.model.DeptjxpjDefined;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DrgjxpjMapper {
    int deleteByPrimaryKey(@Param("jbgdate") String jbgdate, @Param("jbgtype") String jbgtype, @Param("jbgdrgcode") String jbgdrgcode);

    int insert(Drgjxpj record);

    Drgjxpj selectByPrimaryKey(@Param("jbgdate") String jbgdate, @Param("jbgtype") String jbgtype, @Param("jbgdrgcode") String jbgdrgcode);

    List<Drgjxpj> selectAll();

    int updateByPrimaryKey(Drgjxpj record);

	
	 /** 
	 * listPageDrgjxpj:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月28日 下午5:13:14 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Drgjxpj> listPageDrgjxpj(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	List<Drgjxpj> listPageExportDrgjxpj(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	 /** 
	 * queryRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月28日 下午6:00:36 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryRzbas(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryExportRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月4日 下午7:10:39 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<RzbasVo> queryExportRzbas(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
}