
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MdczbfxAPI.java <br/>
* Package Name:com.kindo.bzjxpj.api <br/>
* Date:2018年4月11日上午11:07:12 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.bzjxpj.model.BzjxpjQo;
import com.kindo.bzjxpj.service.DrgjxpjService;
import com.kindo.common.api.BaseApi;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.qyjxpj.model.QyjxpjQo;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.intf.BaseApiInterface;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;
import com.kindo.utils.Tools;

/**
 * ClassName: DrgzbfxAPI <br/>
 * Function:.<br/>
 * Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
 * date: 2018年4月11日 上午11:07:12 *<br/>
 * 
 * @author whk00196
 * @version
 * @since JDK 1.8
 **/
@RestController
@RequestMapping("/bzjxpj/drg")
public class DrgjxpjAPI  extends BaseApi   {

	private static final Logger LOGGER = LoggerFactory.getLogger(DrgjxpjAPI.class);
	@Autowired
	private DrgjxpjService service;
	 @Autowired
	 private RedisOper redisOper;
	
//	@Value("${hos.memberCode}")
	private String memberCode;
	
	@ModelAttribute
    public void initUser(BzjxpjQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		vo.setUser(info);
		
    };
    
//	@ModelAttribute
	public String getMemberCode(BzjxpjQo vo) {
		vo.setMemberCode(memberCode);
		return memberCode;
	};

	/**
	 * querylistpage:综合指标详情. <br/>
	 * @author whk00196
	 * @date 2018年4月11日 下午9:21:13 *
	 * @param vo
	 * @param pagination
	 * @param request
	 * @return
	 * @since JDK 1.8
	 **/
	@RequestMapping(value = "/listpage", method = { RequestMethod.GET })
	public ApiResult querylistpage(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> result = null;
		try {
			LOGGER.info("[/bzjxpj/drg/listpage] DrgjxpjAPI.querylistpage 接受查询请求,请求参数为:{}", vo);
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			if(pagination.getSorts().size()==0) {
				SortPair sortP = new SortPair();
				sortP.setField("JBGDRGCODE");
				pagination.getSorts().add(sortP);
			}
			result = this.service.listPageDrgjxpj(paramMap, pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/drg/listpage] DrgjxpjAPI.querylistpage 接受查询请求,发生异常,信息为:{}",
					e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}
	};
	
	
	@RequestMapping(value = "/exportlistpage", method = { RequestMethod.GET })
	public void exportlistpage(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
		  	LOGGER.info("[/bzjxpj/drg/exportlistpage] DrgjxpjAPI.exportlistpage 接受导出请求,请求参数为:{}", vo);
			
		  	service.getTheRegion(vo);
		  	
		  	paramMap = Tools.convertBean2Map(vo);
			flag = service.exportlistpage(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/drg/exportlistpage] DrgjxpjAPI.exportlistpage 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};

	@RequestMapping(value = "/view", method = { RequestMethod.GET })
	public ApiResult queryView(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/bzjxpj/drg/view] DrgjxpjAPI.queryView 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getDrgCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "drgCode参数为空");
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryView(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/drg/view] DrgjxpjAPI.queryView 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportview", method = { RequestMethod.GET })
	public void exportview(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/drg/exportview] DrgjxpjAPI.exportview 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getDrgCode())) {
				LOGGER.error("[/bzjxpj/drg/exportview] DrgjxpjAPI.exportview 接受导出请求,参数异常,信息为:{}","drgCode参数为空" );
				return;
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportview(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/drg/exportview] DrgjxpjAPI.exportview 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/rzbas", method = { RequestMethod.GET })
	public ApiResult queryRzbas(BzjxpjQo vo, Pageable pagination, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		Map<String, Object> ms = new HashMap<>();
		try {
			LOGGER.info("[/bzjxpj/drg/rzbas] DrgjxpjAPI.queryRzbas 接受查询请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getDrgCode())) {
				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "drgCode参数为空");
			}
			
			service.getTheRegion(vo);
			
			paramMap = Tools.convertBean2Map(vo);
			ms = this.service.queryRzbas(paramMap,pagination);
			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/drg/rzbas] DrgjxpjAPI.queryRzbas 接受查询请求,发生异常,信息为:{}", e.getMessage());
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		} finally {

		}

	};
	
	
	@RequestMapping(value = "/exportrzbas", method = { RequestMethod.GET })
	public void exportrzbas(BzjxpjQo vo, HttpServletResponse re, HttpServletRequest request) {
		Map<String, Object> paramMap = null;
		boolean flag = false;
		try {
			LOGGER.info("[/bzjxpj/drg/exportrzbas] DrgjxpjAPI.exportdrg 接受导出请求,请求参数为:{}", vo);
			if (StringUtils.isEmpty(vo.getDrgCode())) {
				LOGGER.error("[/bzjxpj/drg/exportrzbas] DrgjxpjAPI.exportrzbas 接受导出请求,参数异常,信息为:{}","drgCode参数为空" );
				return;
			}
			
			service.getTheRegion(vo);
			
			
			paramMap = Tools.convertBean2Map(vo);
			flag = service.exportrzbas(paramMap, re);
			if (!flag) {
				throw new Exception("excel 导出模板或者模型错误");
			}
			// return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", null);
		} catch (Exception e) {
			LOGGER.error("[/bzjxpj/drg/exportrzbas] DrgjxpjAPI.exportdrg 接受导出请求,发生异常,信息为:{}", e.getMessage());
			// return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION,
			// e.getMessage());
		} finally {

		}

	}
	
	
//	@RequestMapping(value = "/querydetail", method = { RequestMethod.GET })
//	public ApiResult queryDetail(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
//		Map<String, Object> paramMap = null;
//		Map<String, Object> result = null;
//		try {
//			LOGGER.info("[/bzjxpj/drg/querydetail] DrgjxpjAPI.queryDetail 接受查询请求,请求参数为:{}", vo);
//			if(StringUtils.isEmpty(vo.getId())) {
//				return new ApiResult(Constants.RESULT.PARAM_LEAK, Constants.RESULT_MSG.PARAM_LEAK, "id参数为空");
//			}
//			paramMap = Tools.convertBean2Map(vo);
//			pagination=null;
//			result = this.service.listPageDeptjxpjDetail(paramMap, pagination);
//			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", result);
//		} catch (Exception e) {
//			LOGGER.error("[/bzjxpj/drg/querydetail] DrgjxpjAPI.queryDetail 接受查询请求,发生异常,信息为:{}",
//					e.getMessage());
//			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
//		} finally {
//
//		}
//	};
	

//
//	@RequestMapping(value = "/medlowdeathrate", method = { RequestMethod.GET })
//	public ApiResult queryMedLowDeathRate(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
//		Map<String, Object> paramMap = null;
//		List<Map<String, Object>> list = null;
//		Map<String, Object> ms = new HashMap<>();
//		try {
//			LOGGER.info("[/bzjxpj/drg/medlowdeathrate] DrgjxpjAPI.queryMedLowDeathRate 接受查询请求,请求参数为:{}", vo);
//			paramMap = Tools.convertBean2Map(vo);
//			// 得到图例上各个点
//			list = this.service.queryLeftMedLowDeathRate(paramMap);
//			ms.put("rows", list);
//
//			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
//		} catch (Exception e) {
//			LOGGER.error("[/bzjxpj/drg/medlowdeathrate] DrgjxpjAPI.queryMedLowDeathRate 接受查询请求,发生异常,信息为:{}",
//					e.getMessage());
//			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
//		} finally {
//
//		}
//
//	};
//
//	@RequestMapping(value = "/lowdeathrate", method = { RequestMethod.GET })
//	public ApiResult queryLowDeathRate(QyjxpjQo vo, Pageable pagination, HttpServletRequest request) {
//		Map<String, Object> paramMap = null;
//		List<Map<String, Object>> list = null;
//		Map<String, Object> ms = new HashMap<>();
//		// BigDecimal
//		try {
//			LOGGER.info("[/bzjxpj/drg/lowdeathrate] DrgjxpjAPI.queryLowDeathRate 接受查询请求,请求参数为:{}", vo);
//			paramMap = Tools.convertBean2Map(vo);
//			// 得到图例上各个点
//			list = this.service.queryRightLowdeathrate(paramMap);
//			ms.put("rows", list);
//
//			return new ApiResult(Constants.RESULT.SUCCESS.intValue(), "查询成功", ms);
//		} catch (Exception e) {
//			LOGGER.error("[/bzjxpj/drg/lowdeathrate] DrgjxpjAPI.queryLowDeathRate 接受查询请求,发生异常,信息为:{}",
//					e.getMessage());
//			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
//		} finally {
//
//		}
//
//	};
//


};
