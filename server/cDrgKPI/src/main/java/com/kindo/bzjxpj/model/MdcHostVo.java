
/** 
* Project Name:cDrgKPI <br/> 
* File Name:MdcHostVo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年7月2日下午9:32:05 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Data;

/** 
* ClassName: MdcHostVo <br/>
* Function: MdcHostBo ==>行转列!.<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月2日 下午9:32:05 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class MdcHostVo  implements Serializable{
	
	
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -3382216902816998042L;
	String yyName;
	String yyCode;
//	List<Map<String,MdcHostVoMdcKeyValue>> map = new Vector<Map<String,MdcHostVoMdcKeyValue>>();
	//String 为 zhzs drgnum bls rwt 对应的为各mdc的数值 list Map中的string为mdcCode...
	// List中的1个Map 对应1个MDC 之前为:List<MdcHostVoMdcKeyValue> 
	//后来为List<Map<String,MdcHostVoMdcKeyValue>> 好根据 MDC编码直接获得对象...
//	Map<String,List<Map<String,MdcHostVoMdcKeyValue>>> zbTypeList = new ConcurrentHashMap<>(8);
//	Map<String,Map<String,MdcHostVoMdcKeyValue>> zbTypeList=new ConcurrentHashMap<>(8);
	// 后来根据前端的请求.... 第一个Map<String 为zhzs drgnum bls rwt
	//后面 的map对应 各MDC A之类的... MDC A:0.1这样的
	Map<String,Map<String,MdcHostVoMdcKeyValue>> zbTypeList=new ConcurrentHashMap<>(8);
	Double zhZhzs;
	Integer zhZhzsSequ;
	Integer zhDrgnum;
	Integer zhDrgnumSequ;
	Integer zhBls;
	Integer zhBlsSequ;
	Double zhRwt;
	Integer zhRwtSequ;
	Double zhFwnlzs;
	Integer zhFwnlzsSequ;
	Double zhFwxlzs;
	Integer zhFwxlzsSequ;
	Double zhNxzs;
	Integer zhNxzsSequ;
	
	Integer qsZys;
	Integer qsZysSequ;
	Integer zyzs;
	Integer zyzsSequ;
	
	@Data
	public class MdcHostVoMdcKeyValue implements Serializable{
		
		
	
		/**
		* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
		**/
		private static final long serialVersionUID = 2784207344742264376L;
		String mdcCode;
		String mdcName;
		Object value;
		
		/**
		* tote:总数. 
		**/
		int  tote;
		
		/**
		* sequ:排序. 
		**/
		int sequ;
		/** 
		**/
		public MdcHostVoMdcKeyValue() {
		
		}
		/** 
		* @param mdcCode2
		* @param mdcName2
		* @param zhzsV
		**/
		public MdcHostVoMdcKeyValue(String mdcCode2, String mdcName2, Object v) {
			this.mdcCode=mdcCode2;
			this.mdcName=mdcName2;
			this.value=v;
		}

		
		
	}
};
