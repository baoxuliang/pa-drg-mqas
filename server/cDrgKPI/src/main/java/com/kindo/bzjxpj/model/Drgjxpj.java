package com.kindo.bzjxpj.model;

import java.io.Serializable;
import java.util.Date;

import com.kindo.aria.excel.annotation.PoiExcelField;

public class Drgjxpj implements Serializable {
    
	/**
	* serialVersionUID:TODO(用一句话描述这个变量表示什么). 
	**/
	private static final long serialVersionUID = -3304519340048547899L;

	private String jbgdate;

    private String jbgtype;
	@PoiExcelField(index=0,title="DRG编码",halign="left")
    private String jbgdrgcode;

    private String qyCode;

    private String jbgmdccode;

    private String jbgmdcname;

    private String jbgadrgcode;
    @PoiExcelField(index=1,title="DRG名称",halign="left")
    private String jbgdrgname;
    @PoiExcelField(index=2,title="入组病案数",halign="right")
    private Integer jbgindrgnum;
    @PoiExcelField(index=3,title="相对权重",halign="right")
    private Double jbgrwt;

    private Double jbgnxzs;

    private Double jbgfwnlzs;

    private Double jbgfwxlzs;
    @PoiExcelField(index=4,title="CMI值",halign="right")
    private Double jbgcmi;


    private Double jbgfxzs;
  

    private Double jbghyljfy;
    @PoiExcelField(index=5,title="时间消耗指数",halign="right")
    private Double jbgtimesi;

    private Double jbgsxzs;
    @PoiExcelField(index=6,title="平均住院日(天)",halign="right")
    private Double jbgpjzyr;
    
    @PoiExcelField(index=7,title="费用消耗指数",halign="right")
    private Double jbgcostsi;
    @PoiExcelField(index=8,title="次均费用(元)",halign="right")
    private Double jbgcjfy;
    
    private Double jbghypjzyr;

    private Integer jbglownum;

    private String jbglowswl;

    private Integer jbgmednum;

    private String jbgmedswl;
    @PoiExcelField(index=9,title="死亡人数",halign="right")
    private Integer jbgswnum;

    private String jbgswl;
    @PoiExcelField(index=10,title="风险等级",halign="right")
    private String fxcd;

    private Date moddate;


    public String getJbgdate() {
        return jbgdate;
    }

    public void setJbgdate(String jbgdate) {
        this.jbgdate = jbgdate == null ? null : jbgdate.trim();
    }

    public String getJbgtype() {
        return jbgtype;
    }

    public void setJbgtype(String jbgtype) {
        this.jbgtype = jbgtype == null ? null : jbgtype.trim();
    }

    public String getJbgdrgcode() {
        return jbgdrgcode;
    }

    public void setJbgdrgcode(String jbgdrgcode) {
        this.jbgdrgcode = jbgdrgcode == null ? null : jbgdrgcode.trim();
    }

    public String getQyCode() {
        return qyCode;
    }

    public void setQyCode(String qyCode) {
        this.qyCode = qyCode == null ? null : qyCode.trim();
    }

    public String getJbgmdccode() {
        return jbgmdccode;
    }

    public void setJbgmdccode(String jbgmdccode) {
        this.jbgmdccode = jbgmdccode == null ? null : jbgmdccode.trim();
    }

    public String getJbgmdcname() {
        return jbgmdcname;
    }

    public void setJbgmdcname(String jbgmdcname) {
        this.jbgmdcname = jbgmdcname == null ? null : jbgmdcname.trim();
    }

    public String getJbgadrgcode() {
        return jbgadrgcode;
    }

    public void setJbgadrgcode(String jbgadrgcode) {
        this.jbgadrgcode = jbgadrgcode == null ? null : jbgadrgcode.trim();
    }

    public String getJbgdrgname() {
        return jbgdrgname;
    }

    public void setJbgdrgname(String jbgdrgname) {
        this.jbgdrgname = jbgdrgname == null ? null : jbgdrgname.trim();
    }

    public Integer getJbgindrgnum() {
        return jbgindrgnum;
    }

    public void setJbgindrgnum(Integer jbgindrgnum) {
        this.jbgindrgnum = jbgindrgnum;
    }

    public Double getJbgrwt() {
        return jbgrwt;
    }

    public void setJbgrwt(Double jbgrwt) {
        this.jbgrwt = jbgrwt;
    }

    public Double getJbgnxzs() {
        return jbgnxzs;
    }

    public void setJbgnxzs(Double jbgnxzs) {
        this.jbgnxzs = jbgnxzs;
    }

    public Double getJbgfwnlzs() {
        return jbgfwnlzs;
    }

    public void setJbgfwnlzs(Double jbgfwnlzs) {
        this.jbgfwnlzs = jbgfwnlzs;
    }

    public Double getJbgfwxlzs() {
        return jbgfwxlzs;
    }

    public void setJbgfwxlzs(Double jbgfwxlzs) {
        this.jbgfwxlzs = jbgfwxlzs;
    }

    public Double getJbgcmi() {
        return jbgcmi;
    }

    public void setJbgcmi(Double jbgcmi) {
        this.jbgcmi = jbgcmi;
    }

    public Double getJbgcostsi() {
        return jbgcostsi;
    }

    public void setJbgcostsi(Double jbgcostsi) {
        this.jbgcostsi = jbgcostsi;
    }

    public Double getJbgfxzs() {
        return jbgfxzs;
    }

    public void setJbgfxzs(Double jbgfxzs) {
        this.jbgfxzs = jbgfxzs;
    }

    public Double getJbgcjfy() {
        return jbgcjfy;
    }

    public void setJbgcjfy(Double jbgcjfy) {
        this.jbgcjfy = jbgcjfy;
    }

    public Double getJbghyljfy() {
        return jbghyljfy;
    }

    public void setJbghyljfy(Double jbghyljfy) {
        this.jbghyljfy = jbghyljfy;
    }

    public Double getJbgtimesi() {
        return jbgtimesi;
    }

    public void setJbgtimesi(Double jbgtimesi) {
        this.jbgtimesi = jbgtimesi;
    }

    public Double getJbgsxzs() {
        return jbgsxzs;
    }

    public void setJbgsxzs(Double jbgsxzs) {
        this.jbgsxzs = jbgsxzs;
    }

    public Double getJbgpjzyr() {
        return jbgpjzyr;
    }

    public void setJbgpjzyr(Double jbgpjzyr) {
        this.jbgpjzyr = jbgpjzyr;
    }

    public Double getJbghypjzyr() {
        return jbghypjzyr;
    }

    public void setJbghypjzyr(Double jbghypjzyr) {
        this.jbghypjzyr = jbghypjzyr;
    }

    public Integer getJbglownum() {
        return jbglownum;
    }

    public void setJbglownum(Integer jbglownum) {
        this.jbglownum = jbglownum;
    }

    public String getJbglowswl() {
        return jbglowswl;
    }

    public void setJbglowswl(String jbglowswl) {
        this.jbglowswl = jbglowswl;
    }

    public Integer getJbgmednum() {
        return jbgmednum;
    }

    public void setJbgmednum(Integer jbgmednum) {
        this.jbgmednum = jbgmednum;
    }

    public String getJbgmedswl() {
        return jbgmedswl;
    }

    public void setJbgmedswl(String jbgmedswl) {
        this.jbgmedswl = jbgmedswl;
    }

    public Integer getJbgswnum() {
        return jbgswnum;
    }

    public void setJbgswnum(Integer jbgswnum) {
        this.jbgswnum = jbgswnum;
    }

    public String getJbgswl() {
        return jbgswl;
    }

    public void setJbgswl(String jbgswl) {
        this.jbgswl = jbgswl;
    }

    public String getFxcd() {
        return fxcd;
    }

    public void setFxcd(String fxcd) {
        this.fxcd = fxcd == null ? null : fxcd.trim();
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", jbgdate=").append(jbgdate);
        sb.append(", jbgtype=").append(jbgtype);
        sb.append(", jbgdrgcode=").append(jbgdrgcode);
        sb.append(", qyCode=").append(qyCode);
        sb.append(", jbgmdccode=").append(jbgmdccode);
        sb.append(", jbgmdcname=").append(jbgmdcname);
        sb.append(", jbgadrgcode=").append(jbgadrgcode);
        sb.append(", jbgdrgname=").append(jbgdrgname);
        sb.append(", jbgindrgnum=").append(jbgindrgnum);
        sb.append(", jbgrwt=").append(jbgrwt);
        sb.append(", jbgnxzs=").append(jbgnxzs);
        sb.append(", jbgfwnlzs=").append(jbgfwnlzs);
        sb.append(", jbgfwxlzs=").append(jbgfwxlzs);
        sb.append(", jbgcmi=").append(jbgcmi);
        sb.append(", jbgcostsi=").append(jbgcostsi);
        sb.append(", jbgfxzs=").append(jbgfxzs);
        sb.append(", jbgcjfy=").append(jbgcjfy);
        sb.append(", jbghyljfy=").append(jbghyljfy);
        sb.append(", jbgtimesi=").append(jbgtimesi);
        sb.append(", jbgsxzs=").append(jbgsxzs);
        sb.append(", jbgpjzyr=").append(jbgpjzyr);
        sb.append(", jbghypjzyr=").append(jbghypjzyr);
        sb.append(", jbglownum=").append(jbglownum);
        sb.append(", jbglowswl=").append(jbglowswl);
        sb.append(", jbgmednum=").append(jbgmednum);
        sb.append(", jbgmedswl=").append(jbgmedswl);
        sb.append(", jbgswnum=").append(jbgswnum);
        sb.append(", jbgswl=").append(jbgswl);
        sb.append(", fxcd=").append(fxcd);
        sb.append(", moddate=").append(moddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}