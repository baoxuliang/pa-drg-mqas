package com.kindo.bzjxpj.dao;

import com.kindo.aria.base.Pageable;
import com.kindo.bzjxpj.model.Drgjxpj;
import com.kindo.bzjxpj.model.DrgnumVo;
import com.kindo.bzjxpj.model.Mdcjxpj;
import com.kindo.bzjxpj.model.RzbasVo;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface MdcjxpjMapper {
    int deleteByPrimaryKey(@Param("jbmdate") String jbmdate, @Param("jbmtype") String jbmtype, @Param("jbmmdccode") String jbmmdccode);

    int insert(Mdcjxpj record);

    Mdcjxpj selectByPrimaryKey(@Param("jbmdate") String jbmdate, @Param("jbmtype") String jbmtype, @Param("jbmmdccode") String jbmmdccode);

    List<Mdcjxpj> selectAll();

    int updateByPrimaryKey(Mdcjxpj record);

	
	 /** 
	 * listPageMdcjxpj:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午3:47:34 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Mdcjxpj> listPageMdcjxpj(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	List<Mdcjxpj> listPageExportMdcjxpj(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	 /** 
	 * queryRzbas:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年6月28日 下午6:00:36 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryRzbas(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	List<RzbasVo> queryExportRzbas(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryDrg:(这里用一句话描述这个方法的作用). <br/>
	 * @author whk00196 
	 * @date 2018年7月3日 下午4:18:11 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String, Object>> queryDrg(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);

	
	 /** 
	 * queryExportDrg:(这里用一句话描述这个方法的作用). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选). <br/>
	 * TODO(这里描述这个方法的执行流程 – 可选). <br/>
	 * TODO(这里描述这个方法的使用方法 – 可选). <br/>
	 * TODO(这里描述这个方法的注意事项 – 可选). <br/>
	 * @author whk00196 
	 * @date 2018年7月4日 下午8:53:51 *
	 * @param paramMap
	 * @param pagination
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<DrgnumVo> queryExportDrg(@Param("param")Map<String, Object> paramMap, @Param("pagination")Pageable pagination);
	
	
	 /** 
	 * queryMdc:得到所有的mdc-List. <br/>
	 * @author whk00196 
	 * @date 2018年7月9日 下午3:02:33 *
	 * @return 
	 * @since JDK 1.8 
	 **/
	List<Map<String,String>> queryMdc();
};