
/** 
* Project Name:cDrgKPI <br/> 
* File Name:DrgnumVo.java <br/>
* Package Name:com.kindo.bzjxpj.model <br/>
* Date:2018年7月3日下午3:56:37 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.bzjxpj.model;

import com.kindo.aria.excel.annotation.PoiExcelField;

import lombok.Data;

/** 
* ClassName: DrgnumVo <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年7月3日 下午3:56:37 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Data
public class DrgnumVo {
	@PoiExcelField(index=0,title="DRG编码",halign="left")
	String drgcode;
	@PoiExcelField(index=1,title="DRG名称",halign="left")
	String drgname;
	@PoiExcelField(index=2,title="入组病案数",halign="right")
	Object rzbls;
	@PoiExcelField(index=3,title="相对权重",halign="right")
	Double drgrwt;
	@PoiExcelField(index=4,title="CMI值",halign="right")
	Double cmi;
	@PoiExcelField(index=5,title="时间消耗指数",halign="right")
	Double timesi;
	
	@PoiExcelField(index=6,title="平均住院日（天）",halign="right")
	Double  pjzyr;
	@PoiExcelField(index=7,title="费用消耗指数",halign="right")
	Double costsi;
	@PoiExcelField(index=8,title="次均费用（元）",halign="right")
	Double  cjfy;
	@PoiExcelField(index=9,title="死亡人数",halign="right")
	Integer swnum;
	@PoiExcelField(index=10,title="风险等级",halign="right")
	Object risk;
};
