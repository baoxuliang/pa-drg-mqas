package com.kindo.plugin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.kindo.common.cache.RedisData;

/**
 * 自定义拦截器
 * 
 * @author 张喻龙
 *
 */
@Service
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

	protected static RedisData redisData;

	@Autowired
	public void setRedisData(RedisData redisData) {
		AuthorizationInterceptor.redisData = redisData;
	}

	/**
	 * 登录token验证
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		/*if(true)
			return true;
		if (!isLoginUri(request.getRequestURI()) && !isDownloadUri(request.getRequestURI())) {
			String token = request.getHeader("token");

			if (token == null) {
				response.setContentType("text/json;charset=UTF-8");
				response.getWriter().append(
						new ApiResult(Constants.RESULT.AUTH_TOKEN, Constants.RESULT_MSG.AUTH_TOKEN, "").toJsonString())
						.close();
				return false;
			}
			String oriToken = new String(AESUtil.decrypt(Encodes.decodeBase64(token)));
			String[] arr = oriToken.split("_");
			if (arr == null || arr.length != 3) {
				response.setContentType("text/json;charset=UTF-8");
				response.getWriter().append(
						new ApiResult(Constants.RESULT.AUTH_TOKEN, Constants.RESULT_MSG.AUTH_TOKEN, "").toJsonString())
						.close();
				return false;
			}

			// if (!arr[2].equals(request.getRemoteAddr())) {
			// response.setContentType("text/json;charset=UTF-8");
			// response.getWriter().append(new
			// ApiResult(Constants.RESULT.AUTH_TOKEN,
			// Constants.RESULT_MSG.AUTH_TOKEN, "")
			// .toJsonString()).close();
			// return false;
			// }

			UserLoginInfo info = (UserLoginInfo) redisData.getData("loginInfo_" + arr[0]);
			if (info == null || !arr[0].equals(info.getLoginNo())) {
				response.setContentType("text/json;charset=UTF-8");
				response.getWriter().append(
						new ApiResult(Constants.RESULT.AUTH_TOKEN, Constants.RESULT_MSG.AUTH_TOKEN, "").toJsonString())
						.close();
				return false;
			}
			// if (!arr[2].equals(info.getLoginIPAddr())) {
			// writer.append(new
			// ApiResult(Constants.RESULT.AUTH_TOKEN,
			// Constants.RESULT_MSG.AUTH_LOGIN_OTHER,
			// "").toJsonString()).close();
			// return false;
			// }

			// long duration = System.currentTimeMillis() -
			// info.getLoginDate().getTime();
			// if (duration > 20 * 60 * 1000) {
			// redisData.deleteData("loginInfo_" + arr[0]);
			// response.setContentType("text/json;charset=UTF-8");
			// response.getWriter()
			// .append(new ApiResult(Constants.RESULT.AUTH_EXPIRE,
			// Constants.RESULT_MSG.AUTH_EXPIRE, "")
			// .toJsonString())
			// .close();
			// return false;
			// } else if (duration > 10 * 60 * 1000) {
			// info.setLoginDate(new Date());
			// redisData.insertData("loginInfo_" + arr[0], info);
			// }

			String httpMethod = request.getMethod();
			if ("POST".equals(httpMethod)) {
				// TODO: 添加接口调用记录操作
			}
		} else {
			// TODO: 添加接口调用记录登录日志
		}*/

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// System.out.println("求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// System.out.println("在整个请求结束之后被调用，也就是在DispatcherServlet
		// 渲染了对应的视图之后执行（主要是用于进行资源清理工作）");
	}

	public static void main(String[] args) {
		String str = "/sysmgr/user/login/signIn";
		System.out.println(isLoginUri(str));
	}

	private static boolean isLoginUri(String uri) {
		if (uri.contains("/login/signIn") || uri.contains("/login/SSO")) {
			return true;
		}
		return false;
	}

	private static boolean isDownloadUri(String uri) {
		if (uri.contains("/download/") || uri.contains("/export/") || uri.endsWith("/download")
				|| uri.endsWith("/export")) {
			return true;
		}
		return false;
	}
}
