package com.kindo.difficultCases.utils;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelUtil {
	/**
	 * 导出Excel公共方法
	 * @version 1.0
	 * 
	 * @author wangcp
	 *
	 */
	
		// 最大列宽
	    private static final int MAX_WIDTH = 20 * 256;
	    // 标题字体
	    private static final String FONT_NAME_HEADER = "宋体 Light";
	    // 数据字体
	    private static final String FONT_NAME_DATA = "宋体";
	    
	    //显示的导出表的标题
	    //private String title;
	    //导出表的列名
	    private String[] rowName ;
	    
	    private String[] lcr;
	    
	    private String[] rowNameList;
	    
	    private List<Object[]>  dataList = new ArrayList<Object[]>();
	    
	    
	    //构造方法，传入要导出的数据
	    public ExcelUtil(String[] rowNameList,List<Object[]>  dataList){
	        this.dataList = dataList;
	        this.rowNameList = rowNameList;
	        
	        String[] rs = new String[rowNameList.length];
        	String[] ls = new String[rowNameList.length];
	        for(int i=0;i<rowNameList.length;i++){
	        	rs[i] = rowNameList[i].split("_")[0];
	        	ls[i] = rowNameList[i].split("_")[1];
	        }
	        rowName = rs;
	        lcr = ls;
	        //this.title = title;
	    }
	            
	    /**
	     * 导出数据
	     * @param response
	     * @param fileName 文件名
	     * @param mergedRegion 合并的数据行号
	     * @throws Exception
	     */
	    public void export(HttpServletResponse response,String fileName,Workbook workbook,Sheet sheet) throws Exception{
	        try{
	            ///HSSFWorkbook workbook = new HSSFWorkbook();                        // 创建工作簿对象
	            //HSSFSheet sheet = workbook.createSheet(title);                     // 创建工作表
	            
	            // 产生表格标题行
	           // HSSFRow rowm = sheet.createRow(0);
	            //HSSFCell cellTiltle = rowm.createCell(0);
	            
	            //sheet样式定义【getColumnTopStyle()/getStyle()均为自定义方法 - 在下面  - 可扩展】
	            //HSSFCellStyle columnTopStyle = this.getColumnTopStyle(workbook);//获取列头样式对象
	            //HSSFCellStyle style = this.getStyle(workbook);                    //单元格样式对象
	            
	            //sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, (rowName.length-1)));  
	           //cellTiltle.setCellStyle(columnTopStyle);
	            //cellTiltle.setCellValue(title);
	        	
	        	// 初始化样式属性
		        Font font = workbook.createFont();
		        font.setBold(true);
		        font.setFontName(FONT_NAME_HEADER);
		        font.setFontHeightInPoints((short) 10);

		        CellStyle style = workbook.createCellStyle();
		        style.setFont(font);
		        style.setAlignment(HorizontalAlignment.CENTER);
		        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
		        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	            
	            // 定义所需列数
	            int columnNum = rowName.length;
	            Row titleRow = sheet.createRow(0);           // 在索引0的位置创建行(最顶端的行开始的第1行)
	            
	            // 将列头设置到sheet的单元格中
	            for(int i=0;i<rowName.length;i++){
	            	Cell cell = titleRow.createCell(i);
	            	cell.setCellValue(rowName[i]);
	            	cell.setCellStyle(style);
	            }
	            
	            Font font_data = workbook.createFont();
	            font_data.setFontHeightInPoints((short) 10);
	            font_data.setFontName(FONT_NAME_DATA);
                CellStyle style_data = workbook.createCellStyle();
                style_data.setFont(font_data);
                style_data.setVerticalAlignment(VerticalAlignment.CENTER);
                
                CellStyle cStyle_r = workbook.createCellStyle();
                cStyle_r.cloneStyleFrom(style_data);
            	
            	CellStyle cStyle_c = workbook.createCellStyle();
            	cStyle_c.cloneStyleFrom(style_data);
                
	            //将查询出的数据设置到sheet对应的单元格中
	            for(int i=0;i<dataList.size();i++){
	                Object[] obj = dataList.get(i);//遍历每个对象
	                Row row = sheet.createRow(i+1);//创建所需的行数
	                
	                for(int j=0; j<obj.length; j++){
	                    Cell  cell = row.createCell(j);;   //设置单元格的数据类型
	                    
                        if(!"".equals(obj[j]) && obj[j] != null){
                            cell.setCellValue(obj[j].toString());                        //设置单元格的值
                            
                            //如果单元格的值为数值型则居右
                            /*if(isNumber(obj[j])){
                            	style_data.setAlignment(HorizontalAlignment.RIGHT);
                            	cell.setCellStyle(style_data);
                            } else {
                            	CellStyle cStyle = workbook.createCellStyle();
                            	cStyle.cloneStyleFrom(style_data);
                            	cStyle.setAlignment(HorizontalAlignment.LEFT);
                            	cell.setCellStyle(cStyle);
                            }*/
                            
                            if("L".equals(lcr[j])){
                            	style_data.setAlignment(HorizontalAlignment.LEFT);
                            	cell.setCellStyle(style_data);
                            } else if("R".equals(lcr[j])){
                            	cStyle_r.setAlignment(HorizontalAlignment.RIGHT);
                            	cell.setCellStyle(cStyle_r);
                            } else if("C".equals(lcr[j])){
                            	cStyle_c.setAlignment(HorizontalAlignment.CENTER);
                            	cell.setCellStyle(cStyle_c);
                            }
                        }
	                }
	            }
	            
	            
	          //让列宽随着导出的列长自动适应
	            for (int colNum = 0; colNum < columnNum; colNum++) {
	               /* int columnWidth = sheet.getColumnWidth(colNum) / 256;
	                for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
	                    Row currentRow;
	                    //当前行未被使用过
	                    if (sheet.getRow(rowNum) == null) {
	                        currentRow = sheet.createRow(rowNum);
	                    } else {
	                        currentRow = sheet.getRow(rowNum);
	                    }
	                    if (currentRow.getCell(colNum) != null) {
	                        Cell currentCell = currentRow.getCell(colNum);
	                        if (currentCell.getCellTypeEnum() == CellType.STRING) {
	                            int length = currentCell.getStringCellValue().getBytes().length;
	                            if (columnWidth < length) {
	                                columnWidth = length;
	                            }
	                        }
	                    }
	                }*/
	                /*if(colNum == 0){
	                    sheet.setColumnWidth(colNum, (columnWidth-2) * 256);
	                }else{
	                    sheet.setColumnWidth(colNum, (columnWidth+4) * 256);
	                }*/
	                
	                sheet.setColumnWidth(colNum, (rowName[colNum].getBytes().length + 2)*256);
	                
	            }
	            
	            if(workbook !=null){
	            	response.setContentType("multipart/form-data;charset=UTF-8");
	                response.setCharacterEncoding("UTF-8");
	                response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
	                try {
	                    String formatFileName = URLEncoder.encode(fileName, "UTF-8");
	                    response.setHeader("Content-Disposition", "attachment;fileName=" + formatFileName + ".xlsx");
	                    OutputStream out = response.getOutputStream();
	                    workbook.write(out);
	                } catch (UnsupportedEncodingException e) {
	                    e.printStackTrace();
	                }
	            }

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        
	    }
}
