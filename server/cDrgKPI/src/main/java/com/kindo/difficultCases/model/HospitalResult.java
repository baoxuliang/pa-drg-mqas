package com.kindo.difficultCases.model;

import lombok.Data;

@Data
public class HospitalResult {

	private String memberCode;
	
	private String memberName;
	
	private Integer caseSum;
	
	private Integer casesNum_1;
	
	private Double ratio_1;
	
	private Integer casesNum_2;
	
	private Double ratio_2;
	
	private Integer casesNum_3;
	
	private Double ratio_3;
	
	private Integer casesNum_4;
	
	private Double ratio_4;
	
}
