package com.kindo.difficultCases.model;

import lombok.Data;

@Data
public class DepartmentResult {
	
	private String stdksmc;
	
	private String stdksdm;
	
	private Integer caseSum;
	
	private Integer casesNum_1;
	
	private Double ratio_1;
	
	private Integer casesNum_2;
	
	private Double ratio_2;
	
	private Integer casesNum_3;
	
	private Double ratio_3;
	
	//private String rwRangeExpression_4;
	
	private Integer casesNum_4;
	
	private Double ratio_4;
	
}
