package com.kindo.difficultCases.model;

import lombok.Data;

@Data
public class RwtRangeStrResult {

	private String rwRangeExpression_1;
	
	private String rwRangeExpression_2;
	
	private String rwRangeExpression_3;
	
	private String rwRangeExpression_4;
	
	private String v1;
	
	private String v2;
	
	private String v3;
	
	private String v4;
}
