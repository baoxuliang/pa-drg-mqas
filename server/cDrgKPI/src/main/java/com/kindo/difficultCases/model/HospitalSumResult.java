package com.kindo.difficultCases.model;

import lombok.Data;

@Data
public class HospitalSumResult {

	private Integer caseSumTotal;
	
	private Integer casesNumTotal_1;
	
	private Double ratioTotal_1;
	
	private Integer casesNumTotal_2;
	
	private Double ratioTotal_2;
	
	private Integer casesNumTotal_3;
	
	private Double ratioTotal_3;
	
	private Integer casesNumTotal_4;
	
	private Double ratioTotal_4;
	
}
