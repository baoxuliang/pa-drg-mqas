package com.kindo.difficultCases.model;

import com.kindo.uas.common.model.UserLoginInfo;

import lombok.Data;

/**
 * 医院，科室 疑难病例查询参数pojo
 * @author likai
 *
 */
@Data
public class HospitalQo {

	/**
	 * 行政区划类型（省:1    市:2    区:3）
	 */
	private String city;
	
	/**
	 * 行政区划代码
	 */
	private String cnty;
	
	/**
	 * 区域code
	 */
	private String qy_code;
	
	/**
	 * 年
	 */
	private String year;
	
	/**
	 * 季度
	 */
	private String quarter;
	
	/**
	 * 月份
	 */
	private String month;
	
	/**
	 * 科室编码
	 */
	private String stdksdm;
	
	/**
	 * 医疗机构编码
	 */
	private String memberCode;
	
	/**
	 * 医疗机构类型代码（0:综合医院 1:转科医院）
	 */
	private String memberTypeCode;
	
	/**
	 * 医疗机构等级（1:一级 2：二级 3：三级）
	 */
	private String memberLev1;
	
	/**
	 * 医疗机构等级（1:特 2：甲 3：乙 4：丙）
	 */
	private String memberLev2;
	
	/**
	 * 公式1
	 */
	private String re1;
	
	/**
	 * 公式2
	 */
	private String re2;
	
	/**
	 * 公式3
	 */
	private String re3;
	
	/**
	 * 公式4
	 */
	private String re4;
	
	private String rangeExpression;
	
	private String rangeExpression_val1;
	
	private String rangeExpression_val2;
	
	private UserLoginInfo user;
	
}
