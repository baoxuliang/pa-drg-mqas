package com.kindo.difficultCases.model;

import lombok.Data;

/**
 * 权重计算范围配置
 * @author likai
 *
 */
@Data
public class RwtRangeObj {
	
	private String ID;
	
	/**
	 * 序号
	 */
	private String XH;
	
	/**
	 * 
	 */
	private String YNBL_FH1;
	
	private Integer YNBL_VAL1;
	
	private String YNBL_FH2;
	
	private Integer YNBL_VAL2;
	
}
