package com.kindo.difficultCases.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.kindo.aria.base.Pageable;
import com.kindo.difficultCases.model.DepartmentResult;
import com.kindo.difficultCases.model.HospitalNumResult;
import com.kindo.difficultCases.model.HospitalQo;
import com.kindo.difficultCases.model.HospitalResult;
import com.kindo.difficultCases.model.HospitalSumResult;
import com.kindo.difficultCases.model.RwtRangeObj;
import com.kindo.difficultCases.model.RwtRangeStrResult;

@Mapper
public interface HospitalDataStatisticalMapper {
	
	List<HospitalResult> queryHospitalData(@Param("param")HospitalQo Qo);
	
	List<HospitalResult> queryHospitalData(@Param("param")HospitalQo Qo,@Param("pagination") Pageable pagination);
	
	List<DepartmentResult> queryHospitalQueryInfo(@Param("param")HospitalQo Qo,@Param("pagination") Pageable pagination);
	
	List<DepartmentResult> queryHospitalQueryInfo(@Param("param")HospitalQo Qo);
	
	List<RwtRangeObj> queryRwtRange();
	
	List<HospitalNumResult> queryHospitalByNumClick(@Param("param") HospitalQo Qo,@Param("pagination") Pageable pagination);
	
	List<HospitalNumResult> queryHospitalByNumClick(@Param("param") HospitalQo Qo);
	
	HospitalSumResult queryHospitalDataSum(@Param("param")HospitalQo Qo);
	
	List<RwtRangeStrResult> queryRangeStr();
}
