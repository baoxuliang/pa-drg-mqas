package com.kindo.difficultCases.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;
import com.kindo.difficultCases.dao.HospitalDataStatisticalMapper;
import com.kindo.difficultCases.model.DepartmentResult;
import com.kindo.difficultCases.model.HospitalNumResult;
import com.kindo.difficultCases.model.HospitalQo;
import com.kindo.difficultCases.model.HospitalResult;
import com.kindo.difficultCases.model.HospitalSumResult;
import com.kindo.difficultCases.model.RwtRangeObj;
import com.kindo.difficultCases.model.RwtRangeStrResult;
import com.kindo.difficultCases.utils.ExcelUtil;
import com.kindo.uas.common.model.UserLoginInfo;

@Service
public class HospitalDataStatisticalService {
	
	@Autowired
	private HospitalDataStatisticalMapper mapper;
	
	/**
	 * 点击入组数量弹出数据分页
	 * @param map
	 * @param pageAble
	 * @return
	 */
	public ApiResult queryHospitalByNumClick(HospitalQo numQo,Pageable pagination){
		if(numQo.getRangeExpression() == null){
			return new ApiResult(Constants.RESULT.FAIL, "未传入RW范围参数",null);
		}
		numQo.setRangeExpression_val1(numQo.getRangeExpression().split(",")[0]);
		numQo.setRangeExpression_val2(numQo.getRangeExpression().split(",")[1]);
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" memberName ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		SortPair sp2= new SortPair();
    		sp2.setField(" ,bah ");
    		sp2.setAsc(true);
    		sorts.add(sp2);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.queryHospitalByNumClick(numQo,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	/**
	 * 列表查询(分页)，图形数据(不分页)
	 * @param Qo
	 * @return
	 */
	public ApiResult getHospitalQueryByPage(HospitalQo Qo,Pageable pagination){
		List<RwtRangeObj> list_range = mapper.queryRwtRange();
		if(list_range.size() < 4){
			return new ApiResult(Constants.RESULT.FAIL, "未配置权重区间",null);
		}
		
		if(pagination != null){
			if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
	    		List<SortPair> sorts = new ArrayList<>();
	    		SortPair sp1= new SortPair();
	    		sp1.setField(" casesNum_1 ");
	    		sp1.setAsc(false);
	    		sorts.add(sp1);
	    		pagination.setSorts(sorts);
	    	}
			RowsWithTotal rt = new RowsWithTotal();
			rt.setRows(mapper.queryHospitalData(getHosParameter(list_range, Qo),pagination));
			rt.setTotal(pagination.getTotal());
			return new ApiResult(Constants.RESULT.SUCCESS, 
					Constants.RESULT_MSG.QUERY_SUCCESS,rt);
		} else {
			Qo.setMemberCode(null);//图形数据不需要医院编码条件查询
			return new ApiResult(Constants.RESULT.SUCCESS, 
					Constants.RESULT_MSG.QUERY_SUCCESS,mapper.queryHospitalData(getHosParameter(list_range, Qo)));
		}
	}
	
	/**
	 * 列表数据点击查询详情列表页数据-分页
	 * @param Qo
	 * @return
	 */
	public ApiResult getHospitalQueryInfo(HospitalQo Qo,Pageable pagination){
		List<RwtRangeObj> list_range = mapper.queryRwtRange();
		if(list_range.size() < 4){
			return new ApiResult(Constants.RESULT.FAIL, "未配置权重区间",null);
		}
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" casesNum_1 ");
    		sp1.setAsc(false);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.queryHospitalQueryInfo(getHosParameter(list_range, Qo),pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, 
				Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	/**
	 * 列表查询(合计数据)
	 * @param Qo
	 * @return
	 */
	public ApiResult queryHospitalDataSum(HospitalQo Qo){
		
		List<RwtRangeObj> list_range = mapper.queryRwtRange();
		if(list_range.size() < 4){
			return new ApiResult(Constants.RESULT.FAIL, "未配置权重区间",null);
		}
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,mapper.queryHospitalDataSum(getHosParameter(list_range, Qo)));
	}
	
	/**
	 * 组装参数
	 * @param list_range
	 * @param Qo
	 * @return
	 */
	public HospitalQo getHosParameter(List<RwtRangeObj> list_range,HospitalQo Qo){
		String re1 = (list_range.get(0).getYNBL_VAL1()!=null ? "rwt>="+list_range.get(0).getYNBL_VAL1():"") +
				(list_range.get(0).getYNBL_VAL2()!=null ? " and rwt<"+list_range.get(0).getYNBL_VAL2():"");
		String re2 = (list_range.get(1).getYNBL_VAL1()!=null ? "rwt>="+list_range.get(1).getYNBL_VAL1():"") +
				(list_range.get(1).getYNBL_VAL2()!=null ? " and rwt<"+list_range.get(1).getYNBL_VAL2():"");
		String re3 = (list_range.get(2).getYNBL_VAL1()!=null ? "rwt>="+list_range.get(2).getYNBL_VAL1():"") +
				(list_range.get(2).getYNBL_VAL2()!=null ? " and rwt<"+list_range.get(2).getYNBL_VAL2():"");
		String re4 = (list_range.get(3).getYNBL_VAL1()!=null ? "rwt>="+list_range.get(3).getYNBL_VAL1():"") +
				(list_range.get(3).getYNBL_VAL2()!=null ? " and rwt<"+list_range.get(3).getYNBL_VAL2():"");
		Qo.setRe1(re1);
		Qo.setRe2(re2);
		Qo.setRe3(re3);
		Qo.setRe4(re4);
		return Qo;
	}
	
	/**
	 * 获取导出列头权重范围
	 * @param list_range
	 * @return
	 */
	public String[] getExportTitle(List<RwtRangeObj> list_range){
		String[] res = new String[4];
		if(list_range.get(0).getYNBL_VAL1() == null && list_range.get(0).getYNBL_VAL2() == null){
			res[0] = "";
        } else if(list_range.get(0).getYNBL_VAL1() == null && list_range.get(0).getYNBL_VAL2() != null){
        	res[0] = "RW<"+list_range.get(0).getYNBL_VAL2();
        } else if(list_range.get(0).getYNBL_VAL1() != null && list_range.get(0).getYNBL_VAL2() == null){
        	res[0] = "RW>="+list_range.get(0).getYNBL_VAL1();
        } else {
        	res[0] = list_range.get(0).getYNBL_VAL1()+"<=RW<"+list_range.get(0).getYNBL_VAL2();
        }
        
        if(list_range.get(1).getYNBL_VAL1() == null && list_range.get(1).getYNBL_VAL2() == null){
        	res[1] = "";
        } else if(list_range.get(1).getYNBL_VAL1() == null && list_range.get(1).getYNBL_VAL2() != null){
        	res[1] = "RW<"+list_range.get(1).getYNBL_VAL2();
        } else if(list_range.get(1).getYNBL_VAL1() != null && list_range.get(1).getYNBL_VAL2() == null){
        	res[1] = "RW>="+list_range.get(1).getYNBL_VAL1();
        } else {
        	res[1] = list_range.get(1).getYNBL_VAL1()+"<=RW<"+list_range.get(1).getYNBL_VAL2();
        }
        
        if(list_range.get(2).getYNBL_VAL1() == null && list_range.get(2).getYNBL_VAL2() == null){
        	res[2] = "";
        } else if(list_range.get(2).getYNBL_VAL1() == null && list_range.get(2).getYNBL_VAL2() != null){
        	res[2] = "RW<"+list_range.get(2).getYNBL_VAL2();
        } else if(list_range.get(2).getYNBL_VAL1() != null && list_range.get(2).getYNBL_VAL2() == null){
        	res[2] = "RW>="+list_range.get(2).getYNBL_VAL1();
        } else {
        	res[2] = list_range.get(2).getYNBL_VAL1()+"<=RW<"+list_range.get(2).getYNBL_VAL2();
        }
        
        if(list_range.get(3).getYNBL_VAL1() == null && list_range.get(3).getYNBL_VAL2() == null){
        	res[3] = "";
        } else if(list_range.get(3).getYNBL_VAL1() == null && list_range.get(3).getYNBL_VAL2() != null){
        	res[3] = "RW<"+list_range.get(3).getYNBL_VAL2();
        } else if(list_range.get(3).getYNBL_VAL1() != null && list_range.get(3).getYNBL_VAL2() == null){
        	res[3] = "RW>="+list_range.get(3).getYNBL_VAL1();
        } else {
        	res[3] = list_range.get(3).getYNBL_VAL1()+"<=RW<"+list_range.get(3).getYNBL_VAL2();
        }
        return res;
	}
	
	public List<RwtRangeStrResult> queryRwtRangeStr(){
		return mapper.queryRangeStr();
	}
	
	/**
	 * 医院疑难病例表格数据导出
	 * @param response
	 * @param qo
	 */
	public void exportHospitalData(HttpServletResponse response,HospitalQo qo){
		String fileName = "医院疑难病例" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        List<RwtRangeObj> list_range = mapper.queryRwtRange();
        String[] res = getExportTitle(list_range);
        String[] rowsName = new String[]{"序号_C","医院名称_L","入组病案数_R",res[0]+"病案数_R",
        		res[0]+"占比(%)_R",res[1]+"病案数_R",res[1]+"占比(%)_R",res[2]+"病案数_R",res[2]+"占比(%)_R",
        		res[3]+"病案数_R",res[3]+"占比(%)_R"};
		
        List<HospitalResult> list = mapper.queryHospitalData(getHosParameter(list_range, qo));
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        if(list.size() > 0){
        	for (int i = 0; i < list.size(); i++) {
            	HospitalResult hos = list.get(i);
                objs = new Object[rowsName.length];
                objs[0] = i+1;
                objs[1] = hos.getMemberName();
                objs[2] = hos.getCaseSum();
                objs[3] = hos.getCasesNum_1();
                objs[4] = fm.format(hos.getRatio_1());
                objs[5] = hos.getCasesNum_2();
                objs[6] = fm.format(hos.getRatio_2());
                objs[7] = hos.getCasesNum_3();
                objs[8] = fm.format(hos.getRatio_3());
                objs[9] = hos.getCasesNum_4();
                objs[10] = fm.format(hos.getRatio_4());
                dataList.add(objs);
            }
            
            HospitalSumResult sum = mapper.queryHospitalDataSum(getHosParameter(list_range, qo));
            objs = new Object[rowsName.length];
            objs[0] = "合计";
            objs[2] = sum.getCaseSumTotal();
            objs[3] = sum.getCasesNumTotal_1();
            objs[4] = fm.format(sum.getRatioTotal_1());
            objs[5] = sum.getCasesNumTotal_2();
            objs[6] = fm.format(sum.getRatioTotal_2());
            objs[7] = sum.getCasesNumTotal_3();
            objs[8] = fm.format(sum.getRatioTotal_3());
            objs[9] = sum.getCasesNumTotal_4();
            objs[10] = fm.format(sum.getRatioTotal_4());
            
            dataList.add(new Object[rowsName.length]);
            dataList.add(objs);
            
            sheet.addMergedRegion(new CellRangeAddress(list.size()+1,list.size()+1,0,12));//横向：合并第一行的第1列到第12列  
        }
        
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
        	ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	/**
	 * 医院疑难病例查看明细弹出数据导出
	 * @param response
	 * @param qo
	 */
	public void exportQueryInfoData(HttpServletResponse response,HospitalQo qo){
		String fileName = "医院疑难病例查看明细" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
        
        List<RwtRangeObj> list_range = mapper.queryRwtRange();
        String[] res = getExportTitle(list_range);
        String[] rowsName = new String[]{"科室名称_L","入组病案数_R",res[0]+"病案数_R",
        		res[0]+"占比(%)_R",res[1]+"病案数_R",res[1]+"占比(%)_R",res[2]+"病案数_R",res[2]+"占比(%)_R",
        		res[3]+"病案数_R",res[3]+"占比(%)_R"};
		
        List<DepartmentResult> list = mapper.queryHospitalQueryInfo(getHosParameter(list_range, qo));
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        for (int i = 0; i < list.size(); i++) {
        	DepartmentResult hos = list.get(i);
            objs = new Object[rowsName.length];
            objs[0] = hos.getStdksmc();
            objs[1] = hos.getCaseSum();
            objs[2] = hos.getCasesNum_1();
            objs[3] = fm.format(hos.getRatio_1());
            objs[4] = hos.getCasesNum_2();
            objs[5] = fm.format(hos.getRatio_2());
            objs[6] = hos.getCasesNum_3();
            objs[7] = fm.format(hos.getRatio_3());
            objs[8] = hos.getCasesNum_4();
            objs[9] = fm.format(hos.getRatio_4());
            dataList.add(objs);
        }
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表				   
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
        	ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 医院疑难病例查看明细弹出数据导出
	 * @param response
	 * @param qo
	 */
	public void exportByNumClickData(HttpServletResponse response,HospitalQo qo){
		String fileName = "医院疑难病例入组数明细" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        List<Object[]>  dataList = new ArrayList<Object[]>();
       
        String[] rowsName = new String[]{"医院名称_L","病案号_L","DRG编码_L",
        		"DRG名称_L","相对权重_R","主要诊断_L","主要操作_L","住院天数_R",
        		"出院日期_C","住院总费用_R"};
		
        qo.setRangeExpression_val1(qo.getRangeExpression().split(",")[0]);
        qo.setRangeExpression_val2(qo.getRangeExpression().split(",")[1]);
        List<HospitalNumResult> list = mapper.queryHospitalByNumClick(qo);
        Object[] objs = null;
        DecimalFormat fm = new DecimalFormat("0.00");
        for (int i = 0; i < list.size(); i++) {
        	HospitalNumResult hos = list.get(i);
            objs = new Object[rowsName.length];
            objs[0] = hos.getMemberName();
            objs[1] = hos.getBah();
            objs[2] = hos.getDrgCode();
            objs[3] = hos.getDrgName();
            objs[4] = hos.getRwt()!=null?fm.format(hos.getRwt()):hos.getRwt();
            objs[5] = hos.getAdrgName();
            objs[6] = hos.getCchiName();
            objs[7] = hos.getZyts();
            objs[8] = hos.getCyrq();
            objs[9] = hos.getZfy()!=null?fm.format(hos.getZfy()):hos.getZfy();
            dataList.add(objs);
        }
        
        Workbook workbook = new XSSFWorkbook();                        // 创建工作簿对象
        Sheet sheet = workbook.createSheet(fileName); 				   // 创建工作表
        ExcelUtil ex = new ExcelUtil(rowsName, dataList);
        try {
        	ex.export(response,fileName,workbook,sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 /** 
		 * getTheRegion:得到用户的地区/医院. <br/>
		 * @author whk00196 
		 * @date 2018年7月17日 下午9:00:45 *
		 * @param vo 
		 * @throws Exception 
		 * @since JDK 1.8 
		 **/
		public void  getTheRegion(HospitalQo vo) throws Exception {
			UserLoginInfo user = null;
			user =  vo.getUser();
			if(user == null) {
				throw new Exception("用户未登陆或者已失效");
			}
			if(
			!( (vo.getCity() == null || "".equals(vo.getCity().trim()))
			&&
			(vo.getCnty() == null || "".equals(vo.getCnty().trim()))
			&&
			(vo.getMemberCode() == null ||  "".equals(vo.getMemberCode().trim())) )
			  ) {
				return;
			}
		
			String orgaType = user.getOrgaType();
			String region = null;
			if("".equals(orgaType)||"ROOT".equals(orgaType)) {
			}else if("SHWJW".equals(orgaType)) {
			}else if("SWJW".equals(orgaType)) {
				region = user.getOrgaId();
				vo.setCity(region);
			}else if("XWJW".equals(orgaType)) {
				region = user.getOrgaId();
				vo.setCnty(region);
			}else if("HOS".equals(orgaType)) {
				region = user.getOrgaId();
				vo.setMemberCode(region);
			}
		};
}
