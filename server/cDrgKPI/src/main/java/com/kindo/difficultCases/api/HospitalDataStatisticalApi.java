package com.kindo.difficultCases.api;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.difficultCases.model.HospitalQo;
import com.kindo.difficultCases.model.RwtRangeStrResult;
import com.kindo.difficultCases.service.HospitalDataStatisticalService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;


/**
 * 医院疑难病例数据统计
 * @author likai
 *
 */

@RestController
@RequestMapping("/drgjx/hosdifficult")
public class HospitalDataStatisticalApi {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HospitalDataStatisticalApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private HospitalDataStatisticalService service;
	@Autowired
	private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(HospitalQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		vo.setUser(info);
     };
	
	/**
	 * 查询医院疑难病例数据-不分页
	 * @param Qo
	 * @return
	 */
	@RequestMapping(value = "/hospital/query", method = RequestMethod.GET)
	public ApiResult getHospitalQuery(HospitalQo Qo){
		try {
			service.getTheRegion(Qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		ApiResult apiResult = service.getHospitalQueryByPage(Qo,null);
		return apiResult;
	}

	/**
	 * 查询医院疑难病例数据-分页
	 * @param Qo
	 * @return
	 */
	@RequestMapping(value = "hospital/queryByPage", method = RequestMethod.GET)
	public ApiResult getHospitalQueryByPage(Pageable pagination,HospitalQo Qo){
		try {
			service.getTheRegion(Qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		ApiResult apiResult = service.getHospitalQueryByPage(Qo,pagination);
		return apiResult;
	}
	
	@RequestMapping(value = "/hospital/queryInfo", method = RequestMethod.GET)
	public ApiResult getHospitalQueryInfo(Pageable pagination,HospitalQo Qo){
		try {
			service.getTheRegion(Qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		ApiResult apiResult = service.getHospitalQueryInfo(Qo,pagination);
		return apiResult;
	}
	
	@RequestMapping(value = "/hospital/exportData/export", method = RequestMethod.GET)
	public void exportHospitalData(HttpServletResponse response,HospitalQo qo){
		try {
			service.getTheRegion(qo);
			service.exportHospitalData(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/hospital/queryByNumClick", method = RequestMethod.GET)
	public ApiResult queryHospitalByNumClick(Pageable pagination,HospitalQo numQo){
		try {
			service.getTheRegion(numQo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		ApiResult apiResult = service.queryHospitalByNumClick(numQo, pagination);
		return apiResult;
	}
	
	@RequestMapping(value = "/hospital/queryHosDataSum", method = RequestMethod.GET)
	public ApiResult queryHospitalDataSum(HospitalQo Qo){
		try {
			service.getTheRegion(Qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		ApiResult apiResult = service.queryHospitalDataSum(Qo);
		return apiResult;
	}
	
	@RequestMapping(value = "/hospital/exportByNumClickData/export", method = RequestMethod.GET)
	public void exportByNumClickData(HttpServletResponse response,HospitalQo qo){
		try {
			service.getTheRegion(qo);
			service.exportByNumClickData(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/hospital/exportQueryInfoData/export", method = RequestMethod.GET)
	public void exportQueryInfoData(HttpServletResponse response,HospitalQo qo){
		try {
			service.getTheRegion(qo);
			service.exportQueryInfoData(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/hospital/queryRwtRangeStr", method = RequestMethod.GET)
	public ApiResult queryRwtRangeStr(){
		List<RwtRangeStrResult> list = service.queryRwtRangeStr();
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,list);
	}
}