package com.kindo.difficultCases.model;

import lombok.Data;

/**
 * 点击病案数查询返回pojo
 * @author likai
 *
 */
@Data
public class HospitalNumResult {
	
	private String id;
	
	private String memberCode;
	
	private String memberName;
	
	private String bah;
	
	private String drgCode;
	
	private String drgName;
	
	private Double rwt;
	
	private String adrgName;
	
	private String cchiName;
	
	private Integer zyts;
	
	private String cyrq;
	
	private Double zfy;

}
