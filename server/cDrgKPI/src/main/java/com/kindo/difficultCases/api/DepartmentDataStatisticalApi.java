package com.kindo.difficultCases.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kindo.aria.base.Pageable;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.difficultCases.model.HospitalQo;
import com.kindo.difficultCases.service.DepartmentDataStatisticalService;
import com.kindo.difficultCases.service.HospitalDataStatisticalService;
import com.kindo.uas.common.constant.AuthConstants;
import com.kindo.uas.common.model.UserLoginInfo;
import com.kindo.uas.common.redis.RedisOper;

@RestController
@RequestMapping("/drgjx/depdifficult")
public class DepartmentDataStatisticalApi {
	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentDataStatisticalApi.class);
	private static final int ReceiveSize = 50000;
	
	@Autowired
	private DepartmentDataStatisticalService service;
	@Autowired
	private HospitalDataStatisticalService hosService;
	@Autowired
	private RedisOper redisOper;
	
	@ModelAttribute
    public void initUser(HospitalQo vo,HttpServletRequest request) {
		UserLoginInfo info = null;
		String token = null;
		try {
			token=request.getHeader(AuthConstants.HEADER_TOKEN);
			info=(UserLoginInfo) redisOper.getData(AuthConstants.TOKEN_CACHE_PREFIX + token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		vo.setUser(info);
     };
	
	
	@RequestMapping(value = "/department/query", method = RequestMethod.GET)
	public ApiResult getDepartmentQuery(HospitalQo qo){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getDepQueryByPage(qo,null);
	}
	
	@RequestMapping(value = "department/queryByPage", method = RequestMethod.GET)
	public ApiResult getDepartmentQueryByPage(HospitalQo qo,Pageable pagination){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getDepQueryByPage(qo,pagination);
	}
	
	@RequestMapping(value = "/department/queryInfo", method = RequestMethod.GET)
	public ApiResult getDepartmentQueryInfo(HospitalQo qo,Pageable pagination){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.getDepQueryInfo(qo, pagination);
	}
	
	@RequestMapping(value = "/department/exportData/export", method = RequestMethod.GET)
	public void exportDepartmentData(HttpServletResponse response,HospitalQo qo){
		try {
			hosService.getTheRegion(qo);
			service.exportDepData(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/department/queryByNumClick", method = RequestMethod.GET)
	public ApiResult queryDepartmentByNumClick(HospitalQo qo,Pageable pagination){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.queryDepByNumClick(qo, pagination);
	}
	
	@RequestMapping(value = "/department/queryDepDataSum", method = RequestMethod.GET)
	public ApiResult queryDepartmentDataSum(HospitalQo qo){
		try {
			hosService.getTheRegion(qo);
		} catch (Exception e) {
			return new ApiResult(Constants.RESULT.ERROR, Constants.RESULT_MSG.EXECEPTION, e.getMessage());
		}
		return service.queryDepDataSum(qo);
	}
	
	@RequestMapping(value = "/department/exportByNumClickData/export", method = RequestMethod.GET)
	public void exportByNumClickData(HttpServletResponse response,HospitalQo qo){
		try {
			hosService.getTheRegion(qo);
			service.exportByNumClickDataByDep(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/department/exportQueryInfoData/export", method = RequestMethod.GET)
	public void exportQueryInfoData(HttpServletResponse response,HospitalQo qo){
		try {
			hosService.getTheRegion(qo);
			service.exportQueryInfoDataByDep(response, qo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
