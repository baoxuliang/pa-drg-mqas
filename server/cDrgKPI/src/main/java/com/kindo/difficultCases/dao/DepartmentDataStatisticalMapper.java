package com.kindo.difficultCases.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.kindo.aria.base.Pageable;
import com.kindo.difficultCases.model.DepartmentResult;
import com.kindo.difficultCases.model.HospitalNumResult;
import com.kindo.difficultCases.model.HospitalQo;
import com.kindo.difficultCases.model.HospitalResult;
import com.kindo.difficultCases.model.HospitalSumResult;

@Mapper
public interface DepartmentDataStatisticalMapper {

	List<DepartmentResult> queryDepartmentData(@Param("param")HospitalQo Qo);
	
	List<DepartmentResult> queryDepartmentData(@Param("param")HospitalQo Qo,@Param("pagination") Pageable pagination);
	
	List<HospitalResult> queryDepartmentQueryInfo(@Param("param")HospitalQo Qo,@Param("pagination") Pageable pagination);
	
	List<HospitalResult> queryDepartmentQueryInfo(@Param("param")HospitalQo Qo);
	
	List<HospitalNumResult> queryDepartmentByNumClick(@Param("param") HospitalQo Qo,@Param("pagination") Pageable pagination);
	
	List<HospitalNumResult> queryDepartmentByNumClick(@Param("param") HospitalQo Qo);
	
	HospitalSumResult queryDepartmentDataSum(@Param("param")HospitalQo Qo);
	
}
