
/** 
* Project Name:cDrgKPI <br/> 
* File Name:PagePlugConfig.java <br/>
* Package Name:com.kindo.config <br/>
* Date:2018年6月21日下午2:29:07 *<br/>
* Copyright (c) 2018, HC., LTD. All Rights Reserved.<br/>
* */
package com.kindo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kindo.plugin.PagePlugin;

/** 
* ClassName: PagePlugConfig <br/>
* Function: TODO (这里描述这个类提供什么功能/服务/能力 – 可选).<br/> 
* Reason: TODO (这里描述这个类的必要性 – 可选).<br/>
* date: 2018年6月21日 下午2:29:07 *<br/>
* @author whk00196 
* @version  
* @since JDK 1.8
**/
@Configuration
public class PagePlugConfig {
	
	@Bean
	public PagePlugin pagePlugin() {
		return new PagePlugin();
	}
}
