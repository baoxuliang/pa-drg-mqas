package com.kindo.basicDict.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.kindo.aria.base.Pageable;
import com.kindo.basicDict.model.BasicDictQueryObj;
import com.kindo.basicDict.model.CCDTResult;
import com.kindo.basicDict.model.CCHIResult;
import com.kindo.basicDict.model.DrgResult;
import com.kindo.basicDict.model.ICD10Result;
import com.kindo.basicDict.model.ICD9Result;

@Mapper
public interface BasicDictMapper {

	List<DrgResult> queryDrgDictData(@Param("param")BasicDictQueryObj queryObj,@Param("pagination")Pageable pagination);
	
	List<ICD10Result> getICD10DictData(@Param("param")BasicDictQueryObj queryObj,@Param("pagination")Pageable pagination);
	
	List<ICD9Result> getICD9DictData(@Param("param")BasicDictQueryObj queryObj,@Param("pagination")Pageable pagination);
	
	List<CCHIResult> queryCCHIDictData(@Param("param")BasicDictQueryObj queryObj,@Param("pagination")Pageable pagination);
	
	List<CCDTResult> getCCDTDictData(@Param("param")BasicDictQueryObj queryObj,@Param("pagination")Pageable pagination);
}
