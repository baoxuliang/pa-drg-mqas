package com.kindo.basicDict.model;

import lombok.Data;

@Data
public class CCDTResult {

	/**
	 * ccdt编码
	 */
	private String ccdtCode;
	
	/**
	 * ccdt名称
	 */
	private String ccdtName;
}
