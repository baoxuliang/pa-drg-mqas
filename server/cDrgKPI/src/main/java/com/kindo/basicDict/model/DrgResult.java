package com.kindo.basicDict.model;

import lombok.Data;

@Data
public class DrgResult {

	private String mdcCode;
	
	private String mdcName;
	
	private String drgCode;
	
	private String drgName;
	
	private Double rwt;
}
