package com.kindo.basicDict.model;

import lombok.Data;

@Data
public class CCHIResult {

	private String cchiCode;
	
	private String cchiName;
	
	private String jsnd;
	
	private String fxcd;
}
