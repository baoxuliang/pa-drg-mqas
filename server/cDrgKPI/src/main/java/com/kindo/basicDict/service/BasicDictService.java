package com.kindo.basicDict.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kindo.aria.base.Pageable;
import com.kindo.aria.base.SortPair;
import com.kindo.basicDict.dao.BasicDictMapper;
import com.kindo.basicDict.model.BasicDictQueryObj;
import com.kindo.common.model.ApiResult;
import com.kindo.common.model.Constants;
import com.kindo.common.model.RowsWithTotal;

@Service
public class BasicDictService {

	@Autowired
	private BasicDictMapper mapper;
	
	public ApiResult queryDrgDictData(BasicDictQueryObj obj,Pageable pagination){
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.queryDrgDictData(obj,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryICD10DictData(BasicDictQueryObj obj,Pageable pagination){
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.getICD10DictData(obj,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryICD9DictData(BasicDictQueryObj obj,Pageable pagination){
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.getICD9DictData(obj,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryCCHIDictData(BasicDictQueryObj obj,Pageable pagination){
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.queryCCHIDictData(obj,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
	
	public ApiResult queryCCDTDictData(BasicDictQueryObj obj,Pageable pagination){
		if(pagination.getSorts().size()==0||(pagination.getSorts().get(0).getField().isEmpty())){
    		List<SortPair> sorts = new ArrayList<>();
    		SortPair sp1= new SortPair();
    		sp1.setField(" ccdtCode ");
    		sp1.setAsc(true);
    		sorts.add(sp1);
    		pagination.setSorts(sorts);
    	}
		RowsWithTotal rt = new RowsWithTotal();
		rt.setRows(mapper.getCCDTDictData(obj,pagination));
		rt.setTotal(pagination.getTotal());
		return new ApiResult(Constants.RESULT.SUCCESS, Constants.RESULT_MSG.QUERY_SUCCESS,rt);
	}
}
