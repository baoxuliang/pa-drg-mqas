package com.kindo.basicDict.model;

import lombok.Data;

@Data
public class BasicDictQueryObj {

	/**
	 * 	DRG编码或名称
	 */
	private String drgCodeOrName;
	
	/**
	 * 	MDC编码或名称
	 */
	private String mdcCodeOrName;
	
	/**
	 * 类目
	 */
	private String category;
	
	/**
	 * ICD10编码或名称
	 */
	private String icd10CodeOrName;
	
	/**
	 * ICD9编码或名称
	 */
	private String icd9CodeOrName;
	
	/**
	 * cchi编码或名称
	 */
	private String cchiCodeOrName;
	
	/**
	 * ccdt编码或名称
	 */
	private String ccdtCodeOrName;
}
