package com.kindo.basicDict.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kindo.aria.base.Pageable;
import com.kindo.basicDict.model.BasicDictQueryObj;
import com.kindo.basicDict.service.BasicDictService;
import com.kindo.common.model.ApiResult;

/**
 * 基础字典
 * @author likai
 *
 */
@RestController
@RequestMapping("/basic/dictionary")
public class BasicDictApi {
	
	@Autowired
	private BasicDictService service;

	@RequestMapping(value = "/drgDataQuery", method = RequestMethod.GET)
	public ApiResult queryDrgDictData(BasicDictQueryObj obj,Pageable pagination){
		return service.queryDrgDictData(obj,pagination);
	}
	
	@RequestMapping(value = "/icd10DataQuery", method = RequestMethod.GET)
	public ApiResult queryICD10DataQuery(BasicDictQueryObj obj,Pageable pagination){
		return service.queryICD10DictData(obj,pagination); 
	}
	
	@RequestMapping(value = "/icd9DataQuery", method = RequestMethod.GET)
	public ApiResult queryICD9DictData(BasicDictQueryObj obj,Pageable pagination){
		return service.queryICD9DictData(obj,pagination);
	} 
	
	@RequestMapping(value = "/cchiDictDataQuery", method = RequestMethod.GET)
	public ApiResult queryCCHIDictData(BasicDictQueryObj obj,Pageable pagination){
		return service.queryCCHIDictData(obj,pagination);
	}
	
	@RequestMapping(value = "/ccdtDictDataQuery", method = RequestMethod.GET)
	public ApiResult queryCCDTDictData(BasicDictQueryObj obj,Pageable pagination){
		return service.queryCCDTDictData(obj,pagination);
	}
}
