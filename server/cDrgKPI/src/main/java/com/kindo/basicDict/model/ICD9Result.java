package com.kindo.basicDict.model;

import lombok.Data;

@Data
public class ICD9Result {

	/**
	 * 	icd9编码
	 */
	private String icd9Code;
	
	/**
	 * icd9附加编码
	 */
	private String icd9AdditionalCode;
	
	/**
	 * 	icd9名称
	 */
	private String icd9Name;
	
	/**
	 * 属性
	 */
	private String attribute;
}
