package com.kindo.basicDict.model;

import lombok.Data;

@Data
public class ICD10Result {

	/**
	 * 	icd10编码
	 */
	private String icd10Code;
	
	/**
	 * icd10附加编码
	 */
	private String icd10AdditionalCode;
	
	/**
	 * 	icd10名称
	 */
	private String icd10Name;
}
